importScripts('/sw-toolbox.js')

toolbox.options.debug = true;

toolbox.precache([
	'/js/materialize/plugins/jquery-1.11.2.min.js',
	'/pwa-assets/webcss/font-awesome.css',
	'https://fonts.googleapis.com/icon?family=Material+Icons',
	'https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900',
	'/js/materialize/materialize.min.js',
	'/pwa-assets/assets/moment.min.js',
	'/js/angular/angular.min.js',
	'/js/angular/angular-route.min.js',
	'/js/angular/angular-animate.min.js'
]);

toolbox.router.get('/js/materialize/plugins/jquery-1.11.2.min.js', toolbox.cacheFirst);
toolbox.router.get('/css/materialize/materialize.min.css', toolbox.cacheFirst);
toolbox.router.get('/pwa-assets/webcss/font-awesome.css', toolbox.cacheFirst);
toolbox.router.get('/js/materialize/materialize.min.js', toolbox.cacheFirst);
toolbox.router.get('/js/angular/angular.min.js', toolbox.cacheFirst);
toolbox.router.get('/js/angular/angular-route.min.js', toolbox.cacheFirst);
toolbox.router.get('/js/angular/angular-animate.min.js', toolbox.cacheFirst);
toolbox.router.get('/pwa-assets/assets/moment.min.js', toolbox.cacheFirst);
toolbox.router.get('/pwa-assets/assets/app.js', toolbox.fastest);
toolbox.router.get('/pwa-assets/assets/style.css', toolbox.fastest);
toolbox.router.get('/favicon.ico', toolbox.fastest);
toolbox.router.get('/pwa', toolbox.fastest);
toolbox.router.get('/pwapp/jobs/(.*)', toolbox.fastest);

self.addEventListener('install', function(event) {
	event.waitUntil(self.skipWaiting());
});

self.addEventListener('activate', function(event) {
	event.waitUntil(self.clients.claim())
});