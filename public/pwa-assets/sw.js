var cacheName = 'v1';
var cacheFiles = [
	// '/pwa',
	// '/pwapp/jobs',
	// '/pwa/assets/app.js',
	'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js',
	'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
	'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
	'https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900'
];

self.addEventListener('install', function(e) {
	console.log("[Service Worker] Installed")
	e.waitUntil(

		caches.open(cacheName).then(function(cache) {
			console.log("[Service Worker] Caching Cache Files")
			return cache.addAll(cacheFiles);
		})
	)
})
self.addEventListener('activate', function(e) {
	console.log("[Service Worker] Activated")
	e.waitUntil(
		caches.keys().then(function(cacheNames) {
			//Removing cache if cache not equals `v1`
			return Promise.all(cacheNames.map(function(thisCacheName) {
				if(thisCacheName !== cacheName) {
					console.log("[Service Worker] Removing Cached Files from "+thisCacheName)
					return caches.delete(thisCacheName);
				}
			}));
		})
	)
})

self.addEventListener('fetch', function(e) {
	console.log('[ServiceWorker] Fetch', e.request.url);

	// e.respondWidth Responds to the fetch event
	if(e.request.url.indexOf('pwa') < 0 )
	{
		var requestClone = e.request.clone();
		fetch(requestClone)
			.then(function(response) {

				if ( !response ) {
					console.log("[ServiceWorker] No response from fetch ")
					return response;
				}
			});
			return;
	}
	e.respondWith(

		// Check in cache for the request being made
		caches.match(e.request)

			.then(function(response) {
				url = e.request.url;
				if(url.indexOf('pwapp/jobs') > -1) {
					// If the request is in the cache
					var requestClone = e.request.clone();
					fetch(requestClone)
						.then(function(response) {

							if ( !response ) {
								console.log("[ServiceWorker] No response from fetch ")
								return response;
							}

							var responseClone = response.clone();

							//  Open the cache
							caches.open(cacheName).then(function(cache) {

								// Put the fetched response in the cache
								cache.put(e.request, responseClone);
								console.log('[ServiceWorker] New Data Cached', e.request.url);

								// Return the response
								return response;

							}); // end caches.open

						})
						.catch(function(err) {
							if ( response ) {
								console.log("[ServiceWorker] Found in Cache", e.request.url, response);
								// Return the cached version
								return response;
							}
							console.log('[ServiceWorker] Error Fetching & Caching New Data', err);
						});

					if ( response ) {
						console.log("[ServiceWorker] Found in Cache", e.request.url, response);
						// Return the cached version
						return response;
					}

					// If the request is NOT in the cache, fetch and cache

				} else {
					// If the request is in the cache
					if ( response ) {
						console.log("[ServiceWorker] Found in Cache", e.request.url, response);
						// Return the cached version
						return response;
					}

					// If the request is NOT in the cache, fetch and cache

					var requestClone = e.request.clone();
					fetch(requestClone)
						.then(function(response) {

							if ( !response ) {
								console.log("[ServiceWorker] No response from fetch ")
								return response;
							}

							var responseClone = response.clone();

							//  Open the cache
							caches.open(cacheName).then(function(cache) {

								// Put the fetched response in the cache
								cache.put(e.request, responseClone);
								console.log('[ServiceWorker] New Data Cached', e.request.url);

								// Return the response
								return response;

							}); // end caches.open

						})
						.catch(function(err) {
							console.log('[ServiceWorker] Error Fetching & Caching New Data', err);
						});


				}


			}) // end caches.match(e.request)
	); // end e.respondWith
});