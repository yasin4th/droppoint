function onUpdateFound(registration) {
	console.log('onUpdateFound');
	var newWorker = registration.installing;

	registration.installing.addEventListener('statechange', function(e) {
		console.log("state" + e.target.state);
	});
}

if('serviceWorker' in navigator) {
	console.log('Service Worker found.');

	navigator.serviceWorker
		.register('/service-worker.js',{scope:'/pwa'})
		.then(registration => {

			registration.addEventListener('updatefound', () => onUpdateFound(registration));
			if(typeof registration.update == 'function') {
				registration.update();
			}
		});
}

unr = function() {
	caches.delete('v1');
	navigator.serviceWorker.getRegistrations().then(function(registrations) {
		for(let registration of registrations) {
			registration.unregister()
		}
	});
}

// Function to perform HTTP request
var get = function(url) {
	return new Promise(function(resolve, reject) {

		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
			if (xhr.readyState === XMLHttpRequest.DONE) {
				if (xhr.status === 200) {
					var result = xhr.responseText
					result = JSON.parse(result);
					resolve(result);
				} else {
					reject(xhr);
				}
			}
		};

		xhr.open("GET", url, true);
		xhr.send();
	});
};


var app = angular.module('myapp', ['ngAnimate']);

app.controller('ctrl', function($scope, $http, $window, $timeout) {
	$scope.jobs = [];
	$scope.active = [];
	$scope.tab = 1;
	$scope.applicant = {};
	$scope.job = {
		title: '',
		about: '',
		date: '',
		vacancy: 0,
		salary: 0,
		roles: '',
		contactemail: '',
		contactphone: '',
		contactlink: '',
		status: true
	};
	$scope.createjob = [];

	if (localStorage.getItem('user') == null)
	{
		Materialize.toast('Please login first.', 2000, 'rounded');

		// setTimeout(function() {
		// 	location.replace('/login?redirect=/pwa');
		// }, 2000);
		return;
	}
	$scope.user = localStorage.getItem('user');

	$http.get('/pwapp/jobs/'+$scope.user).then(function(response) {

		if(response.data.status == 'FAILED') {
			Materialize.toast('Please login first.', 2000, 'rounded');

			setTimeout(function() {
				location.replace('/login?redirect=/pwa');
			}, 2000);

		} else {
			$scope.jobs = response.data.jobs;
		}
	});
	$scope.change = function(job) {
		$scope.active = job;
	};
	$scope.hire = function() {
		index = angular.element(document.getElementById('applicant-modal')).attr('index');
		$http.post('applicants/update',{field:'status',value:2,ids:[$scope.active.applicants[index].id]}).then(function(response) {
			if(response.data.status == 'FAILED') {
				Materialize.toast('Please login first.');
			} else {
				Materialize.toast('Successfully hired.')
				app = $scope.active.applicants.splice(index,1);
				$scope.active.hired.push(app);
			}
		});
	}

	$scope.unhire = function(index) {
		// $http.post('applicants/update',{field:'status',value:1,ids:[$scope.active.applicants[index]]}).then(function(response) {
		// 	if(response.data.status == 'FAILED') {
		// 		Materialize.toast('Please login first.', 2000);
		// 	} else {
		// 		app = $scope.active.applicants.splice(index,1);
		// 		$scope.active.hired.push(app);
		// 	}
		// });
	}

	$scope.open = function(app, index) {
		$scope.applicant = app;
		$('#applicant-modal').openModal();
		$('#applicant-modal').attr('index', index);
	}

	$scope.updateScreen = function(scr) {
		// console.log('scr')
	}

	$scope.submit = function() {
		$http.post(`/pwapp/user/${$scope.user}/create`,{job:$scope.job}).then(function(response) {
			console.log(response);
			$scope.job.status = true;
			var job = {
				title: $scope.job.title,
				about: $scope.job.about,
				date: $scope.job.date,
				vacancy: $scope.job.vacancy,
				salary: $scope.job.salary,
				contactemail: $scope.job.contactemail,
				contactphone: $scope.job.contactphone,
				contactlink: $scope.job.contactlink,
				status: true
			}
			$scope.createjob.push(job);
			Materialize.toast('Job Posted.', 1000);
		}, function(error) {
			console.log('Error: ', error)
			$scope.job.status = false;
			var job = {
				title: $scope.job.title,
				about: $scope.job.about,
				date: $scope.job.date,
				vacancy: $scope.job.vacancy,
				salary: $scope.job.salary,
				contactemail: $scope.job.contactemail,
				contactphone: $scope.job.contactphone,
				contactlink: $scope.job.contactlink,
				status: false
			}
			$scope.createjob.push(job);
			Materialize.toast('Network error, Please try Again.', 1000);
		});
	}

	$scope.sendagain = function(j) {
		console.log("Job to resend",j);
		$http.post(`/pwapp/user/${$scope.user}/create`,{job:j}).then(function(response) {
			j.status = true;
			Materialize.toast('Job Posted.', 1000);
		}, function(error) {
			j.status = false;
			Materialize.toast('Network error, Please try Again.', 1000);
		});
	}
});

app.filter('time', function() {
	return function(input) {
		input = input || '';
		return moment(input,'YYYY-MM-DD HH:ii:ss').format('ddd DD MMM');
	};
});


$(document).ready(function() {
	$('#createjob').click(function(e){
		$('.createjobs-content').show();
		$('.jobs-content').hide();
		$(this).hide();
		$('#back').show();

	});

	$('#back').click(function(e){
		$('.createjobs-content').hide();
		$('.jobs-content').show();
		$(this).hide();
		$('#createjob').show();
	});
})