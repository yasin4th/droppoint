/*================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 3.1
	Author: GeeksLabs
	Author URL: http://www.themeforest.net/user/geekslabs
================================================================================

NOTE:
------
PLACE HERE YOUR OWN JS CODES AND IF NEEDED.
WE WILL RELEASE FUTURE UPDATES SO IN ORDER TO NOT OVERWRITE YOUR CUSTOM SCRIPT IT'S BETTER LIKE THIS. */

String.prototype.trunc =
    function( n, useWordBoundary ){
        var isTooLong = this.length > n,
        s_ = isTooLong ? this.substr(0,n-1) : this;
        s_ = (useWordBoundary && isTooLong) ? s_.substr(0,s_.lastIndexOf(' ')) : s_;
        return  isTooLong ? s_ + '&hellip;' : s_;
    };

    function setJcrop(input, output, callback, aspectRatio, jcrop_api) {
        $(input).on('change', function(e) {
            if($(this)[0].files[0].size < 1048576) {
                var file, img;
                if ((file = this.files[0])) {
                    if (jcrop_api == 1) {
                        add_jcrop_api.setImage(URL.createObjectURL($(this)[0].files[0]));
                    }
                    else {
                        edit_jcrop_api.setImage(URL.createObjectURL($(this)[0].files[0]));
                    };
                    if (jcrop_api) {
                        $('#jcrop-image-modal').modal('show');
                    } else{
                        $('#jcrop-image-modal-edit').modal('show');
                    };
               }
            }
            else {
                toastr.error('File size larger than 1 MB.')
            }
        });


        $(output).Jcrop({
            onChange: callback,
            onSelect: callback,
            aspectRatio: aspectRatio,
            boxWidth: 500,
            boxHeight: 500,

        },function(){
          // Store the API in the jcrop_api variable
          if (jcrop_api == 1) {
            add_jcrop_api = this;
          }
          else {
            edit_jcrop_api = this;
          };

        });



    }

    function updateCords(c) {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
    };
