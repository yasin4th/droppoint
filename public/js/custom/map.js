var _place = {}, marker, api_key = 'AIzaSyDFvcodwxV3SQptSx4LQ-H-gZVcAk-1lyI';
var icn = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';

function initAutocomplete() {

	if (document.getElementById('latitude').value > 0) {
		var myLatLng = new google.maps.LatLng(
			document.getElementById('latitude').value,
			document.getElementById('longitude').value);
	} else {
		var myLatLng = new google.maps.LatLng(3.14, 101.6956316);
	}

	var map = new google.maps.Map(document.getElementById('map'), {
		center: myLatLng,
		zoom: 8
	});

	// Try HTML5 geolocation.
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};

			map.setCenter(pos);
			if(marker) {
				marker.setMap(null);
			}

			marker = new google.maps.Marker({
				position: pos,
				map: map,
				title: 'Location',
				draggable: true
			});
			map.setZoom(17);
			map.panTo(marker.position);
			map.setCenter(marker.position);
			window.dispatchEvent( new Event('resize') );

			document.getElementById('latitude').value = pos.lat;
			document.getElementById('longitude').value = pos.lng;
		}, function() {
			handleLocationError(true, infoWindow, map.getCenter());
		});
	} else {
	// Browser doesn't support Geolocation
	handleLocationError(false, infoWindow, map.getCenter());
	}

	if(marker) {
		marker.setMap(null);
	}

	if(!marker) {
		marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			title: 'Location',
			draggable: true
		});
	}
	window.dispatchEvent( new Event('resize') );

	map.setCenter(marker.getPosition());
	google.maps.event.addDomListener(window, 'resize', function() {
		map.setCenter(marker.getPosition());
		google.maps.event.trigger(map, 'resize');
	});

	google.maps.event.addListener(marker, 'dragend', function (event) {

		updatePlace();
	});

	// Create the search box and link it to the UI element.
	var input = document.getElementById('pac-input');
	var searchBox = new google.maps.places.SearchBox(input);
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	// Bias the SearchBox results towards current map's viewport.
	map.addListener('bounds_changed', function() {
		searchBox.setBounds(map.getBounds());
		lat = marker.getPosition().lat();
		lng = marker.getPosition().lng();

		$('input[name=latitude]').val(lat);
		$('input[name=longitude]').val(lng);

		updatePlace();
	});
	searchBox.addListener('places_changed', function() {
		var places = searchBox.getPlaces();

		if (places.length == 0) {
			return;
		}

		marker.setMap(null);
		flag = 0; // only one marker
		places.forEach(function(place) {

			if(flag == 0)
			{
				if (!place.geometry) {
				  console.log("Returned place contains no geometry");
				  return;
				}

				map.setCenter(place.geometry.location);

				updatePlace();

				marker = new google.maps.Marker({
					position: place.geometry.location,
					map: map,
					title: 'Location',
					draggable: true
				});

				setTimeout(function(){window.dispatchEvent( new Event('resize') );}, 500);

				flag++;
			}

		});

	});
}

function createMarker(latlng, name) {
	marker = new google.maps.Marker({
		position: latlng,
		map: map,
		// icon: icn,
		title: name
	});

	map.setZoom(17);
	map.panTo(marker.position);
	map.setCenter(marker.position);
	updatePlace();
}

function updatePlace()
{
	if(marker)
	{
		lat = marker.getPosition().lat();
		lng = marker.getPosition().lng();

		$('input[name=latitude]').val(lat);
		$('input[name=longitude]').val(lng);
		// $.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lng+'&key=AIzaSyDFvcodwxV3SQptSx4LQ-H-gZVcAk-1lyI', function(res){

		// });
		$.ajax({
			type : 'get',
			url  : 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lng+'&key='+api_key,
		}).done(function(res) {

			if(res.status == 'OK')
			{
				if(res.results.length > 0){
					ac = res.results[0].address_components;
				}
				_place.address = res.results[0].formatted_address;

				ac.forEach(function(address){
					if(address.types.indexOf('country') != -1){
						_place.country = address.long_name;
						_place._country = address.short_name;
					}
					if(address.types.indexOf('administrative_area_level_1') != -1){
						_place.state = address.long_name;
						_place._state = address.short_name;
					}
					if(address.types.indexOf('locality') != -1){
						_place.city = address.long_name;
						_place._city = address.short_name;
					}
					if(address.types.indexOf('route') != -1){
						_place.route = address.long_name;
						_place._route = address.short_name;
					}
					if(address.types.indexOf('postal_code') != -1){
						_place.postal_code = address.long_name;
					}
				});
				$('input[name=loc_city]').val(_place.city);
				$('input[name=zipcode]').val(_place.postal_code);
				$('select[name=loc_country]').val(_place.country).change();
				$('[name=loc_address]').val(_place.address);

				var address_chunks = _place.address.split(',');
				if(address_chunks.length > 3) {
					$('[name=loc_address]').val(address_chunks[0] + address_chunks[1] + address_chunks[2]);
				}
				Materialize.updateTextFields();
			}
		});
	}
}
