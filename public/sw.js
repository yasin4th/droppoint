var cacheName = 'v1';
var cacheFiles = [
	'/js/materialize/plugins/jquery-1.11.2.min.js',
	'/pwa-assets/webcss/font-awesome.css',
	'https://fonts.googleapis.com/icon?family=Material+Icons',
	'https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900',
	'/js/materialize/materialize.min.js',
	'/pwa-assets/assets/moment.min.js',
	'/js/angular/angular.min.js',
	'/js/angular/angular-route.min.js',
	'/js/angular/angular-animate.min.js',
];

self.addEventListener('install', function(e) {
	console.log("[Service Worker] Installed")
	e.waitUntil(

		caches.open(cacheName).then(function(cache) {
			console.log("[Service Worker] Caching Cache Files")
			return cache.addAll(cacheFiles);
		})
	)
})

self.addEventListener('activate', function(e) {
	console.log("[Service Worker] Activated")
	e.waitUntil(
		caches.keys().then(function(cacheNames) {
			//Removing cache if cache not equals `v1`
			return Promise.all(cacheNames.map(function(thisCacheName) {
				if(thisCacheName !== cacheName) {
					console.log("[Service Worker] Removing Cached Files from "+thisCacheName)
					return caches.delete(thisCacheName);
				}
			}));
		})
	)
})
self.addEventListener('fetch', function(event) {
	url = event.request.url;
	console.log(url);
	event.respondWith(
	fetch(event.request).catch(function() {
	return caches.match(event.request);
	})
	);
});

/*self.addEventListener('fetch', function(e) {
	url = e.request.url;
	console.log('[ServiceWorker] Fetch', url);
	var requestClone = e.request.clone();
	if(url.indexOf('/pwa') > -1) {
		e.respondWith(
			fetch(requestClone).then(function(response){
				if ( !response ) {
					console.log("[ServiceWorker] No response from fetch")
					caches.match(e.request).then(function(response) {
						if ( !response ) {
							console.log("[ServiceWorker] No response from fetch")

						} else {
							console.log("[ServiceWorker] Is in cache")
							return response;
						}
					});

				} else {
					console.log("[ServiceWorker] Should search cache")
					return response;
				}
			})
		)
	} else {
		e.respondWith(
			caches.match(e.request).then(function(response) {
				if ( !response ) {
					console.log("[ServiceWorker] No response from fetch")
					fetch(requestClone).then(function(response){
						if ( !response ) {
							console.log("[ServiceWorker] No response from fetch")
							return response;
						} else {
							console.log("[ServiceWorker] Should search cache")
							return response;
						}
					})
				} else {
					console.log("[ServiceWorker] Is in cache")
					return response;
				}
			});
		);
	}
})
*/

/*self.addEventListener('fetch', function(e) {
	console.log('[ServiceWorker] Fetch', e.request.url);

	// e.respondWidth Responds to the fetch event
	if(e.request.url.indexOf('pwa') < 0 )
	{
		var requestClone = e.request.clone();
		fetch(requestClone)
			.then(function(response) {

				if ( !response ) {
					console.log("[ServiceWorker] No response from fetch ")
					return response;
				}
			});
			return;
	}
	e.respondWith(

		// Check in cache for the request being made
		caches.match(e.request)

			.then(function(response) {
				url = e.request.url;

				// network first then cache
				if(url.indexOf('pwapp/jobs') > -1 || url.indexOf('/pwa')) {
					// If the request is in the cache
					var requestClone = e.request.clone();
					fetch(requestClone)
						.then(function(response) {

							if ( !response ) {
								console.log("[ServiceWorker] No response from fetch ")
								return response;
							}

							var responseClone = response.clone();

							//  Open the cache
							caches.open(cacheName).then(function(cache) {

								// Put the fetched response in the cache
								cache.put(e.request, responseClone);
								console.log('[ServiceWorker] New Data Cached', e.request.url);

								// Return the response
								return response;

							}); // end caches.open
						})
						.catch(function(err) {
							if ( response ) {
								console.log("[ServiceWorker] Found in Cache", e.request.url, response);
								// Return the cached version
								return response;
							}
							console.log('[ServiceWorker] Error Fetching & Caching New Data', err);
						});

					if ( response ) {
						console.log("[ServiceWorker] Found in Cache", e.request.url, response);
						// Return the cached version
						return response;
					}

					// If the request is NOT in the cache, fetch and cache

				} else {
					// If the request is in the cache
					if ( response ) {
						console.log("[ServiceWorker] Found in Cache", e.request.url, response);
						// Return the cached version
						return response;
					}

					// If the request is NOT in the cache, fetch and cache

					var requestClone = e.request.clone();
					fetch(requestClone)
						.then(function(response) {

							if ( !response ) {
								console.log("[ServiceWorker] No response from fetch ")
								return response;
							}

							var responseClone = response.clone();

							//  Open the cache
							caches.open(cacheName).then(function(cache) {

								// Put the fetched response in the cache
								cache.put(e.request, responseClone);
								console.log('[ServiceWorker] New Data Cached', e.request.url);

								// Return the response
								return response;

							}); // end caches.open

						})
						.catch(function(err) {
							console.log('[ServiceWorker] Error Fetching & Caching New Data', err);
						});


				}


			}) // end caches.match(e.request)
	); // end e.respondWith
});*/