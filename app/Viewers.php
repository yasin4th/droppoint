<?php
namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Viewers extends Model
{

	protected $table = 'viewers';

	protected $fillable = [ 'name',
							'email',
							'deleted_at',

	];
}
