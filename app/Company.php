<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
	use SoftDeletes;

	protected $table = 'company';

	protected $guarded = ['id','user_id'];

	protected $fillable = ['user_id',
						   'name',
						   'logo',
						   'type',
						   'description',
						   'building',
						   'address',
						   'city',
						   'state',
						   'country',
						   'zipcode',
						   'website',
						   'tagline',
						   'phone',
						   'industry',
						   'about',
						   'status',
						   'size',
						   'gst_info',
						   'gst_no'
	];

	protected $appends = ['industry_name'];

	protected $dates = ['deleted_at'];

	public function user() {
		return $this->belongsTo('App\User', 'user_id', 'id');
	}

	public function indus() {
			return $this->belongsTo('App\Industry', 'industry', 'id');
			/*dd($this);*/
	}

	public function jobs() {
		return $this->hasMany('App\CmpJob', 'user_id', 'user_id');
	}

	public function orders() {
		return $this->hasMany('App\Orders', 'user_id', 'user_id');
	}

	public function recent() {
		return $this->jobs()->where('start_date','>=', date('Y-m-d'))->orderBy('start_date');
	}

	public function previous() {
		return $this->jobs()->where('start_date','<', date('Y-m-d'));
	}

	public function getImageAttribute()
	{
		return '/uploads/logo/'.$this->attributes['logo'];
	}

	public function getIndustryNameAttribute()
	{
		return $this->belongsTo('App\Industry', 'industry');
	}


	// public function getTypeAttribute()
	// {
	//     switch ($this->attributes['type'])
	//     {
	//         case 'startup':
	//             $type = "Start up";
	//             break;
	//         case 'studio':
	//             $type = "Studio";
	//             break;
	//         case 'small':
	//             $type = "Small Business";
	//             break;
	//         case 'midsized':
	//             $type = "Mid-sized Business";
	//             break;
	//         case 'large':
	//             $type = "Large Organisation";
	//             break;
	//         case 'educational':
	//             $type = "Educational Institution";
	//             break;
	//         case 'nonprofit':
	//             $type = "Non Profit Organisation";
	//             break;
	//         default:
	//             $type = "";
	//             break;
	//     }

	//     return $type;
	// }
}