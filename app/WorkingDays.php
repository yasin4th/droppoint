<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkingDays extends Model
{
    protected $table = 'working_days';

	protected $fillable = [
							'job_id',
							'date',
							'start_time',
							'end_time',
							'deleted_at'
	];
}
