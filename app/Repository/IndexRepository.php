<?php 

namespace App\Repository;

use Auth;
use Carbon\Carbon;
use App\tb_company_profile; 
use App\User;
use App\Job;

class IndexRepository
{

    protected $company_profile; 
    protected $users; 
    protected $jobs; 


    /**
     * Initializes the variables 
     * 
     * @param tb_company_profile $company_profile
     * 
     */
        public function __construct(tb_company_profile $company_profile,User $users,Job $jobs)
    {
        $this->company_profile = $company_profile; 
        $this->users = $users; 
        $this->jobs = $jobs; 
    }

    /**
     * tb_company_profile::count() 
     * 
     */
        public function company()
    {
        return $this->users->count(); 
    }


        public function job()
    {
        return $this->jobs; 
    }

 

 
}