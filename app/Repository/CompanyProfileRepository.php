<?php 

namespace App\Repository;

use Auth;
use Carbon\Carbon;
use App\tb_company_profile; 
use App\User;

class CompanyProfileRepository
{

    /**
     * Instance of tb_company_profile
     * 
     */
    protected $company_profile; 


    /**
     * Initializes the variables 
     * 
     * @param tb_company_profile $company_profile
     * 
     */
        public function __construct(tb_company_profile $company_profile)
    {
        $this->company_profile = $company_profile; 
    }

    /**
     * tb_company_profile::all() 
     * 
     */
        public function companyProfileAll()
    {
        return $this->company_profile
                    ->all(); 
    }

    /**
     * tb_company_profile::findOrFail($id) 
     * 
     */
        public function companyProfileById($id)
    {
        return $this->company_profile
                    ->findorFail($id); 
    }

 
        public function companyJob($id)
    {
        return $this->company_profile
                    ->company($id)
                    ->get();
    }

 
}