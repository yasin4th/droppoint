<?php 

namespace App\Repository;

use Auth;
use Carbon\Carbon;
use App\tb_company_profile; 
use App\User;
use App\Job;

class JobRepository
{

    /**
     * Instance of Job
     * 
     */
    protected $Jobs; 


    /**
     * Initializes the variables 
     * 
     * @param Job $Jobs
     * 
     */
        public function __construct(Job $Jobs)
    {
        $this->Jobs = $Jobs; 
    }

    //get the job belong to the company
        public function companyJob($userid)
    {
        return $this->Jobs
                    ->userid($userid);
    }

 
}