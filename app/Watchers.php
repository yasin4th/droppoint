<?php
namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Watchers extends Model
{
	protected $table = 'watchers';

	protected $fillable = [ 'name',
							'email',
							'deleted_at',
	];
}
