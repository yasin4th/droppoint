<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    // use SoftDeletes;

    protected $table = 'notifications';

    // protected $guarded = ['id'];

    protected $fillable = [ 'user_id', 'message', 'posted_by', 'is_read'];

    protected $appends = ['ago'];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function by() {
        return $this->belongsTo('App\User', 'posted_by');
    }

    public function setMessageAttribute($value)
    {
        $this->attributes['message'] = ucfirst($value);
    }

    public function getAgoAttribute()
    {
        $dt = Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at']);

        return $dt->diffForHumans(Carbon::now());
    }
    protected $dates = ['created_at'];

}