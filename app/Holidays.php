<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Holidays extends Model
{

	protected $table = 'holidays';

	protected $fillable = [ 'id',
							'year',
							'holidayprofile',
							'holidaydate',
							'holidaytype',
							'description',
							'timezone_type',
							'timezone'
	];
}