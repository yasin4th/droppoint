<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Applicants extends Model
{
		use SoftDeletes;

		protected $table = 'applicants';

		protected $guarded = ['id'];

		protected $fillable = ['job_id',
								'name',
								'email',
								'phone',
								'status',
								'starred',
								'user_id',
								'image_url',
								'working_hours',
								'deleted_at'
		];

		protected $dates = ['deleted_at'];

		protected $appends = ['has_feedback'];

		public function job() {
				return $this->belongsTo('App\CmpJob', 'job_id', 'id');
		}

		public function feedback() {
				return $this->hasOne('App\Models\ApplicantFeedback', 'applicant_id');
		}

		public function setNameAttribute($value)
		{
				$this->attributes['name'] = title_case($value);
		}

		public function getHasFeedbackAttribute()
		{
				return ($this->feedback) ? true : false;
		}


}