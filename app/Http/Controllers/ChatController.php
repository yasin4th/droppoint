<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Company;
use Carbon\Carbon;
use App\Applicants;
use App\ChatMessages;
use App\ChatRoom;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ChatController extends Controller
{
	public function messages($room_id, $id) {

		$messages = ($id == 0) ?
			ChatMessages::where('room_id', $room_id)->get() :
			ChatMessages::where('room_id', $room_id)->where('id','>=',$id)->get();

		$grouped = [];
		$sender = 0;
		foreach ($messages as $key => $message) {
			if($sender == $message->sender_id) {
				$time = Carbon::parse($message->created_at);
				$time = $time->format('h:i A');

				end($grouped)->message .= "<br><div class='row'> <div class='col s12'><span class='left text_cardwrapper'>{$message->message}</span><span class='blue-text ultra-small right'>{$time}</span></div></div>";
				end($grouped)->created_at = $message->created_at;
			} else {
				$sender = $message->sender_id;
				$time = Carbon::parse($message->created_at);
				$time = $time->format('h:i A');
				$message->message = "<div class='row'> <div class='col s12'><span class='left text_cardwrapper'>{$message->message}</span><span class='blue-text ultra-small right'>{$time}</span></div></div>";
				array_push($grouped, $message);
			}
		}

		return $grouped;
	}

	public function boards($user_id) {
		$rooms = ChatRoom::where('type', 'board')->where('status','active')->with('user')->get();

		if($user_id == 0) {
			$rooms = $rooms->merge(ChatRoom::where('type', 'query')->where('status','active')->with('user')->get());
		} else {
			$rooms = $rooms->merge(ChatRoom::where('user_id', $user_id)->where('type', 'query')->where('status','active')->with('user')->get());
		}

		return $rooms;
	}

	public function upload(Request $request) {
		$filenames = [];
		foreach ($request->file as $file) {
			$ext = $file->getClientOriginalExtension();
			$name = md5(time()).'.'.$ext;
			$origname = $file->getClientOriginalName();
			$file->move('uploads/data', $name);
			array_push($filenames, ['old' => $origname, 'new' => $name, 'ext' => $ext]);
		}
		return $filenames;
	}

	public function admins() {
		return User::where('role', 1)->get();
	}
}
