<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Image;
use App\User;
use App\Tag;
use App\CmpJob;
use App\Company;
use Carbon\Carbon;
use App\Applicants;
use App\WorkingDays;
use App\JobTemplate;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\CreditsApi\GACoreController;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;



class JobController extends Controller
{
	public function index(Request $request) {
		if(Auth::user()->isAdmin())
		{
			$jobs = DB::table('jobs')->join('users','jobs.user_id','=','users.id')
									->where('start_date', '<', date('Y-m-d'))
									->where('status', '=',1)
									->whereNull('jobs.deleted_at')
									->select(DB::raw('(select count(*) from applicants where jobs.id=applicants.job_id and deleted_at is null) as applicants_count, (select count(*) from applicants where status=2 and jobs.id=applicants.job_id and deleted_at is null) as hired_count, title, users.name, state, jobs.user_id, jobs.id,jobs.working_hours,country,location,salary,type,salary_unit,uniform,start_time,end_time,requirements,vacancy,about,jobs.start_date,jobs.status'));

			/*$jobs = CmpJob::where('start_date', '<', date('Y-m-d'));*/
		}
		else
		{
			$jobs = Auth::user()->company->jobs()->where('start_date', '<' ,date('Y-m-d'));
		}

		if ($request->input('query'))
		{
			if(Auth::user()->isAdmin())
			{

				$q = $request->input('query');

				if($q != '') {
					$jobs = $jobs->where(function ($query) use ($q)
					{
						$query->where('jobs.title','like',"%$q%")
						->orWhere('users.name','like',"%$q%");
					});
				}

				$jobs = $jobs->orderBy('start_date','desc')->paginate(25);

			}
			else
			{
				$jobs = $jobs->orderBy('start_date', 'desc')->where('title','like',"%{$request->input('query')}%")->paginate(25);
			}
		}
		else
		{
			$jobs = $jobs->orderBy('start_date', 'desc')->paginate(25);
		}

		return view('job.index')->with(['jobs' => $jobs, 'query' => $request->input('query')]);
	}

	public function create() {		
		return view('job.create');
	}

	public function store(Request $request) {
		/*dd($request);*/
		$dates = explode(',', $request->start_date);

		if($request->has('user_id')) {
			$user_id = $request->user_id;
		} else {
			$user_id = Auth::user()->id;
		}

		if(User::find($user_id)->levels->credits < 1) {
			return redirect()->route('dashboard')->with('error', 'Credits expired.');
		}

		if(Input::file('avatar')) {
			$image = sha1(time()) . '.png';
			$image_path = 'uploads/jobs/' . $image;
			Image::make(Input::file('avatar'))->save($image_path);

			$client = new Client();
			$file = public_path($image_path);
			$req = $client->post("https://aws.eyewil.com/api/upload/files/new",
				[
					'multipart' => [
						[
						'name'     => 'files',
						'contents' => fopen($file, 'r')
						],
						[
						'name'     => 'path',
						'contents' => 'jobs-imgs'
						]
					],
					'verify' => false
				]);
			$data = json_decode($req->getBody()->getContents());
			$image = $data->data->uploads[0]->cloudFrontPath;

		} else {
			$image = "/uploads/jobs/default.jpg";
		}

		$tags = array();

		if (count($request->input('cert')) > 0) {
			$tags = array_merge($tags, $request->input('cert'));
		}
		/*if ($request->input('character')) {
			$tags = array_merge($tags, $request->input('character'));
		}*/

		if ($request->input('trans')) {
			$tags = array_merge($tags, $request->input('trans'));
		}
		if ($request->input('lang')) {
			$tags = array_merge($tags, $request->input('lang'));
		}
		if ($request->input('exp')) {
			$tags = array_merge($tags, $request->input('exp'));
		}

		if ($request->input('edu')) {
			$tags = array_merge($tags, $request->input('edu'));
		}

		if (!$request->input('vacancy')) {
			$request->vacancy = 0;
		}
		$tags = array_values(array_diff( $tags, [''] ));

		$this->validate($request, [
			'title'         => 'required|max:255',
			'start_date'    => 'required',
		]);

		$salary_unit = ($request->input('salary') == 'other') ? trim($request->input('payment')) : 'per day';
		$salary = ($request->input('salary') == 'other') ? $request->input('amount') : $request->input('salary');
		//$working_hours = ($request->input('working_hours') == 'other') ? $request->input('working_hours') : $request->input('working_hours');
	
		$content = $request->input('requirements');
		
		if(strpos($content,'&lt;')) {
			$requirements_html = str_replace('<div>','',$content);
			$requirements_html = str_replace('</div>','',$requirements_html);
			$requirements_html = str_replace('<p>','',$requirements_html);
			$content = str_replace('</p>','',$requirements_html);
		}
		$requirements_html = html_entity_decode($content);	

		$requirements = preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i",'<$1$2>', $requirements_html);
		
		$requirements = preg_replace('/\s+/', ' ', $requirements);
		$requirements = str_replace('<br/>','',$requirements);

		$job = CmpJob::create([
			'user_id'  => $user_id,
			'type'  => $request->input('type'),
			'title'    => $request->input('title'),
			'about'    => $request->input('about'),
			'salary_unit'     => $salary_unit,
			'salary'  => $salary,
			'vacancy'  => $request->input('vacancy'),
			'working_hours'  => $request->input('working_hours'),
			'uniform'       => ($request->uniform == 1) ? "Full uniform provided" :(($request->uniform_details == '') ? "NO":$request->input('uniform_details')),
			'contact_email' => $request->input('contact_email'),
			'contact_phone' => $request->input('contact_phone'),
			'contact_link'  => $request->input('contact_link'),
			'nationality'     => $request->input('nationality'),
			// 'start_date'    => Carbon::createFromFormat('j F, Y', $request->input('start_date'))->toDateTimeString(),
			'start_time' => $request->input('start_time'),
			'city'       => $request->input('loc_city'),
			'state'      => $request->input('loc_state'),
			'country'    => $request->input('loc_country'),
			'street'    => $request->input('street'),
			'location'   => $request->input('loc_address'),
			'requirements' => $requirements,
			'requirements_text' => strip_tags($request->input('requirements')),
			'zipcode'    => $request->input('zipcode'),
			'latitude'   => $request->input('latitude'),
			'longitude'  => $request->input('longitude'),
			'end_time'   => $request->input('end_time'),
			'status'     => '1',
			'experience' => ($request->input('exp')) ? implode(',',$request->input('exp')):'',
			'cover_picture'     => $image
		]);

		if(count($dates)>0 && $dates[0] != '') {
			$job->start_date = $dates[0];
			$job->save();
			foreach ($dates as $date) {
				WorkingDays::create([
					'job_id' => $job->id,
					'date' => $date,
					'start_time' => '09:00:00',
					'end_time'   => '17:00:00'
				]);
			}
		}

		$obj = new GACoreController();
		$obj->process($user_id, 3);

		$newtag = explode(',', $request->newtag);
		if(count($newtag)) {
			foreach ($newtag as $tag) {
				if (!is_numeric($tag))
				{
					$searchTag = Tag::firstOrCreate(['name' => title_case($tag),'category' => 'tag']);
					if ($searchTag)
					{
						array_push($tags, $searchTag->id);
					}
				}
				else
				{
					array_push($tags, $tag);
				}
			}
		}

		if (count($tags) > 0) {
			$job->tags()->sync($tags);
		}

		return redirect()->route('job.show', $job->id)->with('info', 'Job created successfully.');
	}

	public function show($job) {
		return view('job.preview')->withJob(CmpJob::findorFail($job));
	}
	public function edit($job) {		
		$id = $job;
		$job = CmpJob::findorFail($id);
		$tags = Tag::get();
		return view('job.edit', compact('id', 'job', 'tags'));
	}

	public function update($id, $prop, Request $request)
	{

		$job = CmpJob::findorFail($id);

		if ($prop == 'tags' && count($request->value))
		{
			$tags = array();

			foreach ($request->value as $tag) {
				if (!is_numeric($tag))
				{
					$searchTag = Tag::firstOrCreate(['name' => title_case($tag),'category' => 'tag']);

					if ($searchTag)
					{
						array_push($tags, $searchTag->id);
					}
				}
				else
				{
					array_push($tags, $tag);
				}
			}

			$job->tags()->sync($tags);
		}
		else if ($prop != 'start_date')
		{
			$job->update([
				"$prop" => $request->input('value'),
			]);
		}
		else
		{
			$job->update([
				"$prop" => Carbon::createFromFormat('j F, Y', $request->input('value'))->toDateTimeString(),
			]);
		}

		return "Successfully updated";
	}



	public function updateAvatar($id, Request $request) {

		$job = CmpJob::findorFail($id);
		$image = sha1(time()) . '.png';
		$image_path = 'uploads/jobs/' . $image;
		Image::make(Input::file('avatar'))->save($image_path);

		$client = new Client();
		$file = public_path($image_path);
/*		dd($file);*/
		$req = $client->post("https://aws.eyewil.com/api/upload/files/new",
			[
				'multipart' => [
					[
					'name'     => 'files',
					'contents' => fopen($file, 'r')
					],
					[
					'name'     => 'path',
					'contents' => 'jobs-imgs'
					]
				],
				'verify' => false
			]);
		$data = json_decode($req->getBody()->getContents());
		$image = $data->data->uploads[0]->cloudFrontPath;
		$job->update([
			"cover_picture" => $image
		]);

		return response()->json([
			'status' => 'SUCCESS'
		]);
	}



	public function updateAll($id, Request $request)
	{

		$job = CmpJob::findorFail($id);

		if (count($request->tags))
		{
			$tags = array();
			foreach ($request->tags as $tag) {
				if (!is_numeric($tag))
				{
					$searchTag = Tag::firstOrCreate(['name' => title_case($tag),'category' => 'tag']);
					if ($searchTag)
					{
						array_push($tags, $searchTag->id);
					}
				}
				else
				{
					array_push($tags, $tag);
				}
			}

			$job->tags()->sync($tags);
		}

		$dates = explode(',', $request->input('start_date'));

		if(count($dates) > 0) {
			$date0 = $dates[0];
		}

		$content = $request->input('requirements');
		
		if(strpos($content,'&lt;')) {
			$requirements_html = str_replace('<div>','',$content);
			$requirements_html = str_replace('</div>','',$requirements_html);
			$requirements_html = str_replace('<p>','',$requirements_html);
			$content = str_replace('</p>','',$requirements_html);
		}
		$requirements_html = html_entity_decode($content);	

		$requirements = preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i",'<$1$2>', $requirements_html);
		
		$requirements = preg_replace('/\s+/', ' ', $requirements);
		$requirements = str_replace('<br/>','',$requirements);
		
		$job->update([
			"title" => $request->input('title'),
			"start_date" => $date0,
			"working_hours" => $request->input('working_hours'),
			"salary" => $request->input('salary'),
			"vacancy" => $request->input('vacancy'),
			"type" => $request->input('job_type'),
			"status" => $request->input('status'),
			"location" => $request->input('location'),
			"about" => $request->input('description'),
			"requirements" => $requirements,
			"requirements_text" =>strip_tags($request->input('requirements')),
			"contact_email" => $request->input('contact_email'),
			"contact_phone" => $request->input('contact_phone'),
			"contact_link" => $request->input('contact_link'),
			'uniform' => $request->input('uniform_details'),/*($request->uniform == 1) ? "Full uniform provided" :*/ 
			"nationality" => $request->input('nationality'),
			"salary_unit" => $request->input('salary_unit')
		]);
		//dates add to working days
		WorkingDays::where(
				'job_id', $job->id
			)->whereNotIn('date',$dates)->delete();

		foreach ($dates as $date) {
			$d = WorkingDays::firstOrCreate([
				'job_id' => $job->id,
				'date' => $date,
				'start_time' => '09:00:00',
				'end_time'   => '17:00:00'
			]);
		}

		return response()->json([
			'message' => "Job successfully updated",
			'requirements' => $requirements,
			'status' => "ok"
		]);
	}

	public function export(Request $request) {

		if(Auth::user()->isAdmin()) {
			$jobs = CmpJob::where('start_date', '<', date('Y-m-d'));

			if ($request->has('query')) {
				$jobs = $jobs->where('title', $request->input('query'));
			}

			$jobs = $jobs->orderBy('start_date', 'desc')->get();

		} else {
			$jobs = Auth::user()->company->jobs->where('start_date', '<', date('Y-m-d'));
		}

		$header = ['S. No.', 'Title', 'About', 'Vacancy', 'Hired', 'Applicants', 'Start Date', 'Salary', 'Working Days'];
		$csv = implode(",", $header)."\n"; //Column headers
		$i = 1;
		foreach ($jobs as $job){
			$csv .= '"'. $i.'","'.$job->title.'","'.$job->about.'","'.$job->vacancy.'","'.$job->hired_count.'","'.$job->applicants_count.'","'.$job->start_date_read.'","'.$job->salary.'","'.$job->working_days.'"'."\n";
			$i++;
		}

		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=AllJobs.csv');

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
		fwrite ($output,$csv);
		fclose ($output);
	}

	public function updateApplicantField(Request $request) {
		Applicants::whereIn('id',$request->ids)->update(["{$request->field}"=>$request->value]);

		return response()->json(['status' => true, 'mesage' => 'Successfully updated.']);
	}

	public function applicants($id, Request $request)
	{
		$applicants = Applicants::where('job_id',$id);

		if ($request->input('query'))
		{
			$app = $request->input('query');

			if($app != '') {
				$applicants = $applicants->where('name','like',"%$app%");
			}
			$applicants = $applicants->orderBy('created_at','desc')->get();//->paginate(25);

			return response()->json(['applicants' => $applicants, 'query' => $request->input('query'), 'job' => CmpJob::findorFail($id)]);
		} else {

			$job = CmpJob::findorFail($id);

			$applicants = $job->applicants()->paginate(25, ['*'], 'applicants');
			$starred = $job->starred()->paginate(25, ['*'], 'starred');
			$hired = $job->hired()->paginate(25, ['*'], 'hired');
			$archived = $job->archived()->paginate(25, ['*'], 'archived');

			// {{$produk->appends(['region' => $region->currentPage()])->links()}}
			// {{$region->appends(['produk' => $produk->currentPage()])->links()}}

			return view('job.applicants')->withjob($job)->withApplicants($applicants)->withStarred($starred)->withHired($hired)->withArchived($archived);

		}
	}

	public function  selectJob($id)
	{
		$tmp=explode('.',$id);
		if($tmp[0]=='t')
		{
			$job = JobTemplate::findorFail($tmp[1]);
			//dd($job);
			return response()->json([
			'job'=> $job,
			]);
		}
		else
		{
			$job = CmpJob::findorFail($tmp[1]);
			//dd($job);
			return response()->json([
			'job'=> $job,
			]);
		}

	}

	public function postAddress($id, Request $request) {
		$job = CmpJob::findorFail($id);
		$job->city = $request->city;
		$job->state = $request->state;
		$job->street = $request->street;
		$job->country = $request->country;
		$job->location = $request->location;
		$job->latitude = $request->latitude;
		$job->longitude = $request->longitude;
		$job->save();
		return response()->json([
			'job'=> $job,
			'message' => 'Address successfully updated.'
		]);
	}

	public function active(Request $request) {

		$jobs=Auth::user()->company->recent();
		if ($request->input('query'))
		{
			$jobs = $jobs->orderBy('start_date', 'desc')->where('title','like',"%{$request->input('query')}%");
		}
		$jobs = $jobs->paginate(5);

		return view('company.job.active')->with(['jobs' => $jobs, 'query' => $request->input('query')]);
		/*$jobs = CmpJob::where('user_id', '=' ,Auth::user()->id)->where('status', '=' ,0)->orderBy('start_date', 'desc')->get();*/
	}

	public function allTitles() {
		$data =DB::table('ref_job_title')->get()->pluck('value');

		return response()->json([
			'data' => $data,
			'status' => 'SUCCESS'
		]);

	}

	public function previewAdmin($id)
	{ 
		/*dd($id);*/
		$data = CmpJob::findorFail($id);
		return response()->json([
			'data' => $data,
			'status' => 'SUCCESS'
		]);
	}
}
