<?php 
namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Applicants;


class ApiController extends Controller {
    
    public function __construct() 
    {
             
    }

    public function create($job_id, Request $request)
    {   
        
        $applicant = Applicants::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'user_id' => $request->input('user_id'),
            'working_hours' => $request->input('working_hours'),
            'image_url' => $request->input('image_url'),
            'phone' => $request->input('phone'),
            'status' => '1',
            'job_id' => $job_id,
        ]);

        return response([
            'applicant_id' => $applicant->id,
            'message' => 'Applicant successfully added.',
            'status' => 'SUCCESS',
        ], 200);
    }
    
}