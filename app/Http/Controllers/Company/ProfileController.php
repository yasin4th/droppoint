<?php
namespace App\Http\Controllers\Company;
use DB;
use Auth;
use Image;
use App\User;
use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CreditsApi\GACoreController;

// use Guzzle\Http\Client;
use GuzzleHttp\Client;


class ProfileController extends Controller {

    public function __construct()
    {
        // $this->middleware('company');
    }

    public function getProfile()
    {
        return view('company.profile');
    }

    public function getReset()
    {
        return view('company.reset');
    }

    public function getProfileView()
    {
        return view('company.view');
    }

    public function postProfile(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50'
        ]);

        Auth::user()->update([
            'name' => title_case($request->input('name')),
        ]);

        Auth::user()->company->update([
            'about' => trim($request->input('about')),
            'size' => trim($request->input('size')),
            'phone' => trim($request->input('phone')),
            'building' => trim($request->input('building')),
            'address' => trim($request->input('address')),
            'country' => trim($request->input('country')),
             'state' => trim($request->input('state')),
            'tagline' => trim($request->input('tagline')),
            'zipcode' => trim($request->input('zipcode')),
            'website' => trim($request->input('website')),
            'industry' => trim($request->input('industry')),
            'type' => trim($request->input('type')),
            'city' => trim($request->input('city')),
        ]);

        return redirect()->back()->with('info', 'Profile updated successfully.');
    }

    public function postLogo(Request $request)
    {
        $this->validate($request, [
            'logo' => 'required'
        ]);

        $file_path = $request->file('logo')->getPathName();
        $file_size = $request->file('logo')->getClientSize();


        $image = sha1(time()) . '.png';
        $image_path = 'uploads/logo/' . $image;
        $image_thumb_path = 'uploads/logo/thumb/' . $image;
        Image::make(Input::file('logo'))->save($image_path);

        $client = new Client();
        $file = public_path($image_path);
        $req = $client->post("https://aws.eyewil.com/api/upload/files/new",
                [
                    'multipart' => [
                            [
                                'name'     => 'files',
                                'contents' => fopen($file, 'r')
                            ],
                            [
                                'name'     => 'path',
                                'contents' => 'company-pic'
                            ]
                            ],
                        'verify' => false
                ]);
        $data = json_decode($req->getBody()->getContents());
        Auth::user()->company->update([
            'logo' => trim($data->data->uploads[0]->cloudFrontPath),
        ]);

        return redirect()->back()->with('info', 'Company logo updated successfully.');
    }

    public function State(Request $request)
   {/*dd($request->code);*/
       $states = DB::table('ref_state_list')->where('country_code',$request->code)->get();
       /*dd($states);*/
       $result = '<option value="" disabled selected>-Choose-</option>';
       foreach ($states as $state) {
           $result .= "<option value='{$state->name}'>{$state->name}</option>";
       }

       return $result;
   }

}