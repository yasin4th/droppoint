<?php
namespace App\Http\Controllers\Company;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Company;
use App\Applicants;
use App\Models\ApplicantFeedback;
use App\CmpJob;
use Auth;


class ApplicantController extends Controller {

	public function __construct()
	{
		// $this->middleware('company');
	}

	public function show($id)
	{
		// return view('company.job.applicants')->with('job_id', $id);
	}

	public function status($id, $status)
	{
		$applicant = Applicants::findOrFail($id);
		$job = $applicant->job;
		$count = $job->hired->count();
		if($status  == 1 || ($status == 2 && $count < $job->vacancy) || $status == 4) {
			$applicant->update([
				'status' => $status,
			]);
		}
		else
		{
			return [
				"message" => "Vacancy Filled.",
				"status"  => 0
			];
		}

		$info = ($status == 2) ? 'Successfully Hired.' : 'Successfully Removed.';

		return [
			"message" => $info,
			"status"  => 1
		];
	}

	public function starred($id, $starred)
	{
		$applicant = Applicants::findOrFail($id);
		$applicant->update([
			'starred' => $starred,
		]);

		$info = ($starred == 0) ? 'Removed from favourites' : 'Added to favourites';

		return [
			"message" => $info,
			"status"  => 1
		];
	}

	public function destroy($id)
	{
		$applicant = Applicants::findOrFail($id)->delete();

		return [
			'status' => 1,
			'message' => "Successfully Deleted."
		];
	}

	public function feedback($id, Request $request)
	{


		$applicant = Applicants::findOrFail($id);

		$feedback = ApplicantFeedback::updateOrCreate(['applicant_id' => $id],
			[
				'ratings'  => $request->rating,
				'feedback' => $request->feedback,
				'experience'=> $request->experience,
				'compliment'=> $request->compliment
			]);

		return [
			'status' => 1,
			'message' => "Feedback Successfully Submitted."
		];
	}

	public function exportHired($job_id)
	{
		$subscribers = Applicants::where("job_id", $job_id)->where('status', '2')->get();

		$header = ['S. No.', 'User ID', 'Name', 'Email'];
		$csv = implode(",", $header)."\n"; //Column headers
		$i = 1;
		foreach ($subscribers as $subscriber){
			$csv.='"'. $i.'","'.$subscriber->user_id.'","'.$subscriber->name.'","'.$subscriber->email.'"'."\n";
			$i++;
		}

		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=HiredApplicants.csv');

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
		fwrite ($output,$csv);
		fclose ($output);
	}
}