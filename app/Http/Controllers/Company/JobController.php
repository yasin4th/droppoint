<?php
namespace App\Http\Controllers\Company;

use DB;

use Auth;
use Image;
use App\Tag;
use App\User;
use App\CmpJob;
use App\Company;
use Carbon\Carbon;
use App\Applicants;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\CreditsApi\GACoreController;


class JobController extends Controller {

	public function __construct()
	{
		// $this->middleware('company');
	}

	public function index()
	{

		$credits = DB::table('temp_user_levels')->where('userid', Auth::user()->id)->first();
		$credits = ($credits) ? $credits->credits : 0;
		$previous = Auth::user()->company->previous->count();
		$jobs = Auth::user()->company->recent()->paginate(5);
		return view('company.job.index', compact(['jobs','previous','credits']));
	}

	public function create()
	{
		return view('company.job.create');
	}

	public function copy($job_id)
	{
		$job = CmpJob::findorFail($job_id);
		return view('job.create')->withJobCopy($job);
	}

	public function show($id)
	{
		$job = CmpJob::findorFail($id);
		$tags = Tag::get();

		return view('company.job.show', compact('id', 'job', 'tags'));
	}

	public function store(Request $request)
	{

		if(Input::file('avatar')) {
			$image = sha1(time()) . '.png';
			$image_path = 'uploads/jobs/' . $image;
			Image::make(Input::file('avatar'))->save($image_path);
		} else {
			$image = "default.jpg";
		}

		$tags = array();

		if (count($request->input('cert')) > 0) {
			$tags = array_merge($tags, $request->input('cert'));
		}

		if ($request->input('language')) {
			array_push($tags, $request->input('language'));
		}

		if ($request->input('vehicle')) {
			array_push($tags, $request->input('vehicle'));
		}

		if (!$request->input('vacancy')) {
			$request->vacancy = 0;
		}

		$this->validate($request, [
			'title'   => 'required|max:255',
			'start_date'    => 'required',
		]);

		$salary = ($request->input('salary') == 'other') ? $request->input('amount') : $request->input('salary');
		$working_days = ($request->input('working') == 'other') ? $request->input('working_days') : $request->input('working');

		$job = CmpJob::create([
			'user_id'  => Auth::user()->id,
			'type'    => $request->input('type'),
			'title'    => $request->input('title'),
			'about'    => $request->input('about'),
			'salary'   => $salary,
			'vacancy'  => $request->input('vacancy'),
			'vehicle'  => $request->input('vehicle'),
			'language' => $request->input('language'),
			'working_days'  => $working_days,
			'contact_email' => $request->input('contact_email'),
			'contact_phone' => $request->input('contact_phone'),
			'contact_link'  => $request->input('contact_link'),
			'certificates'  => ($request->input('cert')) ? implode(',', $request->input('cert')) : '',
			'start_date'    => Carbon::createFromFormat('j F, Y', $request->input('start_date'))->toDateTimeString(),
			'start_time'    => $request->input('start_time'),
			'city'          => $request->input('loc_city'),
			'state'         => $request->input('loc_state'),
			'country'       => $request->input('loc_country'),
			'location'      => $request->input('loc_address'),
			'requirements'  => $request->input('requirements'),
			'zipcode'       => $request->input('zipcode'),
			'latitude'      => $request->input('latitude'),
			'longitude'     => $request->input('longitude'),
			'end_time'      => $request->input('end_time'),
			'status'        => '0',
			'highest_education' => $request->input('highest_education'),
			'avatar'       => $image
		]);

		if (count($tags) > 0) {
			$job->tags()->sync($tags);
		}

		$obj = new GACoreController();
		$obj->process(Auth::user()->id, 3);

		return redirect()->route('job.preview', $job->id)->with('info', 'Job created successfully.');
	}

	public function update($id, $prop, Request $request)
	{

		$job = CmpJob::findorFail($id);

		if ($prop == 'tags' && count($request->value))
		{
			$tags = array();

			foreach ($request->value as $tag) {
				if (!is_numeric($tag))
				{
					$searchTag = Tag::firstOrCreate(['name' => title_case($tag)]);

					if ($searchTag)
					{
						array_push($tags, $searchTag->id);
					}
				}
				else
				{
					array_push($tags, $tag);
				}
			}

			$job->tags()->sync($tags);
		}
		else if ($prop != 'start_date')
		{
			$job->update([
				"$prop" => $request->input('value'),
			]);
		}
		else
		{
			$job->update([
				"$prop" => Carbon::createFromFormat('j F, Y', $request->input('value'))->toDateTimeString(),
			]);
		}

		return "Successfully updated";
	}

	public function preview($job_id) {
		return view('company.job.preview')->withJob(CmpJob::findorFail($job_id));
	}

	public function destroy($id)
	{
		CmpJob::findorFail($id)->delete();
		return redirect()->back();
	}

	public function delete($job_id)
	{
		CmpJob::findorFail($job_id)->update([
			'status' => 0
		]);
		return redirect()->back();
	}

	public function getAll(Request $request)
	{
		$jobs = Auth::user()->company->jobs()->where('start_date', '<' ,date('Y-m-d'));

		if ($request->input('query'))
		{
			$jobs = $jobs->orderBy('start_date', 'desc')->where('title','like',"%{$request->input('query')}%")->paginate(5);
		}
		else
		{
			$jobs = $jobs->orderBy('start_date', 'desc')->paginate(5);
		}

		return view('company.job.all')->with(['jobs' => $jobs, 'query' => $request->input('query')]);
	}

	public function recent(Request $request)
	{
		$jobs = Auth::user()->company->jobs()->where('start_date', '>=', date('Y-m-d'));

		if($request->input('query') != '') {
			$jobs = $jobs->where('title','like',"%{$request->input('query')}%");
		}

		if($request->date && $request->date != '') {
			$jobs1 = clone $jobs;
			$dates = explode(',', $request->date);
			$jobs1 = (count($dates) > 0) ? $jobs1->whereIn('start_date', $dates) : $jobs1;

			if($jobs1->get()->count() > 0) {
				$jobs = $jobs->whereIn('start_date', $dates);
			}
		}


		if ($request->sort && $request->reverse == 'false') {
			$jobs = $jobs->orderBy($request->sort);
		} else if ($request->sort && $request->reverse == 'true') {
			$jobs = $jobs->orderBy($request->sort, 'desc');
		}

		$jobs = $jobs->paginate(5);

		return $jobs;
	}

	public function progress(Request $request)
	{

		$jobs = Auth::user()->company->jobs()->where('start_date', '<', date('Y-m-d'));

		if($request->input('query') != '') {
			// $jobs = $jobs->where('title','like',"%{$request->input('query')}%");
		}

		if($request->input('date') != '') {
			// $jobs = $jobs->where('start_date', "{$request->input('date')}");
		}

		if ($request->sort && $request->reverse == 'false') {
			// $jobs = $jobs->orderBy($request->sort);
		} else if ($request->sort && $request->reverse == 'true') {
			// $jobs = $jobs->orderBy($request->sort, 'desc');
		}
		$jobs = $jobs->get()->pluck('id');

		return Applicants::whereIn('job_id', $jobs)->with('job')->get();

		return $jobs;
	}

	public function applicants($job_id, Request $request)
	{
		$applicants = CmpJob::findOrFail($job_id)->applicants();

		if($request->input('status')) {
			$applicants = $applicants->where('status', $request->input('status'));
		}

		switch ($request->input('sort'))
		{
			case 'all':
				$applicants = $applicants;
				break;
			case 'newest':
				$applicants = $applicants->orderBy('created_at', 'desc');
				break;
			case 'oldest':
				$applicants = $applicants->orderBy('created_at');
				break;
			case 'hours':
				$applicants = $applicants->orderBy('working_hours','desc');
				break;
			case 'rejected':
				$applicants = CmpJob::findOrFail($job_id)->applicants()->where('status', 4)->orderBy('working_hours','desc');
				break;
		}

		if($request->input('favourites') == 1)
		{
			$applicants->where('starred', 1);
		}

		return ($request->has('page')) ? $applicants->paginate(25) : $applicants->get();
	}

	public function hired($job_id)
	{
		$hired = CmpJob::findOrFail($job_id)->hired;
		return $hired;
	}

	public function export()
	{
		$jobs = Auth::user()->company->jobs;

		$header = ['S. No.', 'Title', 'About', 'Vacancy', 'Start Date', 'Salary', 'Working Days'];
		$csv = implode(",", $header)."\n"; //Column headers
		$i = 1;
		foreach ($jobs as $job){
			$csv .= '"'. $i.'","'.$job->title.'","'.$job->about.'","'.$job->vacancy.'","'.$job->start_date_read.'","'.$job->salary.'","'.$job->working_days.'"'."\n";
			$i++;
		}

		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=Jobs.csv');

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
		fwrite ($output,$csv);
		fclose ($output);
	}
}