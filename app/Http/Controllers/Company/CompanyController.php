<?php
namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Company;
use Auth;
use Hash;
use App\tb_company_profile;
use Illuminate\Support\Facades\Input;


class CompanyController extends Controller {

    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function getIndex()
    {
        return redirect()->route('job.index');
    }

    public function postReset(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:4',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|min:6',
        ]);

        $user = Auth::user();

        if (Hash::check($request->input('password'), Auth::user()->password)) {
            $user->password = bcrypt($request->input('new_password'));
            $user->save();
            return redirect()->route('cmp.profile')->with('info', 'Password changed successfully!');
        }

        return redirect()->route('cmp.profile')->with('info', 'Something went wrong!');
    }
}