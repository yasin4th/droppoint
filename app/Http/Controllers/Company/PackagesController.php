<?php
namespace App\Http\Controllers\Company;

use Auth;
use Image;
use App\User;
use App\Orders;
use App\CmpJob;
use App\Company;
use Carbon\Carbon;
use App\Applicants;
use App\Models\GABadges;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\PaypalIPNListener;



class PackagesController extends Controller {

	public function __construct()
	{
		// $this->middleware('admin');
	}

	public function getPurchase()
	{
		return view('company.purchasePackage');
	}

	public function saveGst(Request $request)
	{
		Auth::user()->company->update([
			'gst_info' => $request->gst_info,
			'gst_no'   => $request->gst_no
		]);
		return redirect('/cmp/package');
	}

	public function postPurchase(Request $request)
	{
		switch($request->credits) {
			case '10':
				$amount = 5;
				break;
			case '20':
				$amount = 10;
				break;
			case '30':
				$amount = 25;
				break;
			case '40':
				$amount = 50;
				break;
			default:
				break;
		}
		return view('company.redirect')->withCredits($request->credits)->withAmount($amount);
	}

	public function success(Request $request)
	{
		return view('company.success');
	}

	public function failed(Request $request)
	{
		return view('company.failed');
	}

	public function listener(Request $request)
	{
		$ipn = new PaypalIPNListener();
		$ipn->use_sandbox = true;
		// $ipn->force_ssl_v3 = true;

		$verified = $ipn->processIpn();

		$report = $ipn->getTextReport();

		Log::info("-----new payment-----");
		Log::info('Date: '.date('Y-m-d H:i:s'));
		Log::info($report);

		if ($verified) {
			if ($_POST['address_status'] == 'confirmed' && $_POST['payment_status'] == 'Completed') {

				$order = new Orders();
				$order->user_id = $_POST['custom'];
				$order->plan = $_POST['item_number'];
				$order->amount = $_POST['payment_gross'];
				$order->status = $_POST['payment_status'];
				$order->payment_via = 'Paypal';
				$order->payment_date = date('Y-m-d H:i:s');
				$order->save();

				$level = User::find($order->user_id)->levels;
				$level->credits = floatval($level->credits) + floatval($order->plan);
				$level->save();
				// Check outh POST variable and insert your logic here
				Log::info("payment verified and inserted to db");
			}
		} else {
			Log::info("Some thing went wrong in the payment !");
		}

	}
}
