<?php
namespace App\Http\Controllers\Company;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class CountryController extends Controller {

	public function index()
	{
		$countries = DB::table('ref_country_list')->get();
		return $countries;
	}

	public function show($id)
	{
		$states = DB::table('ref_state_list')->where('country_id', $id)->get();
		$result = '<option value="" disabled selected>-Choose-</option>';
		foreach ($states as $state) {
			$result .= "<option value='{$state->name}'>{$state->name}</option>";
		}

		return $result;
	}

}