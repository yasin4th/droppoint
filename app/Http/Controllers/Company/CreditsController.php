<?php
namespace App\Http\Controllers\Company;

use Auth;
use Image;
use App\User;
use App\CmpJob;
use App\Company;
use Carbon\Carbon;
use App\Applicants;
use App\Models\GABadges;
use App\Models\GAUserAchievements;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\CreditsApi\GACoreController;


class CreditsController extends Controller {

	public function __construct()
	{
		// $this->middleware('admin');
	}

	public function credits()
	{
		$user = Auth::user();
		$achievements = $user->achievements()->paginate(25);
		$badges = GABadges::where('type', 5)->get();
		return view('company.credits', compact(['user', 'achievements', 'badges']));
	}

	public function summary()
	{
		$user = Auth::user();
		return view('company.summary')->withUser($user);
	}

	public function coupon(Request $request)
	{
		$badge = GABadges::where('title', $request->coupon)->first();
		if($badge) {
			$history = GAUserAchievements::where('userid',Auth::user()->id)->where('badge_id',$badge->id)->get();
			if(count($history) > 0) {
				return view('company.summary')
					->with('message', 'Already used coupon code.')
					->with('status', 'FAILED');
			}
			$obj = new GACoreController();
			$obj->process(Auth::user()->id, $badge->id);
			return view('company.summary')
				->with('message', 'Coupon Successfully Applied')
				->with('status', 'SUCCESS');
		} else {
			return view('company.summary')
				->with('message', 'Whoops! There were some problems with your input or <br>The selected code is invalid.')
				->with('status', 'FAILED');
		}
	}

	// public function manage($user_id)
	// {
	//     $user = User::findOrFail($user_id);
	//     return view('admin.company.manage')->withUser($user);
	// }

	// public function giveBadge($user_id, $badge)
	// {
	//     $user = User::findOrFail($user_id);
	//     $obj = new GACoreController();
	//     $obj->process($user_id, $badge);
	//     return [
	//         'status' => 1,
	//         'message' => 'Successfully assigned credits.'
	//     ];
	// }
}