<?php
namespace App\Http\Controllers\Company;

use Auth;
use Image;
use App\User;
use App\CmpJob;
use App\Company;
use Carbon\Carbon;
use App\Applicants;
use App\Models\GABadges;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;



class PaymentsController extends Controller {

	public function __construct()
	{
		// $this->middleware('admin');
	}

	public function payments()
	{
		return view('company.payments');
	}
}
