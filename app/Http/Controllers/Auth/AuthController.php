<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Mail;
use Session;
use Hash;
use DB;
use Socialite;
use App\User;
use App\Company;
use App\ResetPassword;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CreditsApi\GACoreController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('guest', ['except' => 'logout']);
	}

	public function getLogin()
	{
		if (Auth::check()) {
			return redirect()->route('dashboard');
			/*if (Auth::user()->isAdmin()) {
			}
			if (Auth::user()->isCompany()) {
				return redirect()->route('job.index');
			}*/
		}
		return view('auth.login');
	}

	public function getReset()
	{
		if (Auth::check()) {
			return redirect()->route('job.index');
		}
		return view('auth.reset');
	}

	public function postReset(Request $request)
	{
		if (Auth::check()) {
			return redirect()->route('job.index');
		}

		$user = User::where('email', $request->email)->first();
		if(!$user) {
			return redirect()->back()->with([
				'message' => 'User with this email doesn\'t exists',
				'status' => 'FAILED'
			]);
		} else {
			if($user->provider == 'facebook' || $user->provider == 'google') {
				return redirect()->back()->with('message', 'Please login via '.$user->provider.' login.')->with('status', 'FAILED');
			} else {
				$token = md5(time());

				$reset = ResetPassword::create([
					'email'      => $user->email,
					'token'      => $token
				]);

				$data = [
					'token' => $token
				];

				Mail::send('mail.forgot', compact('user', 'token'), function ($message) use ($user, $token) {
					$message->from('send@eyewil.com', 'DropPoint Support');
					$message->to($user->email)->subject('Reset Password');
				});

				return redirect()->back()->with('message', 'Password reset link sent to your registered email.')->with('status','SUCCESS');
			}
		}
	}

	public function getForgot($token)
	{
		$reset = ResetPassword::where('token',$token)->first();
		if($reset){
			return view('auth.forgot');
		} else {
			return redirect()->route('auth.login')->with('message','Invalid token');
		}
	}

	public function postForgot($token, Request $request) {
		if($request->password != $request->confirm){
			return redirect()->back()->with('message','Confirm Password dont match')->with('status','FAILED');
		}
		$reset = ResetPassword::where('token',$token)->first();
		if($reset){
			$user = User::where('email', $reset->email)->first();
				$user->password = bcrypt($request->password);
			$user->save();
			DB::table('password_resets')->where('email',$reset->email)->delete();
			return redirect()->route('auth.login')->with('message','Your password has been changed successfully.')->with('status','SUCCESS');
		}else{
			return redirect()->route('auth.login')->with('message','Invalid token');
		}
	}

	public function postLogin(Request $request)
	{
		$this->validate($request, [
			'email' => 'required',
			'password' => 'required',
		]);

		if(!Auth::attempt($request->only(['email','password']), $request->has('remember')))
		{
			return redirect()->back()->with('message','Invalid credentials!');
		}

		if(Auth::check()) {
			if (Auth::user()->isCompany() && Auth::user()->company->status != 1) {
					Auth::logout();
					return redirect()->back();
			}
			if($request->redirect) {
				return redirect($request->redirect);
			}
			return redirect()->route('dashboard');
		}
	}

	public function getLogout()
	{
		Auth::logout();

		return redirect()->route('auth.login');
	}

	public function getRegister()
	{
		return view('auth.register');
	}

	public function postRegister(Request $request)
	{
		$this->validate($request, [
			'email'    => 'required|unique:users|email|max:255',
			'name' => 'required|max:50',
			'password' => 'required|min:6',
		]);

		$user = User::create([
			'email' => $request->input('email'),
			'name' => $request->input('name'),
			'password' => bcrypt($request->input('password')),
			'role' => '2',
		]);

		$company = Company::create([
			'user_id' => $user->id,
		]);

		$obj = new GACoreController();
		$obj->process($user->id, 1);

		Mail::send('mail.confirm', compact('user'), function ($message) use ($user) {
			$message->from('send@eyewil.com', 'DropPoint Support');
			$message->to($user->email)->subject('Confirm Email');
		});

		return redirect()->route('auth.login')->with('message', 'Successfully registered. Please confirm your email address.')->with('status','SUCCESS');
	}

	public function getProvider($provider) {
		return Socialite::driver($provider)->redirect();
	}

	public function handleProviderCallback($provider) {
		try {

			$user = Socialite::driver($provider)->user();

			$authUser = $this->findOrCreateUser($user, $provider);
			Auth::login($authUser, true);
			return redirect()->route('auth.login');
		} catch (\Exception $e) {
			return redirect()->route('auth.login');
		}
	}

	public function findOrCreateUser($user, $provider) {
		$authUser = User::where('provider_id', $user->id)->first();

		if ($authUser) {
			return $authUser;
		}

		$user = User::create([
			'name'     => $user->name,
			'email'    => $user->email,
			'provider' => $provider,
			'provider_id' => $user->id,
			'role'     => 2
		]);

		$company = Company::create([
			'user_id' => $user->id,
		]);

		$obj = new GACoreController();
		$obj->process($user->id, 1);

		return $user;
	}

}
