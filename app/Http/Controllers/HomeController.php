<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\tb_company_profile;
use App\User;
use App\Job;
use Auth;
use DB;


class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct ()
	{
		// $this->middleware('auth');
	}

	public function index()
	{
		if(Auth::user()->isCompany()) {

			$credits = DB::table('temp_user_levels')->where('userid', Auth::user()->id)->first();
			$credits = ($credits) ? $credits->credits : 0;
			$previous = Auth::user()->company->previous->count();
			$jobs = Auth::user()->company->recent()->paginate(5);

			return view('company.job.index', compact(['jobs','previous','credits']));

		} else {

			return view('admin.dashboard');
		}
	}

	public function dashboard()
	{
		$id = \Auth::user()->id;
		$companyJobs = Job::where('user_id','=',$id)->whereRaw('date(created_at) = ?', [Carbon::now()->toDateString()])->take(5)->get();
		$companyJobsToday = Job::whereRaw('date(start_date) = ?', [Carbon::now()->toDateString()])->count();

		//admin
		$jobsToday = Job::whereRaw('date(created_at) = ?', [Carbon::now()->toDateString()])->count();
		$jobs = Job::count();
		$CompanyToday = User::whereRaw('date(created_at) = ?', [Carbon::now()->toDateString()])->count();
		$Company = User::count();
		$IndexCompany = User::orderBy('created_at','asc')->take(5)->get();
		$today = Carbon::now()->format('jS F Y ');

		return view('dashboard',compact('jobs','jobsToday','Company','CompanyToday','IndexCompany','today','companyJobs'));
	}

	 public function mail()
    {
        $user = User::find(1)->toArray();
        Mail::send('emails.mailEvent', $user, function($message) use ($user) {
            $message->to($user->email);
            $message->subject('Sendgrid Testing');
        });
        //dd('Mail Send Successfully');
    }
}