<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use App\CmpJob;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ServiceController extends Controller
{
	public function get($user_id, Request $request) {
		$user = User::find($user_id);
		if($user) {
			return response()->json(array(
				'jobs' => $user->company->recent()->with('applicants')->with('hired')->get(),
				'status' => 'SUCCESS'
			));
		} else {
			return response()->json(array(
				'jobs' => [],
				'status' => 'FAILED'
			));
		}
	}

	public function post($user_id, Request $request) {
		$user = User::find($user_id);
		// dd($request->job['title']);
		if($user) {
			$job = CmpJob::insert([
				'user_id' => $user_id,
				'title' => $request->job['title'],
				'about' => $request->job['about'],
				'vacancy' => $request->job['vacancy'],
				'start_time' => '09:00:00',
				'end_time' => '17:00:00',
				'start_date' => $request->job['date'],
				'status' => 1,
				'contact_email' => $request->job['contactemail'],
				'contact_phone' => $request->job['contactphone'],
				'contact_link' => $request->job['contactlink']
			]);

			return response()->json(array(
				'message' => 'Job posted successfully.',
				'status' => 'SUCCESS'
			));
		} else {
			return response()->json(array(
				'message' => 'Something went wrong.',
				'status' => 'FAILED'
			));
		}
	}
}
