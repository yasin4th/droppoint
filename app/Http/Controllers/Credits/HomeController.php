<?php
namespace App\Http\Controllers\Credits;

use App\Http\Controllers\Controller;

use DB;
use Image;
use App\Models\GABadges;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use GuzzleHttp\Client;

// use Illuminate\Database\Eloquent;
// use Illuminate\Support\Facades\DB;

class HomeController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');

        //parent::__construct();

        //$this->news = $news;
        //$this->user = $user;
    }


    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {
        // return view('admin.credits.home');
        return view('pages.home');
    }

    public function manage()
    {
        return view('pages.manage');
    }

    public function simulate()
    {
        return view('pages.simulate');
    }

    public function display()
    {
        return view('pages.display');
    }

    public function upload(Request $request)
    {
        if(Input::file('avatar')) {
            $image = sha1(time()) . '.png';
            $image_path = 'uploads/badges/' . $image;
            Image::make(Input::file('avatar'))->save($image_path);

            $client = new Client();
            $file = public_path($image_path);
            $req = $client->post("https://aws.eyewil.com/api/upload/files/new",
                    [
                        'multipart' => [
                                [
                                    'name'     => 'files',
                                    'contents' => fopen($file, 'r')
                                ],
                                [
                                    'name'     => 'path',
                                    'contents' => 'jobs'
                                ]
                            ],
                    'verify' => false
                    ]);
            $data = json_decode($req->getBody()->getContents());
            $image = $data->data->uploads[0]->cloudFrontPath;

        } else {
            $image = "/uploads/badges/default.jpg";
        }

        $badge = GABadges::find($request->badge_id);
        $badge->update([
            'icon' => $image
        ]);

        return redirect()->back();

    }
}