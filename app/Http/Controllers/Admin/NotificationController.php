<?php
namespace App\Http\Controllers\Admin;

USE DB;
use Auth;
use App\User;
use App\CmpJob;
use App\Company;
use Carbon\Carbon;
use App\Notification;
use App\ChatMessages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class NotificationController extends Controller {

    public function __construct()
    {
        // $this->middleware('admin');
    }

    public function index()
    {
        $notifications = DB::table('notifications')
            ->select('message','posted_by','created_at')
            ->groupBy('created_at','message','posted_by')
            ->orderBy('created_at','desc')
            ->get();
        return view('admin.notification.index')->withNotifications($notifications);
    }

    public function create()
    {
        // $notification = Notification::where('user_id', Auth::user()->id)->first();
        // $dt = Carbon::createFromFormat('Y-m-d H:i:s', $notification->created_at);
        // // dd($dt);
        // return $dt->diffForHumans(Carbon::now());

        $users = User::where('role', 2)->get();
        return view('admin.notification.create')->withUsers($users);
    }

    public function store(Request $request)
    {
        $users = ($request->type == 'on') ? User::where('role', 2)->get()->pluck('id')->toArray() : [$request->users];

        foreach ($users as $user) {
            Notification::create([
                'posted_by' => Auth::user()->id,
                'user_id' => $user,
                'message' => $request->message
            ]);
        }
        if($request->type == 'on') {
            ChatMessages::insert([
                'sender_id' => Auth::user()->id,
                'room_id' => $request->rooms,
                'message' => $request->message
            ]);
        }

        return redirect()->back()->withInfo('Notification Successfully Created.');

    }

    public function read()
    {
        $notifications = Notification::where('user_id', Auth::user()->id)->get();

        foreach ($notifications as $key => $message) {
            $message->is_read = 1;
            $message->save();
        }

        return;
    }
}