<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\User;
use App\CmpJob;
use App\Applicants;
use Carbon\Carbon;

class ReportsController extends Controller {

	public function __construct()
	{
		// $this->middleware('admin');
	}

	public function jobs()
	{
		$months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

				$jobs_published = CmpJob::select('id', 'title', 'created_at')
						->get()
						->groupBy(function($val) {
							return Carbon::parse($val->created_at)->format('m-Y');
						});
				$published = [];
				foreach($jobs_published as $month=>$collection) {

					array_push($published,
							[	'year' => explode('-', $month)[1],
								'month' => $months[intval(explode('-', $month)[0])-1],
								'count' => $collection->count()
							]);
				}

				$jobs_expired = CmpJob::select('id', 'title', 'start_date')
						->get()
						->groupBy(function($val) {
							return Carbon::parse($val->start_date)->format('m-Y');
						});
				$expired = [];
				foreach($jobs_expired as $month=>$collection) {

					array_push($expired,
							[	'year' => explode('-', $month)[1],
								'month' => $months[intval(explode('-', $month)[0])-1],
								'count' => $collection->count()
							]);
				}


				$jobs_fulltime = CmpJob::select('id', 'title', 'created_at')->where('type','full-time')
						->get()
						->groupBy(function($val) {
							return Carbon::parse($val->created_at)->format('m-Y');
						});
				$fulltime = [];
				foreach($jobs_fulltime as $month=>$collection) {

					array_push($fulltime,
							[	'year' => explode('-', $month)[1],
								'month' => $months[intval(explode('-', $month)[0])-1],
								'count' => $collection->count()
							]);
				}

				$jobs_parttime = CmpJob::select('id', 'title', 'created_at')->where('type','part-time')
						->get()
						->groupBy(function($val) {
							return Carbon::parse($val->created_at)->format('m-Y');
						});

				$parttime = [];
				foreach($jobs_parttime as $month=>$collection) {

					array_push($parttime,
							[	'year' => explode('-', $month)[1],
								'month' => $months[intval(explode('-', $month)[0])-1],
								'count' => $collection->count()
							]);
				}

		return view('admin.reports.jobs')
			->with('published', $published)
			->with('expired', $expired)
			->with('fulltime', $fulltime)
			->with('parttime', $parttime);
	}



	public function users()
	{
		$months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		$all_users = Applicants::select('id','created_at')
						->get()
						->groupBy(function($val) {
							return Carbon::parse($val->created_at)->format('m-Y');
						});
				$users = [];
				foreach($all_users as $month=>$collection) {

					array_push($users,
							[	'year' => explode('-', $month)[1],
								'month' => $months[intval(explode('-', $month)[0])-1],
								'count' => $collection->count()
							]);
				}
		$all_company = User::select('id','created_at')->where('role','2')
				->get()
				->groupBy(function($val) {
					return Carbon::parse($val->created_at)->format('m-Y');
				});
		$company = [];
		foreach($all_company as $month=>$collection) {

			array_push($company,
					[	'year' => explode('-', $month)[1],
						'month' => $months[intval(explode('-', $month)[0])-1],
						'count' => $collection->count()
					]);
		}
		return view('admin.reports.users')
				->with('users',$users)
				->with('company',$company);
	}

	public function location() {
		$jobs = CmpJob::where('start_date', '>=', date('Y-m-d'))->where('status', '=', 1)->whereNotNull('latitude')->select(['title','latitude', 'longitude','salary','id', 'salary_unit', 'start_date'])->get();
		return view('admin.reports.location')->withJobs($jobs);
	}

	public function tags(Request $request)
	{

		$total_tags = DB::table('report_tags')->select('name', DB::raw('sum(countTags) as user_count'), 'category');

		if($request->days && $request->days != 0) {
			$dates = date('Y-m-d',strtotime('-'.$request->days.'days'));
			$total_tags = $total_tags->where('date','>=',$dates);
		}

		$total_tags = $total_tags->groupBy('name','category')
						->orderBy('user_count','DESC')
						->take(10)->get();

		$tags = [];
		foreach($total_tags as $tag) {
			array_push($tags,
				[
					'name' => $tag->name,
					'countTags' => $tag->user_count,
					'category' => $tag->category
				]);
		}
		return view('admin.reports.tags')
					->with('tags',$tags);
	}


	public function industry()
	{
		$total_job = CmpJob::select('id')->get()->count();
		$jobs_published = CmpJob::select('id', 'user_id', 'created_at') 
						->get()
						->groupBy('user_id');

		$industry = [];
		foreach ($jobs_published as $key => $value) {

			$ind = DB::table('ref_industry_list')->find($key);
			$industry_name = ($key > 0 && $ind) ? $ind->name : '';
			$jobs_posted = $value->count();
			$users = DB::table('users')->find($key);
			$company_name = ($key > 0 && $users) ? $users->name : '';
			/*dd($company_name);*/

			if($key > 0 && $ind) {
				array_push($industry,
					[	'industry_id' => $key,
						'industry_name' => $industry_name,
						'jobs_posted' => $jobs_posted,
						'company_name'=>$company_name
					]);
			}
		}
		return view('admin.reports.industry')
				->with('industry',$industry);
	}

}