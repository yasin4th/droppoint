<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\CmpJob;
use App\Company;
use App\Applicants;
use Image;
use Auth;

use GuzzleHttp\Client;


class CompanyController extends Controller {

    public function __construct()
    {
        // $this->middleware('admin');
    }

    public function create()
    {
        return view('admin.company.create');
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|unique:users|email|max:255',
            'name' => 'required|max:50',
            'password' => 'required|min:6',
        ]);

        $user = User::create([
            'email' => $request->input('email'),
            'name' => $request->input('name'),
            'password' => bcrypt($request->input('password')),
            'role' => '2',
        ]);

        $company = Company::create([
            'user_id' => $user->id,
        ]);

        return redirect()->back()->with('info', 'Company successfully created.');
    }

    public function edit($user_id)
    {
        $user = User::findOrFail($user_id);
        return view('admin.company.profile')->with('user', $user);
    }

    public function logo($user_id)
    {
        $user = User::findOrFail($user_id);
        return view('admin.company.logo')->with('user', $user);
    }

    public function postLogo($user_id, Request $request)
    {
        $this->validate($request, [
            'logo' => 'required'
        ]);

        $image = sha1(time()) . '.png';
        $image_path = 'uploads/logo/' . $image;
        $image_thumb_path = 'uploads/logo/thumb/' . $image;
        Image::make(Input::file('logo'))->save($image_path);

        $client = new Client();
        // $client->setDefaultOption();
        $file = public_path($image_path);
        $req = $client->post("https://aws.eyewil.com/api/upload/files/new",
                [
                    'multipart' => [
                            [
                                'name'     => 'files',
                                'contents' => fopen($file, 'r')
                            ],
                            [
                                'name'     => 'path',
                                'contents' => 'company-pic'
                            ]
                        ],
                    'verify' => false
                ]);
        $data = json_decode($req->getBody()->getContents());

        $user = User::findOrFail($user_id);
        $user->company->update([
            'logo' => trim($data->data->uploads[0]->cloudFrontPath),
        ]);

        return redirect()->back()->withInfo('Company logo updated successfully.')->withUser($user);
    }

    public function reset($user_id)
    {
        $user = User::findOrFail($user_id);
        return view('admin.company.reset')->with('user', $user);
    }

    public function postReset($user_id, Request $request)
    {
        $user = User::findOrFail($user_id);

        $user->update([
            'password' => bcrypt($request->new_password)
        ]);

        return redirect()->back()->withUser($user)->withInfo('Successfully changed password.');
    }

    public function update($user_id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50'
        ]);

        User::findOrFail($user_id)->update([
            'name' => title_case($request->input('name')),
        ]);

        User::findOrFail($user_id)->company->update([
            'type' => trim($request->input('type')),
            'size' => trim($request->input('size')),
            'about' => trim($request->input('about')),
            'size' => trim($request->input('size')),
            'phone' => trim($request->input('phone')),
            'address' => trim($request->input('address')),
            'country' => trim($request->input('country')),
            'tagline' => trim($request->input('tagline')),
            'zipcode' => trim($request->input('zipcode')),
            'website' => trim($request->input('website')),
            'industry' => trim($request->input('industry')),
            'city' => trim($request->input('city')),
        ]);

        return redirect()->back()->with('info', 'Profile updated successfully.');
    }

    public function delete($user_id)
    {
        User::findOrFail($user_id)->delete();
        return redirect('/admin/users');
    }

    public function status($user_id, $status)
    {
        Company::where('user_id', $user_id)->update([
            'status' => $status,
        ]);

        return redirect('/admin/users');
    }

}