<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\JobTemplate;
use App\User;
use App\Tag;
use Image;
use Auth;
use DB;

use GuzzleHttp\Client;

class TemplateController extends Controller {

	public function __construct()
	{
	// $this->middleware('admin');
	}

	public function getTemplate(Request $request) {
		if ($request->input('query'))
		{
			$q = $request->input('query');

			if($q != '') {
				$tmpJobs = JobTemplate::where(function ($query) use ($q)
				{
					$query->where('jobTemplates.title','like',"%$q%");
				/*	->orWhere('users.name','like',"%$q%");*/
				});
			}
			$tmpJobs = $tmpJobs->orderBy('created_at','desc')->paginate(25);
		}
		else
		{
			$tmpJobs=JobTemplate::orderBy('created_at', 'desc')->paginate(25);
		}
		return view('admin.template')->with(['tmpJobs' => $tmpJobs, 'query' => $request->input('query')]);
	}


	public function getCreate() {
		$temp='';
		return view('admin.createTemplate')->with('temp',$temp);
	}

	public function postCreate(Request $request) {

		if(Input::file('avatar')) {
			$image = sha1(time()) . '.png';
			$image_path = 'uploads/jobs/' . $image;
			Image::make(Input::file('avatar'))->save($image_path);


			$client = new Client();
			$file = public_path($image_path);
			$req = $client->post("https://aws.eyewil.com/api/upload/files/new",
					[
						'multipart' => [
								[
									'name'     => 'files',
									'contents' => fopen($file, 'r')
								],
								[
									'name'     => 'path',
									'contents' => 'template'
								]
							],
							'verify' => false
					]);
			$data = json_decode($req->getBody()->getContents());

			$image = $data->data->uploads[0]->cloudFrontPath;
		} else {
			$image = "/uploads/jobs/default.jpg";
		}

		$tags = array();

		if (count($request->input('cert')) > 0) {
			$tags = array_merge($tags, $request->input('cert'));
		}
		if ($request->input('character')) {
			$tags = array_merge($tags, $request->input('character'));
		}

		if ($request->input('trans')) {
			$tags = array_merge($tags, $request->input('trans'));
		}

		if ($request->input('lang')) {
				$tags = array_merge($tags, $request->input('lang'));
			}

		if ($request->input('edu')) {
			$tags = array_merge($tags, $request->input('edu'));
		}

		if ($request->input('exp')) {
			$tags = array_merge($tags, $request->input('exp'));
		}

		if ($request->input('character')) {
			$tags = array_merge($tags, $request->input('character'));
		}


		$newtag = explode(',', $request->newtag);
		if(count($newtag)) {
			foreach ($newtag as $tag) {
				if (!is_numeric($tag))
				{
					$searchTag = Tag::firstOrCreate(['name' => title_case($tag),'category' => 'tag']);
					if ($searchTag)
					{
						array_push($tags, $searchTag->id);
					}
				}
				else
				{
					array_push($tags, $tag);
				}
			}
		}

		$tags = array_values(array_diff( $tags, [''] ));

		$str_tags = implode(',', $tags);
		$template = JobTemplate::create([
			'title'		=>		$request->input('title'),
			'cover_picture'		=> $image,
			'requirements'		=> strip_tags($request->input('requirements')),
			'status'		=>$request->input('status'),
			'tags'=> $str_tags
		]);


	return redirect()->back()->withSuccess( 'JobTemplate created successfully');

	}

	public function delete($id) {
		JobTemplate::findorfail($id)->delete();
		return redirect()->back();
	}

	public function status($id) {
		$temp = JobTemplate::findorfail($id);
		if($temp->status == 1){
			$temp-> update([
				'status' =>'0'
			]);
		}else {
			$temp-> update([
				'status' =>'1'
			]);
		}
	return redirect()->back();
	}

	public function edit($id) {

		$temp = JobTemplate::findorfail($id);
		$temp->tags = explode(',', $temp->tags);
		return view('admin.editTemplate')->with('temp',$temp);
	}

	public function updateAll($id,Request $request){

		/*dd($request);*/
		$temp = JobTemplate::findorfail($id);
		if($request != '') {

			if(Input::file('avatar')) {
				$image = sha1(time()) . '.png';
				$image_path = 'uploads/jobs/' . $image;
				Image::make(Input::file('avatar'))->save($image_path);

				$client = new Client();
				$file = public_path($image_path);
				$req = $client->post("https://aws.eyewil.com/api/upload/files/new",
						[
							'multipart' => [
									[
										'name'     => 'files',
										'contents' => fopen($file, 'r')
									],
									[
										'name'     => 'path',
										'contents' => 'template'
									]
								],
							'verify' => false
						]);
				$data = json_decode($req->getBody()->getContents());

				$image = $data->data->uploads[0]->cloudFrontPath;

			} else {
				$image = "/uploads/jobs/default.jpg";
			}

			$tags = array();

			if (count($request->input('cert')) > 0) {
				$tags = array_merge($tags, $request->input('cert'));
			}
			if ($request->input('character')) {
				$tags = array_merge($tags, $request->input('character'));
			}

			if ($request->input('trans')) {
				$tags = array_merge($tags, $request->input('trans'));
			}

			if ($request->input('lang')) {
					$tags = array_merge($tags, $request->input('lang'));
				}

			if ($request->input('edu')) {
				$tags = array_merge($tags, $request->input('edu'));
			}

			if ($request->input('exp')) {
				$tags = array_merge($tags, $request->input('exp'));
			}

			$newtag = explode(',', $request->newtag);
			if(count($newtag)) {
				foreach ($newtag as $tag) {
					if (!is_numeric($tag))
					{
						$searchTag = Tag::firstOrCreate(['name' => title_case($tag),'category' => 'tag']);
						if ($searchTag)
						{
							array_push($tags, $searchTag->id);
						}
					}
					else
					{
						array_push($tags, $tag);
					}
				}
			}

			$tags = array_values(array_diff( $tags, [''] ));

			$str_tags = implode(',', $tags);
			$temp-> update([
				'title'  => $request->input('title'),
			/*	'about'    => $request->input('aboutWork'),*/
				'cover_picture'     => $image,
				'requirements'  =>strip_tags($request->input('requirements')),
				'status'       =>$request->input('status'),
				'tags'=> $str_tags
			]);
			return redirect()->back()->withSuccess( 'JobTemplate updated successfully');
			/*return response()->json([
			'message' => "Job successfully updated",
			'status' => "ok"
			]);*/
		}
	}

}



