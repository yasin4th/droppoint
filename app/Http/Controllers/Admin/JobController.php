<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\Tag;
use App\Industry;
use App\CmpJob;
use App\Company;
use App\Applicants;
use Auth;
use DB;


class JobController extends Controller {

	public function __construct()
	{
		// $this->middleware('admin');
	}

	public function recent(Request $request)
	{
		// $jobs = CmpJob::where('start_date', '>=', '2016-12-12');
		$jobs = DB::table('jobs')	->join('users','jobs.user_id','=','users.id')
									->where('start_date', '>=', date('Y-m-d'))
									->whereNull('jobs.deleted_at')
									->select(DB::raw('(select count(*) from applicants where jobs.id=applicants.job_id and deleted_at is null) as applicants_count, (select count(*) from applicants where status=2 and jobs.id=applicants.job_id and deleted_at is null) as hired_count, title,type,start_date,uniform,start_time,end_time,requirements, users.name, state,cover_picture,jobs.user_id, jobs.id, salary, jobs.working_hours,country, salary_unit,vacancy'));

		if ($request->has('query')) {
			$q = $request->input('query');

			$jobs = $jobs->where(function ($query) use ($q) {
				$query->where('jobs.title','like',"%$q%")
					  ->orWhere('users.name','like',"%$q%");
			});
		}
		$jobs = $jobs->orderBy('start_date')->paginate(25);
		return $jobs;

	}

	public function users(Request $request)
	{
		$users = User::where('role', 2)->orderBy('created_at','desc');
		/*$i = Industry::get();
		dd($i);*/
		/*DB::table('users')->select('users.id','users.name','profiles.photo')->join('profiles','profiles.id','=','users.id')->where(['something' => 'something', 'otherThing' => 'otherThing'])->get();*/
		if($request->has('query')) {
			$query = $request->input('query');
			$users = $users->where('name','like',"%$query%")->orWhere('email','like',"%$query%");
		}
		else {
			$query = '';
		}
		$users = $users->paginate(25);
		/*dd($users);*/

		return view('admin.users', compact('users', 'query'));
	}

	public function jobs(Request $request)
	{
		$jobs = CmpJob::where('start_date', '<', date('Y-m-d'));

		if($request->has('query')) {
			$query = $request->input('query');
			$jobs = $jobs->where('title','like',"%$query%");
		}
		else {
			$query = '';
		}

		$jobs = $jobs->orderBy('start_date', 'asc')->paginate(25);
		return view('admin.jobs', compact('jobs', 'query'));
	}

	public function create()
	{
		$users = User::where('role', 2)->orderBy('name')->get();

		return view('admin.create')->with('users', $users);
	}

	public function store(Request $request)
	{
		if(Input::file('avatar')) {
			$image = sha1(time()) . '.png';
			$image_path = 'uploads/jobs/' . $image;
			Image::make(Input::file('avatar'))->save($image_path);
		} else {
			$image = "default.jpg";
		}

		$tags = array();

		if (count($request->input('cert')) > 0) {
			$tags = array_merge($tags, $request->input('cert'));
		}

		if ($request->input('language')) {
			array_push($tags, $request->input('language'));
		}

		if ($request->input('vehicle')) {
			array_push($tags, $request->input('vehicle'));
		}

		if (!$request->input('vacancy')) {
			$request->vacancy = 0;
		}

		$this->validate($request, [
			'title'   => 'required|max:255',
			// 'about'   => 'required',
			// 'salary'  => 'required',
			// 'vacancy' => 'required',
			'start_date'    => 'required',
			// 'working_days'  => 'required',
			// 'contact_link'  => 'required',
			// 'contact_phone' => 'required',
			// 'contact_email' => 'required|email',
		]);

		$salary = ($request->input('salary') == 'other') ? $request->input('amount') : $request->input('salary');
		$working_days = ($request->input('working') == 'other') ? $request->input('working_days') : $request->input('working');

		$job = CmpJob::create([
			'user_id'  => $request->input('user_id'),
			'type'  => $request->input('type'),
			'title'    => $request->input('title'),
			'about'    => $request->input('about'),
			'salary'   => $salary,
			'vacancy'  => $request->input('vacancy'),
			'vehicle'  => $request->input('vehicle'),
			'language' => $request->input('language'),
			'working_days'  => $working_days,
			'contact_email' => $request->input('contact_email'),
			'contact_phone' => $request->input('contact_phone'),
			'contact_link'  => $request->input('contact_link'),
			'certificates'  => ($request->input('cert')) ? implode(',', $request->input('cert')) : '',
			'start_date'    => Carbon::createFromFormat('j F, Y', $request->input('start_date'))
								->toDateTimeString(),
			'start_time' => $request->input('start_time'),
			'city'       => $request->input('loc_city'),
			'state'      => $request->input('loc_state'),
			'country'    => $request->input('loc_country'),
			'location'   => $request->input('loc_address'),
			'requirements'   => $request->input('requirements'),
			'zipcode'    => $request->input('zipcode'),
			'latitude'   => $request->input('latitude'),
			'longitude'  => $request->input('longitude'),
			'end_time'   => $request->input('end_time'),
			'status'     => '0',
			'highest_education' => $request->input('highest_education'),
			'avatar'       => $image
		]);

		if (count($tags) > 0) {
			$job->tags()->sync($tags);
		}

		return redirect()->route('admin.dashboard')->with('info', 'Job created successfully.');
	}

	public function delete($job_id)
	{ /*dd($job_id);*/
		CmpJob::findorFail($job_id)->delete();
		return redirect()->back();
	}

	public function show($id)
	{
		$job = CmpJob::findorFail($id);
		$tags = Tag::get();

		return view('admin.show', compact('id', 'job', 'tags'));
	}

	public function map()
	{
		$jobs = CmpJob::where('start_date', '>=', date('Y-m-d'))->where('status', '=', 1)->whereNotNull('latitude')->select(['title','latitude', 'longitude','salary','id', 'salary_unit', 'start_date'])->get();
		return view('admin.map')->withJobs($jobs);
	}

	public function export(Request $request)
	{
		$jobs = CmpJob::where('start_date', '<', date('Y-m-d'));

		if ($request->has('query')) {
			$jobs = $jobs->where('title', $request->input('query'));
		}

		$jobs = $jobs->orderBy('start_date', 'desc')->get();

		$header = ['S. No.', 'Title', 'About', 'Vacancy', 'Hired', 'Applicants', 'Start Date', 'Salary', 'Working Days'];
		$csv = implode(",", $header)."\n"; //Column headers
		$i = 1;
		foreach ($jobs as $job){
			$csv .= '"'. $i.'","'.$job->title.'","'.$job->about.'","'.$job->vacancy.'","'.$job->hired_count.'","'.$job->applicants_count.'","'.$job->start_date_read.'","'.$job->salary.'","'.$job->working_days.'"'."\n";
			$i++;
		}

		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=AllJobs.csv');

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
		fwrite ($output,$csv);
		fclose ($output);
	}

}