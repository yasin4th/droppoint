<?php
namespace App\Http\Controllers\Admin;

use Auth;
use Image;
use App\User;
use App\CmpJob;
use App\Company;
use Carbon\Carbon;
use App\Applicants;
use App\Models\GABadges;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\CreditsApi\GACoreController;


class CreditsController extends Controller {

    public function __construct()
    {
        // $this->middleware('admin');
    }

    public function credits($user_id)
    {
        $user = User::findOrFail($user_id);
        $achievements = $user->achievements()->paginate(25);
        $badges = GABadges::where('type', 5)->get();
        return view('admin.company.credits', compact(['user', 'achievements', 'badges']));
    }


    public function summary($user_id)
    {
        $user = User::findOrFail($user_id);
        return view('admin.company.summary')->withUser($user);
    }

    public function manage($user_id)
    {
        $user = User::findOrFail($user_id);
        return view('admin.company.manage')->withUser($user);
    }

    public function giveBadge($user_id, $badge)
    {
        $user = User::findOrFail($user_id);
        $obj = new GACoreController();
        $obj->process($user_id, $badge);
        return [
            'status' => 1,
            'message' => 'Successfully assigned credits.'
        ];
    }
}