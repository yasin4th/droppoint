<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'job_title' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'wages' => 'required',
            'dress_code' => 'required',
            'responsibilities' => 'required',
            'category' => 'required',
            'job_location' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'status' => 'required',
        ];
    }
}
