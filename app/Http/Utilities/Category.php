<?php

namespace App\Http\Utilities;

class Category
{
	protected $categories = [
		"pr" 		=> "Advertising",
		"mr" 		=> "Marketing",
		"rt" 		=> "Restaurant",
		"acc"		=> "Accounting",
		"edit"		=> "Writing/Editing",
		"tech"		=> "Technology"
	];

	public static function all () 
	{
		return (new static)->categories;
	} 
	
		
} 

	