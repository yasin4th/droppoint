<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class MustBeCompany
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && $request->user()->isCompany()) {

            return $next($request);

        }

        return redirect('/login');
       // return response()->view('company.auth.login');
    }
}
