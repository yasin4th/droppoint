<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class tb_company_profile extends Model
{
    use SoftDeletes;

     protected $guarded = ['id','user_id'];


    protected $fillable = ['user_id',
                           'company_name',
                           'company_address',
                           'company_logo',
                           'company_email',
                           'company_contactno',
                           'person_incharge_name_1',
                           'person_incharge_contactno_1',
                           'person_incharge_email_1',
                           'status',
                           'company_state',
                           'company_country',
                           'industry'

    ];

    protected $dates = ['deleted_at'];

    public function user() {
         return $this->belongsTo('App\User', 'user_id','id');
     }

}
