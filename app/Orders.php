<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{
    protected $table = 'orders';

    protected $guarded = ['id','user_id'];

    protected $fillable = [ 'plan',
                            'payment_via',
                            'amount',
                            'link',
                            'status',
                            'payment_date',

    ];

    protected $dates = ['payment_date'];

}