<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChatRoom extends Model
{
	protected $table = 'chat_rooms';

	protected $guarded = ['id'];

	protected $fillable = [
		'name',
		'user_id',
		'type',
		'status'
	];

	protected $dates = ['created_at'];

	public function user() {
		return $this->belongsTo('App\User', 'user_id', 'id');
	}

}
