<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
	use Notifiable;
	use SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'email', 'password', 'role', 'provider', 'provider_id'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	protected $appends = ['role_name','avatar'];

	protected $dates = ['deleted_at'];

	public function company()
	{
		return $this->hasOne('App\Company', 'user_id');
	}

	/*public function industry_name($industry_id)
	{
	$inin = DB::table('ref_industry_list')
         ->select(DB::raw('name'))
         ->where('id', '=', $industry_id)
         ->first();
		return $inin;
	}
*/
	public function achievements()
	{
		return $this->hasMany('App\Models\GAUserAchievements', 'userid')->orderBy('added_date', 'desc');
		//return $this->hasMany('App\Models\GAUserAchievements', 'userid');
	}

	public function received()
	{
		// return $this->hasMany('App\Models\GAUserAchievements', 'userid')->orderBy('added_date', 'desc');
		return $this->hasMany('App\Models\GAUserAchievements', 'userid')->where('description', 'like', '%received%');
	}

	public function levels()
	{
		return $this->hasone('App\Models\GAUserLevel', 'userid');
	}

	public function isAdmin()
	{
		return ($this->role == 1);
	}

	public function isCompany()
	{
		return ($this->role == 2);
	}

	public function getRoleNameAttribute()
	{
		switch($this->attributes['role'])
		{
			case '1':
				return 'Administrator';
			case '2':
				return 'Company';
		}
		return null;
	}

	public function getAvatarAttribute()
	{
		switch($this->attributes['role'])
		{
			case '1':
				return '/uploads/logo/avatar.jpg';
			case '2':
				if (!$this->company) {
					return 'default.jpg';
				}
				return $this->company->logo;
		}
		return null;
	}
}
