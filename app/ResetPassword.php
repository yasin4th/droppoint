<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Auth;


class ResetPassword extends model
{
	protected $table = 'password_resets';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [ 'email', 'token' ];

	public function setUpdatedAt($value) {}
}