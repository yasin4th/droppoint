<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Job extends Model
{
    use SoftDeletes;

    protected $table = 'jobs';

    protected $guarded = ['id','user_id'];


    protected $fillable = [ 'user_id',
                           'company_name',
                           'company_address',
                           'job_title',
                           'start_date',
                           'end_date',
                           'start_time',
                           'end_time',
                           'wages',
                           'dress_code',
                           'responsibilities',
                           'category',
                           'requirement',
                           'full_address',
                           'job_location',
                           'latitude',
                           'longitude',
                           'status'
    ];

    protected $dates = ['deleted_at'];

    // protected static function boot()
    // {
    //     parent::boot();

    //     static::creating(function ($model) {
    //         $user = Auth::user();
    //         $model->user_id = $user->id;
    //     });

    //     static::updating(function ($model) {
    //         if (Auth::check()) {
    //             $user = Auth::user();
    //             $model->user_id = $user->id;
    //         }
    //     });
    // }

    public function user() {
         return $this->belongsTo('App\User', 'user_id','id');
     }


    public function scopeUserid($query,$userid)
    {
        return $query->where('user_id',$userid);
    }


   public function scopeJobid($query,$jobid)
    {
        return $query->where('id',$jobid);
    }


}
