<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class JobTemplate extends Model
{
    use SoftDeletes;

    protected $table = 'jobTemplates';


    protected $fillable = [ 
                           'title',
                           'about',
                           'requirements',
                           'job_location',
                           'latitude',
                           'longitude',
                           'tags',
                           'cover_picture',
                           'deleted_at',
                           'status'

    ];

    protected $dates = ['deleted_at'];

}
