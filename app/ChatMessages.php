<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChatMessages extends Model
{
	protected $table = 'chat_messages';

	protected $guarded = ['id'];

	protected $fillable = [
		'room_id',
		'sender_id',
		'message',
		'type',
		'status'
	];

	protected $appends = ['human', 'sender_name'];

	protected $dates = ['created_at'];

	public function room() {
		return $this->belongsTo('App\ChatRoom', 'room_id', 'id');
	}

	public function sender() {
		return $this->belongsTo('App\User', 'sender_id', 'id');
	}

	public function getSenderNameAttribute() {
		return $this->sender->name;
	}

	public function getHumanAttribute() {
		return Carbon::parse($this->attributes['created_at'])->format('j M, H:i');
		// return Carbon::parse($this->attributes['created_at'])->diffForHumans();
	}
}