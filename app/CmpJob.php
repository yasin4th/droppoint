<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CmpJob extends Model
{
	use SoftDeletes;

	protected $table = 'jobs';

	protected $guarded = ['id','user_id'];

	protected $fillable = [ 
							'user_id',
							'type',
							'title',
							'about',
							'start_date',
							'start_time',
							'end_time',
							'working_days',
							'working_hours',
							'salary',
							'salary_unit',
							'vacancy',
							'requirements',
							'requirements_text',
							'nationality',
							'contact_email',
							'contact_phone',
							'contact_link',
							'character',
							'highest_education',
							'cover_picture',
							'certificates',
							'vehicle',
							'language',
							'category',
							'location',
							'city',
							'state',
							'street',
							'country',
							'zipcode',
							'latitude',
							'longitude',
							'status',
							'favourites',
							'uniform',
							'experience'
	];

	protected $dates = ['deleted_at', 'start_date', 'created_at'];

	protected $appends = ['username', 'hired_count', 'applicants_count', 'tag_ids', 'start_date_read', 'created_at_date', 'working_days_count', 'working_days_last'];

	public function user() {
		return $this->belongsTo('App\User', 'user_id', 'id');
	}

	public function getWorkingDaysCountAttribute() {
		return ($this->days) ? $this->days()->count() : 0;
	}

	public function getWorkingDaysLastAttribute() {
		return ($this->days()->orderBy('date','desc')->first()) ? $this->days()->orderBy('date','desc')->first()->date : '0000-00-00';
	}

	public function days() {
		return $this->hasMany('App\WorkingDays', 'job_id', 'id');
	}

	public function tags() {
		return $this->belongsToMany('App\Tag', 'job_tag', 'job_id', 'tag_id');
	}

	public function applicants() {
		return $this->hasMany('App\Applicants', 'job_id', 'id');
	}

	public function hired() {
		return $this->hasMany('App\Applicants', 'job_id', 'id')->where('status', 2);
	}

	public function archived() {
		return $this->hasMany('App\Applicants', 'job_id', 'id')->where('status', 4);
	}

	public function starred() {
		return $this->hasMany('App\Applicants', 'job_id', 'id')->where('starred', 1);
	}

	public function viewers() {
		return $this->hasMany('App\Viewers', 'job_id','id');
	}

	public function watchers() {
		return $this->hasMany('App\Watchers', 'job_id', 'id');
	}

	public function getTagIdsAttribute() {
		return $this->tags->pluck('id')->toArray();;
	}

	public function getUsernameAttribute()
	{
		return $this->user()->first()->name;
	}

	public function setTitleAttribute($value)
	{
		$this->attributes['title'] = title_case($value);
	}

	public function getHiredCountAttribute()
	{
		return count($this->hired()->get());
	}

	public function getApplicantsCountAttribute()
	{
		return count($this->applicants()->get());
	}

	public function getStartDateReadAttribute()
	{
		return Carbon::parse($this->attributes['start_date'])->format('D M j');
	}

	public function getCreatedAtDateAttribute()
	{
		return Carbon::parse($this->attributes['created_at'])->format('Y-m-d');
	}

	public function scopeIsWithinMaxDistance($query, $location, $radius = 25) {

		$haversine = "(6371 * acos(cos(radians($location->latitude))
							* cos(radians(model.latitude))
							* cos(radians(model.longitude)
							- radians($location->longitude))
							+ sin(radians($location->latitude))
							* sin(radians(model.latitude))))";

		return $query
		->select() //pick the columns you want here.
		->selectRaw("{$haversine} AS distance")
		->whereRaw("{$haversine} < ?", [$radius]);
	}

}