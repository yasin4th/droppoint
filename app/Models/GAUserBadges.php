<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class GAUserBadges extends Model
{

    protected $table = 'temp_user_badges';

    protected $primaryKey = 'userid';

    public $timestamps = false;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];



}
