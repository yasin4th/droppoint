<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplicantFeedback extends Model
{
    protected $table = 'feedback';

    protected $guarded = ['id'];

    protected $fillable = ['applicant_id',
                           'experience',
                           'feedback',
                           'compliment',
                           'ratings'
    ];

    public function applicant() {
        return $this->belongTo('App\Applicants', 'applicant_id', 'id');
    }

}