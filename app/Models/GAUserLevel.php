<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class GAUserLevel extends Model
{

    protected $table = 'temp_user_levels';

    protected $primaryKey = 'userid';

    public $timestamps = false;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];



}
