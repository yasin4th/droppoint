<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class GAUserAchievements extends Model
{

    protected $table = 'temp_user_achievements';

    public $timestamps = false;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

    // protected $dates = ['added_date'];

    protected $appends = ['transaction'];

    public function getTransactionAttribute() {
    	if(str_contains($this->attributes['description'], ['receive']))
    		return 'Credit';
    	else
    		return 'Debit';
    }
}
