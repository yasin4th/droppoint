<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    protected $table = 'tags';

    protected $guarded = ['id'];

    protected $fillable = [ 'name', 'category' ];

    public function jobs() {
        return $this->belongsToMany('App\CmpJob', 'job_tag', 'tag_id', 'job_id');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = title_case($value);
    }
}