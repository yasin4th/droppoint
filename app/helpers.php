<?php

function setActive($route, $user = 0)
{
    if ($user == 0)
        return url()->current() == route($route) ? ' active' : '';
    else
        return url()->current() == route($route, $user) ? ' active' : '';

}

function getStatusBadge($status)
{
    switch ($status) {
        case '1':
            $str = '<span class="task-cat green">Active</span>';
            break;
        case '2':
            $str = '<span class="task-cat cyan">Pending</span>';
            break;
        case '4':
            $str = '<span class="task-cat orange">Inactive</span>';
            break;
        default:
            $str = '';
            break;
    }
    return $str;
}

function http_url($url)
{
    $url = preg_replace('#^https?://#', '', $url);
    $url = "http://" . $url;
    return $url;
}
