var app = require('express')()
var http = require('http').Server(app)
// var server = http.createServer(app)
var io = require('socket.io')(http)
var cors = require('cors')
var mysql = require('mysql')
var fs = require('fs'),
configPath = './config.json';
var parsed = JSON.parse(fs.readFileSync(configPath, 'UTF-8'));

app.use(cors())

// First you need to create a connection to the db
var con = mysql.createConnection({
	host: parsed.host,
	user: parsed.user,
	password: parsed.password,
	database: parsed.database
})

con.connect(function(err) {
	if(err) {
		console.log('Error connecting to Db')
		return;
	}
	console.log('Connection established to Db')
})

io.on('connection', function(socket){
	socket.emit('greet', 'Connected')
	socket.on('send', function (data) {
		dt = getDateTime();

		con.query("INSERT INTO chat_messages (message,sender_id,user_id,created_at,updated_at) VALUES ('"+data.message+"','"+data.sender_id+"','"+data.user_id+"','"+dt+"','"+dt+"')", function(err, result) {
			if(!err) {
				io.emit('received',{data: data, result: result});
			}
		})
	})
})

http.listen(3000, function () {
	console.log('Chat app listening on port 3000!')
})

getDateTime = function() {
	d = new Date();

	var fromdate = new Date();
	var dd = fromdate.getDate();
	var mm = fromdate.getUTCMonth()+1; //January is 0!
	var yyyy = fromdate.getUTCFullYear();
	var hh = fromdate.getUTCHours();
	var ii = fromdate.getUTCMinutes();
	var ss = fromdate.getUTCSeconds();

	if(dd < 10)
	{
		dd = '0'+ dd;
	}
	if(mm < 10)
	{
		mm = '0' + mm;
	}
	if(hh < 10)
	{
		hh = '0' + hh;
	}
	if(ii < 10)
	{
		i = '0' + ii;
	}
	if(ss < 10)
	{
		ss = '0' + ss;
	}
	return yyyy+'-'+mm+'-'+dd+' '+hh+':'+ii+':'+ss;
}
