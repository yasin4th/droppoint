 <?php
 ini_set("memory_limit", "-1");
set_time_limit(0);

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
	return view('welcome');
})->middleware('guest');

Route::get('/admin', function () {
	return view('welcome');
})->middleware('guest');

Route::group(['middleware' => 'auth'], function () {
	Route::get('/job/{id}/previewAdmin', 'JobController@previewAdmin');
	Route::get('/dashboard', 'HomeController@index')->name('dashboard');
	Route::post('/job/{id}/update/{prop}', 'JobController@update')->name('job.update');
	Route::get('/job/export', 'JobController@export')->name('job.export');
	Route::get('/job/active', 'JobController@active');

	Route::post('/job/{id}/updateAll', 'JobController@updateAll')->name('job.updateAll');
	Route::post('/job/{id}/update-avatar', 'JobController@updateAvatar')->name('job.updateAvatar');

	Route::post('/applicants/update', 'JobController@updateApplicantField');
	Route::get('/job/{id}/applicants', 'JobController@applicants');
	Route::get('/job/selectJob/{id}', 'JobController@selectJob');
	Route::post('/job/{id}/postAddress', 'JobController@postAddress');
	Route::get('/job/allTitles', 'JobController@allTitles');
	Route::resource('job', 'JobController');

	Route::get('/messages/{room_id}/{id}', 'ChatController@messages');
	Route::get('/boards/{user_id}', 'ChatController@boards');
	Route::get('/admins', 'ChatController@admins');
	Route::post('/upload', 'ChatController@upload');

	Route::get('/chat', function() {
		return view('admin.chat');
	})->name('chat');

});



Route::get('mail', 'HomeController@mail');

Route::any ( 'sendemail', function () {
	if (Request::get ( 'message' ) != null)
		$data = array (
				'bodyMessage' => Request::get ( 'message' )
		);
	else
		$data [] = '';
	Mail::send ( 'email', $data, function ($message) {

		$message->from ( 'donotreply@demo.com', 'Just Laravel' );

		$message->to ( Request::get ( 'toEmail' ) )->subject ( 'Just Laravel demo email using SendGrid' );
	} );
	return Redirect::back ()->withErrors ( [
			'Your email has been sent successfully'
	] );
});



Route::group(['namespace' => 'Auth', 'middleware' => 'web'], function () {
	Route::get('/auth/{provider}', 'AuthController@getProvider');
	Route::get('/auth/{provider}/callback', 'AuthController@handleProviderCallback');
});


Route::group(['namespace' => 'Auth'], function () {

	Route::get('/login', 'AuthController@getLogin')->name('auth.login');
	Route::post('/login', 'AuthController@postLogin');

	Route::get('/register', 'AuthController@getRegister')->name('auth.register');
	Route::post('/register', 'AuthController@postRegister');

	Route::get('/reset', 'AuthController@getReset')->name('auth.reset');
	Route::post('/reset', 'AuthController@postReset');
	Route::get('/forgot/{token}', 'AuthController@getForgot')->name('auth.forgot');
	Route::post('/forgot/{token}', 'AuthController@postForgot');
});

Route::group(['namespace' => 'Admin', 'middleware' => 'auth'], function () {
	Route::get('/admin/notification/read', 'NotificationController@read')->name('notification.read');
});

Route::group(['namespace' => 'Auth'], function () {
	Route::get('/logout', 'AuthController@getlogout')->middleware('auth')->name('auth.logout');

});

Route::group(['namespace' => 'Company', 'middleware' => 'company'], function () {
	Route::get('/cmp', 'CompanyController@getIndex')->name('cmp.home');

	Route::get('/cmp/profile/view', 'ProfileController@getProfileView')->name('cmp.profile.view');
	Route::get('/cmp/profile', 'ProfileController@getProfile')->name('cmp.profile');
	Route::get('/cmp/profile/logo', function() {
		return view('company.logo');
	})->name('cmp.profile.logo');
	Route::post('/cmp/profile/logo', 'ProfileController@postLogo');
	Route::post('/cmp/profile/state','ProfileController@State');//getting state
	Route::post('/cmp/profile', 'ProfileController@postProfile');
	Route::get('/cmp/profile/reset', 'ProfileController@getReset')->name('cmp.profile.reset');
	Route::post('/cmp/profile/reset', 'CompanyController@postReset');

	Route::get('/cmp/job/{job_id}/delete', 'JobController@delete')->name('job.delete');
	Route::get('/cmp/job/all', 'JobController@getAll')->name('job.all');
	Route::get('/cmp/job/{job_id}/copy', 'JobController@copy')->name('job.copy');
	Route::any('/cmp/job/recent', 'JobController@recent')->name('job.recent');
	Route::get('/cmp/job/progress', 'JobController@progress')->name('job.progress');
	Route::get('/cmp/job/{job_id}/preview', 'JobController@preview')->name('job.preview');
	// Route::resource('/cmp/job', 'JobController');
	Route::resource('/cmp/applicants', 'ApplicantController');

	Route::get('/cmp/applicants/{id}/destroy', 'ApplicantController@destroy')->name('applicants.delete');
	Route::get('/cmp/applicants/{job_id}/exportHired', 'ApplicantController@exportHired')->name('applicants.exportHired');
	// Route::get('/cmp/job/all/export', 'JobController@export')->name('job.export');

	Route::get('/cmp/credits/summary', 'CreditsController@summary')->name('cmp.credits.summary');
	Route::post('/cmp/credits/summary', 'CreditsController@coupon');
	Route::get('/cmp/credits', 'CreditsController@credits')->name('cmp.credits');
	Route::get('/cmp/payments', 'PaymentsController@payments')->name('cmp.payments');
	Route::get('/cmp/package', 'PackagesController@getPurchase')->name('cmp.package');
	Route::post('/cmp/package', 'PackagesController@postPurchase');
	Route::post('/cmp/gst', 'PackagesController@saveGst');
	Route::get('/cmp/redirect', 'PackagesController@getRedirect');
	Route::get('/payment/success', 'PackagesController@success');
	Route::get('/payment/failed', 'PackagesController@failed');
});

Route::group(['namespace' => 'Company', 'middleware' => 'auth'], function () {
	Route::get('/cmp/applicants/{id}/status/{status}', 'ApplicantController@status')->name('applicants.status');
	Route::get('/cmp/applicants/{id}/starred/{starred}', 'ApplicantController@starred')->name('applicants.starred');
	Route::post('/cmp/applicants/{id}/feedback', 'ApplicantController@feedback')->name('applicants.feedback');
});

Route::group(['namespace' => 'Company'], function () {
	Route::resource('/country', 'CountryController', ['except' => ['create', 'store', 'edit', 'update', 'destroy']]);
	Route::get('/cmp/job/{job_id}/hired', 'JobController@hired')->name('job.hired');
	Route::post('/cmp/job/{id}/update/{prop}', 'JobController@update')->name('job.update');
	Route::post('/cmp/job/{job_id}/applicants', 'JobController@applicants')->name('job.applicants');
});

Route::group(['namespace' => 'Admin', 'middleware' => 'admin'], function () {
	Route::get('/admin/dashboard', 'AdminController@dashboard')->name('admin.dashboard');
	Route::get('/admin/users', 'JobController@users')->name('admin.users');
	Route::get('/admin/users/create', 'CompanyController@create')->name('admin.users.create');
	Route::post('/admin/users/register', 'CompanyController@register')->name('admin.users.register');
	Route::get('/admin/users/{user_id}/edit', 'CompanyController@edit')->name('admin.users.edit');
	Route::get('/admin/users/{user_id}/logo', 'CompanyController@logo')->name('admin.users.logo');
	Route::get('/admin/users/{user_id}/reset', 'CompanyController@reset')->name('admin.users.reset');
	Route::post('/admin/users/{user_id}/logo', 'CompanyController@postLogo');
	Route::post('/admin/users/{user_id}/reset', 'CompanyController@postReset');
	Route::post('/admin/users/{user_id}/update', 'CompanyController@update')->name('admin.users.update');
	Route::get('/admin/users/{user_id}/status/{status}', 'CompanyController@status')->name('admin.users.status');
	Route::get('/admin/users/{user_id}/delete', 'CompanyController@delete')->name('admin.users.delete');
	Route::get('/admin/users/{user_id}/credits', 'CreditsController@credits')->name('admin.users.credits');
	Route::get('/admin/users/{user_id}/credits/{badge}', 'CreditsController@giveBadge')->name('admin.users.badge');
	Route::get('/admin/users/{user_id}/credits/manage', 'CreditsController@manage')->name('admin.users.credits.manage');
	Route::get('/admin/users/{user_id}/summary', 'CreditsController@summary')->name('admin.users.summary');
	Route::get('/admin/jobs', 'JobController@jobs')->name('admin.jobs');
	Route::get('/admin/jobs/recent', 'JobController@recent')->name('admin.jobs.recent');
	// Route::get('/admin/jobs/export', 'JobController@export')->name('admin.jobs.export');
	// Route::get('/admin/jobs/create', 'JobController@create')->name('admin.jobs.create');
	// Route::post('/admin/jobs/store', 'JobController@store')->name('admin.jobs.store');
	Route::get('/admin/jobs/{job_id}/delete', 'JobController@delete')->name('admin.jobs.delete');
	Route::get('/admin/jobs/{id}', 'JobController@show')->name('admin.jobs.show');
	Route::get('/admin/map', 'JobController@map')->name('admin.map');

	Route::get('/admin/report/jobs', 'ReportsController@jobs')->name('report.jobs');
	Route::get('/admin/report/users', 'ReportsController@users')->name('report.users');
	Route::get('/admin/report/location', 'ReportsController@location')->name('report.location');
	Route::get('/admin/report/tags', 'ReportsController@tags')->name('report.tags');
	Route::get('/admin/report/industry', 'ReportsController@industry')->name('report.industry');
	Route::get('/admin/template', 'TemplateController@getTemplate')->name('admin.template');
	Route::post('/admin/template', 'TemplateController@postTemplate');
	Route::get('/admin/template/create', 'TemplateController@getCreate')->name('admin.template.create');
	Route::post('/admin/template/create', 'TemplateController@postCreate');
	Route::get('/admin/template/{id}/delete/', 'TemplateController@delete');
	Route::get('/admin/template/{id}/status', 'TemplateController@status');
	Route::get('/admin/template/{id}/edit', 'TemplateController@edit');
	Route::post('/admin/template/{id}/updateAll', 'TemplateController@updateAll');

	Route::resource('/admin/notification', 'NotificationController');

});

Route::group(['namespace' => 'Company', 'middleware' => 'api'], function () {
	Route::post('/payment/listener', 'PackagesController@listener');
});

Route::group(['namespace' => 'Credits', 'prefix' => 'admin', 'middleware' => 'admin'], function () {
	Route::get('credits/', 'HomeController@manage')->name('admin.credits');
	Route::get('credits/home', 'HomeController@manage')->name('admin.credits.home');
	Route::get('credits/manage', 'HomeController@manage')->name('admin.credits.manage');
	Route::get('credits/simulate', 'HomeController@simulate')->name('admin.credits.simulate');
	Route::get('credits/display', 'HomeController@display')->name('admin.credits.display');
});


Route::group(['prefix' => 'api', 'after' => 'allowOrigin', 'namespace' => 'CreditsApi'], function() {
	Route::pattern('id', '[0-9]+');
	Route::pattern('id2', '[0-9]+');

	#Gamify
	#i: GA Categories
	Route::post('gacategories/load', 'GACategoriesController@load');
	Route::post('gacategories/proc', 'GACategoriesController@proc');
	Route::post('gacategories/getinfo', 'GACategoriesController@getinfo');
	Route::post('gacategories/remove', 'GACategoriesController@remove');

	#i: GA Badges
	Route::post('gabadges/load', 'GABadgesController@load');
	Route::post('gabadges/proc', 'GABadgesController@proc');
	Route::post('gabadges/getinfo', 'GABadgesController@getinfo');
	Route::post('gabadges/remove', 'GABadgesController@remove');
	Route::post('gabadges/updatethumb', 'GABadgesController@updatethumb');
	Route::post('gabadges/removethumb', 'GABadgesController@removethumb');
	Route::post('gabadges/upload', 'GABadgesController@upload');
	Route::post('gabadges/getmax', 'GABadgesController@getmax');

	#i: GA Badge Events
	Route::post('gabadgeevents/proc', 'GABadgeEventsController@proc');

	#i: GA Events
	Route::post('gaevents/load', 'GAEventsController@load');
	Route::post('gaevents/proc', 'GAEventsController@proc');
	Route::post('gaevents/getinfo', 'GAEventsController@getinfo');
	Route::post('gaevents/remove', 'GAEventsController@remove');

	#i: GA Level Associate
	Route::post('galevelassociate/load', 'GALevelAssociateController@load');
	Route::post('galevelassociate/proc', 'GALevelAssociateController@proc');
	Route::post('galevelassociate/getinfo', 'GALevelAssociateController@getinfo');

	  #i: GA User Badges
	Route::post('gauserbadges/load', 'GAUserBadgesController@load');
	Route::post('gauserbadges/proc', 'GAUserBadgesController@proc');
	Route::post('gauserbadges/getinfo', 'GAUserBadgesController@getinfo');
	#i: GA Core BLL
	Route::post('gacore/award', 'GACoreController@award');
	Route::post('gacore/loaduserlevels', 'GACoreController@loaduserlevels');
	Route::post('gacore/loaduserinfo', 'GACoreController@loaduserinfo');
	Route::post('gacore/loadachievements', 'GACoreController@loadachievements');

});
Route::group(['namespace' => 'Credits', 'prefix' => 'admin'], function () {
	Route::post('credits/', 'HomeController@upload');
});
Route::get('/pwapp/jobs/{user_id}', 'ServiceController@get');
Route::post('/pwapp/user/{user_id}/create', 'ServiceController@post');