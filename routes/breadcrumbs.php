<?php


Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Dashboard', route('cmp.home'));
});

Breadcrumbs::register('profile', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Profile', route('cmp.profile'));
});

Breadcrumbs::register('logo', function($breadcrumbs)
{
    $breadcrumbs->parent('profile');
    $breadcrumbs->push('Logo');
});

Breadcrumbs::register('job', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Add Job');
});
