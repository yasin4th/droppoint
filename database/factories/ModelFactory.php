<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Job::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'job_title' => $faker->streetName,
        'start_date' => $faker->date($format = 'Y-m-d', $max = '2017-06-09'),
        'end_date' => $faker->date($format = 'Y-m-d', $max = '2017-06-09'),
        'start_time' => $faker->numberBetween($min = 1000, $max = 9000),
        'end_time' => $faker->numberBetween($min = 1000, $max = 9000),
        'wages' => $faker->numberBetween($min = 1000, $max = 9000),
        'dress_code' => $faker->sentence(1),
        'responsibilities' => $faker->paragraph(1),
        'category' => $faker->word(1),
        'job_location' => $faker->streetName,
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
        'status' => $faker->randomDigit,
    ];
});