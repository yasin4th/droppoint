<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJobLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cmp_jobs', function (Blueprint $table) {
           $table->string('zipcode')->nullable()->after('location');
           $table->string('country')->nullable()->after('location');
           $table->string('state')->nullable()->after('location');
           $table->string('city')->nullable()->after('location');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cmp_jobs', function ($table) {
            $table->dropColumn(['zipcode', 'country', 'state', 'city']);
        });
    }
}
