<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditCmpJobsSalary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cmp_jobs', function (Blueprint $table) {
            $table->string('character')->nullable()->after('contact_link');
            $table->renameColumn('avatar', 'cover_picture');
            $table->string('salary_unit', 45)->nullable()->after('salary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cmp_jobs', function (Blueprint $table) {
            $table->dropColumn('character');
            $table->renameColumn('cover_picture' ,'avatar');
            $table->dropColumn('salary_unit');
        });
    }
}
