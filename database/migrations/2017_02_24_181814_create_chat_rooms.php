<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatRooms extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('chat_rooms', function (Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->integer('user_id')->nullable();
			$table->string('name')->nullable();
			$table->text('description')->nullable();
			$table->string('type')->default('query');
			$table->string('status')->default('active');
			$table->timestamps();
		});

		Schema::table('chat_messages', function(Blueprint $table) {
	        $table->dropForeign('chat_messages_user_id_foreign');
			$table->dropColumn(['user_id']);
			$table->integer('room_id')->nullable()->after('id');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('chat_rooms');
	}
}
