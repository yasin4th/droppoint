<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewers extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('viewers', function (Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->integer('job_id')->unsigned()->index();
			$table->string('name')->nullable();
			$table->string('email')->nullable();
			$table->foreign('job_id')->references('id')->on('cmp_jobs')->onDelete('cascade');
			$table->timestamp('deleted_at')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('viewers');
	}
}
