<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditCmpJob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('cmp_jobs', function (Blueprint $table) {
            $table->string('experience')->nullable()->after('contact_link');
      });
      Schema::table('tags', function (Blueprint $table) {
            $table->string('category')->nullable()->after('name');
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('cmp_jobs', function (Blueprint $table) {
            $table->dropColumn('experience');
        });
      Schema::table('tags', function (Blueprint $table) {
            $table->dropColumn('category');
        });
    }
}
