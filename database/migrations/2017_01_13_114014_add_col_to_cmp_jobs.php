<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToCmpJobs extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cmp_jobs', function (Blueprint $table) {
			$table->string('highest_education')->nullable()->after('status');
			$table->string('avatar')->default('default.jpg')->nullable()->after('highest_education');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cmp_jobs', function (Blueprint $table) {
			$table->dropColumn('avatar');
			$table->dropColumn('highest_education');
		});
	}
}
