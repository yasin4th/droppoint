<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TemplateJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobTemplates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('job_title');
            $table->string('about')->nullable();
            $table->text('requirements')->nullable();
            $table->integer('status')->nullable();
            $table->string('tags')->nullable();
            $table->string('cover_picture')->default('avatar.jpg');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobTemplates');
    }
}
