<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyProfileAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
      public function up()
    {
          Schema::table('tb_company_profiles', function (Blueprint $table) {
           $table->string('company_address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
