<?php

use Illuminate\Database\Seeder;
use App\ChatMessages;

class ChatMessagesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		$faker = Faker\Factory::create();

		ChatMessages::truncate();

		foreach(range(1,100) as $index)
		{
			ChatMessages::create([
				'user_id' => '1',
				'sender_id' => '2',
				'message' => $faker->text,
				'type' => 'text',
				'status' => 1,
				'created_at' => $faker->dateTime($max = 'now'),
				'updated_at' => $faker->dateTime($max = 'now'),
			]);

			ChatMessages::create([
				'user_id' => '2',
				'sender_id' => '1',
				'message' => $faker->text,
				'type' => 'text',
				'status' => 1,
				'created_at' => $faker->dateTime($max = 'now'),
				'updated_at' => $faker->dateTime($max = 'now'),
			]);
		}
	}
}