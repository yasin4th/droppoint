<?php

use Illuminate\Database\Seeder;
use App\CmpJob;

class ApplicantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();

        // Applicants::truncate();

        foreach(range(1,150000) as $index)
        {
            CmpJob::create([
                'title' => $faker->name,
                'start_date' => '2017-06-10',
                'vacancy' => 10,
                'user_id' => 2,
                'status' => '1',
                'created_at' => $faker->dateTime($max = 'now'),
                'updated_at' => $faker->dateTime($max = 'now'),
            ]);
        }
    }
}
