<!DOCTYPE html>
<html>
<body>
<div text="#000000" style="background-color:#ffffff;width:100%!important">
	<div>
		<table style="font-size:11px;line-height:14px;color:#666666;background-color: #f5f5f5;font-family:Helvetica,Arial,sans-serif" border="0" cellspacing="0" cellpadding="0" width="100%" align="center" bgcolor="#e4e4e4">
			<tbody>
				<tr>
					<td>
						<table style="font-size:11px;line-height:14px;color:#666666" border="0" cellspacing="0" cellpadding="0" width="648" align="center">
							<tbody>
								<tr>
									<td style="line-height:0;font-size:0" height="47">&nbsp;</td>
								</tr>
							</tbody>
						</table>
						<table style="font-size:13px;line-height:18px;background: transparent;color:#666666;border-radius:4px;" border="0" cellspacing="0" cellpadding="0" width="648" align="center" bgcolor="#ffffff">
							<tbody>
								<tr>
									<td>
										<table style="font-size:14px;line-height:18px;color:#666666" border="0" cellspacing="0" cellpadding="0" width="648" align="center">
											<tbody>
												<tr>
													<td width="40">&nbsp;</td>
													<td align="center">
														<p>
															<a href="#" target="_blank"><img style="border:0px;" src="https://beta.eyewil.com/images/logo.png" alt="DropPoint"></a>
														</p>
													</td>
													<td width="40">&nbsp;</td>
												</tr>
												<tr>
													<td style="line-height:0;font-size:0" colspan="3" height="20">&nbsp;</td>
												</tr>
												<tr>
													<td width="40">&nbsp;</td>
													<td>
														<p>Hi {{$user->name}},</p>
														<p>To complete the process of changing your email address, you must confirm your address below</p>
													</td>
													<td width="40">&nbsp;</td>
												</tr>
												<tr>
													<td style="line-height:0;font-size:0" colspan="3" height="5">&nbsp;</td>
												</tr>
												<tr>
													<td width="40">&nbsp;</td>
													<td style="color: #fff;border-radius:4px" align="center" bgcolor="#ff5a5f">
														<a href="#" style="color: #fff; text-decoration: none;">
															<div style="padding: 12px;">
																<p style="margin: 0px;"><strong>Confirm Email</strong></p>
															</div>
														</a>
													</td>
													<td width="40">&nbsp;</td>
												</tr>
												<tr>
													<td style="line-height:0;font-size:0" colspan="3" height="10">&nbsp;</td>
												</tr>
												<tr>
													<td width="40">&nbsp;</td>
													<td>
														<p>Thanks,</p>
														<p>The DropPoint Team</p>
													</td>
													<td width="40">&nbsp;</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
						<table style="font-size:13px;line-height:18px;color:#666666" border="0" cellspacing="0" cellpadding="0" width="650" align="center">
							<tbody>
								<tr>
									<td colspan="3" height="44">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="3" align="center">
										<table border="0" cellspacing="0" cellpadding="0" width="200">
											<tbody>
												<tr>
													<td width="50">
														<a style="display: inline-block;padding: 10px;border: 2px solid #888;border-radius: 50%;" href="#" target="_blank"><img style="width: 20px;display:block" src="https://beta.eyewil.com/images/facebook.png" border="0" alt="" class="CToWUd"></a>
													</td>
													<td width="50">
														<a style="display: inline-block;padding: 10px;border: 2px solid #888;border-radius: 50%;" href="#" target="_blank"><img style="width: 20px;display:block" src="https://beta.eyewil.com/images/twitter.png" border="0" alt="" class="CToWUd"></a>
													</td>
													<td width="50">
														<a style="display: inline-block;padding: 10px;border: 2px solid #888;border-radius: 50%;" href="#" target="_blank"><img style="width: 20px;display:block" src="https://beta.eyewil.com/images/instagram.png" border="0" alt="" class="CToWUd"></a>
													</td>
													<td width="50">
														<a style="display: inline-block;padding: 10px;border: 2px solid #888;border-radius: 50%;" href="#" target="_blank"><img style="width: 20px;display:block" src="https://beta.eyewil.com/images/pinterest.png" border="0" alt="" class="CToWUd"></a>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td style="line-height:0;font-size:0" colspan="3" height="18">&nbsp;</td>
								</tr>
								<tr>
									<td style="color:#777;padding:0 30px" colspan="3" align="center">
										<p style="margin: 0px;">The DropPoint</p>
										<p style="margin-top: 0px;"><a href="#" style="color: #777;">Email preferences</a></p>
									</td>
								</tr>
								<tr>
									<td style="line-height:0;font-size:0" colspan="3" height="40">&nbsp;</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
</body>
</html>