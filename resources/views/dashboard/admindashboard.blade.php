@extends('layouts.materialize')
@section('content')

<!--card stats start-->
<div id="card-stats">
    <div class="row">
        <div class="col s12 m6 l3">
            <div class="card">
                <div class="card-content  purple darken-2 white-text">
                    <p class="card-stats-title"><i class="mdi-content-add-circle"></i>  New Company</p>
                    <h4 class="card-stats-number">{{$CompanyToday}}</h4>
                </div>
            </div>
        </div>
        <div class="col s12 m6 l3">
            <div class="card">
                <div class="card-content teal white-text">
                    <p class="card-stats-title"><i class="mdi-communication-business"></i> Total Company</p>
                    <h4 class="card-stats-number">{{$Company}}</h4>
                </div>
            </div>
        </div>
        <div class="col s12 m6 l3">
            <div class="card">
                <div class="card-content purple darken-2 white-text">
                    <p class="card-stats-title"><i class="mdi-action-trending-up"></i> New Jobs</p>
                    <h4 class="card-stats-number">{{$jobsToday}}</h4>
                </div>
            </div>
        </div>
        <div class="col s12 m6 l3">
            <div class="card">
                <div class="card-content teal white-text">
                    <p class="card-stats-title"><i class="mdi-action-wallet-travel"></i> Total Jobs</p>
                    <h4 class="card-stats-number">{{$jobs}}</h4>
                </div>
            </div>
        </div>
    </div>
</div>
<!--card stats end-->

<!-- //////////////////////////////////////////////////////////////////////////// -->

<!--work collections start-->
<div id="work-collections">
    <div class="row">
        <div class="col s12 m12 l6">
            <ul id="projects-collection" class="collection">
                <li class="collection-item avatar">
                    <i class="mdi-action-wallet-travel circle purple"></i>
                    <span class="collection-header">Jobs</span>
                    <p>Jobs Industry</p>
                    <!-- <a href="#" class="secondary-content"><i class="mdi-action-grade"></i></a> -->
                </li>
                <li class="collection-item">
                    <div class="row">
                        <div class="col s6">
                            <p class="collections-title" style="font-size:16px"><b>Industry</b></p>
                        </div>
                        <div class="col s3">
                            <p class="collections-title" style="font-size:16px"><b>Job Today</b></p>
                        </div>
                        <div class="col s3">
                            <p class="collections-title" style="font-size:16px"><b>Job Active</b></p>
                        </div>
                    </div>
                </li>
                  <li class="collection-item">
                    <div class="row">
                        <div class="col s6">
                            <p class="collections-title">Accounting</p>
                        </div>                
                        <div class="col s3">
                            <span class="task-cat black">123</span>
                        </div>
                         <div class="col s3">
                            <span class="task-cat black">123</span>
                        </div>
                    </div>
                </li>
                <li class="collection-item">
                    <div class="row">
                        <div class="col s6">
                            <p class="collections-title">Animation</p>
                        </div>
                        <div class="col s3">
                            <span class="task-cat black">12</span>
                        </div>
                        <div class="col s3">
                            <span class="task-cat black">12</span>
                        </div>
                    </div>
                </li>
                <li class="collection-item">
                    <div class="row">
                        <div class="col s6">
                            <p class="collections-title">Automative</p>
                        </div>
                         <div class="col s3">
                            <span class="task-cat black">22</span>
                        </div>
                        <div class="col s3">
                            <span class="task-cat black">22</span>
                        </div>
                    </div>
                </li>
                <li class="collection-item">
                    <div class="row">
                        <div class="col s6">
                            <p class="collections-title">Banking And Financing</p>
                        </div>
                        <div class="col s3">
                            <span class="task-cat black">12</span>
                        </div>
                         <div class="col s3">
                            <span class="task-cat black">12</span>
                        </div>
                    </div>
                </li>
                  <li class="collection-item">
                    <div class="row">
                        <div class="col s6">
                            <p class="collections-title">Wholesale</p>
                        </div>
                        <div class="col s3">
                            <span class="task-cat black">29</span>
                        </div>
                        <div class="col s3">
                            <span class="task-cat black">29</span>
                        </div>
                    </div>
                </li>
                 <li class="collection-item">
                    <div class="row">
                        <div class="col s6">
                            <p class="collections-title">Writing and Editing</p>
                        </div>
                        <div class="col s3">
                            <span class="task-cat black">29</span>
                        </div>
                        <div class="col s3">
                            <span class="task-cat black">29</span>
                        </div>
                    </div>
                </li>
                 <li class="collection-item">
                    <div class="row">
                        <div class="col s6">
                            <p class="collections-title">Technology</p>
                        </div>
                        <div class="col s3">
                            <span class="task-cat black">29</span>
                        </div>
                        <div class="col s3">
                            <span class="task-cat black">29</span>
                        </div>
                    </div>
            </ul>
        </div>
        <div class="col s12 m12 l6">
            <ul id="issues-collection" class="collection">
                <li class="collection-item avatar">
                    <i class="mdi-communication-business circle teal"></i>
                    <span class="collection-header"> Company</span>
                     <p><span class="task-cat pink">New</span> Register Company</p>
                  <!--   <a href="#" class="secondary-content"><i class="mdi-action-grade"></i></a> -->
                </li>
                 <li class="collection-item">
                    <div class="row">
                        <div class="col s6">
                            <p class="collections-title" style="font-size:16px"><b>Company</b></p>
                        </div>
                        <div class="col s3">
                            <p class="collections-title" style="font-size:16px"><b>Job Post</b></p>
                        </div>
                        <div class="col s3">
                            <p class="collections-title" style="font-size:16px"><b>Job Token</b></p>
                        </div>
                    </div>
                </li>
                @foreach($IndexCompany as $Company)
                <li class="collection-item">
                    <div class="row">
                        <div class="col s6">
                            <p class="collections-title">{{$Company->name}}</p>
                        </div>
                       <div class="col s3">
                            <span class="task-cat  pink lighten-1">10</span>
                        </div>
                        <div class="col s3">
                             <span class="task-cat deep-purple accent-2">100</span>                                              
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
<!--work collections end-->

<!-- Floating Action Button 
<div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
    <a class="btn-floating btn-large">
      <i class="mdi-action-stars"></i>
    </a>
    <ul>
      <li><a href="css-helpers.html" class="btn-floating red"><i class="large mdi-communication-live-help"></i></a></li>
      <li><a href="app-widget.html" class="btn-floating yellow darken-1"><i class="large mdi-device-now-widgets"></i></a></li>
      <li><a href="app-calendar.html" class="btn-floating green"><i class="large mdi-editor-insert-invitation"></i></a></li>
      <li><a href="app-email.html" class="btn-floating blue"><i class="large mdi-communication-email"></i></a></li>
    </ul>
</div>
Floating Action Button -->


@endsection
