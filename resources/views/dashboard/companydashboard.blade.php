
                    <!--card stats start-->
                    <div id="card-stats">
                        <div class="row">
                            <div class="col s12 m6 l3">
                                <div class="card">
                                    <div class="card-content  blue darken-2 white-text">
                                        <p class="card-stats-title"><i class="mdi-content-add-circle"></i>  Pending Jobs</p>
                                        <h4 class="card-stats-number">0</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m6 l3">
                                <div class="card">
                                    <div class="card-content pink lighten-1 white-text">
                                        <p class="card-stats-title"><i class="mdi-communication-business"></i> Actived Job</p>
                                        <h4 class="card-stats-number">10</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m6 l3">
                                <div class="card">
                                    <div class="card-content teal white-text">
                                        <p class="card-stats-title"><i class="mdi-action-trending-up"></i> Expired Jobs</p>
                                        <h4 class="card-stats-number">2</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m6 l3">
                                <div class="card">
                                    <div class="card-content purple white-text">
                                        <p class="card-stats-title"><i class="mdi-action-stars"></i> Token Left</p>
                                        <h4 class="card-stats-number">12</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--card stats end-->

                    <!-- //////////////////////////////////////////////////////////////////////////// -->

                    <!--work collections start-->
                    <div id="work-collections">
                        <div class="row">
                            <div class="col s12 m12 l6">
                                <ul id="issues-collection" class="collection">
                                    <li class="collection-item avatar">
                                        <i class="mdi-action-wallet-travel circle deep-purple darken-4"></i>
                                        <span class="collection-header"> Job of the Days<!-- <span class="new badge" data-badge-caption="Total">1</span> --></span>
                                         {{-- <p>{{$today}}</p> --}}
                                      <!--   <a href="#" class="secondary-content"><i class="mdi-action-grade"></i></a> -->
                                    </li>
                                     <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title" style="font-size:16px"><b>Job Title</b></p>
                                            </div>
                                            <div class="col s3">
                                                <p class="collections-title" style="font-size:16px"><b>Location</b></p>
                                            </div>
                                            <div class="col s3">
                                                <p class="collections-title" style="font-size:16px"><b>Start Time</b></p>
                                            </div>
                                        </div>
                                    </li>
                                @if(count($companyJobs)>0)
                                 @foreach($companyJobs as $companyJob)
                                    <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title">qwe</p>
                                            </div>
                                           <div class="col s3">
                                                <span class="task-cat  pink lighten-1">{{$companyJob->job_location}}</span>
                                            </div>
                                            <div class="col s3">
                                                 <span class="task-cat deep-purple accent-2">{{$companyJob->start_time}}</span>
                                            </div>
                                        </div>
                                    </li>
                                      @endforeach
                                    @else
                                     <li class="collection-item">
                                        <div class="row">
                                            <div class="col s12">
                                                <p class="collections-title" style="text-align:center">There is no job for today</p>
                                            </div>
                                        </div>
                                    </li>
                                    @endif

                                </ul>
                            </div>

                            <div class="col s12 m12 l6">
                                <ul id="projects-collection" class="collection">
                                    <li class="collection-item avatar">
                                        <i class="mdi-action-wallet-travel circle purple"></i>
                                        <span class="collection-header">Jobs Post</span>
                                        <p>Jobs Post Industry</p>
                                        <!-- <a href="#" class="secondary-content"><i class="mdi-action-grade"></i></a> -->
                                    </li>
                                    <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title" style="font-size:16px"><b>Industry</b></p>
                                            </div>
                                            <div class="col s3">
                                                <p class="collections-title" style="font-size:16px"><b>Job Active</b></p>
                                            </div>
                                            <div class="col s3">
                                                <p class="collections-title" style="font-size:16px"><b>Job Expired</b></p>
                                            </div>
                                        </div>
                                    </li>
                                      <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title">Accounting</p>
                                            </div>
                                            <div class="col s3">
                                                <span class="task-cat red">123</span>
                                            </div>
                                             <div class="col s3">
                                                <span class="task-cat red">123</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title">Animation</p>
                                            </div>
                                            <div class="col s3">
                                                <span class="task-cat pink">12</span>
                                            </div>
                                            <div class="col s3">
                                                <span class="task-cat pink">12</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title">Automative</p>
                                            </div>
                                             <div class="col s3">
                                                <span class="task-cat purple darken-2">22</span>
                                            </div>
                                            <div class="col s3">
                                                <span class="task-cat purple darken-2">22</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title">Banking And Financing</p>
                                            </div>
                                            <div class="col s3">
                                                <span class="task-cat green">12</span>
                                            </div>
                                             <div class="col s3">
                                                <span class="task-cat green">12</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title">Dairy</p>
                                            </div>
                                            <div class="col s3">
                                                <span class="task-cat light-green">29</span>
                                            </div>
                                            <div class="col s3">
                                                <span class="task-cat light-green">29</span>
                                            </div>
                                        </div>
                                    </li>
                                      <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title">Energy</p>
                                            </div>
                                            <div class="col s3">
                                                <span class="task-cat blue">29</span>
                                            </div>
                                            <div class="col s3">
                                                <span class="task-cat blue">29</span>
                                            </div>
                                        </div>
                                    </li>
                                      <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title">Wholesale</p>
                                            </div>
                                            <div class="col s3">
                                                <span class="task-cat amber">29</span>
                                            </div>
                                            <div class="col s3">
                                                <span class="task-cat amber">29</span>
                                            </div>
                                        </div>
                                    </li>
                                     <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title">Writing and Editing</p>
                                            </div>
                                            <div class="col s3">
                                                <span class="task-cat cyan darken-4">29</span>
                                            </div>
                                            <div class="col s3">
                                                <span class="task-cat cyan darken-4">29</span>
                                            </div>
                                        </div>
                                    </li>
                                     <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title">Technology</p>
                                            </div>
                                            <div class="col s3">
                                                <span class="task-cat  red accent-2">29</span>
                                            </div>
                                            <div class="col s3">
                                                <span class="task-cat  red accent-2">29</span>
                                            </div>
                                        </div>
                                    </li>
                                     <li class="collection-item">
                                        <div class="row">
                                            <div class="col s6">
                                                <p class="collections-title">Textiles</p>
                                            </div>
                                            <div class="col s3">
                                                <span class="task-cat  cyan lighten-1">29</span>
                                            </div>
                                            <div class="col s3">
                                                <span class="task-cat  cyan lighten-1">29</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--work collections end-->

