@extends('layouts.default')

@section('content')

<div id="login-page" class="row">
	<div class="col s12 z-depth-4 card-panel">
		<form class="login-form" method="POST" action="">
			{{ csrf_field() }}

			<div class="row">
				<div class="input-field col s12 center">
					<h4>Forgot Password</h4>
					@if (Session::has('message'))
						<div class="alert
							@if (Session::has('status') && Session::get('status') == 'FAILED')
								red-text
							@else
								green-text
							@endif
							center-align">
							{{ Session::get('message') }}
						</div>
					@endif
					<p class="center">You can reset your password</p>
				</div>
			</div>
			<div class="row margin">
				<div class="input-field col s12{{ $errors->has('email') ? ' has-error' : '' }}">
					<i class="mdi-social-person-outline prefix"></i>
					<input id="email" name="email" type="text" value="{{ old('email') }}">
					<label for="email" class="center-align">Email</label>
					@if ($errors->has('email'))
						<span class="help-block">
							<span>{{ $errors->first('email') }}</span>
						</span>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<button type="submit" class="btn waves-effect waves-light col s12">Send Reset Request</button>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s6 m6 l6">
					<p class="margin medium-small"><a href="{{ route('auth.login') }}">Login</a></p>
				</div>
				<div class="input-field col s6 m6 l6">
					<p class="margin right-align medium-small"><a href="{{ route('auth.register') }}">Register</a></p>
				</div>
			</div>

		</form>
	</div>
</div>

@endsection
