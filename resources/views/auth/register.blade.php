@extends('layouts.default')

@section('content')

<style>
	.card-panel {
		padding: 0px 0px 0px 0px;
	}

	.row-padding {
		padding: 0px 20px 20px 20px;
	}
</style>

<div id="login-page" class="row">

	<div class="card-panel">
		<div class="col s12 center" style="background-color: #1d1c2c; padding: 12px 0px 5px 0px; margin-bottom: 20px;">
			<img src="{{ env('APP_ICON') }}" alt="" class="responsive-img login-logo valign">
			<!-- <a href="/auth/facebook">
			<div class="collection facebook-btn" style="">
			<span class="collection-item left"><i class="fa fa-facebook"></i></span>
			<p>Facebook</p>
		</div>
	</a>
	<a href="/auth/google">
	<div class="collection google-btn" style="">
	<span class="collection-item left google-icons"><i class="fa fa-google-plus"></i></span>
	<p>Google</p>
	</div>
	</a>
	</div>
	<!-- <div class="col s12">
	<div class="center or_div">
	<div class="line"></div>
	<div class="circle_or">OR</div>
	<div class="line"></div>
	</div>
	</div> -->
	</div>

		<form class="login-form" role="form" method="POST" action="{{ route('auth.register') }}">
			{{ csrf_field() }}

			<div class="row row-padding">
		<!-- 	<h6 class="sign-up">Sign up with email</h6> -->
				<div class="input-field col s12{{ $errors->has('name') ? ' has-error' : '' }}">
					<i class="mdi-social-person-outline prefix"></i>
					<input id="name" name="name" type="text" value="{{ old('name') }}" required autofocus>
					<label for="name" class="center-align">Name</label>
					@if ($errors->has('name'))
						<span class="help-block">
							<span>{{ $errors->first('name') }}</span>
						</span>
					@endif
				</div>
			</div>
			<div class="row margin row-padding">
				<div class="input-field col s12{{ $errors->has('email') ? ' has-error' : '' }}">
					<i class="mdi-communication-email prefix"></i>
					<input id="email" type="email" name="email">
					<label for="email" class="center-align">Email</label>
					@if ($errors->has('email'))
						<span class="help-block">
							<span>{{ $errors->first('email') }}</span>
						</span>
					@endif
				</div>
			</div>
			<div class="row margin row-padding">
				<div class="input-field col s12{{ $errors->has('password') ? ' has-error' : '' }}">
					<i class="mdi-action-lock-outline prefix"></i>
					<input id="txtNewPassword" type="password" name="password">
					<label for="password">Create New Password</label>
					@if ($errors->has('password'))
						<span class="help-block">
							<span>{{ $errors->first('password') }}</span>
						</span>
					@endif
				</div>
			</div>
			<div class="row margin row-padding">
				<div class="input-field col s12">
					<i class="mdi-action-lock-outline prefix"></i>
					<input id="txtConfirmPassword" type="password" name="confirm" >
					<label for="confirm">Confirm New Password</label>
				</div>
			</div>

			<div class="row row-padding">
				<div class="input-field col s12">
					<button id="submit" class="btn purple waves-effect waves-light col s12" >Register Now</button>
				</div>
			</div>
			<div class="row row-padding">
				<div class="input-field col s7 m7 l7">
					<p class="margin medium-small">Already have an Eyewil account?</p>
				</div>
				<div class="input-field col s5 m5 l5">
					<p class="margin right btn purple waves-effect waves-light"><a href="{{ route('auth.login') }}" style="color: #FFF">Login</a></p>
				</div>
			</div>
		</form>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
/*
	$(document).ready(function() {
		save()
	});
	function save() {
		$('#txtConfirmPassword').on('change', function(event) {
			var pas = $("#txtNewPassword").val();
				var con = $("#txtConfirmPassword").val();
				if(pas !=con)
				{
					alert("passwords do not match");
					return false ;
				}
		});
	}*/

	$ (document).ready(function(){
			$('#submit').click(function(event){
				var pas = $("#txtNewPassword").val();
				var con = $("#txtConfirmPassword").val();
				if(pas !=con)
				{
					alert("passwords do not match");
					return false ;
				}else{
					return true ;
				}
			})
		});
</script>
@endsection