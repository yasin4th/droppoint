@extends('layouts.default')

@section('content')

<style>
	.card-panel {
		padding: 0px 0px 0px 0px;
	}

	.row-padding {
		padding: 0px 20px 20px 20px;
	}
</style>


<div id="login-page" class="row">
	<div class="card-panel">
		<div class="col s12 center" style="background-color: #1d1c2c; padding: 12px 0px 5px 0px; margin-bottom: 20px;">
			<img src="{{ env('APP_ICON') }}" alt="" class="responsive-img login-logo valign">
			<!-- <a href="/auth/facebook">
			<div class="collection facebook-btn" style="">
			<span class="collection-item left"><i class="fa fa-facebook"></i></span>
			<p>Facebook</p>
		</div>
	</a>
	<a href="/auth/google">
	<div class="collection google-btn" style="">
	<span class="collection-item left google-icons"><i class="fa fa-google-plus"></i></span>
	<p>Google</p>
</div>
</a>
</div>
<!-- <div class="col s12">
<div class="center or_div">
<div class="line"></div>
<div class="circle_or">OR</div>
<div class="line"></div>
</div>
</div> -->
</div>

<form class="login-form" method="POST" action="">
	{{ csrf_field() }}

	<div class="row">
		<div class="row margin row-padding">
			<!-- <h6 class="sign-up">Login with email</h6> -->
			@if (Session::has('message'))
			<div class="alert
			@if(Session::has('status'))
			green-text
			@else
			red-text
			@endif
			center-align">
			{{ Session::get('message') }}
		</div>
		@endif
		<div class="input-field col s12{{ $errors->has('email') ? ' has-error' : '' }}" style="padding-top:">
			<i class="mdi-social-person-outline prefix"></i>
			<input id="email" name="email" type="text" value="{{ old('email') }}">
			<label for="email" class="center-align">Email</label>
			@if ($errors->has('email'))
			<span class="help-block">
				<span>{{ $errors->first('email') }}</span>
			</span>
			@endif
		</div>
	</div>
	<div class="row margin row-padding">
		<div class="input-field col s12">
			<i class="mdi-action-lock-outline prefix"></i>
			<input id="password" type="password" name="password" required>
			<label for="password">Password</label>
		</div>
	</div>
	<div class="row row-padding">
		<div class="input-field col s6 m6 l6  login-text">
			<input type="checkbox" id="remember-me" name="remember">
			<label for="remember-me">Remember me</label>
		</div>
		<div class="input-field col s6 m6 l6 forgot">
			<p class="margin right-align medium-small"><a href="{{ route('auth.reset') }}">Forgot password ?</a></p>
		</div>
	</div>
	<div class="row row-padding">
		<div class="input-field col s12">
			<button type="submit" class="btn purple waves-effect waves-light col s12">Login</button>
		</div>
	</div>
	<div class="row row-padding">
		<div class="input-field col s7 m7 l7">
			<p class="margin medium-small">Don't have an account?</p>
		</div>
		<div class="input-field col s5 m5 l5">
			<p class="margin right btn purple waves-effect waves-light"><a href="{{ route('auth.register') }}" style="color: #FFF">Sign up</a></p>
		</div>
	</div>

</form>
</div>
</div>

@endsection
