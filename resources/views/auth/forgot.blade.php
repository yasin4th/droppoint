@extends('layouts.default')

@section('content')

<div id="login-page" class="row">
	<div class="col s12 z-depth-4 card-panel">
		<form class="login-form" method="POST" action="">
			{{ csrf_field() }}

			<div class="row">
				<div class="input-field col s12 center">
					<h4>Forgot Password</h4>
					@if (Session::has('message'))
						<div class="alert
							@if (Session::has('status') && Session::get('status') == 'FAILED')
								red-text
							@else
								green-text
							@endif
							center-align">
							{{ Session::get('message') }}
						</div>
					@endif
					<p class="center">You can reset your password</p>
				</div>
			</div>
			<div class="row margin">
				<div class="input-field col s12{{ $errors->has('email') ? ' has-error' : '' }}">
					<i class="mdi-social-person-outline prefix"></i>
					<input id="password" name="password" type="password">
					<label for="password" class="center-align">New Password</label>
				</div>
			</div>
			<div class="row margin">
				<div class="input-field col s12">
					<i class="mdi-social-person-outline prefix"></i>
					<input id="confirm" name="confirm" type="password">
					<label for="confirm" class="center-align">Confirm Password</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<button type="submit" id= "submit" class="btn waves-effect waves-light col s12">Set Password</button>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s6 m6 l6">
					<p class="margin medium-small"><a href="{{ route('auth.login') }}">Login</a></p>
				</div>
				<div class="input-field col s6 m6 l6">
					<p class="margin right-align medium-small"><a href="{{ route('auth.register') }}">Register</a></p>
				</div>
			</div>

		</form>
	</div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">

		$ (document).ready(function(){
			$('#submit').click(function(){
				var pas = $("#password").val();
				var con = $("#confirm").val();
				if(pas !=con)
				{
					alert("passwords do not match");
					return false ;
				}
			})
		});


</script>
@endsection
