@extends('layouts.material')

@section('content')

<div class="row">

	<!-- <div class="col s12 m3">
		@if(Auth::user()->isAdmin())
			@include('includes.admin.sidemenu')
		@else
			@include('includes.sidemenu')
		@endif
	</div> -->
	<div class="col s12 m12">
		<div class="card">
			<div class="card-content">
				<div class="row">
					<div class="col s10 m4">
						<div class="header-search-wrapper">
							<i class="mdi-action-search"></i>
							<form role="form" action="{{ route('job.index') }}" name="search" method="GET">
								<input type="text" name="query" class="header-search-input z-depth-2 table-search" placeholder="Search by Job Title or User Name" value="{{ $query }}">
							</form>
						</div>
					</div>
					<div class="col s12 m8">
						<a href="{{ route('job.export') }}" class="btn-flat right teal-text btn-get-csv"><img src="/images/csv-icon.png" class="responsive-img"> Get CSV</a>
						<a href="{{ route('job.create') }}" class="waves-effect waves-light btn teal darken-1 right mrg-tb15">Post Job</a>
						@if(Auth::user()->isAdmin())
							<a href="{{ route('admin.map') }}" class="waves-effect waves-light btn right mrg-tb15 map-btn">Map</a>
						@endif
					</div>

					<div class="col s6">
					</div>
					<div class="col s12">
						<h4>
							@if($query)
								Search results '{{$query}}'
							@else
								All Jobs
							@endif
						</h4>
						<div class="table-scroll">
							<table class="bordered all-table" id="jobs">
								<thead>
									<tr>
										<th>Job Title</th>
									@if(Auth::user()->isAdmin())
										<th>User</th>
									@endif
										<th>Location</th>
										<th>Salary (RM)</th>
										<th>Vacancies</th>
										<th>Applicants</th>
										<th>Created</th>
										<th>Status </th>
									</tr>
								</thead>
								<tbody>
									@foreach($jobs as $job)
									<tr>
										<td>
											<a href="{{ route('job.edit', $job->id) }}"><strong>{{ $job->title }}</strong></a>
											<br>
											{{ str_limit($job->about,40) }}
										</td>
									@if(Auth::user()->isAdmin())
										<td><a href="/admin/users/{{$job->user_id}}/edit">{{ $job->name }}</a></td>
									@endif
										<td>{{ $job->location }}</td>
										<td>{{ $job->salary }} {{ $job->salary_unit }}</td>
										<td><a href="/job/{{$job->id}}/applicants">0 hired / {{ $job->vacancy }}</a></td>
										<td>{{ $job->applicants_count }}</td>
										<td>{{ \Carbon\Carbon::parse($job->start_date)->format('D M j, Y') }}</td>
										@if($job->status == 1)
											<td><span class="task-cat green">Active</span></td>
										@else
											<td><span class="task-cat orange">Inactive</span></td>
										@endif
										<td><a href="#" class="dropdown-button font-color" data-activates="dropdown-{{$job->id}}" data-beloworigin="true">More<i class="mdi-navigation-arrow-drop-down"></i></a>
											<ul id="dropdown-{{$job->id}}" class="dropdown-content">
												<li><a class="preview" href="#preview" id="{{$job->id}}" onclick="preview(this.id)">Preview Job</a></li>
												<li><a href="{{ route('job.edit', $job->id) }}">Edit</a></li>
												<li><a href="/admin/jobs/{{ $job->id }}/delete" class="DeleteJob">Delete</a></li>
											<!-- <a href="#" class="dropdown-button font-color" data-activates="dropdown'+job.id+'" data-beloworigin="true">More<i class="mdi-navigation-arrow-drop-down"></i></a></td> -->
											</ul>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div> 
					<div class="col s12 m6 mrg-tb15">
						<p>Showing {{ ($jobs->firstItem()) ?:0 }} to {{ $jobs->lastItem()?:0 }} of {{ $jobs->total() }} entries</p>
					</div>
					<div class="col s12 m6 mrg-tb15">
						<div class="right">
							{{ $jobs->links() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="preview" class="modal modal-fixed-footer preview-modal">
		<div class="modal-content">
			<!-- <h5 class="center">Preview Job</h5> -->
			<div class="row">

				<div class="col m12 s12">
					<div class="card-content">
						<div class="row">
							<div class="col s12">
								<a href="#!" class="modal-action modal-close close-preview"><i class="mdi-navigation-cancel"></i></a>
							</div>
						</div>
						<div class="row">
							<div class="col s12">

								<img src="#" class="responsive-img" id="Avatar">
							</div>
						</div>
						<div class="row  retail-sales">
							<div class="col s12">
								<h5 id="Title"></h5>
								<P class="" id="Location"></P>
							</div>
						</div>
						<hr>
						<div class="row icon-div">
						<div class="col s2 m2">
						<img src="/images/dollar.png" class="responsive-img">
						</div>
						<div class="col s5 m5">
						<h6 >Salary</h6>
						</div>
						<div class="col s5 m5">
						<h6 id="Salary"></h6>
						</div>
					</div>
					<div class="row icon-div">
						<div class="col s2 m2">
						<img src="/images/calendar.png" class="responsive-img">
						</div>
						<div class="col s5 m5">
						<h6> Working start Date</h6>
						</div>
						<div class="col s5 m5">
						<h6 id="StartDate"> </h6>
						</div>
					</div>
					<div class="row icon-div">
						<div class="col s2 m2">
						<img src="/images/watch.png" class="responsive-img">
						</div>
						<div class="col s5 m5">
						<h6>Working Hours</h6>
						</div>
						<div class="col s5 m5">
						<h6 id="StartTime"></h6>
						</div>
					</div>
					<div class="row icon-div">
						<div class="col s2 m2">
						<img src="/images/job_icon.png" class="responsive-img">
						</div>
						<div class="col s5 m5">
						<h6>Type of job</h6>
						</div>
						<div class="col s5 m5">
						<h6 id="Type"></h6>
						</div>
					</div>
						<!--<div class="row icon-div">
							<div class="col s3 m3">
								<img src="/images/dollar.png" class="responsive-img">
								<h6 </h6>
							</div>
							<div class="col s3 m3">
								<img src="/images/calendar.png" class="responsive-img">
								<h6 id=""></h6>
							</div>
							<div class="col s3 m3">
								<img src="/images/watch.png" class="responsive-img">
								<h6 id="">10am-6pm</h6>
							</div>
							<div class="col s3 m3">
								<img src="/images/car_1.png" class="responsive-img">
								<h6>0.9km</h6>
							</div>
						</div>!-->
						<hr>
						<div class="row role-responsibility">
							<div class="col s12">
								<h5>Role and Responsibilities</h5>
								<textarea class="materialize-textarea border_hide" rows="6" cols="8" id="roles"></textarea>
							</div>
						</div>
						<hr>
						<div class="row about-job">
							<div class="col s12">
								<h5>Tags</h5>
								<ul>
									<li></li>
								</ul>
							</div>
						</div>
						<hr>
						<div class="row dress-code">
							<div class="col s12">
								<h5>Dress Code</h5>
								<p id="Uniform"></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script 'text/javascript'>
	$(document).ready (function(){
		$('.dropdown-button').dropdown();
			$('.preview').click(function(){
				$('#preview').openModal();
		});
		$('.pagination li [rel="next"]').html('more »');

		$('.DeleteJob').click(function(e){
			e.preventDefault();
			var answer = confirm('Do you want to delete this Job?');
			if(answer == true){
				location.replace($(this).attr('href'));
			}
			else{
				return false;
			}
		});
	});


	function preview(id)
	{ 
		var id = id;

		$.ajax({
				'type': 'get',
				'url':'/job/'+id+'/previewAdmin'
			}).done(function(response) {

				$('#Avatar').attr('src',response.data.cover_picture);
				$('#Location').html(response.data.location);
				$('#Salary').html("RM"+ " "+ response.data.salary +" "+ response.data.salary_unit);
				$('#StartTime').html(response.data.start_time +" "+response.data.end_time);
				$('#StartDate').html(response.data.start_date);
				$('#About').html(response.data.about);
				$('#Uniform').html(response.data.uniform);
				$('#Title').html(response.data.title);
				$('#Type').html(response.data.type);
				$('.materialize-textarea').html(response.data.requirements);

				html = '';
				response.data.tags.forEach(function(tag) {
					if (tag) {
						html +='<li>'+ tag.name + '</li>';
						$('#preview ul').html(html);
					}
				});

					$('#preview ol').html(html);
				/*if(response.data.requirements){

					html += '<li>'+ response.data.requirements +'</li>';
				}*/
			});
	}

</script>
@endsection