@extends('layouts.material')

@section('content')

<div class="row">
	<div class="col s12 m12">
		<div class="supervisor-div">
			<label>Job Title</label>
			<h5><a href="/job/{{$job->id}}/edit" ><strong> {{$job->title}} </strong></a></h5>
			<div class="row">
				<div class="col s12 m8">
					<ul class="tabs">
						<li class="tab"><a href="#applicant">APPLICANTS <span>(0)</span></a></li>
						<li class="tab"><a href="#shortlist">SHORTLIST <span>(0)</span></a></li>
						<li class="tab"><a href="#hired">HIRED <span>(0)</span></a></li>
						<li class="tab"><a href="#archived">ARCHIVED <span>(0)</span></a></li>
					</ul>
				</div>
			</div>
			<div class="row mrg-t30 supervisor">
				<div id="applicant" class="col s12">
					<div class="row">
						<div class="col s12 m4 right search-box">
							<form role="form" action="/job/{{$job->id}}/applicants" name="search">
							<input type="text" name="query" class="search" placeholder="Search" value="" id="query">
							<i class="fa fa-search"></i>
							</form>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m8">
							<ul class="tabs-button">
								<li>
									<p class="check-box">
										<input type="checkbox" id="checklist" name="checkToAll"/>
										<label for="checklist"></label>
									</p>
								</li>
								<li>
									<span style="cursor: pointer;">
										<i class="fa fa-repeat" onclick="load_applicants(0, false)" ></i>
										<!--<input type="button" value="RELOAD" onclick="location.reload();" />!-->
									</span>
								</li>
								<li class="more hide">
									<a class='dropdown-button' href='#' data-activates='dropdown1' data-beloworigin="true">More <i class="fa fa-caret-down"></i></a>
									<ul id='dropdown1' class='dropdown-content app_more'>
										<li><a href="#!" onclick="updateField('status',2)">Hired</a></li>
										<li class="menu_unshort"><a href="#!" onclick="updateField('starred',0)">Unshortlist</a></li>
										<li class="menu_short"><a href="#!" onclick="updateField('starred',1)">Shortlist</a></li>
										<li><a href="#!" onclick="updateField('status',4)">Archived</a></li>
									</ul>
								</li>
							</ul>
						</div>
						<div class="col s12 m4">
							<div class="pagination-div">
								<p>Showing 1 to {{count($job->applicants)}} of {{count($job->applicants)}} entries</p>
								<div class="pagination">
									<a href="">
										<i class="fa fa-chevron-left"></i>
									</a>
									<a href="">
										<i class="fa fa-chevron-right"></i>
									</a>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m12 mrgt-20">
							<table class="bordered">
								<thead>
								<tr>
									<th data-field="id" class="user-data"></th>
									<th data-field="price" style="width:12%;">Name <i class="fa fa-caret-down"></i></th>
									<th data-field="price" class="hours-data">Hours <i class="fa fa-caret-down"></i></th>
									<th data-field="price" style="width:5%;">Contact</th>
									<th data-field="price" style="width:7%;">Date <i class="fa fa-caret-down"></i></th>
								</tr>
								</thead>

								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="shortlist" class="col s12">
					<div class="row">
						<div class="col s12 m4 right search-box">
							<input type="text" name="search" class="search" placeholder="Search">
							<i class="fa fa-search"></i>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m8">
							<ul class="tabs-button">
								<li>
									<p class="check-box">
										<input type="checkbox" id="checklist2" name="checkToAll"/>
										<label for="checklist2"></label>
										<!-- <i class="fa fa-caret-down"></i> -->
									</p>
								</li>
								<li>
									<span style="cursor: pointer;">
										<i class="fa fa-repeat" onclick="load_applicants(0, true)" ></i>
										<!--<input type="button" value="RELOAD" onclick="location.reload();" />!-->
									</span>
								</li>
								<li class="more hide">
									<a class='dropdown-button' href='#' data-activates='dropdown2'>More <i class="fa fa-caret-down"></i></a>
									<ul id='dropdown2' class='dropdown-content'>
										<li class="menu_unshort"><a href="#!" onclick="updateField('starred',0)">Unshortlist</a></li>
										<li><a href="#!" onclick="updateField('status',2)">Hired</a></li>
										<li><a href="#!" onclick="updateField('status',4)">Archived</a></li>
									</ul>

								</li>
							</ul>
						</div>
						<div class="col s12 m4">
							<div class="pagination-div">
								<p>Showing 1 to {{count($job->starred)}} of {{count($job->starred)}} entries</p>
								<div class="pagination">
									<a href="{{$starred->previousPageUrl()}}">
										<i class="fa fa-chevron-left"></i>
									</a>
									<a href="{{$starred->nextPageUrl()}}">
										<i class="fa fa-chevron-right"></i>
									</a>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col s12 m12 mrgt-20">
							<table class="bordered">
								<thead>
								<tr>
									<th data-field="id" class="user-data"></th>
									<th data-field="price" style="width:12%;">Name <i class="fa fa-caret-down"></i></th>
									<th data-field="price" class="hours-data">Hours <i class="fa fa-caret-down"></i></th>
									<th data-field="price" style="width:5%;">Contact</th>
									<th data-field="price" style="width:7%;">Date <i class="fa fa-caret-down"></i></th>
								</tr>
								</thead>

								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="hired" class="col s12">
					<div class="row">
						<div class="col s12 m4 right search-box">
							<input type="text" name="search" class="search" placeholder="Search">
							<i class="fa fa-search"></i>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m8">
							<ul class="tabs-button">
								<li>
									<p class="check-box">
										<input type="checkbox" id="checklist3"  name="checkToAll"/>
										<label for="checklist3"></label>
										<!-- <i class="fa fa-caret-down"></i> -->
									</p>
								</li>
								<li>
									<span style="cursor: pointer;">
										<i class="fa fa-repeat" onclick="load_applicants(2, false)" ></i>
										<!--<input type="button" value="RELOAD" onclick="location.reload();" />!-->
									</span>
								</li>
								<li class="more hide">
									<a class='dropdown-button' href='#' data-activates='dropdown3'>More <i class="fa fa-caret-down"></i></a>
									<ul id='dropdown3' class='dropdown-content'>
										<li><a href="#!" onclick="updateField('status',1)">Unhired</a></li>
										<li class="menu_unshort"><a href="#!" onclick="updateField('starred',0)">Unshortlist</a></li>
										<li class="menu_short"><a href="#!" onclick="updateField('starred',1)">Shortlist</a></li>
										<li><a href="#!" onclick="updateField('status',1)">Archived</a></li>
									</ul>

								</li>
							</ul>
						</div>
						<div class="col s12 m4">
							<div class="pagination-div">
								<p>Showing 1 to {{count($job->hired)}} of {{count($job->hired)}} entries</p>
								<div class="pagination">
									<a href="{{$hired->previousPageUrl()}}">
										<i class="fa fa-chevron-left"></i>
									</a>
									<a href="{{$hired->nextPageUrl()}}">
										<i class="fa fa-chevron-right"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m12 mrgt-20">
							<table class="bordered">
								<thead>
								<tr>
									<th data-field="id" class="user-data"></th>
									<th data-field="price" style="width:12%;">Name <i class="fa fa-caret-down"></i></th>
									<th data-field="price" class="hours-data">Hours <i class="fa fa-caret-down"></i></th>
									<th data-field="price" style="width:5%;">Contact</th>
									<th data-field="price" style="width:7%;">Date <i class="fa fa-caret-down"></i></th>
								</tr>
								</thead>

								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="archived" class="col s12">
					<div class="row">
						<div class="col s12 m4 right search-box">
							<input type="text" name="search" class="search" placeholder="Search">
							<i class="fa fa-search"></i>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m8">
							<ul class="tabs-button">
								<li>
									<p class="check-box">
										<input type="checkbox" id="test6"  name="checkToAll" />
										<label for="test6"></label>
										<!-- <i class="fa fa-caret-down"></i> -->
									</p>
								</li>
								<li>
									<span style="cursor: pointer;">
										<i class="fa fa-repeat" onclick="load_applicants(4, false)" ></i>
										<!--<input type="button" value="RELOAD" onclick="location.reload();" />!-->
									</span>
								</li>
								<li class="more hide">
									<a class='dropdown-button' href='#' data-activates='dropdown4'>More <i class="fa fa-caret-down"></i></a>
									<ul id='dropdown4' class='dropdown-content'>
										<li><a href="#!" onclick="updateField('status',1)">Unarchived</a></li>
										<li><a href="#!" onclick="updateField('status',2)">Hired</a></li>
										<li><a href="#!" onclick="updateField('starred',1)">Shortlist</a></li>
									</ul>

								</li>
							</ul>
						</div>
						<div class="col s12 m4">
							<div class="pagination-div">
								<p>Showing 1 to {{count($job->archived)}} of {{count($job->archived)}} entries</p>
								<div class="pagination">
									<a href="{{$hired->previousPageUrl()}}">
										<i class="fa fa-chevron-left"></i>
									</a>
									<a href="{{$hired->nextPageUrl()}}">
										<i class="fa fa-chevron-right"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m12 mrgt-20">
							<table class="bordered">
								<thead>
								<tr>
									<th data-field="id" class="user-data"></th>
									<th data-field="price" style="width:12%;">Name <i class="fa fa-caret-down"></i></th>
									<th data-field="price" class="hours-data">Hours <i class="fa fa-caret-down"></i></th>
									<th data-field="price" style="width:5%;">Contact</th>
									<th data-field="price" style="width:7%;">Date <i class="fa fa-caret-down"></i></th>
								</tr>
								</thead>

								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

</div>
@endsection

@section('scripts')

	<script src="/js/materialize/plugins/fullcalendar/lib/moment.min.js" type="text/javascript" charset="utf-8"></script>
	<script>
		var applicant_page = 1;
		$(document).ready(function() {

			load_applicants(0, true); // all - starred
			load_applicants(0, false); // all - not starred
			load_applicants(2, false); // hired - not starred
			load_applicants(4, false); // archived

			$(".app-dropdown").on('click','li',function (){
				var liValue = $(this).text();
				href = $('a.active').attr('href').replace('#','');
				switch(liValue) {
					case 'All':
						$(`input[type=checkbox][id^=${href}]`).prop("checked",true);
						break;
					case 'None':
						$(`input[type=checkbox][id^=${href}]`).prop("checked",false);
						break;
					case 'Shortlist':
						$(`input[type=checkbox][id^=${href}][starred!=1]`).prop("checked",false);
						$(`input[type=checkbox][id^=${href}][starred=1]`).prop("checked",true);
						break;
					case 'Hire':
						$(`input[type=checkbox][id^=${href}][status=2]`).prop("checked",true);
						$(`input[type=checkbox][id^=${href}][status!=2]`).prop("checked",false);
						break;
					case 'Archived':
						$(`input[type=checkbox][id^=${href}][status=4]`).prop("checked",true);
						$(`input[type=checkbox][id^=${href}][status!=4]`).prop("checked",false);
						break;
					case 'Unshortlist':
						$(`input[type=checkbox][id^=${href}][starred=1]`).prop("checked",false);
						$(`input[type=checkbox][id^=${href}][starred!=1]`).prop("checked",true);
						break;
					default:
						break;
				}
				updateUI();
			});

			$(`input[type=checkbox][name='checkToAll']`).click(function(e){
				href = $('a.active').attr('href').replace('#','');
				if($(this).is(':checked'))
				{
					$(`#${href} input[type=checkbox][name=allCheck]`).prop("checked",true);
				}
				else
				{
					$(`#${href} input[type=checkbox][name=allCheck]`).prop("checked",false);
				}
				updateUI();
			});
		});
	</script>

	<script>
		toggleStarred = function (el, id) {
			star = $(el).hasClass('star-active') ? 0 : 1;
			$.ajax({
				type : 'get',
				url  : '{{ route('applicants.index') }}/' + id + '/starred/'+star,
			}).done(function(res) {
				Materialize.toast(res.message, 1000);
				if(res.status == 1){
					load_applicants(0, true);
					load_applicants(0, false);
					$(el).toggleClass('star-active');
				}
			});
		}

		/* fav
		 * 0 : not starred
		 * 1 : starred
		 */
		load_applicants = function(status, starred)
		{
			if (applicant_page != 0)
			{
				var req = {};
				req.sort = 'all';
				if(starred) {
					req.favourites = 1;
				}
				if(status != 0) {
					req.status = status;
				}
				req._token  = '{{ csrf_token() }}';


				$.ajax({
					type : 'post',
					// url  : '{{ route('job.applicants', $job->id) }}?page='+applicant_page,
					url  : '{{ route('job.applicants', $job->id) }}',
					data : req,
				}).done(function(res) {

					if(status == 0 && starred == false) {
						id = 'applicant';
					}
					if(status == 0 && starred == true) {
						id = 'shortlist';
					}
					if(status == 2 && starred == false) {
						id = 'hired';
					}
					if(status == 4 && starred == false) {
						id = 'archived';
					}

					html = '';
					res.forEach(function(app) {
						html += `<tr>
									<td>
										<p>
											<input type="checkbox" id="${id}${app.id}" name="allCheck" status="${app.status}" starred="${app.starred}" onchange="updateUI();" data-id="${app.id}">
											<label for="${id}${app.id}"></label>
										</p>
										<span>
											<i class="fa fa-star ${(app.starred == 1) ? 'star-active' : ''}" onclick="toggleStarred(this,${app.id})"></i>
											<i class="fa fa-circle" style="color:${(app.status == 2) ? 'red' : 'white'}"></i>
										</span>
										<div class="img-box">
											<img src="/uploads/logo/avatar.jpg" alt="" class="circle responsive-img">
										</div>
									</td>
									<td style="font-weight:600;">${app.name}</td>
									<td>${app.working_hours} hrs</td>
									<td>${app.email}</td>
									<td>${moment(app.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD MMM YYYY')}</td>
								</tr>`;

					});

					$('#'+id+' table tbody').html(html);
					$('#'+id+' .more').addClass('hide');
					$('li.tab a[href=#'+id+'] span').html(`(${res.length})`);
				});
			}
		}

		updateUI = function() {
			divs = $('.supervisor > div');

			$.each(divs, function(i, d) {
				check = $(d).find('input[type=checkbox]:checked');
				if(check.length > 0) {
					if($(d).find('input[type=checkbox][starred=1]:checked').length == 0){
						$('.menu_unshort').addClass('hide')
					} else {
						$('.menu_unshort').removeClass('hide')
					}

					if($(d).find('input[type=checkbox][starred=0]:checked').length == 0){
						$('.menu_short').addClass('hide')
					} else {
						$('.menu_short').removeClass('hide')
					}
					$(d).find('.more').removeClass('hide');
				} else {
					$(d).find('.more').addClass('hide');
				}
			})
		}

		updateField = function(key, value) {
			href = $('a.active').attr('href').replace('#','');
			checks = $(`#${href} table input:checked`);
			ids = [];
			if(checks.length > 0) {
				$.each(checks, function(i, c) {
					ids.push($(c).attr('data-id'));
				})
			}

			$.ajax({
				type : 'post',
				url  : '/applicants/update',
				data : {ids: ids, field: key,value: value,_token: '{{csrf_token()}}'},
			}).done(function(res) {
				load_applicants(0, true); // all - starred
				load_applicants(0, false); // all - not starred
				load_applicants(2, false); // hired - not starred
				load_applicants(4, false); // archived
				Materialize.toast(res.message,1500);
			});
		}

		$(document).ready(function() {
			$('form[name=search]').on('submit', function(ev) {
				ev.preventDefault();
				$.ajax({
					type : 'get',
					url  : '/job/{{$job->id}}/applicants',
					data : {query: document.getElementById('query').value,_token: '{{csrf_token()}}'},
				}).done(function(res) {
					html = '';
					res.applicants.forEach(function(app) {
						html += `<tr>
									<td>
										<p>
											<input type="checkbox" id="${id}${app.id}" name="allCheck" status="${app.status}" starred="${app.starred}" onchange="updateUI();" data-id="${app.id}">
											<label for="${id}${app.id}"></label>
										</p>
										<span>
											<i class="fa fa-star ${(app.starred == 1) ? 'star-active' : ''}" onclick="toggleStarred(this,${app.id})"></i>
											<i class="fa fa-circle" style="color:${(app.status == 2) ? 'red' : 'white'}"></i>
										</span>
										<div class="img-box">
											<img src="/uploads/logo/avatar.jpg" alt="" class="circle responsive-img">
										</div>
									</td>
									<td style="font-weight:600;">${app.name}</td>
									<td>${app.working_hours} hrs</td>
									<td>${app.email}</td>
									<td>${moment(app.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD MMM YYYY')}</td>
								</tr>`;

					});

					$('#applicant table tbody').html(html);
				});
			})
		})
	</script>

@endsection
