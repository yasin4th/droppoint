@extends('layouts.material')

@section('content')

<div class="row">
	<div class="col m12">
		<div class="row">
			<div class="input-field col m4">
				<input type="text" class="validate" id="title" name="title" value="{{ $job->title }}">
				<label for="title">Job Title</label>
			</div>
			<div class="input-field col m4 right">
				@if(Auth::user()->role == 1)
				<a class="waves-effect btn right btn-update edit">Update</a>
				@endif
				<!-- {!!($job->created_at->diffInHours(\Carbon\Carbon::now()) < 24) ? "disabled onclick='alert(33)'" : '' !!}     !-->
			</div>
		</div>
		<div class="row">
			<div class="col m3">
				<div class="row job-start-details">
					<div class="col s4">
						<strong>Start Date: </strong>
					</div>
					<div class="col s8">
						<input type="text" class="pikaday start_date" name="start_date" value="{{$job->start_date->format('Y-m-d')}}">
					</div>
				</div>
			</div>
			<div class="col m3">
				<div class="row job-start-details">
					<div class="col s5">
						<strong>Working Days: </strong>
					</div>
					@php
					$date = DB::table('working_days')->where('job_id',$job->id)->get();
					$total=count($date);
					@endphp
					<div class="col s4">
						<input type="text" class="working_days" name="working_days" value="{{ $total }}">
					</div>
				</div>
			</div>
			<div class="col m3">
				<div class="row job-start-details">
					<div class="col s5">
						<strong>Pay Rate (RM):</strong>
					</div>
					<div class="col s3">
						<input type="number" class="salary" name="salary" value="{{ $job->salary }}">
					</div>
					<div class="col s4">
						<select name="salary_unit" id="salary_unit">
							<option value="per hour" {{ ($job->salary_unit=='per hour') ? 'selected' : '' }}>Per Hour</option>
							<option value="per day" {{ ($job->salary_unit=='per day') ? 'selected' : '' }}>Per Day</option>
							<option value="per month" {{ ($job->salary_unit=='per month') ? 'selected' : '' }}>Per Month</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col m3">
				<div class="row job-start-details">
					<div class="col s4">
						<strong>Job Type: </strong>
					</div>
					<div class="col s5">
						<select name="type" id="job_type">
							<option value="part-time" {{ ($job->type=='part-time') ? 'selected' : '' }}>Part Time</option>
							<option value="full-time" {{ ($job->type=='full-time') ? 'selected' : '' }}>Full time</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<br><br>
	</div>

	<div class="col m3">
		<div class="card" style="overflow:visible;">
			<div class="card-content wob-input">
				<div class="border-bot">
					<strong> Status</strong>
				</div>
				<select name="status" id="status">
					<option value="1" {{ ($job->status == 1) ? 'selected' : '' }}>Active</option>
					<option value="0" {{ ($job->status == 0) ? 'selected' : '' }}>Closed</option>
				</select>
			</div>
		</div>
		<a onclick="$('#location-modal').openModal();
		setTimeout(
			function(){
				window.dispatchEvent( new Event('resize') );
			},1500);
		setTimeout(
			function(){
				window.dispatchEvent( new Event('resize') );
			},3500);
		setTimeout(
			function(){
				window.dispatchEvent( new Event('resize') );
			},5500);
		setTimeout(
			function(){
				window.dispatchEvent( new Event('resize') );
			},7500);

			" style="cursor: pointer;">
			<div class="card">
					<div class="card-content wob-input">
						<div class="border-bot">
							<strong> Locations</strong>
						</div>
						<p id="addressDiv">{{$job->street}}<br>{{ $job->location }}<br>{{$job->city}}<br>{{$job->state}}<br>{{$job->country}}.</p>
					</div>
			</div>
		</a>
		<div class="card">
			<div class="card-content wob-input">
				<div class="border-bot">
					<strong> Statistics</strong>
				</div>
				<table class="table-static">
					<tbody>
						<tr>
							<td>Total Viewers</td>
							<td>{{count($job->viewers)}}</td>
						</tr>
						<tr>
							<td>Total Watchers</td>
							<td>{{count($job->watchers)}}</td>
						</tr>
						<tr>
							<td>Total Applicants</td>
							<td>{{count($job->applicants)}}</td>
						</tr>
						<tr>
							<td>Total Hired</td>
							<td>{{count($job->hired)}}/{{$job->vacancy}}</td>
						</tr>
						<tr>
							<td>Vacancies Filled</td>
							<td>
								<select name="vacancy" class="browser-default validate" id="vacancy">
									<option value="1" {{($job->vacancy == '1')?'selected':''}}> 1 vacancies</option>
									<option value="2" {{($job->vacancy == '2')?'selected':''}}> 2 vacancies</option>
									<option value="3" {{($job->vacancy == '3')?'selected':''}}> 3 vacancies</option>
									<option value="4" {{($job->vacancy == '4')?'selected':''}}> 4 vacancies</option>
									<option value="5" {{($job->vacancy == '5')?'selected':''}}> 5 vacancies</option>
									<option value="6" {{($job->vacancy == '6')?'selected':''}}> 6 vacancies</option>
									<option value="7" {{($job->vacancy == '7')?'selected':''}}> 7 vacancies</option>
									<option value="8" {{($job->vacancy == '8')?'selected':''}}> 8 vacancies</option>
									<option value="9" {{($job->vacancy == '9')?'selected':''}}> 9 vacancies</option>
								</select>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col m9">
		<div class="card">
			<br>
			<div class="col s12">
				<div class="row">
					<div class="col s12">
					  <ul class="tabs">
						<li class="tab col s3 active"><a class="active" href="#details">Details</a></li>
						<li class="tab col s3"><a href="#applicants">Applicants (<span id="applicants-count"></span>)</a></li>
						<li class="tab col s3"><a href="#hired">Hired (<span id="hired-count"></span>)</a></li>
					  </ul>
					</div>
					<div class="clearfix"></div>
					<div class="card-content">
						{{-- Details --}}
						<div id="details" class="col s12">
							<div class="row">
								<!-- <div>
									<strong>Job Description</strong>
									<textarea class="textarea-hauto" name="about" id="description" cols="30" rows="5" maxlength="250">{{$job->about}}</textarea>
								</div> -->
								<div>
									<strong>Roles and Responsibilities</strong>
									<textarea class="textarea-hauto" id="requirements" name="requirements" cols="30" rows="6" wrap="hard">{{ $job->requirements }}</textarea>
								</div>
								<div class="card">
									<div class="card-content">
										<div class="col s12 mrg-bot30">
											<b>Nationality/Work Permit</b>
											<div class="row">
												<div class="col s12 m6">
													<p class="text-muted"></p>
													@php
													$nationality = DB::table('ref_nationality')->get();
													@endphp
													<select name="nationality" id="nationality">
														@foreach($nationality as $na)
														<option value="{{$na->value}}" {{($na->value == $job->nationality ? 'selected' : '')}} >{{$na->value}}</option>
														@endforeach
													</select>
												</div>
											</div>
										</div>
										<div class="col s12 mrg-bot30">
											<p><b>Upload / Edit Pictures</b></p>
											<div class="row">
												<div class="col s12 m6">
													<form action="#" method="post" enctype="multipart/form-data">
														{{csrf_field()}}
														<div class="file-field input-field">
															<div class="btn light-blue darken-2">
																<span>Upload</span>
																<input type="file" name="avatar" id="avatar"  placeholder="Upload any image">
															</div>
															<div class="file-path-wrapper">
																<input class="file-path validate" type="text" placeholder="Upload any image"  id="cover_picture" value="{{$job->cover_picture}}">
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
										<div class="col s12 mrg-bot30">
											<p><b>Uniform Details</b></p>
											<!-- <div class="row">
												<div class="input-field col s12 m6">
													<p>
														<input name="uniform" class="uniform" type="radio" id="yes" value="1"> {{ Form::radio('ctype', '1', $job->uniform =='Full uniform provided')}}
														<label for="yes">Yes</label>
													</p>
													<p>
														<input name="uniform" class="uniform" type="radio" id="no" value="0" >{{ Form::radio('ctype', '1', $job->uniform !="Full uniform provided")}}
														<label for="no">No</label>
													</p>
												</div>
											</div> -->
										</div>
										<div class="col s12 mrg-bot3" id="uniform_details">
											<div class="input-field col s12 m6">
												<label>Dress Code for your Business</label>
												<textarea id="textarea1" class="materialize-textarea" value="{{ $job->uniform }}" name="uniform_details" placeholder="Example: Uniform collared shirts is provided. Wear your own black or dark blue pants with dark colour losed toe shoes." cols="30" rows="10" length="200" maxlength="200">{{ $job->uniform }}</textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-content">
										<div>
											<strong>Contact Preferences</strong>
											<table class="table-static">
												<tbody>
													<tr>
														<td>Contact Email</td>
														<td><input type="text" class="email" name="contact_email" value="{{ $job->contact_email }}"></td>
													</tr>
													<tr>
														<td>Contact Phone</td>
														<td><input type="text" class="phone" name="contact_phone" value="{{ $job->contact_phone }}"></td>
													</tr>
													<tr>
														<td>Contact Link</td>
														<td><input type="text" class="link" name="contact_link" value="{{ $job->contact_link }}"></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div>
									<strong>Tags</strong>
									<select name="tags" multiple class="browser-default tags">
										@foreach($tags as $tag)
										<option value="{{$tag->id}}" {{ (in_array($tag->id, $job->tag_ids)) ? 'selected' : ''}}>{{$tag->name}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						{{-- Applicants --}}
						<div id="applicants" class="col s12">
							<div class="row">
								<div class="col s6">
									<label class="left label-left">Sort:</label>
									<select class="left col s10" id="sort">
										<option value="all" selected >Recommended for this job</option>
										<option value="newest">Newest Applicants</option>
										<option value="oldest">Oldest applicants</option>
										<option value="hours">Most hours worked</option>
										<option value="rejected">Rejected</option>
									</select>
								</div>
								<div class="col s6">
									<p class="line-height50">
										<input type="checkbox" id="favourites">
										<label for="favourites">Only show shortlisted applicants</label>
									</p>
								</div>
							</div>
							<ul class="collection">

							</ul>
							<div class="row">
								<div class="col s12 m6 mrg-tb15">
									<p>Showing <span id="from">0</span> to <span id="to">0</span> of <span id="total">0</span> entries</p>
								</div>
								<div class="col s12 center loadmorediv">
									<a class="btn" onclick="load_applicants();">View more</a>
								</div>
							</div>
						</div>
						{{-- Hired --}}
						<div id="hired" class="col s12">
							<div class="row">
								<div class="col s8">
									<p class="hired-applicants-counts"><i class="mdi-hardware-keyboard-arrow-up"></i> <strong><span id="hire-count">0</span> Hired Applicants</strong> out of <strong><span id="vacancy-count">{{ App\CmpJob::find($id)->vacancy }}</span> Vacancies</strong></p>
								</div>
								<div class="col s4">
									<a href="{{ route('applicants.exportHired', $id) }}" class="btn-flat right teal-text btn-get-csv"><img src="/images/csv-icon.png" class="responsive-img"> Get CSV</a>
								</div>
							</div>
							<ul class="collection">
								{{-- <li class="collection-item avatar">
									<img src="/images/avatar.jpg" alt="" class="circle">
									<span class="title">Title</span>
									<p>First Line
										<a href="#" class="waves-effect waves-light btn deep-purple darken-2 right btn-small">REMOVE</a>
									</p>
								</li>
								<li class="collection-item avatar">
									<i class="mdi-file-folder circle"></i>
									<span class="title">Title</span>
									<p>First Line
										<a href="#" class="waves-effect waves-light btn deep-purple darken-2 right btn-small">REMOVE</a>
									</p>
								</li> --}}
							</ul>
						</div>

						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<!--Applicant Modal Structure -->
<div id="applicant-modal" class="modal applicant-modal">
	<div class="modal-content center">
		<div class="applicant-pic">
			<img src="/images/avatar.jpg" class="responsive-img" alt="Profile Image" id="applicantImage">
		</div>
		<h6><strong class="applicantName"></strong></h6>
		<p><small><i class="mdi-communication-phone"></i><span id="applicantPhone"></span></small><br><small><i class="mdi-communication-email"></i><span id="applicantEmail"></span></small></p>
		<div class="applicant-border">
			<p class="applicant-time">
				<span id="applicantHours">00</span><br>
				<span>HOUR</span>
			</p>
		</div>
		@if ($job->start_date >= date('Y-m-d'))
		<div class="applicant-border" id="hireDiv">
			<button type="button" class="waves-effect waves-light btn teal darken-1 btn-small hire-applicant modal-action modal-close" id="applicantHired">Hire</button>
		</div>
		@endif
		<div class="modal-close-div">
			<a href="#!" class="modal-action modal-close"><i class="mdi-navigation-close"></i></a>
		</div>
	</div>
</div>
<!--Applicant Feedback Modal Structure -->
<div id="applicant-feedback-modal" class="modal">
	<div class="modal-content">
		<h5 class="center">FEEDBACK</h5>
		<div class="display-flex">
			<div class="applicant-pic">
				<img src="/images/avatar.jpg" class="responsive-img" alt="Profile Image">
			</div>
			<div class="applicant-feedback-content">
				<h6><strong class="feedbackName"></strong></h6>
				<label for="experience">Share your experience of James Nelson to the community:</label>
				<div class="input-field col s12">
					<textarea name="experience" class="materialize-textarea validate"rows="10"></textarea>

				</div>
				<label for="user">Quick suggested feedbacks:</label>
				<div class="input-field col s12">
					<select name="feedback" id="user">
						<option value="0"> Please Select --</option>
						<option value="1"> Great Work</option>
						<option value="2"> It was a real pleasure working with him.</option>
						<option value="3"> I'm looking forward to working with him again. Great worker.</option>
					</select>

				</div>
				<p>Would you recommend this worker to a friend or a colleague?</p>
				{{-- <div id="rating"></div> --}}
				<div class="star-rating-div">
					<i class="mdi-action-star-rate active" data-id="1"></i>
					<i class="mdi-action-star-rate" data-id="2"></i>
					<i class="mdi-action-star-rate" data-id="3"></i>
					<i class="mdi-action-star-rate" data-id="4"></i>
					<i class="mdi-action-star-rate" data-id="5"></i>
				</div>
			</div>
		</div>
		<div class="applicant-border center">
			<button type="button" class="waves-effect waves-red btn-flat modal-action modal-close btn-small">CANCEL</button>
			<a href="#applicant-feeback1-modal" class="waves-effect waves-light btn teal darken-1 btn-small" id="feedbackNext">NEXT</a>
		</div>
	</div>
</div>

<!--Applicant Feedback Modal Structure -->
<div id="applicant1-feedback-modal" class="modal">
	<div class="modal-content">
		<h5 class="center">FEEDBACK</h5>
		<div class="display-flex">
			<div class="applicant-pic">
				<img src="/images/avatar.jpg" class="responsive-img" alt="Profile Image">
			</div>
			<div class="applicant-feedback-content">
				<h6><strong class="feedbackName1"></strong></h6>
				<label for="experience">Share your experience of James Nelson to the community:</label>
				<div class="input-field col s12">
					<textarea name="experience1" class="materialize-textarea validate" rows="10" disabled></textarea>

				</div>
				<label for="user">Quick suggested feedbacks:</label>
				<div class="input-field col s12">
					<select name="feedback1" id="user1" disabled>
						<option value="0"> Please Select --</option>
						<option value="1"> Great Work</option>
						<option value="2"> It was a real pleasure working with him.</option>
						<option value="3"> I'm looking forward to working with him again. Great worker.</option>
					</select>

				</div>
				<p>Would you recommend this worker to a friend or a colleague?</p>
				{{-- <div id="rating"></div> --}}
				<div class="star-rating-div">
					<i class="mdi-action-star-rate active" data-id="1"></i>
					<i class="mdi-action-star-rate" data-id="2"></i>
					<i class="mdi-action-star-rate" data-id="3"></i>
					<i class="mdi-action-star-rate" data-id="4"></i>
					<i class="mdi-action-star-rate" data-id="5"></i>
				</div>

				<h6 class="mrg-bot30"><strong class="feedbackName1"></strong></h6>
				<h6><strong>Compliment</strong></h6>
				<div class="row mrg-tb40">
					<div class="col s3 center compliment" data-value="Neat and Tidy">
						<img src="/images/compliments/suit.png" class="feedback-icon responsive-img compliment-img">
						<p><small>Neat and Tidy</small></p>
					</div>
					<div class="col s3 center compliment" data-value="Excellent Service">
						<img src="/images/compliments/jewels.png" class="feedback-icon responsive-img compliment-img">
						<p><small>Excellent Service</small></p>
					</div>
					<div class="col s3 center compliment" data-value="Above and Beyond">
						<img src="/images/compliments/rocket.png" class="feedback-icon responsive-img compliment-img">
						<p><small>Above and Beyond</small></p>
					</div>
					<div class="col s3 center compliment" data-value="Punctuality">
						<img src="/images/compliments/stopwatch.png" class="feedback-icon responsive-img compliment-img">
						<p><small>Punctuality</small></p>
					</div>
					<div class="col s3 center compliment" data-value="Awesomeness">
						<img src="/images/compliments/fireworks.png" class="feedback-icon responsive-img compliment-img">
						<p><small>Awesomeness</small></p>
					</div>
				</div>
			</div>
		</div>
		<div class="applicant-border center">
			<button type="button" class="waves-effect waves-red btn-flat modal-action modal-close btn-small">CANCEL</button>
			<a href="#applicant1-feeback-modal" class="waves-effect waves-light btn teal darken-1 modal-action modal-close btn-small" id="feedbackNext1">OK</a>
		</div>
	</div>
</div>
<!--Applicant Feedback 1 Modal Structure -->
<div id="applicant-feedback1-modal" class="modal">
	<div class="modal-content">
		<h5 class="center">FEEDBACK</h5>
		<div class="display-flex">
			<div class="applicant-pic">
				<img src="/images/avatar.jpg" class="responsive-img" alt="Profile Image">
			</div>
			<div class="applicant-feedback-content">
				<h6 class="mrg-bot30"><strong class="feedbackName"></strong></h6>
				<h6><strong>Give a compliment?</strong></h6>
				<div class="row mrg-tb40">
					<div class="col s3 center compliment" onclick="compliment='Neat and Tidy';">
						<img src="/images/compliments/suit.png" class="feedback-icon responsive-img compliment-img">
						<p><small>Neat and Tidy</small></p>
					</div>
					<div class="col s3 center compliment" onclick="compliment='Excellent Service';">
						<img src="/images/compliments/jewels.png" class="feedback-icon responsive-img compliment-img">
						<p><small>Excellent Service</small></p>
					</div>
					<div class="col s3 center compliment" onclick="compliment='Above and Beyond';">
						<img src="/images/compliments/rocket.png" class="feedback-icon responsive-img compliment-img">
						<p><small>Above and Beyond</small></p>
					</div>
					<div class="col s3 center compliment" onclick="compliment='Punctuality';">
						<img src="/images/compliments/stopwatch.png" class="feedback-icon responsive-img compliment-img">
						<p><small>Punctuality</small></p>
					</div>
					<div class="col s3 center compliment" onclick="compliment='Awesomeness';">
						<img src="/images/compliments/fireworks.png" class="feedback-icon responsive-img compliment-img">
						<p><small>Awesomeness</small></p>
					</div>
				</div>
			</div>
		</div>
		<div class="applicant-border center">
			<button type="button" class="waves-effect waves-red btn-flat modal-action modal-close btn-small">CANCEL</button>
			<button type="submit" class="waves-effect waves-light btn teal darken-1 btn-small" id="submitFeedback">SUBMIT FEEDBACK</button>
		</div>
	</div>
</div>
<!-- loaction modal -->
<!-- Modal Structure -->
<div id="location-modal" class="modal">
	<div class="modal-content">
		<div id="tab2" class="tabContent">
			<div class="col s12 mrg-bot30">
				<h5 class="subtitle">Where is your Job Located?</h5>
				<p class="text-muted">Your location is private, and only confirmed applicants can see your exact address. Your exact address is important for us to provide directions and helpful tips to the applicants before they start work.</p>
			</div>
			<div class="col s12">
				<div class="row">
					<div class="col s12">
						<ul class="tabs" id="maptabs">
							<li class="tab col s3 active"><a class="active" href="#test1">Autosearch</a></li>
							<li class="tab col s3"><a href="#test2">Manual</a></li>
						</ul>
					</div>
					<div id="test1" class="col s12">
						<input id="pac-input" type="text" placeholder="Search Box">
						<div id="map"></div>
						<button id="next1" type="buttton" class="waves-effect waves-light btn light-blue darken-2 right">NEXT</button>

					</div>
					<div id="test2" class="col s12">
						<div class="row">
							<div class=" col s12 m12">
								<div class="col s12">
									<label for="loc_country">Country</label>
									<select name="loc_country" id="countries" class="browser-default">
										<option value="" disabled selected>-Choose-</option>
									</select>
								</div>

								<div class="col s6">
									<label for="loc_state">State</label>
									<select name="loc_state" class="browser-default">
										<option value="" disabled selected>-Choose-</option>
									</select>
								</div>

								<div class="input-field col s6">
									<input type="text" name="loc_city" id="" class="validate" value="{{ $job->city }}">
									<label for="loc_city">City</label>
								</div>

								<div class="input-field col s6">
									<textarea name="street" id="" class="validate materialize-textarea">{{ $job->street }}</textarea>
									<label for="street">Unit, Floor, Building</label>
								</div>

								<div class="input-field col s6">
									<textarea  name="loc_address" class="validate materialize-textarea">{{ $job->location }}</textarea>
									<label for="loc_address">Address</label>
								</div>

								<div class="input-field col s12">
									<input type="text" name="zipcode" class="validate" value="{{ $job->zipcode }}">
									<label for="zipcode">Postal/Zip code</label>
								</div>

								<button id="next2" type="buttton" class="waves-effect waves-light btn light-blue darken-2 right">NEXT</button>

							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col s12 mrgt-15">
				<!-- <button id="back" type="buttton" class="waves-effect waves-light btn light-blue darken-2">BACK</button> -->
				<input type="hidden" id="latitude" name="latitude" value="{{$job->latitude}}">
				<input type="hidden" id="longitude" name="longitude" value="{{$job->longitude}}">
			</div>
		</div>
	</div>
	<!-- <div class="modal-footer">
	<a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
	</div> -->
</div>
@endsection

@section('scripts')
	<script src="/js/materialize/plugins/fullcalendar/lib/moment.min.js" type="text/javascript" charset="utf-8"></script>
	<link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
	<script src="{{asset('js/select2.min.js')}}"></script>
	<script src="{{asset('js/tinymce/tinymce.min.js')}}"></script>
	<!--<link href="{{asset('js/pikaday/pikaday.min.css')}}" rel="stylesheet" />!-->
	<!--<script src="{{ asset('js/pikaday/pikaday.min.js') }}"></script>!-->
<!-- /*	<script src="{{asset('js/jquery.js')}}"></script>*/ -->
	<script src="{{asset('/js/datepicker.js')}}"></script>
	<link href="{{asset('/css/DatePicker-master/css/datepicker/base.css')}}" rel="stylesheet" />
	<link href="{{asset('/css/DatePicker-master/css/datepicker/clean.css')}}" rel="stylesheet" />
	<!-- <link href="{{asset('css/DatePicker-master/css/datepicker/dark.css')}}" rel="stylesheet" /> -->

	<script>

	jQuery.curCSS = function(element, prop, val) {
	    return jQuery(element).css(prop, val);
	};

	$(document).ready(function(){
		$('.edit').click(function(){			
				var req = {};
				req._token  = '{{ csrf_token() }}';
				req.title = $('#title').val();
				req.start_date = $('.start_date').val();
				req.vacancy = $('#vacancy').val();
				console.log(req.vacancy);
				req.working_hours = $('.working_hours').val();
				req.salary = $('.salary').val();
				req.salary_unit = $('#salary_unit').val();
				req.job_type = $('#job_type').val();
				req.status = $('#status').val();
				req.location = $('.location').val();
				req.description = $('textarea#description').val();
				req.nationality = $('#nationality').val();
				req.uniform = $("input[name='uniform']:checked"). val();
				req.uniform_details = $('#textarea1').val();
				req.contact_email= $('.email').val();
				req.contact_phone = $('.phone').val();
				req.contact_link = $('.link').val();
				req.tags = $('.tags').val();
				req.requirements = tinyMCE.activeEditor.getContent();
				console.log(req);

				$.ajax({
					type : 'post',
					url  : '/job/{{$job->id}}/updateAll',
					data : req,
				}).done(function(res){
					tinyMCE.activeEditor.setContent(res.requirements);
					Materialize.toast(res.message,4000);
				});
			
		});


		$('#next1').click(function(e){
			$('ul.tabs').tabs('select_tab','test2');
		});
		$('#next2').click(function(e){
			var req = {};
			req._token  = '{{ csrf_token() }}';
			req.latitude = document.getElementById('latitude').value;
			req.longitude = document.getElementById('longitude').value;
			req.street = $('[name=street]').val();
			req.location = $('[name=loc_address]').val();
			req.city = $('[name=loc_city]').val();
			req.state = $('[name=loc_state]').val();
			req.country = $('[name=loc_country]').val();

			$.ajax({
				type : 'post',
				url  : '/job/{{$job->id}}/postAddress',
				data : req,
			}).done(function(res){
				$('p#addressDiv').html(`${res.job.street} <br>${res.job.location} <br>${res.job.city} <br>${res.job.state} <br>${res.job.country}`);
				$('#location-modal').closeModal();
				Materialize.toast(res.message,4000);
			});
		});
	});

		editor = tinymce.init({
			selector: '#requirements',
			height: 600,
			menubar: false,
			// statusbar: false,
			elementpath: false,
			themes: "modern",
			plugins: [
				' advlist lists'
				/*'advlist autolink lists link image charmap print preview anchor',
				'searchreplace visualblocks code fullscreen',
				'insertdatetime media table contextmenu paste code'*/
			],
			toolbar: 'bullist numlist',
			// toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',

			setup: function(editor) {
				editor.on('change', function(e) {
					document.querySelector('[name="requirements"]').innerHTML = editor.getContent();
					document.getElementById('requirements').innerHTML = editor.getContent();
				});
			}

			// setup: function(editor) {
			// 	editor.on('change', function(e) {
			// 		var request = {};
			// 		request._token  = '{{ csrf_token() }}';
			// 		request.value = editor.getContent();

			// 		$.ajax({
			// 			'type': 'POST',
			// 			'url':  "{{ route('job.show', $job->id) }}/update/requirements",
			// 			'data': request
			// 		}).done(function(response) {

			// 		});
			// 	});
			// }
		});

		var applicant_page = 1, ratings = 1, compliment = "";
		$(document).ready (function() {

			load_hired();
			load_applicants();
			setEvent();

			$('select[name=tags]').select2({
				tags: true
			});

			// $('.datepicker').pickadate({
			// 	selectMonths: true,
			// 	selectYears: 15,
			// 	autoclose: true,
			// 	format: 'ddd, mmm d',
			// 	// formatSubmit: 'yyyy-mm-dd',
			// 	onSet: function() {
			// 		$('input[name=start_date]').change();
			// 	}
			// });
		/*	$('.pikaday').multiDatesPicker();*/
		/*console.log("Pikaday")*/

			/*var picker2months = new Pikaday(
			{
				field: document.querySelector('.pikaday'),
				firstDay: 1,
				// minDate: new Date(2000, 0, 1),
				// maxDate: new Date(2020, 12, 31),
				yearRange: [2017, 2020]
			});*/

			// picker = $('.datepicker').pickadate('picker');
			// picker.set('select', '{{$job->start_date}}', { format: 'yyyy-mm-dd' });


			// picker.set('min',true);

			/*$('input[name],textarea[name],select[name]').on('change', function(e) {

				var request = {};
				request._token  = '{{ csrf_token() }}';
				request.value = $(this).val();

				var arr = ['title', 'salary', 'vacancy', 'contact_email', 'contact_phone', 'contact_link', 'working_days', 'about', 'status', 'start_date', 'requirements', 'location', 'tags', 'type'];

				name = $(this).attr('name');

				if (arr.indexOf(name) != -1)
				{
					$.ajax({
						'type': 'POST',
						'url':  "{{ route('job.show', $job->id) }}/update/"+name,
						'data': request
					}).done(function(response) {

					});
				}
			});*/
			$('#applicant-feedback-modal .mdi-action-star-rate').click(function(e) {
				ratings = $(this).attr('data-id');
			});

			$('#applicant-feedback-modal .mdi-action-star-rate').hover(function() {
				index = $(this).attr('data-id');
				elements = $('#applicant-feedback-modal .mdi-action-star-rate');
				$.each(elements, function(i,el) {

					if (i < index) {
						$(this).addClass('active');
					} else {
						$(this).removeClass('active');
					}
				});

			}, function() {
				elements = $('#applicant-feedback-modal .mdi-action-star-rate');
				$.each(elements, function(i,el) {
					if (i>=ratings) {
						$(this).removeClass('active');
					} else {
						$(this).addClass('active');
					}
				});
			});

			$('#applicant-feedback1-modal .compliment').click(function(e) {
				$('#applicant-feedback1-modal .compliment-img').removeClass('compliment-effect');
				$(this).children().first().addClass('compliment-effect');
			});
		});

		setEvent = function()
		{
			$('#sort').change(function(e) {
				applicant_page = 1;
				$('#applicants ul.collection').empty();
				load_applicants();
			});

			$('#favourites').click(function(e) {
				applicant_page = 1;
				$('#applicants ul.collection').empty();
				load_applicants();
			});
		}

		setEvents = function()
		{
			$('[data-href]').click(function(e){
				e.preventDefault();
				url = $(this).attr('data-href');
				$.ajax({
					type : 'get',
					url  : url
				}).done(function(res) {
					Materialize.toast(res.message, 1500);
					load_applicants();
					load_hired();
				});
			});

			$('.hire-applicant').off('click');
			$('.hire-applicant').click(function(e){
				e.preventDefault();
				id = $(this).attr('data-id');
				$.ajax({
					type : 'get',
					url  : '{{ route('applicants.index') }}/' + id + '/status/2',
				}).done(function(res) {
					Materialize.toast(res.message, 1000);
					if(res.status == 1){
						$('#sort').change();
						load_hired();
					}
				});
			});

			$('.star-applicant').off('click');
			$('.star-applicant').click(function(e){
				e.preventDefault();
				ob = this;
				id = $(this).attr('data-id');
				star = ($(ob).attr('data-starred') == 1) ? 0 : 1;
				$.ajax({
					type : 'get',
					url  : '{{ route('applicants.index') }}/' + id + '/starred/'+star,
				}).done(function(res) {
					Materialize.toast(res.message, 1000);
					if (res.status == 1) {
						$(ob).attr('data-starred', star);
						$(ob).children().first().toggleClass('grey-text').toggleClass('pink-text');
					}
				});
			});

			$('.delete-applicant').off('click');
			$('.delete-applicant').click(function(e){
				e.preventDefault();
				ob = this;
				id = $(this).attr('data-id');

				$.ajax({
					type : 'GET',
					// url  : '{{ route('applicants.index') }}/' + id +'/destroy',
					url  : '{{ route('applicants.index') }}/' + id +'/status/4',
				}).done(function(res) {
					Materialize.toast(res.message, 1000);
					$('#sort').change();
					load_hired();
				});
			});

			$('.remove-applicant').off('click');
			$('.remove-applicant').click(function(e){
				e.preventDefault();
				id = $(this).attr('data-id');
				$.ajax({
					type : 'get',
					url  : '{{ route('applicants.index') }}/' + id + '/status/1',
				}).done(function(res) {
					Materialize.toast(res.message, 1000);
					$('#sort').change();
					load_hired();
				});
			});
			$('.modal-trigger.applicant-trigger').off('click');
			$('.modal-trigger.applicant-trigger').click(function(e) {
				$('#applicantHired').attr('data-id', $(this).attr('data-id'));
				$('#applicantName').html($(this).attr('data-name'));
				$('#applicantEmail').html($(this).attr('data-email'));
				$('#applicantHours').html($(this).attr('data-working'));
				$('#applicantPhone').html($(this).attr('data-phone'));
				// $('#applicantImage').attr('src', $(this).attr('data-image_url'));

				if ($(this).attr('data-hired') == 'true') {
					$('#hireDiv').addClass('hide');
				} else {
					$('#hireDiv').removeClass('hide');
				}
				$('#applicant-modal').openModal();
			});

			$('.modal-trigger.hired-trigger').off('click');
			$('.modal-trigger.hired-trigger').click(function(e) {
				// $('#applicantHired').attr('data-id', $(this).attr('data-id'));
				$('.feedbackName').html($(this).attr('data-name'));
				$('#submitFeedback').attr('data-id', $(this).attr('data-id'));

				// $('#applicantEmail').html($(this).attr('data-email'));
				// $('#applicantHours').html($(this).attr('data-working'));
				// $('#applicantPhone').html($(this).attr('data-phone'));
				// $('#applicantImage').attr('src', $(this).attr('data-image_url'));
				$('#applicant-feedback-modal').openModal();
			});

			$('.modal-trigger.given-trigger').off('click');
			$('.modal-trigger.given-trigger').click(function(e) {
				$('.feedbackName1').html($(this).attr('data-name'));
				$('[name=feedback1] option[value='+$(this).attr('data-feedback')+']').attr('selected',true);
				$('select').material_select();

				$('#applicant1-feedback-modal [data-value] img').removeClass('compliment-effect');
				$('#applicant1-feedback-modal [data-value="'+$(this).attr('data-compliment')+'"] img').addClass('compliment-effect');
				$('#applicant1-feedback-modal .mdi-action-star-rate').removeClass('active');
				stars = $('#applicant1-feedback-modal .mdi-action-star-rate');
				ratings = $(this).attr('data-ratings');

				$.each(stars, function(i,s) {
					if (i < ratings) {
						$(s).addClass('active');
					}
				});

				$('[name=experience1]').html($(this).attr('data-experience'));
				$('#applicant1-feedback-modal').openModal();
			});

			$('#feedbackNext').off('click');
			$('#feedbackNext').click(function(e) {
				$('#applicant-feedback-modal').closeModal();
				$('#applicant-feedback1-modal').openModal();
				// $('#submitFeedback').attr('data-id', );
			});

			$('#submitFeedback').off('click');
			$('#submitFeedback').click(function(e) {
				$('#applicant-feedback1-modal').closeModal();
				var request = {};
				request._token  = '{{ csrf_token() }}';
				request.rating = ratings;
				request.compliment = compliment;
				request.feedback = $('[name=feedback]').val();
				request.experience = $('[name=experience]').val();
				id = $(this).attr('data-id');
				$.ajax({
					'type': 'POST',
					'url':  '/cmp/applicants/'+id+'/feedback',
					'data': request
				}).done(function(response) {
					Materialize.toast(response.message, 1500);
					load_hired();
				});

			});
		}


		load_applicants = function()
		{
			if (applicant_page != 0)
			{
				var req = {};
				req.sort = $('#sort').val();
				fav = document.getElementById('favourites');
				req.favourites = (fav.checked) ? 1 : 0;
				req._token  = '{{ csrf_token() }}';

				$.ajax({
					type : 'post',
					url  : '{{ route('job.applicants', $id) }}?page='+applicant_page,
					data : req,
				}).done(function(res) {
					$('.pikaday').DatePicker({
						mode: 'multiple',
						onChange: function(date, el) {
							/*console.log(date);*/
							if(date.length) {
								dateArray = [];
								date.forEach(function(d){
									dateArray.push(moment(d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate()).format('YYYY-MM-DD'))
								})
								$(el).val(dateArray.join(','))
							}
						}
					});
					fillApplicants(res, req.sort);
				});
			}
		}

		load_hired = function()
		{
			$.ajax({
				type : 'get',
				url  : '{{ route('job.hired', $id) }}',
			}).done(function(res) {
				fillhired(res);
			});
		}

		fillApplicants = function(result, sort)
		{
			html = '';

			if (!result.next_page_url) {
				$('.loadmorediv').addClass('hide');
			} else {
				$('.loadmorediv').removeClass('hide');
			}

			if (result.data.length == 0)
			{
				html += '<li class="collection-item avatar">';
				html += '<h5 class="center"><span>No Applicants found for this job.</span></h5>';
				html += '</li>';
				$('#applicants ul.collection').html(html);
				$('.loadmorediv').addClass('hide');
			}
			else
			{
				result.data.forEach(function(applicant)
				{
					html += '<li class="collection-item avatar">';
					html += '<img src="/images/avatar.jpg" alt="" class="circle">';
					html += '<a href="#manage" class="modal-trigger applicant-trigger" data-id="'+applicant.id+'" data-email="'+applicant.email+'" data-name="'+applicant.name+'" data-image_url="'+applicant.image_url+'" data-working="'+applicant.working_hours+'" data-phone="'+applicant.phone+'" data-hired="false"><span class="title"><strong>' + applicant.name + '</strong></span></a>';
					html += '<p><strong>' + applicant.working_hours + '</strong> hrs';

					if(sort != 'rejected')
					{
						html += '<a class="btn-floating waves-effect waves-light right btn-small delete-applicant" data-id="'+applicant.id+'"><i class="mdi-content-clear"></i></a>';

						@if ($job->start_date >= date('Y-m-d'))
							html += '<a data-id="'+applicant.id+'" class="waves-effect waves-light btn teal darken-1 right btn-small hire-applicant">HIRE</a>';
						@endif

						html += '</p>';
						html += '<a data-id="'+applicant.id+'" data-starred="'+applicant.starred+'" class="secondary-content star-applicant" href="#!"><i class="mdi-action-grade ';
						html += (!applicant.starred) ? 'grey-text': 'pink-text';
						html += '"></i></a>';
					}

					html += '</li>';
				});

				$('#applicants ul.collection').append(html);
				setEvents();
			}

			to = (result.to) ? result.to : 0;
			$('#to').html(to);
			$('#from').html((result.total) ? 1 : 0);
			$('#total').html(result.total);
			$('#applicants-count').html(result.total);
			applicant_page = (result.current_page == result.last_page) ? 0 : applicant_page + 1;
		}

		fillhired = function(result)
		{
			html = '';
			if (result.length == 0)
			{
				html += '<li class="collection-item avatar">';
				html += '<h5 class="center"><span>No Hired Applicants found for this job.</span></h5>';
				html += '</li>';
			}
			else
			{
				result.forEach(function(applicant)
				{
					html += '<li class="collection-item avatar">';
					html += '<i class="mdi-file-folder circle"></i>';
					html += '<a href="#manage" class="modal-trigger applicant-trigger" data-id="'+applicant.id+'" data-email="'+applicant.email+'" data-name="'+applicant.name+'" data-image_url="'+applicant.image_url+'" data-working="'+applicant.working_hours+'" data-phone="'+applicant.phone+'" data-hired="true"><span class="title">' + applicant.name + '</span></a>';
					html += '<p><strong>' + applicant.working_hours + '</strong> hrs';

					@if ($job->start_date > date('Y-m-d'))
					html += '<a href="#" data-id="'+applicant.id+'" class="waves-effect waves-light btn deep-purple darken-2 right btn-small remove-applicant">REMOVE</a>';
					@endif

					if (!applicant.has_feedback) {
						html += '<a href="#applicant-feedback-modal" data-id="'+applicant.id+'" data-name="'+applicant.name+'" class="waves-effect waves-light btn right btn-small modal-trigger hired-trigger teal lighten-2">GIVE FEEDBACK</a>';
					} else {
						html += '<a href="#applicant1-feedback-modal" data-id="'+applicant.id+'" data-name="'+applicant.name+'" class="waves-effect waves-light btn deep-purple darken-2 right btn-small modal-trigger given-trigger" data-experience="'+applicant.feedback.experience+'" data-feedback="'+applicant.feedback.feedback+'" data-compliment="'+applicant.feedback.compliment+'" data-ratings="'+applicant.feedback.ratings+'">FEEDBACK</a>';
					}

					html += '</p>';
					html += '</li>';
				});
			}

			$('#hired ul').html(html);
			$('#hire-count').html(result.length);
			$('#hired-count').html(result.length);

			setEvents();
		}

		var marker, _place = {
				state: '{{$job->state}}',
				city: '{{$job->city}}',
				country: '{{$job->country}}',
				postal_code: '{{$job->zipcode}}'
			};
		var  api_key = 'AIzaSyDFvcodwxV3SQptSx4LQ-H-gZVcAk-1lyI';

		initAutocomplete = function () {

			if (document.getElementById('latitude').value > 0) {
				var myLatLng = new google.maps.LatLng(
					document.getElementById('latitude').value,
					document.getElementById('longitude').value);
			} else {
				var myLatLng = new google.maps.LatLng(3.14, 101.6956316);
			}

			var map = new google.maps.Map(document.getElementById('map'), {
				center: myLatLng,
				zoom: 8
			});

			var map = new google.maps.Map(document.getElementById('map'), {
				center: myLatLng,
				zoom: 8
			});

			if(marker) {
				marker.setMap(null);
			}

			if(!marker) {
				marker = new google.maps.Marker({
					position: myLatLng,
					map: map,
					title: 'Location',
					draggable: true
				});
			}

			window.dispatchEvent( new Event('resize') );

			map.setCenter(marker.getPosition());
			google.maps.event.addDomListener(window, 'resize', function() {

				//console.log('window resize');
				map.setZoom(17);
				map.panTo(marker.position);
				map.setCenter(marker.position);
				lat = marker.getPosition().lat();
				lng = marker.getPosition().lng();

				$('input[name=latitude]').val(lat);
				$('input[name=longitude]').val(lng);
				map.setCenter(marker.getPosition());
				google.maps.event.trigger(map, 'resize');
			});

			google.maps.event.addListener(marker, 'dragend', function (event) {
				console.log('marker dragend')
				map.setZoom(17);
				map.panTo(marker.position);
				map.setCenter(marker.position);
				lat = marker.getPosition().lat();
				lng = marker.getPosition().lng();

				$('input[name=latitude]').val(lat);
				$('input[name=longitude]').val(lng);
				updatePlace();
			});

			// Create the search box and link it to the UI element.
			var input = document.getElementById('pac-input');
			var searchBox = new google.maps.places.SearchBox(input);
			map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

			// Bias the SearchBox results towards current map's viewport.
			map.addListener('bounds_changed', function() {
				console.log('bounds changed')

				searchBox.setBounds(map.getBounds());

				lat = marker.getPosition().lat();
				lng = marker.getPosition().lng();

				$('input[name=latitude]').val(lat);
				$('input[name=longitude]').val(lng);

				updatePlace();
			});

			searchBox.addListener('places_changed', function() {
				var places = searchBox.getPlaces();

				if (places.length == 0) {
					return;
				}

				marker.setMap(null);
				flag = 0; // only one marker
				places.forEach(function(place) {

					if(flag == 0)
					{
						if (!place.geometry) {
						  console.log("Returned place contains no geometry");
						  return;
						}

						map.setCenter(place.geometry.location);

						updatePlace();
						// console.log(place.geometry.location);

						marker = new google.maps.Marker({
							position: place.geometry.location,
							map: map,
							title: 'Location',
							draggable: true
						});

						setTimeout(function(){window.dispatchEvent( new Event('resize') );}, 500);

						flag++;
					}
				});
			});
		}

		function createMarker(latlng, name) {
			marker = new google.maps.Marker({
				position: latlng,
				map: map,
				// icon: icn,
				title: name
			});

			map.setZoom(17);
			map.panTo(marker.position);
			map.setCenter(marker.position);
			updatePlace();
		}

		function updatePlace()
		{
			if(marker)
			{
				lat = marker.getPosition().lat();
				lng = marker.getPosition().lng();

				$('input[name=latitude]').val(lat);
				$('input[name=longitude]').val(lng);

				$.ajax({
					type : 'get',
					url  : 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lng+'&key='+api_key,
				}).done(function(res) {
					if(res.status == 'OK')
					{
						if(res.results.length > 0){
							ac = res.results[0].address_components;
						}
						_place.address = res.results[0].formatted_address;

						ac.forEach(function(address){
							if(address.types.indexOf('country') != -1){
								_place.country = address.long_name;
								_place._country = address.short_name;
							}
							if(address.types.indexOf('administrative_area_level_1') != -1){
								_place.state = address.long_name;
								_place._state = address.short_name;
							}
							if(address.types.indexOf('locality') != -1){
								_place.city = address.long_name;
								_place._city = address.short_name;
							}
							if(address.types.indexOf('route') != -1){
								_place.route = address.long_name;
								_place._route = address.short_name;
							}
							if(address.types.indexOf('postal_code') != -1){
								_place.postal_code = address.long_name;
							}
						});

						$('input[name=loc_city]').val(_place.city);
						$('input[name=zipcode]').val(_place.postal_code);
						$('select[name=loc_country]').val(_place.country).change();
						$('[name=loc_address]').val(_place.address);

						var address_chunks = _place.address.split(',');
						if(address_chunks.length > 3) {
							$('[name=loc_address]').val(address_chunks[1] + address_chunks[2]);
							$('[name=street]').val(address_chunks[0]);
						}
						Materialize.updateTextFields();

					}
				});
			}
		}
	</script>

	<script>
		var countries = {};
		$(document).ready(function(){
			getCountries();


			$('select[name=loc_country]').change(function(e) {

				val = $(this).val();
				id = $('option[value="'+val+'"]').attr('data-id');
				$.ajax({
					type : 'get',
					url  : '{{ route('country.index') }}/'+id,
				}).done(function(res) {
					$('select[name=loc_state]').html(res);
					if(_place.state){
						$('select[name=loc_state]').val(_place.state);
					}

					$('select').material_select();
				});
			});
		});

		function getCountries()
		{
			$.ajax({
				type : 'get',
				url  : '{{ route('country.index') }}',
			}).done(function(res) {
				countries = res;
				fillCountries(res);
			});
		}

		function fillCountries(res)
		{
			html = '';
			res.forEach(function(c) {
				if(c.name == '{{$job->country}}') {
					html += '<option value="'+c.name+'" data-id="'+c.id+'" selected>'+c.name+'</option>';
				} else {
					html += '<option value="'+c.name+'" data-id="'+c.id+'">'+c.name+'</option>';
				}
			});
			$('#countries').html(html);
			$('select').material_select();
			$('select[name=loc_country]').change();
		}

		$(document).ready(function(){
			/*$("#yes").click(function(){
				$("#uniform_details").addClass('hide');
			});
			$("#no").click(function(){
				$("#uniform_details").removeClass('hide');
			});*/
			saveAvatar()
		});

		function saveAvatar () {
			$('#avatar').on('change', function(event) {
				$.ajax({
					type : 'post',
					url : '/job/{{$job->id}}/update-avatar',
					processData: false,
					contentType: false,
					data : new FormData($(this).parents('form').get(0))
				}).done(function (response) {
					console.log(response);
				})
			});
		}
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFvcodwxV3SQptSx4LQ-H-gZVcAk-1lyI&libraries=places&callback=initAutocomplete"></script>

@endsection
