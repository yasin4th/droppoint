@php

	$i = \Carbon\Carbon::now();
	$day = $tmp = $i->dayOfWeek;

	$html = "<div class='date'><b><span class='month-alignment' >{$i->format('M')}</span></b>";
	$htmlMonths = "<div class='months'>";
	$months = [];

	while($tmp != 0) {
		$html .= "<span></span>";
	//	dd($html);
		$tmp--;
	}

	$tmp = 1;
	while($tmp < 93) {
		$holiday = \App\Holidays::where('holidaydate',$i->format('Y-m-d'))->where('holidaytype','PH')->first();
		if($holiday) {
			$html .= "<span onclick='selectDay(this);' class='holiday-default holiday tooltipped' data-position='right' data-delay='50' data-tooltip='{$holiday->description}' val='{$i->format('Y-m-d')}'>{$i->day}</span>";
		} else {
			$html .= "<span onclick='selectDay(this);' val='{$i->format('Y-m-d')}'>{$i->day}</span>";
		}

		if($i->dayOfWeek == 6 && $tmp == 92) {
			$html .= "</div>";
		}
		if($i->dayOfWeek == 6 && $tmp < 92) {
			$html .= "</div><div class='date'>";
			$j = \Carbon\Carbon::createFromFormat('d-m-Y', $i->format('d-m-Y'))->addDay();
			if($i->format('m') == $j->format('m')) {
				$html .= "<span style='background: #fff; font-weight:bold' ></span>";
			}
		}
		if($i->dayOfWeek < 6 && $tmp == 92) {
			$html .= "</div>";
		}
		// echo $tmp. " " . $i->dayOfWeek ." \n";
		$i->addDay();
		$tmp++;



		if(!in_array($i->format('M'), $months)) {
			//array_push($months, $i->format('M'));
		}

		if($i->day == 1) {
			$num = $i->dayOfWeek;
			//dd($num);
			if($num != 0) {
				for($z = 1; $z <= 7; $z++) {
					$html .= "<span></span>";
					if(($z+$num) == 7 ) {
						$html .= "</div><div class='date'><b><span class='month-alignment' >{$i->format('M')}</span></b>";
						//dd($html);
					}
				}
			}
			else{
			
				//$html = substr($html, 0, strlen($html) - 57);
				$html .= "<b><span class='month-alignment' >{$i->format('M')}</span></b>";
			}
		}
	}

	foreach ($months as $key => $month) {
		// $htmlMonths .= "<span class='month{$key}'>{$month}</span>";
	}
	$htmlMonths .= "</div>";
@endphp


@extends('layouts.material')

@section('content')

<style>
	.date span {
		cursor: pointer;
	}
	.date span.holiday {
		background-color: orange;
	}
	.date span.purple {
		background-color: #7030a0;
		color: #fff;
	}
</style>
<div class="row">
	<!-- <div class="col s12 m3">
		@if(Auth::user()->isAdmin())
			@include('includes.admin.sidemenu')
		@else
			@include('includes.sidemenu')
		@endif
	</div> -->
	<div class="col m12">
		<div id="rootwizard1" class="form-wizard form-wizard-horizontal">
			<form class="form floating-label" method="POST" action="/job" onsubmit="document.forms.create.newtag.value = $('#select_key').val(); if(!validate(4)) {return false}; myButton.disabled = true; return true;" enctype="multipart/form-data" name="create">
				<input type="hidden" name="newtag">
				{{ csrf_field() }}

				<div class="form-wizard-nav">
					<div class="progress" style="width: 66.667%;"><div class="progress-bar progress-bar-primary"></div></div>
					<ul class="nav nav-justified nav-pills tabs" id="toptabs">
						<li class="tab active"><a href="#tab1" class="active"><span class="step">1</span> <span class="title">Details</span></a></li>
						<li class="tab"><a href="#tab2"><span class="step">2</span> <span class="title">Tags</span></a></li>
						<li class="tab"><a href="#tab3"><span class="step">3</span> <span class="title">Location</span></a></li>
					</ul>
				</div>
				<h5>Post a Job</h5>

				<div class="card">
					<div class="card-content create_card">
						<div id="tab1" class="tabContent">
							@if(Auth::user()->isCompany())
								<div class="col s12 mrg-bot30">
									<h5 class="subtitle">Recent Jobs & Templates</h5>
									<div class="row">
										@php
											$jobs = Auth::user()->company->jobs()->orderBy('created_at','desc')->get();
											$tmpJobs=DB::table('jobTemplates')->where('status','=','1')->whereNull('deleted_at')->distinct('[title]')->orderBy('created_at','desc')->get();
										@endphp

										<div class="input-field col s12 m6">
											<select class="selectpicker"  id="selectBox" onchange="changeFunc(value);" data-placeholder="-- Recent posted jobs and templates --" data-allow-clear="true">
												<option value="" disabled="true" selected="">-- Select previous job --</option>
												@foreach($jobs as $j)
												<option value="j.{{ $j->id}}"> {{ $j->title }} </option>
												@endforeach
												@foreach($tmpJobs as $jb)
 												<option value="t.{{ $jb->id}}"> {{$jb->title.'(Template)'}} </option>
 												@endforeach
											</select>
										</div>
										<!-- </div> -->
										<div class="input-field col s12 m3" id="close" style="display: none;">
											<a href="{{ route('job.create') }}" class="waves-effect waves-light job-close" onclick ="document.forms.create.reset();" style="color: white;"><i class="material-icons">close</i></a>
										</div>
									</div>
								</div>
							@endif


							@if(Auth::user()->isAdmin())
								@php
									$users = App\User::where('role', 2)->orderBy('name')->get();
								@endphp
								<div class="input-field col s12 m6">
									<select name="user_id" id="user">
										<option value=""> Please Select --</option>
										@foreach($users as $user)
											<option value="{{ $user->id }}"> {{ $user->name }}</option>
										@endforeach
									</select>
									<label for="user">Please select a company</label>
								</div>
							@endif

							<div class="col s12 mrg-bot15">
								<h5 class="subtitle">Describe the Job</h5>
							</div>

							<div class="col s12 mrg-bot15">
								<b>What type of job do you have and how many do you need to hire?</b>
							</div>
							<div class="col s6 m3">
								<select class="browser-default validate" name="type" id="time">
									<option value="part-time">Part time</option>
									<option value="full-time">Full time</option>
								</select>
							</div>
							<div class="col s6 m3">
								<select name="vacancy" class="browser-default validate" id="vacancy">
									<option value = "1">I want to hire one person</option>
									<option value = "2">I have 2 vacancies  </option>
									<option value = "3">I have 3 vacancies  </option>
									<option value = "4">I have 4 vacancies  </option>
									<option value = "5">I have 5 vacancies  </option>
									<option value = "6">I have 6 vacancies  </option>
									<option value = "7">I have 7 vacancies  </option>
									<option value = "8">I have 8 vacancies  </option>
									<option value = "9">I have 9 vacancies  </option>
									<option value ="10">I have 10 vacancies </option>
								</select>
							</div>
							<div class="col s7 mrg-bot60"></div>
							<div class="col s12">
								<b>Job Details</b>
							</div>
							<div class="{{ $errors->has('title') ? ' has-error' : '' }}">
								<div class="input-field col s12 m6 mrg-t30">
									<input type="text" name="title" class="validate autocomplete" placeholder="Be simple & specific! Example: Fashion Retail Assistant" id="title">
									<label for="title">Job Title</label>
									@if ($errors->has('title'))
									<span class="help-block">
										<strong>{{ $errors->first('title') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="col s7"></div>
							<!-- <div class="input-field col s12 m6">
								<textarea name="about" id="aboutWork" class="materialize-textarea validate" placeholder="Example: I need someone who has great customer service skills to interact with our customers with a friendly, helpful attitude. You will also need to help with merchandising, housekeeping and other general duties." cols="30" rows="10" length="500" maxlength="500"></textarea>
								<label for="about">Briefly the work to be done</label>
								{{-- <span class="character-counter" style="float: right; font-size: 12px; height: 1px;"> --}}
								</span>
							</div> -->
							<div class="input-field col s12">
								<p class="mrg-bot10"><b>Roles and Responsibilities</b></p>
								<p class="text-muted">Provide the tasks, roles and responsibilities to help the applicant understand what tasks he/she will and will not perform. List the most important duties first and be sure to include specific information regarding frequency of tasks. Example: You will spend 20 percent of the day updating Daily Sales Reports.</p>
								<textarea cols="8" rows="6" class="textarea-hauto validate"  id="requirements" name="requirements" placeholder="Type Here" wrap="hard" ></textarea>
							</div>

							<div class="col s7 mrg-bot30"></div>

							<div class="col s12 m6 mrg-bot30">
								<b>Upload a picture to attract more candidates</b>
								<form action="#">
									<div class="file-field input-field">
										<div class="btn light-blue darken-2">
											<span>Upload</span>
											<input type="file" name="avatar">
										</div>
										<div class="file-path-wrapper">
											<input class="file-path validate" type="text" placeholder="Upload any image"  id="cover_picture">
										</div>
									</div>
								</form>
							</div>

							<div class="col s12 mrg-bot30">
								<b>Do you provide full uniform?</b>
								<div class="row">
									<div class="input-field col s12 m6">
										<p>
											<input name="uniform" type="radio" id="yes" value="1" checked="">
											<label for="yes">Yes</label>
										</p>
										<p>
											<input name="uniform" type="radio" id="no" value="0">
											<label for="no">No</label>
										</p>
									</div>
								</div>
							</div>

							<div class="col s12 mrg-bot30 hide" id="uniform_details">
								<div class="input-field col s12 m6">
									<label>Dress Code for your Business</label>
									<textarea id="textarea1" class="materialize-textarea" name="uniform_details" placeholder="Example: Uniform collared shirts is provided. Wear your own black or dark blue pants with dark colour losed toe shoes." cols="30" rows="10" length="200" maxlength="200"></textarea>
								</div>
							</div>

							<div class="col s7 mrg-bot30"></div>

							<div class="col s12 mrg-bot15">
								<h5 class="subtitle">Rate and Availability</h5>
							</div>
							<input type="hidden" name="start_date">

							<!-- <div class="col s12 mrg-bot30">
								<b>When do you expect this job to start?</b>
								<div class="row">
									<div class="input-field col s12 m6">
										<input name="start_date" type="date" class="datepicker validate" id="start_date">
										<label for="start_date">Working Start Date</label>
									</div>
								</div>
							</div> -->
							<div class="col s12 mrg-bot30">
								<b>Set the working days required for the next 3 months</b>
								<div class="row">
									<div class="col s12 m12">
										<div class="custom-calender">
											{!!$htmlMonths!!}
											<!-- <div class="months">
												<span>Dec 2017</span>
												<span class="month1">Jan 2017</span>
												<span class="month2">Feb 2017</span>
												<span class="month3">Mar 2017</span>
											</div> -->
											<div class="days">
												<span>Sun</span>
												<span>Mon</span>
												<span>Tue</span>
												<span>Wed</span>
												<span>Thu</span>
												<span>Fri</span>
												<span>Sat</span>
											</div>
											{!!$html!!}
										</div>
									</div>
								</div>
							</div>
							<div class="col s12 mrg-bot15">
								<b>What are the working hours for this job?</b>
								<div class="row">
									<div class="input-field col s6 m3">
										<input name="start_time" id="start_time" class="timepicker validate" type="time" value="09:00">
										<label for="start_time">Start Time</label>
									</div>
									<div class="input-field col s6 m3">
										<input name="end_time" id="end_time" class="timepicker validate" type="time" value="17:00">
										<label for="end_time">End Time</label>
									</div>
								</div>
							</div>

							<div class="input-field col s12 inline-radio-input mrg-bot30"  id="payment_details" >
								<b id="pay">How much are you paying per day?</b>
								<p>
									<input name="salary" type="radio" id="salary1" value="50" checked="">
									<label for="salary1">RM 50</label>
								</p>
								<p>
									<input name="salary" type="radio" id="salary2" value="80">
									<label for="salary2">RM 80</label>
								</p>
								<p>
									<input name="salary" type="radio" id="salary3" value="120">
									<label for="salary3">RM 120</label>
								</p>
								<p>
									<input name="salary" type="radio" id="salary4" value="other">
									<label for="salary4">Pick Amount (RM)</label>
								</p>
							</div>
							<div class="col s12 hide" id="byDay">
								<div class="input-field col s6 m3 rm-amount">
									<span>RM</span> 
									<input name="amount" id="amount" type="number" step="any">
									<label for="amount">Enter amount</label>
								</div>
								<div class="input-field col s6 m3">
									@php
										$unit = DB::table('ref_job_salary')->where('status','Active')->get();
									@endphp
									<select name="payment" >
										@foreach($unit as $u)
										<option value="{{$u->name}}" name="payment" selected>{{$u->display}}</option>
										@endforeach
									</select>
									<label for="byDay">Pay by</label>
								</div>
							</div>
							<div class="col s12 mrg-bot30"></div>

							<div class="col s12 mrg-bot30">
								<b>Filter to only Malaysian applicants?</b>
								<div class="row">
									<div class="col s12 m6">
										<p class="text-muted"></p>
										@php
										$nationality = DB::table('ref_nationality')->get();
										@endphp
										<select name="nationality">
											@foreach($nationality as $na)
											<option value="{{$na->value}}" {{($na->value == 'Malaysian' ? 'selected' : '')}}  >{{$na->value}}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>

							<div class="col s12 mrg-bot15">
									<h5 class="subtitle">Contact Preferences</h5>
							</div>

							<div class="col s12 mrg-bot30">
								<b>Do you want applicants to contact you directly?</b>
								<div class="row">
									<div class="col s12 m6">
										<div class="row">
											<div class="input-field col s12 m12">
												<input list="data_email" type="text" name="contact_email" class="validate" id="contact_emails" >
												@if(Auth::user()->isCompany())
												<datalist id="data_email">
													@php
														$jobs = Auth::user()->company->jobs;
													@endphp
													@foreach($jobs->unique('contact_email')->take(5) as $job)
														<option value="{{$job->contact_email }}">
													@endforeach
												</datalist>
												@endif
												<label for="contact_email">Email</label>
											</div>
										</div>
										<div class="row">
											<div class="input-field col s12 m12">
												<input list="data_phone" type="number" name="contact_phone" class="validate no-spinners" value="" id= "contact_phone">
												@if(Auth::user()->isCompany())
													<datalist id="data_phone">
													@foreach($jobs->unique('contact_phone')->take(5) as $job)
														<option value="{{$job->contact_phone }}">
													@endforeach
													</datalist>
												@endif
												<label for="contact_phone">Phone</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col s12">
								<b>Do you have an external link to be shared? (optional)</b>
								<div class="row">
									<div class="input-field col s12 m6">
										<input type="text" name="contact_link" class="validate" id="contact_link">
										<label for="link">Link</label>
									</div>
								</div>
								<br>
							</div>
						</div><!--end #tab1 -->
						<div id="tab3" class="tabContent">
							<div class="col s12 mrg-bot30">
								<h5 class="subtitle">Where is your Job Located?</h5>
								<p class="text-muted">Your location is private, and only confirmed applicants can see your exact address. Your exact address is important for us to provide directions and helpful tips to the applicants before they start work.</p>
							</div>
							<div class="col s12">
								<div class="row">
									<div class="col s12">
										<ul class="tabs" id="maptabs">
											<li class="tab col s3 active"><a class="active" href="#test1">Autosearch</a></li>
											<li class="tab col s3"><a href="#test2">Confirm Address</a></li>
										</ul>
									</div>
									<div id="test1" class="col s12">
										<input id="pac-input" type="text" placeholder="Search Box">
										<div id="map">
										</div>
									</div>
									<div id="test2" class="col s12">
										<div class="row">
											<div class=" col s12 m12">
												<div class="col s12">
													<label for="loc_country">Country</label>
													<select name="loc_country" id="countries" class="browser-default">
														<option value="" disabled selected>-Choose-</option>
													</select>
												</div>

												<div class="col s6">
													<label for="loc_state">State</label>
													<select name="loc_state"  id="state" class="browser-default loc_state">
														<option value="" disabled selected>-Choose-</option>
													</select>
												</div>

												<div class="input-field col s6">
													<input type="text" name="loc_city" id="loc_city" class="validate" style="padding-top: 5px;">
													<label for="loc_city">City</label>
												</div>

												<div class="input-field col s12">
													<textarea name="street" id="floor" class="validate materialize-textarea"></textarea>
													<label for="street">Unit,  Floor,  Building</label>
												</div>

												<div class="input-field col s12">
													<textarea  name="loc_address" id="loc_address" class="validate materialize-textarea"></textarea>
													<label for="loc_address">Address</label>
												</div>

												<div class="input-field col s12">
													<input type="text" name="zipcode" id="zipcode" class="validate">
													<label for="zipcode">Postal/Zip code</label>
												</div>
											</div>
											<!-- <div class="col s1"></div> -->
											<!-- <div class="col s12 m4 right">
												<div class="card-panel hoverable">
													<i class="material-icons">info_outline</i>
													<p>Providing the  essentials helps guests feel at home in your place.</p>
													<br>
													<p>Some hosts provide breakfast, or just coffee and tea. None of these things are required, but sometimes they add a nice touch to help guests feel welcome.</p>
												</div>
											</div> -->
										</div>
									</div>
								</div>
							</div>
						</div><!--end #tab2 -->
						<div id="tab2" class="tabContent mrg-l15">
							<div class="col s12 mrg-bot30">
								<h5 class="subtitle tooltipped" data-tooltip="In this section, your selections will appear as tags within the application.
									Tags are a great way for people to search for jobs that have a common topic.">Job Requirements</h5>
								<p class="text-muted">Select the qualifications and requirements needed for applicants to apply for this job.</p>
							</div>
							<div class="">
								<div class="col s12">
									<p>
										<input type="checkbox" id="skill4" name="skill4" />
										<label for="skill4">Minimum Education</label>
									</p>
									<div class="col s12 skillContent">
									@php
									$tags = DB::table('tags')->get();
									@endphp
									@foreach($tags as $tag)
										@if($tag->category == 'education')
											<p>
												<input type="checkbox" name="edu[]" id="edu{{$tag->id}}" value="{{$tag->id}}" />
												<label for="edu{{$tag->id}}">{{$tag->name}}</label>
											</p>
										@endif
									@endforeach
										<!-- <p>
											<input type="checkbox" name="edu[]" id="edu2" value="15" />
											<label for="edu2">SPM</label>
										</p> -->
									</div>
								</div>
								<div class="col s12">
									<p>
										<input type="checkbox" id="skill5" name="skill5"/>
										<label for="skill5">Experience level</label>
									</p>
									<div class="col s12 skillContent">
									@foreach($tags as $tag)
									@if($tag->category == 'experience')
											<p>
												<input type="checkbox" name="exp[]" id="exp{{$tag->id}}" value="{{$tag->id}}" />
												<label for="exp{{$tag->id}}">{{$tag->name}}</label>
											</p>
									@endif
									@endforeach
									</div>
								</div>
								<div class="col s12">
									<p>
										<input type="checkbox" id="skill1" name="skill1" />
										<label for="skill1">Access to Transportation</label>
									</p>
									<div class="col s12 skillContent">
									@foreach($tags as $tag)
									@if($tag->category == 'transport')
										<p>
											<input type="checkbox" name="trans[]" id="trans{{$tag->id}}" value="{{$tag->id}}" />
											<label for="trans{{$tag->id}}">{{$tag->name}}</label>
										</p>
									@endif
									@endforeach
									</div>
								</div>
								<div class="col s12">
									<p>
										<input type="checkbox" id="skill2" name="skill2"/>
										<label for="skill2">Language</label>
									</p>
									<div class="col s12 skillContent">
									@foreach($tags as $tag)
									@if($tag->category == 'language')
										<p>
											<input type="checkbox" name="lang[]" id="lang{{$tag->id}}" value="{{$tag->id}}" />
											<label for="lang{{$tag->id}}">{{$tag->name}}</label>
										</p>
									@endif
									@endforeach
									</div>
								</div>
								<div class="col s12">
									<p>
										<input type="checkbox" id="skill3" name="skill3" />
										<label for="skill3">Certification and Licences</label>
									</p>
									<div class="col s12 skillContent">
									@foreach($tags as $tag)
									@if($tag->category == 'certification')
										<p>
											<input type="checkbox" name="cert[]" id="cert{{$tag->id}}" value="{{$tag->id}}" />
											<label for="cert{{$tag->id}}">{{$tag->name}}</label>
										</p>
									@endif
									@endforeach
									</div>
								</div>
								<div class="col s7 mrg-bot30"></div>
								<div class="col s12">
									<h5 class="subtitle">Tag your job</h5>
									<p class="text-muted">Tags are a great way for people to search for jobs that have a common topic.</p>
								</div>
								<div class="col s12 mrg-bot30 mrgt-15" id="tagsDisplay">
								</div>
								<div class="col s12 mrg-bot30 mrgt-15">
									<a id="addtags" class="modal-trigger waves-effect waves-light btn teal darken-1" href="#tag-modal">Add tags</a>
								</div>
							</div>
						</div><!--end #tab3 -->
						<div class="col s12 mrg-tb40">
							<button id="back" type="buttton" class="waves-effect waves-light btn grey lighten-1 black-text">BACK</button>
							<!-- <button id="draft" type="buttton" class="waves-effect waves-light btn grey lighten-1">SAVE DRAFT</button> -->
							<button id="submit" type="submit" class="waves-effect waves-light btn green darken-1 right" name="myButton">POST JOB</button>
							<button id="next" type="buttton" class="waves-effect waves-light btn green darken-1 right">NEXT</button>
							<input type="hidden" id="latitude" name="latitude" value="">
							<input type="hidden" id="longitude" name="longitude" value="">
						</div>
				</div><!--end .card -->
			</form>
		</div><!--end #rootwizard -->
	</div>
</div>
<!-- Modal Structure -->
<div id="tag-modal" class="modal">
	<div class="modal-content">
		<h5>Add Tags</h5>
		<p>Tags may contain letters, numbers, colons, dashes and underscores.</p>
		<div class="modal-content">
			<select name="tags" multiple class="browser-default" id="select_key">
				@foreach(\App\Tag::where('category','tag')->get() as $tag)
				<option value="{{$tag->id}}">{{$tag->name}}</option>
				@endforeach
			</select>
		</div>
		<button id="add" type="buttton" class="modal-action modal-close waves-effect waves-light btn teal darken-1 tag-btn" onclick="document.getElementById('addtags').innerHTML='Edit Tags'">Add tags</button>
		<a href="#" class="modal-close"><i class="fa fa-times"></i></a>
	</div>

	<!-- <div class="modal-footer">
	<a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
</div> -->
</div>
@endsection

@section('scripts')

	<link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
	<link href="{{asset('css/EasyAutocomplete-master/dist/easy-autocomplete.css')}}" rel="stylesheet" />
	<script src="{{asset('js/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/custom/map.js') }}"></script>
	<script type="text/javascript" src="{{ asset('css/EasyAutocomplete-master/dist/jquery.easy-autocomplete.js')}}"></script>
	<script src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>
	<script src="/js/materialize/plugins/fullcalendar/lib/moment.min.js" type="text/javascript" charset="utf-8"></script>
	<script>

	validate = function(index) {

		if(index > 1) {
			if($('[name=title]').val() == '') {
				Materialize.toast('Please Enter Title', 1000);
				setTimeout(function() {
					$('ul.tabs').tabs('select_tab', 'tab1');
				}, 10);
				return false;
			}
			// if($('[name=about]').val() == '') {
			// 	Materialize.toast('Please Enter About', 1000);
			// 	setTimeout(function() {
			// 		$('ul.tabs').tabs('select_tab', 'tab1');
			// 	}, 10);
			// 	return false;
			// }

			if($("#requirements").val() == '') {
					Materialize.toast('Please Enter Roles and Responsibilities', 5000);
					setTimeout(function() {
						$('ul.tabs').tabs('select_tab', 'tab1');
					}, 10);
					return false;
			}


			/*@if(isset($job_copy))
				if(tinyMCE.activeEditor.getContent() == '') {
					Materialize.toast('Please Enter Roles and Responsibilities', 5000);
					setTimeout(function() {
						$('ul.tabs').tabs('select_tab', 'tab1');
					}, 10);
					return false;
				}
			@endif*/

			if($('[name=start_date]').val() == '') {
				Materialize.toast('Please Enter Start Date', 1000);
				setTimeout(function() {
					$('ul.tabs').tabs('select_tab', 'tab1');
				}, 10);
				return false;
			}

			if($('[name=start_time]').val() == '') {
				Materialize.toast('Please Enter start_time', 1000);
				setTimeout(function() {
					$('ul.tabs').tabs('select_tab', 'tab1');
				}, 10);
				return false;
			}

			if($('[name=end_time]').val() == '') {
				Materialize.toast('Please Enter end_time', 1000);
				setTimeout(function() {
					$('ul.tabs').tabs('select_tab', 'tab1');
				}, 10);
				return false;
			}

		/*	if($('[name=amount]').val() == '') {
				Materialize.toast('Please Enter amount', 1000);
				setTimeout(function() {
					$('ul.tabs').tabs('select_tab', 'tab1');
				}, 10);
				return false;
			}*/
		}

		if(index > 3) {
			if($('[name=street]').val() == '') {
				Materialize.toast('Please Enter Unit, Floor, Building', 1000);

				setTimeout(function() {
					// $('ul.tabs').tabs('select_tab', 'tab3');
					$('#maptabs').tabs('select_tab','test2');
				}, 100);
				return false;
			}
			if($('[name=loc_address]').val() == '') {
				Materialize.toast('Please Confirm Address', 1000);

				setTimeout(function() {
					// $('ul.tabs').tabs('select_tab', 'tab3');
					$('#maptabs').tabs('select_tab','test2');
				}, 100);
				return false;
			}
			if($('[name=loc_state]').val() == '') {
				Materialize.toast('Please Confirm State', 1000);

				setTimeout(function() {
					// $('ul.tabs').tabs('select_tab', 'tab2');
					$('#maptabs').tabs('select_tab','test2');
				}, 100);
				return false;
			}
			if($('[name=zipcode]').val() == '') {
				Materialize.toast('Please Confirm Postal/Zipcode', 1000);

				setTimeout(function() {
					// $('ul.tabs').tabs('select_tab', 'tab2');
					$('#maptabs').tabs('select_tab','test2');
				}, 100);
				return false;
			}
		}
		return true;
	}

	</script>

	<script>
	var input = document.getElementById('contact_email');
	var datalist = document.getElementById('data-email');

	editor = tinymce.init({
		selector: '#requirements',
		height: 400,
		menubar: false,
		// statusbar: false,
		elementpath: false,
		paste_auto_cleanup_on_paste : true,
		paste_remove_styles: true,
		paste_remove_styles_if_webkit: true,
		paste_strip_class_attributes: true,
		themes: "modern",
		plugins: [
			'advlist lists'
			//'advlist autolink lists link image charmap print preview anchor',
			//'searchreplace visualblocks code fullscreen',
			//'insertdatetime media table contextmenu paste code'
		],
		toolbar: 'bullist numlist',
		// toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
		setup: function(editor) {
			editor.on('change', function(e) {
				document.querySelector('[name="requirements"]').innerHTML = editor.getContent();
				document.getElementById('requirements').innerHTML = editor.getContent();
			});
		}
	});


	$(document).ready (function(){
		$('body').on('keydown', 'input, select', function(e) {
			var self = $(this)
				, form = self.parents('form:eq(0)')
				, focusable
				, next;
			if (e.keyCode == 13) {
				focusable = form.find('input,a,select,button').filter(':visible');
				next = focusable.eq(focusable.index(this)+1);
				if (next.length) {
					next.focus();
				} else {
					form.submit();
				}
				return false;
			}
		});

		$('select').material_select();
		$('select[name=tags]').select2({tags: true});
		$('#back').hide();
		$('#submit').hide();
		$('.skillContent').hide();

		var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);

		$("select[name=tags]").on('change', function(e) {
			$('#add').text('SAVE TAGS');
		});

		var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);
		$('.datepicker').pickadate({
			selectMonths: true, // Creates a dropdown to control month
			selectYears: 15, // Creates a dropdown of 15 years to control year
			autoclose: true,
		});

		d = new Date();
		d.setMonth(d.getMonth() + 3);

		// picker = $('.datepicker').pickadate('picker');
		// picker.set('min', true);
		// picker.set('max', d);

		$('.timepicker[name=start_time]').pickatime({
			formatSubmit: 'HH:i',
			twelvehour: false,
			default: '09:00:00',
			autoclose: true,
		});

		$('.timepicker[name=end_time]').pickatime({
			formatSubmit: 'HH:i',
			twelvehour: false,
			default: '18:00:00',
			autoclose: true,
		});

		var navListItems = $('.nav>.tab>a'), content = $('.tabContent');

		navListItems.click(function(e){
			e.preventDefault();

			var $target = $($(this).attr('href')),
				$item = $(this);
			$target.find('input:eq(0)').focus();
			$item.parent().addClass('active');
			index = parseInt($(this).attr('href').substr(-1));

			if(index != 1 && !validate(index)) {
				// console.log('validation failed');
				return;
			}

			// $('ul.tabs').tabs('select_tab', 'tab'+index);

			for (var i = 1; i <= 3; i++) {
				if(i < index)
				{
					$('[href=#tab'+i+']').parent().addClass('done');
				}
				else if (i == index)
				{
					$('[href=#tab'+i+']').parent().removeClass('done');
					// $('[href=#tab'+i+']').parent().removeClass('active');
				} else
				{
					$('[href=#tab'+i+']').parent().removeClass('done');
					$('[href=#tab'+i+']').parent().removeClass('active');
				}
			}
			if (index == 1) {
				$('#back').hide();
				$('#next').show();
				$('#submit').hide();
				$('#reuseJob').show();
			} else if(index == 3) {
				$('#next').show();
				$('#back').show();
				// $('#submit').show();
				$('#reuseJob').hide();
			} else {
				$('#reuseJob').hide();
				$('#back').show();
				$('#next').show();
				setTimeout(function(){window.dispatchEvent( new Event('resize') );}, 3000);
				$('#submit').hide();
			}

			setTimeout(function() {
				google.maps.event.trigger(map, 'resize');
			}, 1000);

			$('.progress-bar-primary').css('width',((index-1)*50)+'%');
		});

		// $('#back').click(function(e) {
		// 	e.preventDefault();
		// 	$c = $('.nav>.tab>a.active');
		// });

		$('#next').click(function(e) {
			// console.log('clicked next');
			e.preventDefault();
			$next = parseInt($('.nav>.tab>a.active').attr('href').substr(-1)) + 1;
			if($("#maptabs a.active").attr('href') == '#test1' && $('#toptabs li.tab.active a.active').attr('href') == '#tab3') {
				$('#maptabs').tabs('select_tab','test2');
				$('#submit').show();
				$('#next').hide();

				return;
			}
			$('[href=#tab'+$next+']').click();
		});

		$('#back').click(function(e) {
			e.preventDefault();
			$current = parseInt($('.nav>.tab>a.active').attr('href').substr(-1));
			if($current == 3) {
				if(parseInt($('#maptabs>.tab>a.active').attr('href').substr(-1)) == 2) {
					$('#maptabs').tabs('select_tab','test1');
					$('#submit').hide();
					$('#next').show();
					return;
				}
			}
			$back = $current - 1;
			$('[href=#tab'+$back+']').click();
		});

		$('[id^=skill]').on('change', function(e) {
			ob = $(this).parent().next().toggleClass('abcd');
			if(ob.hasClass('abcd')){
				ob.show(300);
			}
			else{
				ob.hide(300);
			}
		});
		$('a[href=#tab3]').click(function(e) {
			$('#tab2 ul.tabs li:first-child a').click();
		});
		$('a[href=#test2]').click(function(e) {
			updatePlace();
			$('#submit').show();
			$('#next').hide();
		});

		$('select[name=loc_country]').change(function(e) {

			val = $(this).val();
			id = $('option[value="'+val+'"]').attr('data-id');
			$.ajax({
				type : 'get',
				url  : '{{ route('country.index') }}/'+id,
			}).done(function(res) {
				$('select[name=loc_state]').html(res);
				if(_place.state){
					$('select[name=loc_state]').val(_place.state);
				}

				$('select').material_select();
			});
		});

		$('[name=working]').click(function(e) {
			if(this.value == 'other')
			{
				$(this).parent().parent().children().eq(-1).removeClass('hide');
			}
			else
			{
				$(this).parent().parent().children().eq(-1).addClass('hide');
			}
		})

		$('#payment_details [name=salary]').click(function(e) {
			if(this.value == 'other')
			{
				$('#byDay').removeClass('hide');
			}
			else
			{
				$('#byDay').addClass('hide');
			}
		})
	});
	</script>

	<script>
		@if(isset($job_copy))
		$(document).ready(function(){
			changeFunc('j.{{$job_copy->id}}');
			$("#close").hide();
		})
		@endif

		function changeFunc(id) {

			$("#close").show();
			$.ajax({
				type : 'get',
				url  : '/job/selectJob/'+id,
			}).done(function(data) {
				if(data.job.type){
					$("#time").val(data.job.type);
				}
				if(data.job.vacancy){
					$("#vacancy").val(data.job.vacancy);
				}
				/*if(data.job.tag_ids){

					nationality = (data.job.tag_ids.indexOf(44) > -1) ? '44' : '43';
					$("#character").val(nationality);
				}*/
			/*	console.log(data.job.title);*/
				$("[name=title]").val(data.job.title);
				if(data.job.start_time){
					$("#start_date").val(data.job.start_time);
					$("#start_time").val(moment(data.job.start_time,'HH:mm:ss').format('HH:mm'));
				}
				if(data.job.end_time){
					$("#end_time").val(moment(data.job.end_time,'HH:mm:ss').format('HH:mm'));
				}
				$("#contact_phone").val(data.job.contact_phone);
				$("#nationality").val(data.job.nationality);
				$("#contact_link").val(data.job.contact_link);
				$("#contact_emails").val(data.job.contact_email);
				$("#cover_picture").val(data.job.cover_picture);
				//$("#aboutWork").val(data.job.about);
				/*$("#requirements").val(data.job.requirements);*/
				/*if(data.job.requirements) {
					tinyMCE.activeEditor.setContent(data.job.requirements);*/
					$('[name=requirements]').val(data.job.requirements);
				/*} else {
					tinyMCE.activeEditor.setContent(data.job.requirements);
				}*/
				//$("#uniform_details").val(data.job.uniform);
				//$("#amount").val(data.job.salary_unit);
				/**/
				$("#countries").val(data.job.country);
				$("#state").val(data.job.state);
				$("#loc_city").val(data.job.city);
				$("#floor").val(data.job.street);
				$("#loc_address").val(data.job.location);
				$("#zipcode").val(data.job.latitude +" "+data.job.longitude);

				$(`input[name=working_hours][value="${data.job.working_hours}"]`).prop('checked', true);
				if(data.job.salary){
					$(`input[name=salary][value="${data.job.salary}"]`).prop('checked', true);
				}
				if(data.job.salary_unit){
					$(`input[name=payment][value="${data.job.salary_unit}"]`).prop('checked', true);
				}

				/*console.log(data.job.tags.category);*/
				if(data.job.tag_ids && data.job.tag_ids.length > 0) {

					$('[name=tags]').val(data.job.tag_ids).change()
					data.job.tag_ids.forEach(function(tag) {
						/*console.log(data.job);*/

						if(tag >= 1 && tag <=3) {
							$('#skill1').prop('checked', true).change();
						} else if(tag == 4 || tag ==5 || tag ==6 || tag==22) {
							$('#skill2').prop('checked', true).change();
						} else if(tag >= 7 && tag <=11) {
							$('#skill3').prop('checked', true).change();
						} else if(tag >= 14 && tag <=21) {
							$('#skill4').prop('checked', true).change();
						} else if(tag >= 39 && tag <=42) {
							$('#skill5').prop('checked', true).change();
						}
						$('.skillContent input:checkbox[value='+tag+']').prop('checked', true);

					});
				}else
				if(data.job.tags && data.job.tags.length > 0)
				{	var a = new Array();
					console.log(data.job.tags);
					var a = data.job.tags.split(',');
					a.forEach(function(tag) {
						if(tag >= 1 && tag <=3) {
							$('#skill1').prop('checked', true).change();
						} else if(tag == 4 || tag ==5 || tag ==6 || tag==22) {
							$('#skill2').prop('checked', true).change();
						} else if(tag >= 7 && tag <=11) {
							$('#skill3').prop('checked', true).change();
						} else if(tag >= 14 && tag <=21) {
							$('#skill4').prop('checked', true).change();
						} else if(tag >= 39 && tag <=42) {
							$('#skill5').prop('checked', true).change();
						}
						$('.skillContent input:checkbox[value='+tag+']').prop('checked', true);

					});
				}

				if(data.job.uniform != 'Yes') {
					$('#no').click();
					$("#textarea1").val(data.job.uniform);
				} else {
					$('#yes').click();
				}

				updatePlace();
				Materialize.updateTextFields();
			});
		}


		function ClearFields(){

			document.getElementById("selectBox").value = "";
			document.getElementById("time").value = "";
			document.getElementById("vacancy").value = "";
			// document.getElementById("title").value = "";
			document.getElementById("start_date").value = "";
			document.getElementById("contact_phone").value = "";
			document.getElementById("contact_link").value = "";
			document.getElementById("contact_emails").value = "";
			document.getElementById("cover_picture").value = "";
			//document.getElementById("aboutWork").value = "";
			document.getElementById("requirements").value = "";
			document.getElementById("uniform_details").value = "";
			document.getElementById("working_hours").value = "";
			document.getElementById("amount").value = "";
			document.getElementById("pac-input").value = "";
			document.getElementById("countries").value = "";
			document.getElementById("state").value = "";
			document.getElementById("loc_city").value = "";
			document.getElementById("floor").value = "";
			document.getElementById("loc_address").value = "";
			document.getElementById("zipcode").value = "";
			Materialize.updateTextFields();
		}


		var countries = {};
		$(document).ready(function(){
			getCountries();

			$("#yes").click(function(){
				$("#uniform_details").addClass('hide');
			});
			$("#no").click(function(){
				$("#uniform_details").removeClass('hide');
			});

			$('select[name=tags]').on('change', function(e) {
				chips = $(this).val();
				html = '';
				if(chips.length > 0) {
					chips.forEach(function(chip){
						if(!isNaN(chip)) {
							c = $(`select[name=tags] option[value=${chip}]`).html();
							html += `<div class='chip purple white-text'>${c}<i class="close material-icons">close</i></div> `;
						} else {
							html += `<div class='chip purple white-text'>${chip}<i class="close material-icons">close</i></div> `;
						}

					})
				}
				$('#tagsDisplay').html(html);
			});
		});

		function getCountries()
		{
			$.ajax({
				type : 'get',
				url  : '{{ route('country.index') }}',
			}).done(function(res) {
				countries = res;
				fillCountries(res);
			});
		}

		function fillCountries(res)
		{
			html = '';
			res.forEach(function(c) {
				html += '<option value="'+c.name+'" data-id="'+c.id+'">'+c.name+'</option>';
			});
			$('#countries').html(html);
			$('select').material_select();
		}

		function selectDay(el) {
			value = [];
			if($(el).hasClass('holiday-default')) {
				$(el).toggleClass('holiday');
			}
			$(el).toggleClass('purple');
			$('span.purple').each(function(i, item) {
				value.push($(item).attr('val'));
			});
			$('[name=start_date]').val(value.join(','));
		}

		// $('span.holiday').click(function(e) {
		// 	Materialize.toast("It is a holiday.", 1500);
		// });

		//$( "#datepicker" ).datepicker({  maxDate:"+3m"});
	</script>
	<!-- 	url  : '{{ route('country.index') }}/'+id,
			}).done(function(res) {
				$('select[name=loc_state]').html(res);
				if(_place.state){
					$('select[name=loc_state]').val(_place.state);
				}

				$('select').material_select();
			});
 -->
	<script>
	function allTitles() {

		$.ajax({
			'type'	:'get',
			'url'	:'/job/allTitles',
		}).done(function(res){
			var options = {
				data: res.data,
				list: {
					match: {
						enabled: true
					},
					maxNumberOfElements: 10
				}
			};
			$("[name=title]").easyAutocomplete(options);
		});
	}


	</script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFvcodwxV3SQptSx4LQ-H-gZVcAk-1lyI&libraries=places&callback=initAutocomplete" async defer></script>

	<script type="text/javascript">
		$ (document).ready(function(){
			$('#selectBox').select2({
			placeholder: '-- Select previous job --',
			allowClear: true
			});
			allTitles();
		});
	</script>
	<script type="text/javascript">
	$ (document).ready(function(){
		$('#amount').bind('keydown', function(e){
			if(e.keyCode == '69'){
				Materialize.toast('Please Enter a numeric value,no special characters are allowed',3500);
				return false;
			}
			/*if(e.keyCode == '110' || e.keyCode == '190'){
				Materialize.toast('Please insert only numbers ,',3000);
				return false;
			}*/
			/*if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 27 || e.keyCode == 13 || e.keyCode == '110' || e.keyCode == '190'){
				Materialize.toast('ok',3000);
				return true;
			}*/
		});

		$('#contact_phone').bind('keydown', function(e){
			if(e.keyCode == '110' || e.keyCode == '190' || e.keyCode == '189'){
				Materialize.toast('Please insert only numbers.Dot or dash or any special character is not allowed',3500);
				return false;
			}
		});
	});
	</script>
@endsection
