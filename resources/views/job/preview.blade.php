@extends('layouts.material')

@section('content')

<div class="row preview-div">
	<!-- <div class="col m4 s12">
		<div class="livelihood ">
			<h6>Well done! You have successfully create livelihood opportunity to someone.</h6>
			<p>Here is how your job post will be like LIVE! </p>
		</div>
	</div> -->
	<div class="col m4 s12 offset-m4">
		<div class="livelihood ">
			<h6>Well done! You have successfully create livelihood opportunity to someone.</h6>
			<p>Here is how your job post will be like LIVE! </p>
			<a href="/job/{{$job->id}}/edit" id="back" type="buttton" class="waves-effect waves-light btn light-blue darken-2">Edit</a>
			<a href="/dashboard" id="back" type="buttton" class="waves-effect waves-light btn light-blue darken-2 right">Home</a>
		</div>
		<div class="card">
			<div class="card-content">
				<div class="row">
					<div class="col s12">
						<img src="{{$job->cover_picture}}" class="responsive-img">
					</div>
				</div>
				<div class="row  retail-sales">
					<div class="col s12">
						<h4>{{$job->title}}</h4>
						<!-- <P class="">{{$job->street}}</P> -->
					</div>
				</div>
				<hr>
				<div class="row icon-div">
					<div class="col s2 m2">
					<img src="/images/dollar.png" class="responsive-img">
					</div>
					<div class="col s5 m5">
					<h6 >Salary</h6>
					</div>
					<div class="col s5 m5">
					<h6>RM {{$job->salary}} &nbsp; {{$job->salary_unit}}</h6>
					</div>
				</div>
				<div class="row icon-div">
					<div class="col s2 m2">
					<img src="/images/calendar.png" class="responsive-img">
					</div>
					<div class="col s5 m5">
					<h6> Working Date</h6>
					</div>
					<div class="col s5 m5">
					<h6>{{($job->start_date) ? $job->start_date->format('D M j, Y') : ''}}</h6>
					</div>
				</div>
				<div class="row icon-div">
					<div class="col s2 m2">
					<img src="/images/watch.png" class="responsive-img">
					</div>
					<div class="col s5 m5">
					<h6>Working Hours</h6>
					</div>
					<div class="col s5 m5">
					<h6>{{$job->start_time}} &nbsp;to &nbsp; {{$job->end_time}}</h6>
					</div>
				</div>
				<div class="row icon-div">
					<div class="col s2 m2">
					<img src="/images/job_icon.png" class="responsive-img">
					</div>
					<div class="col s5 m5">
					<h6>Type of job</h6>
					</div>
					<div class="col s5 m5">
					<h6>{{$job->type}}</h6>
					</div>
				</div>
				<hr>
				<div class="row role-responsibility">
					<div class="col s12" style ="height:auto;">
						<h5>Role and Resposibilities</h5>
						<!-- <div class="col s12 requirement_height">{!! nl2br($job->requirements) !!}</div> -->
						<textarea class="materialize-textarea border_hide" rows="6" cols="8">{{ strip_tags($job->requirements) }}</textarea>
					</div>
				</div>
				<hr>
				<div class="row dress-code">
					<div class="col s12">
						<h5>Dress Code</h5>
						<p>{{ $job->uniform }}</p>
					</div>
				</div>
				<hr>
				<div class="row dress-code">
					<div class="col s12">
						<h5>Location</h5>
						<p>{{ $job->location }} ,{{ $job->street }},{{ $job->state }},{{ $job->country }},{{ $job->zipcode }}</p>
					</div>
				</div>
				<hr>
				<div class="contact-instructions">
					<div class="row">
						<div class="col s12">
							<h5>Contact Instructions</h5>
						</div>
					</div>
					<div class="row">
						<div class="col s5 m5">
						<h6 >Contact Email</h6>
						</div>
						<div class="col s7 m7">
						<h6>{{ $job->contact_email }}</h6>
						</div>
					</div>
					<div class="row">
						<div class="col s5 m5">
						<h6 >Contact Phone</h6>
						</div>
						<div class="col s7 m7">
						<h6>{{ $job->contact_phone }}</h6>
						</div>
					</div>
					<div class="row">
						<div class="col s5 m5">
						<h6 >Contact Link</h6>
						</div>
						<div class="col s7 m7">
						<h6>{{ $job->contact_link }}</h6>
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col s12 tags">
						<h5>Tags</h5>
						@php
						@endphp
						<ul class="ul-bullets">
							@foreach($job->tags as $tag)
							@if(($tag->category = 'transport') || ($tag->category = 'language') || ($tag->category = 'certification') || ($tag->category = 'education') || ($tag->category = 'tag'))
							<li class="circle">{{$tag->name}}</li>
							@endif
							@endforeach
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!--<div class="col s12 m12">
			<!--<button id="next" type="buttton" class="waves-effect waves-light btn teal left" onclick="location.replace('/job/{{$job->id}}/edit');">Back</button>!-->
			<!--@if($job->status == 0)
				<button id="publish" type="buttton" class="waves-effect waves-light btn teal right" onclick="publish(1);">Publish</button>
			@else
				<button id="publish" type="buttton" class="waves-effect waves-light btn teal right" onclick="publish(0);">Un Publish</button>
			@endif
		</div>!-->

	</div>
</div>

<!-- <div class="row mrg-t7">
	<div class="col m2 s6 offset-m8">
		<button id="next" type="buttton" class="waves-effect waves-light btn right review-btn" onclick="location.replace('/job/{{$job->id}}/edit');">Back</button>
	</div>
	<div class="col s6 m2">
		@if($job->status == 0)
		<button id="publish" type="buttton" class="waves-effect waves-light btn review-btn" onclick="publish(1);">Publish</button>
		@else
		<button id="publish" type="buttton" class="waves-effect waves-light btn review-btn" onclick="publish(0);">Un Publish</button>
		@endif
	</div>
</div> -->

@endsection

@section('scripts')
	<script>
		publish = function(value) {

			var request = {};
			request._token  = '{{ csrf_token() }}';
			request.value = value;

			$.ajax({
				'type': 'POST',
				'url':  "{{ route('job.show', $job->id) }}/update/status",
				'data': request
			}).done(function(response) {
				if(response.trim() == 'Successfully updated' && value == 1) {
					Materialize.toast('Your job has been published.', 500);
					setTimeout(function(){location.replace('/dashboard')}, 500);
					$('#publish').attr('onclick', 'publish(0);');
					$('#publish').html('Un Publish');
				}
				if(response.trim() == 'Successfully updated' && value == 0) {
					Materialize.toast('Your job has been unpublished.', 500);
					$('#publish').attr('onclick', 'publish(1);');
					$('#publish').html('Publish');
				}

			});
		}
	</script>
@endsection