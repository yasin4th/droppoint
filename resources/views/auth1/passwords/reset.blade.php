@extends('layouts.auth')

@section('content')

<div class="row" style="margin-top: 23px;">
    {{-- <div class="panel-heading">Reset Password</div> --}}
    <form class="col s12" role="form" method="POST" action="{{ url('/password/reset') }}">
        {{ csrf_field() }}

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="row">
            <div class="input-field col s4{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="">E-Mail Address</label>

                    <input id="email" type="email" class="validate" name="email" value="{{ $email or old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
            </div>
        </div>

        <div class="row">
            <div class="input-field col s4{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="">Password</label>

                    <input id="password" type="password" class="validate" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
            </div>
        </div>

        <div class="row">
            <div class="input-field col s4{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label for="password-confirm" class="">Confirm Password</label>

                    <input id="password-confirm" type="password" class="validate" name="password_confirmation" required>

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="waves-effect waves-light btn deep-purple lighten-2">
                    Reset Password
                </button>
            </div>
        </div>

    </form>
</div>
@endsection
