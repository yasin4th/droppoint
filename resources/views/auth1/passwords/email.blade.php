@extends('layouts.auth')

@section('content')
  
    <div id="login-page" class="row">
      <div class="col s12 m4 offset-m4" style="margin-top:30px;">
    <div class="col s11 z-depth-4 card-panel">
       <form role="form" method="POST" action="{{ url('/password/email') }}">
         {{ csrf_field() }}
        <div class="row">
          <div class="input-field col s12">
            <img src="/images/login-logo.png" alt="" class="circle responsive-img valign" style="display: block; margin: auto; width: 30%;">
            <h5 class="center">Forgot Password</h5>
          </div>
        </div>

        <div class="container" style="display: flex; flex-direction: column; margin-top: -100px; margin-left: 25px;">


          <div class="row margin">
            <div class="input-field col s10 {{ $errors->has('email') ? ' has-error' : '' }}">
             {{--  <i class="mdi-communication-email prefix"></i> --}}
              <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}" placeholder="Email"   required>
                  @if ($errors->has('email'))
                           <span class="red-text text-darken-1" style="margin-left:40px;">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                    @endif
            </div>
          </div>
   
          <div class="row">
            <div class="input-field col s10" style="margin-left:5px;">
                <button type="submit" class="btn waves-effect waves-light  purple col s12">Send Reset Link</button>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s6 m6 l6" style="margin-left:5px;">
              <p class="margin medium-small"><a href="{{ url('/login') }}">Sign in here </a></p>
            </div>
          </div>

        </div>



      </form>
    </div>
  </div>
  </div>

@endsection
