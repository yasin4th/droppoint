@extends('layouts.auth')

@section('content')

    <div id="login-page" class="row">
      <div class="col s12 m4 offset-m4" style="margin-top:30px;">
    <div class="col s11 z-depth-4 card-panel">
       <form role="form" method="POST" action="{{ url('/login') }}">
         {{ csrf_field() }}
        <div class="row">
          <div class="input-field col s12 ">
            <img src="images/login-logo.png" alt="" class="circle responsive-img valign" style="display: block; margin: auto; width: 30%;"> {{-- profile-image-login --}}
            <h5 class="center">Sign in {{-- to {{ config('app.name', 'Laravel') }} --}}</h5>
          </div>
        </div>
        
        <div class="container" style="display: flex; flex-direction: column; margin-top: -100px; margin-left: 25px;">
          <div class="row margin">
            <div class="input-field col s10 {{ $errors->has('email') ? ' has-error' : '' }}">
              {{-- <label for="email" class="">E-Mail Address</label> --}}
              {{-- <i class="mdi-communication-email prefix"></i> --}}
              <input id="email" type="email" class="validate" name="email" placeholder="Email" value="{{ old('email') }}"  required>
                     @if ($errors->has('email'))
                           <span class="red-text text-darken-1" style="margin-left:40px;">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                    @endif
            </div>
          </div>
            
          <div class="row margin">
            <div class="input-field col s10 {{ $errors->has('password') ? ' has-error' : '' }}">
              {{-- <label for="password" class="">Password</label> --}}
              {{-- <i class="mdi-action-lock-outline prefix"></i> --}}
              <input id="password" type="password" placeholder="Password" class="validate" name="password"   required>
                   @if ($errors->has('password'))
                          <p class="red-text text-darken-1" style="margin-left:40px;">
                              <strong>{{ $errors->first('password') }}</strong>
                          </p>
                     @endif
            </div>
          </div>
          
          {{--<div class="row">          
            <div class="input-field col s12 m12 l12  login-text" style="margin-top:15px;">
                 <input type="checkbox" name="remember">
                <label for="remember-me" style="margin-left: 23px;">Remember me</label>
            </div>
          </div> --}}

          <div class="row">
            <div class="input-field col s10" style="margin-left:5px;">
                <button type="submit" class="btn waves-effect waves-light purple col s12">Login</button>
            </div>
          </div>

        </div>


        <div class="row">
          <div class="input-field col s6 m6 l6">
            <p class="margin medium-small"><a href="{{ url('/register') }}">Not a Member?</a></p>
          </div>
          <div class="input-field col s6 m6 l6">
              <p class="margin right-align medium-small"><a href="{{ url('/password/reset') }}">Forgot Password?</a></p>
          </div>          
        </div>

      </form>
    </div>
  </div>
  </div>

@endsection
