@extends('layouts.gamify')
@section('title') Manage :: @parent @stop


@section('content')
<div class="row">

    <div class="col s12 m3">
        <div class="card profile-sidebar">
            @include('includes.admin.gamify')
        </div>
    </div>
    <div class="col m9">
        {{-- <h2>Manage Gamify Application</h2> --}}
        {{-- <hr /> --}}
        <div class="card">
            <div class="card-content">
                <div class="row">
                    <div class="col m12">
                        <!-- Gamify Angular Js Application -->
                        <div ng-app="mediasoftApp">
                          <div ng-view>loading...</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="avatar-modal" class="modal">
    <div class="modal-content">
        <h5 class="center">Change Photo</h5>
        <div class="display-flex">

        </div>
        <div class="applicant-border center">
            <button type="button" class="waves-effect waves-red btn-flat modal-action modal-close btn-small">CANCEL</button>
            <a href="#applicant-feeback1-modal" class="waves-effect waves-light btn teal darken-1 btn-small" id="feedbackNext">NEXT</a>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.10/angular.min.js"></script>
    <script src="https://code.angularjs.org/1.3.10/angular-route.min.js"></script>

    <script>
        var UID = '{{ Auth::id() }}';
        var CSRF_TOKEN = '{{ csrf_token() }}';
    </script>
    <!-- Angular JS Application For Gamify Management -->
    <script src="{{{ asset('ng/shared/core.js') }}}"></script>
    <script src="{{{ asset('ng/shared/plupload.js') }}}"></script>
    <script src="{{{ asset('ng/gamify/js/manage.js') }}}"></script>

    <!-- Uploader -->
    <script src="{{{ asset('plupload/js/plupload.full.js') }}}"></script>
    <!-- Code Pretifier -->
    <script type="text/javascript" src="{{ asset('js/prettify/prettify.js') }}"></script>
    <script type="text/javascript">
    	$(function () {
    		$(window).load(prettyPrint());
            setTimeout(function(){$('select').material_select()},500);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    	});
    </script>


    <script>
      submitted = function() {
          action = document.location.href;
          action = action.substr(action.lastIndexOf('/')+1);
          $('[name=badge_id]').val(action);
          if($('[name=avatar]').val() != '') {
            $('#uploadForm').submit();
            Materialize.toast('Please select image first.', 1500);
          }
      }
    </script>
@endsection