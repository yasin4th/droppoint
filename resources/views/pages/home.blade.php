@extends('layouts.material')
@section('title') Home :: @parent @stop


@section('content')
<div class="row">

    <div class="col s12 m3">
        <div class="card profile-sidebar">
            @include('includes.admin.sidemenu')
        </div>
    </div>
    <div class="col m9">
        <div class="card">
           <h1>Gamify Solution</h1>
           <p>Go to manage section to manage badges, rewards, points and credits.</p>
        </div>
    </div>
</div>

@endsection
