<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->

    <link rel="stylesheet" href="/css/materialize.min.css"/>

    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"/>

    <link href="/css/app.css" rel="stylesheet">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            ]); ?>
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBtTC_uv0ldhfbuxoC1sRfi-eMynCF8WkQ&libraries=places"
      type="text/javascript"></script>

</head>

<body style="background-color:#f7f7f7;">


    <!-- Navbar -->
    <nav class="navbar-fixed" style="background-color:#1D1C2C;">

        <div class="nav-wrapper">
                <!-- Branding Image -->
            <a class="brand-logo" href="{{ url('/admin') }}" style="margin-left:70px;">
                {{ config('app.name', 'Laravel') }}
            </a>

            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>

            <!-- Right Side Of Navbar -->
            <ul class="right hide-on-med-and-down" style="padding-right:50px;">

                {{-- <li><a href="/admin/jobs/all">Jobs</a></li>
                <li style="padding-right:20px;"><a href="/admin/company">Company</a></li> --}}

                <li style="margin-right: 10px;"><a href="{{route('dashboard')}}"><i class="material-icons">dashboard</i></a>
                </li>

                <!-- Search -->
                {{-- <form>
                    <div class="input-field">
                      <input id="search" type="search" required>
                      <label for="search"><i class="material-icons">search</i></label>
                      <i class="material-icons">close</i>
                    </div>
                </form> --}}

                <!-- Login/Logout -->
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                    <!-- Dropdown -->
                    <li>
                        <a class="dropdown-button" data-beloworigin="true" href="#!" data-activates="dropdown1" style="position:relative; padding-left:50px;">

                            <img src="/company/logo/{{ Auth::user()->company->company_logo }}" style="width:32px; height:32px; position:absolute; top:15px; left:10px; border-radius:50%;">

                            {{-- {{ Auth::user()->name }} --}}
                            <i class="material-icons right">arrow_drop_down</i>
                        </a>
                    </li>
                @endif
            </ul>


            <!-- Dropdown Structure -->
            <ul id="dropdown1" class="dropdown-content">

                {{-- style="padding: 1.5rem 1.75rem; border: 0 none; opacity: 0; border-radius:4px; border: 0 none; box-shadow: 0 0 4px 0 rgba(0,0,0,0.25); background-color: #fff; background-clip: padding-box; width:250px; z-index:98; margin-right:5px; margin-top:5px;" --}}

                {{-- <li style="height: 60px; width: 60px; float:left; border-radius:50%; margin-right:10px;">
                    <img src="/company/logo/{{ Auth::user()->company->company_logo }}" style="height: 60px; width: 60px; float:left; border-radius:50%; margin-right:10px;">

                    <div style="padding-left:10px;">
                        <h5>{{ Auth::user()->name }}</h5>
                        <h6>{{ Auth::user()->email }}</h6>
                    </div>
                </li> --}}

                <li class="divider"></li>

                <li><a href="#" style="color: black;">Profile</a></li>

                <li>

                    <a href="{{ url('/logout') }}" style="color: black;"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">Logout</a>

                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>

                </li>

            </ul>

            <!-- Mobile Dropdown Structure -->
            <ul class="side-nav" id="mobile-demo">

                {{-- <i class="material-icons">cloud</i> --}}

                <li><a href="/admin/jobs/all">Job</a></li>

                <li><a href="/admin/company">Company</a></li>

                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                    <li><a href="#">Profile</a></li>
                    <li>
                        <a href="{{ url('/logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">Logout</a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                @endif
            </ul>

        </div>

    </nav>

    <!-- Sidenav -->
    {{-- <ul id="slide-out" class="side-nav fixed" style=" width:270px; margin-top:70px;">
        <li><a href="/admin"><i class="material-icons">cloud</i>Dashboard</a></li>
        <li><a class="waves-effect" href="/admin/jobs/all"><i class="material-icons">cloud</i>Job List</a></li>
        <li><div class="divider"></div></li>
        <li><a class="waves-effect" href="/admin/company"><i class="material-icons">cloud</i>Company List</a></li>
    </ul> --}}
    {{-- <a href="#" data-activates="slide-out" class="button-collapse"><i class="small material-icons" style="color: purple;">toc</i></a> --}}
    {{-- style="padding-left: 140px; margin-top:20px;" --}}

    <div class="container" style="margin-top: 35px;">
        @yield('content')
    </div>



<!-- Scripts -->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

<script type="text/javascript" src="/js/materialize.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<script src="/js/app.js"></script>

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector:'textarea.editme',
        menu: {
            table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
            tools: {title: 'Tools', items: 'spellchecker code'}
            },
        toolbar: 'bullist',
        themes: "modern"
    });
</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
    $('.timepicker').timepicker({
        timeFormat: 'h:mm p',
        interval: 60,
        minTime: '00',
        maxTime: '11:00pm',
        defaultTime: '8',
        startTime: '08:00',
        dynamic: true,
        dropdown: true,
        scrollbar: true
    });
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
  $('select').select2();
</script>


@yield('after-script')


</body>

</html>
