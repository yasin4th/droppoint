<!DOCTYPE html>
<html lang="en">

<!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 3.1
	Author: GeeksLabs
	Author URL: http://www.themeforest.net/user/geekslabs
================================================================================ -->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="msapplication-tap-highlight" content="no">
	<meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
	<meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
	<title>DropPoint</title>

	<!-- Favicons-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="icon" href="/images/favicon/favicon-32x32.png" sizes="32x32">
	<!-- Favicons-->
	<link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
	<!-- For iPhone -->
	<meta name="msapplication-TileColor" content="#00bcd4">
	<meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
	<!-- For Windows Phone -->

	<!-- CORE CSS-->
	<link href="{{asset('css/materialize/materialize.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
	<link href="{{asset('css/materialize/style.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
	<link href="/css/font-awesome.min.css" rel="stylesheet">
	<!-- Custome CSS-->
	<link href="{{asset('css/materialize/custom/custom.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">


	<link href="{{asset('js/materialize/plugins/prism/prism.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
	<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
	<link href="{{asset('js/materialize/plugins/perfect-scrollbar/perfect-scrollbar.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
	<link href="{{asset('js/materialize/plugins/jvectormap/jquery-jvectormap.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
	<link href="{{asset('css/materialize/dropify/css/dropify.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
	<link href="{{asset('css/materialize.clockpicker.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
	<link href="{{asset('js/jcrop/css/jquery.Jcrop.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
	@include('includes.theme')

</head>

<body>
	<!-- Start Page Loading -->
	<div id="loader-wrapper">
		<div id="loader"></div>
		<div class="loader-section section-left"></div>
		<div class="loader-section section-right"></div>
	</div>
	<!-- End Page Loading -->
	<header>
	@include('includes.header')
	</header>

	<!-- START MAIN -->
	<main>
		<div id="">
			<!-- START WRAPPER -->
			<div class="wrapper">
				<!-- START CONTENT -->
				<section id="content">
					<!--start container-->
					<div class="container">
						@yield('content')
					</div>
					<!--end container-->
				</section>
				<!-- END CONTENT -->
			</div>
			<!-- END WRAPPER -->
		</div>
	</main>
	<!-- END MAIN -->

	@include('includes.footer')

	<!-- ===============================================
	Scripts
	================================================ -->

	<!-- jQuery Library -->

	<script type="text/javascript" src="{{asset('js/materialize/plugins/jquery-1.11.2.min.js')}}"></script>
	<!--materialize js-->
	<script type="text/javascript" src="{{asset('js/materialize/materialize.min.js')}}"></script>
	<!--scrollbar-->
	<script type="text/javascript" src="{{asset('js/materialize/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
	 <!--prism-->
	<script type="text/javascript" src="{{asset('js/materialize/plugins/prism/prism.js')}}"></script>

	<!-- sparkline -->
	<script type="text/javascript" src="{{asset('js/materialize/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/materialize/plugins/sparkline/sparkline-script.js')}}"></script>

	<!--plugins.js - Some Specific JS codes for Plugin Settings-->
	<script type="text/javascript" src="{{asset('js/materialize/plugins.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/jcrop/js/jquery.Jcrop.min.js')}}"></script>
	<!--custom-script.js - Add your own theme custom JS-->
	<script type="text/javascript" src="{{asset('js/materialize/custom-script.js')}}"></script>
	<script type="text/javascript" src="{{asset('css/materialize/dropify/js/dropify.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/materialize.clockpicker.js')}}"></script>

	@yield('scripts')
	@include('includes.mobile-header')

</body>

</html>