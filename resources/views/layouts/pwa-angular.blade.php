<!DOCTYPE HTML>
<html>
<head>
	<title>Eyewil</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="icon" sizes="192x192" href="/images/192-01.png">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="{{asset('css/materialize/materialize.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<!-- Custom Theme files -->
	<link href="/pwa-assets/assets/style.css" rel='stylesheet' type='text/css' />

	<link href='https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
	<link href="/pwa-assets/webcss/font-awesome.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body ng-app="myapp" ng-controller="ctrl">

	<nav class="nav-extended navigation">
		<div class="nav-wrapper container">

			<a id="createjob" class="right btn white btn_createjob">Create job</a>

			<a id="back" class="right btn white btn_createjob" style="display: none;">Back</a>

			<a href="#" class="brand-logo right"><i class="material-icons">add_alert</i></a>

			<a class="dropdown-button" href="#!" data-activates="dropdown1" style="display: inline-block;"><i class="material-icons">menu</i></a>

			<ul id="dropdown1" class="dropdown-content dropdown_area">
				<li><a href="/logout">Log Out</a></li>
			</ul>

		</div>
		<div class="nav-content jobs-content">
			<ul class="tabs nav_tab">
				<li class="tab"><a class="active" href="#" ng-click="tab = 1">Active Jobs</a></li>
				<li class="tab"><a href="#" ng-click="tab = 2">Applicants (@{{active.applicants.length || 0}})</a></li>
				<li class="tab"><a href="#" ng-click="tab = 3">Hired (@{{active.hired.length || 0}})</a></li>
				<!-- <div class="indicator indicator_line" style="right: 210px; left: 80px;"></div> -->
			</ul>
		</div>
	</nav>

	<div id="applicant-modal" class="modal applicant-modal" style="z-index: 1003;transform: scaleX(1); top: 10%;">
		<div class="modal-content center">
			<div class="applicant-pic">
				<img src="/images/avatar.jpg" class="responsive-img" alt="Profile Image" id="applicantImage">
			</div>
			<h6><strong class="applicantName"></strong></h6>
			<p><small><i class="mdi-communication-phone"></i><span id="applicantPhone">@{{applicant.phone}}</span></small><br><small><i class="mdi-communication-email"></i><span id="applicantEmail">@{{applicant.email}}</span></small></p>
			<div class="applicant-border">
				<p class="applicant-time">
					<span id="applicantHours">@{{applicant.working_hours}}</span><br>
					<span>HOUR</span>
				</p>
			</div>
			<div class="applicant-border" id="hireDiv">
				<button type="button" class="waves-effect waves-light btn teal darken-1 btn-small hire-applicant modal-action modal-close" id="applicantHired" data-id="2" ng-click="hire()">Hire</button>
			</div>
			<div class="modal-close-div">
				<a href="#!" class="modal-action modal-close"><i class="mdi-navigation-close"></i></a>
			</div>
		</div>
	</div>

	<section id="createjobsection" style="display: none;" class="createjobs-content">
		<form name="create-job" ng-submit="submit();">
			<div class="row">
				<div class="col s12 m12">
					<ul class="collapsible" data-collapsible="expandable">
						<li>
							<div class="collapsible-header active"><i class="material-icons">filter_drama</i>Basic Details</div>
							<div class="collapsible-body">
								<div class="row">
									<div class="col s12 m12">
										<div class="input-field col s12">
											<input id="title" type="text" class="validate" required ng-model="job.title">
											<label for="title" data-error="wrong" data-success="right">Job Title</label>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s12 m12">
										<div class="input-field col s12">
											<input id="about" type="text" class="validate" ng-model="job.about">
											<label for="about">About</label>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s12 m12">
										<div class="input-field col s12">
											<input id="date" type="text" class="validate" required ng-model="job.date" pattern="\d{4}-\d{2}-\d{2}" placeholder="yyyy-mm-dd format">
											<label for="date" data-error="wrong" data-success="right">Start Date</label>
										</div>
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="collapsible-header"><i class="material-icons">place</i>Job Requirements</div>
							<div class="collapsible-body">
								<div class="row">
									<div class="col s12 m12">
										<div class="input-field col s12">
											<input id="vacancy" type="number" class="validate" ng-model="job.vacancy">
											<label for="vacancy">Vacancy</label>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s12 m12">
										<div class="input-field col s12">
											<input id="salary" type="number" class="validate" ng-model="job.salary">
											<label for="salary">Salary Per Month</label>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s12 m12">
										<div class="input-field col s12">
											<input id="roles" type="text" class="validate" ng-model="job.roles">
											<label for="roles">Roles and Responsibilities</label>
										</div>
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="collapsible-header"><i class="material-icons">whatshot</i>Contact Preferences</div>
							<div class="collapsible-body">
								<div class="row">
									<div class="col s12 m12">
										<div class="input-field col s12">
											<input id="contact-email" type="text" class="validate" ng-model="job.contactemail">
											<label for="contact-email" data-error="wrong" data-success="right">Contact Email</label>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s12 m12">
										<div class="input-field col s12">
											<input id="contact-phone" type="text" class="validate" ng-model="job.contactphone">
											<label for="contact-phone" data-error="wrong" data-success="right">Contact Phone</label>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s12 m12">
										<div class="input-field col s12">
											<input id="contact-link" type="text" class="validate" ng-model="job.contactlink">
											<label for="contact-link" data-error="wrong" data-success="right">Contact Link</label>
										</div>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="col s12 m12">
					<button type="submit" class="btn waves-effect waves-light teal darken-1 btn-full-width">Save</button>
				</div>
			</div>
		</form>

		<div class="row">
			<div class="col s12">
				<h5>Job Status</h5>
				<div class="card-panel grey lighten-5 hide-on-small-only">
					<table class="striped">
						<thead>
							<tr>
								<th>Title</th>
								<th>Description</th>
								<th>Start Date</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="j in createjob">
								<td>@{{j.title}}</td>
								<td>@{{j.about}}</td>
								<td>@{{j.date}}</td>
								<td><span ng-if="j.status==true">Success</span><a class="waves-effect waves-light btn teal lighten-1" ng-if="j.status ==false" ng-click="sendagain(j);">Send<i class="material-icons right">send</i></a></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="card-panel grey lighten-5 hide-on-med-and-up" ng-repeat="j in createjob">
					<table class="striped">
						<tbody>
							<tr>
								<th>Title</th>
								<td>@{{j.title}}</td>
							</tr>
							<tr>
								<th>Description</th>
								<td>@{{j.about}}</td>
							</tr>
							<tr>
								<th>Start Date</th>
								<td>@{{j.date}}</td>
							</tr>
							<tr>
								<th>Action</th>
								<td><span ng-if="j.status==true">Success</span><a class="waves-effect waves-light btn teal lighten-1" ng-if="j.status ==false" ng-click="sendagain(j);">Send<i class="material-icons right">send</i></a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<!--<ul>
			<li ng-repeat="j in createjob">
				<table class="createjob">
					<tr>
						<td>Title</td>
						<td>@{{j.title}}</td>
					</tr>
					<tr>
						<td>Status</td>
						<td>@{{j.status}}</td>
					</tr>
				</table>
			</li>
		</ul>-->
	</section>

	<section id="jobssection" class="jobs-content">

		<div id="test1" ng-if="tab == 1">
			<section class="content_sec" ng-repeat="job in jobs">
				<div class="container margin-top" ng-click="change(job)" style="cursor: pointer;">
					<div class="section_1 row">
						<div class=" col s8">
							<p class="job_intro">
								<b>@{{job.title}}</b>
							</p>
							<p>Start date on <span><b>@{{job.start_date_read}}</b></span></p>
							<p><b>@{{job.hired_count}} hired / @{{job.applicants_count}}</b></p>
						</div>
						<div class="col s4">
							<span class="badge badge__status red text-white" ng-if="job.status == 0">Deleted</span>
							<span class="badge badge__status light-green text-white" ng-if="job.status == 1">Pending Approval</span>
							<span class="badge badge__status light-green text-white" ng-if="job.status == 2">Ready to Start</span>
							<span class="badge badge__status green text-white" ng-if="job.status == 3">Job Completed</span>
							<span class="badge badge__status light-green text-white" ng-if="job.status == 4">Rejected</span>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div id="test2" ng-if="tab == 2">
			<section class="content_sec" ng-repeat="app in active.applicants">
				<div class="container">
					<div class="section_1 row" ng-click="open(app,$index)">
						<div class=" col s4 m3 l2">
							<img src="/images/avatar.jpg" alt="pic" class="circle" width="110px" height="110px" style="margin-top: 10px;">
						</div>
						<div class="col s8 m9 l10">
							<p class="job_intro">
								<b>@{{app.name}}</b>
								<a href="#" class="light-green darken-2 btn activate_button" ng-if="app.status == 1">Hire</a>
								<a href="#" class="light-green darken-2 btn activate_button" ng-if="app.status == 2">UnHire</a>
								<a href="#" class="red darken-2 btn activate_button" ng-if="app.status == 3">Archived</a>
							</p>
							<p>Start date on <span><b>@{{app.created_at|time}}</b></span></p>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div id="test3" ng-if="tab == 3">
			<section class="content_sec" ng-repeat="app in active.hired">
				<div class="container">
					<div class="section_1 row" ng-click="open(app,$index)">
						<div class=" col s4 m3 l2">
							<img src="/images/avatar.jpg" alt="pic" class="circle" width="110px" height="110px" style="margin-top: 10px;">
						</div>
						<div class="col s8 m9 l10">
							<p class="job_intro">
								<b>@{{app.name}}</b>
								<a href="#" class="light-green darken-2 btn activate_button" ng-if="app.status == 1" ng-click="hire($index);">Hire</a>
								<a href="#" class="light-green darken-2 btn activate_button" ng-if="app.status == 2" ng-click="unhire($index);">UnHire</a>
								<a href="#" class="red darken-2 btn activate_button" ng-if="app.status == 3" ng-click="unhire($index);">Archived</a>
							</p>
							<p>Start date on <span><b>@{{app.created_at|time}}</b></span></p>
						</div>
					</div>
				</div>
			</section>


			<!-- <section class="content_sec">
				<div class="container">
					<div class="section_1 row">
						<div class=" col s4 m3 l2">
						<img src="/images/avatar.jpg" alt="pic" class="circle" width="110px" height="110px" style="margin-top: 10px;">
						</div>
						<div class="col s8 m9 l10">
						<p class="job_intro">
							<b>sadasd</b>
							<a herf="#" class="light-green darken-2 btn activate_button">Hire</a>
						</p>
						<p>Start date on <span><b>asd 987</b></span></p>
						<p><b>0 hired / 1</b></p>
						</div>
					</div>
				</div>
			</section> -->

		</div>

	</section>


	<script type="text/javascript" src="{{asset('js/materialize/plugins/jquery-1.11.2.min.js')}}"></script>
	<!--materialize js-->
	<script type="text/javascript" src="/pwa-assets/assets/moment.min.js" charset="utf-8"></script>
	<script type="text/javascript" src="{{ asset('js/materialize/materialize.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/angular/angular.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/angular/angular-route.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/angular/angular-animate.min.js') }}"></script>
	<script type="text/javascript" src="/pwa-assets/assets/app.js?v=1"></script>

</body>
</html>