@extends('layouts.materialize')
@section('content')

<link rel="stylesheet" href="/style.css"/>


<div data-widget-group="group1" class="ui-sortable" style="padding-left: 140px; margin-top:20px;">
	<div class="row">
		<div class="col-sm-12">

			<div class="collection card-panel" style="padding:0px !important;margin-top:20px">
	            <div class="collection-item black-text" style="background-color:#9575cd;"  >
	            <h4 style="font-size:24px;color:#fff;">Company Profile</h4>
	            </div>
    		</div>

				<div data-widget-group="group1" class="ui-sortable">

					<div class="panel panel-default">

						<div class="panel-editbox" data-widget-controls=""></div>

						<div class="panel-body" style="display: block;">
                    		   {!! Form::model($user,['method'=>'POST','route'=>['admin.save.company',$user->id],'class'=>'form-horizontal row-bordere','enctype'=>'multipart/form-data'])!!}
                    		
							    
							    <div class="form-group is-empty">
									<label class="col-sm-2 control-label">Logo </label>
									<div class="col-sm-8">
										 @if( $company->company_logo == null)
		                            	<img src="http://www.gravatar.com/avatar/568c98a61e020dccaaba916ea15c561f.jpg?s=50&amp;d=mm&amp;r=g" class="user-image" style="width:150px;height:150px"  alt="User Image">
		                            	@else
		                           		<img  style="width:150px;height:150px"  alt="Profile Image" src={{ env('companyLogo','/company/logo/').$company->company_logo  }}> 
		                            	@endif
									</div>
								<span class="material-input"></span></div>


							    <div class="form-group is-empty is-fileinput">
									<label class="col-sm-2 control-label">Company Logo</label>
									<div class="file-field input-field">								
								      	<button type="button" class="btn btn-fab btn-fab-mini" style="margin-right:10px">
								        <i class="material-icons">attach_file</i></button>						       
										<input type="file" name="company_logo">
								     	<div class="col-sm-7 input-group">
								        <input class="file-path validate form-control" type="text">
								      </div>
								    </div>					
								<span class="material-input"></span></div>


								<div class="form-group is-empty">
									<label class="col-sm-2 control-label">Company Name</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" value="{{ $user->name }}" name="name" >
									</div>
								<span class="material-input"></span></div>

								<div class="form-group is-empty">
									<label class="col-sm-2 control-label">Company Email</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" value="{{ $user->email }}" name="email">
									</div>
								<span class="material-input"></span></div>

								<div class="form-group is-empty">
									<label class="col-sm-2 control-label">Company Address</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" name="company_address" value="{{ $company->company_address }}" >
									</div>
								<span class="material-input"></span></div>

								<div class="form-group is-empty">
									<label class="col-sm-2 control-label">Company Contact No</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" name="company_contactno" value="{{ $company->company_contactno }}" >
									</div>
								<span class="material-input"></span></div>

								<div class="form-group is-empty">
									<label class="col-sm-2 control-label">Person Incharge Name</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" name="person_incharge_name_1" value="{{ $company->person_incharge_name_1 }}">
									</div>
								<span class="material-input"></span></div>


								<div class="form-group is-empty">
									<label class="col-sm-2 control-label">Person Incharge Email</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" name="person_incharge_email_1" value="{{ $company->person_incharge_email_1 }}">
									</div>
								<span class="material-input"></span></div>


								<div class="form-group is-empty">
									<label class="col-sm-2 control-label">Person Incharge Contact No</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" name="person_incharge_contactno_1" value="{{ $company->person_incharge_contactno_1 }}">
									</div>
								<span class="material-input"></span></div>

								<div class="panel-footer">
							<div class="row">
								<div class="col-sm-8 col-sm-offset-4">
								    {!! Form::submit(trans('Submit'), ['class' => 'btn-raised btn-primary btn']) !!}
									 <button class="btn-raised btn-warning btn" href="{{ route('admin.company') }}"> Cancel</button>
								</div>
							</div>
						</div>

						
							</form>
							
						</div>
						
					</div>

				</div>
			</div>
		</div>
	</div>


@endsection

@section('after-script')
@php ($error = '')
@if ($errors->any())
        @foreach ($errors->all() as $err)
            {{ $error = $err }}<br/>    
        @endforeach
        <script>toastr["error"]("{!! $error !!}", "Error")</script>
@elseif (Session::get('flash_success'))
    
        @if(is_array(json_decode(Session::get('flash_success'),true)))
            {{ $success = implode('', Session::get('flash_success')->all(':message<br/>')) }}
        @else
            <script>toastr["success"]("{!! Session::get('flash_success') !!}", "Success")</script>
        @endif
    
@endif

@endsection

