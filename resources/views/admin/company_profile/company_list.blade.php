@extends('layouts.materialize')
@section('content')

<style type="text/css">

.button-submit{
  background-color:transparent;
  border:none;
}
</style>

<div class="right-align">
  <p><a href="{{route('admin.create.company')}}" class="btn waves-effect waves-light  purple ">Create Company</a></p>
</div>
             
 <div class="row" style="padding-left: 40px; margin-top:20px;">        
      <div class="col s12">
          <div class="row section">
            <div class="col s12 m12 l12">
              <nav>
                 <div class="nav-wrapper">
                     <div class="col s12">
                        <a href="{{route('dashboard')}}" class="breadcrumb">Dashboard</a>
                        <a href="#" class="breadcrumb">All Companies </a>
                      </div>
                      {{-- <span class="new badge">{{$countusers}}</span> --}}
                 </div>
              </nav>
           </div>
         </div>

         

       
            <div class="card-panel">
                <div class="row">
                    <table class="bordered">
                      <thead>
                        <tr>
                            <th data-field="Logo">Logo</th>
                            <th data-field="name">Company</th>
                            <th data-field="email">Email</th>
                            <th data-field="industry">Industry</th>
                            <th data-field="price">Action</th>
                        </tr>
                      </thead>

                    <tbody>
                        <tr>
                            <td></td>
                            <td class="browser-default">
                                <form action="{!! route('admin.company.search.name') !!}" method="get"> 
                                   <input name="name"  placeholder="Company Name"  type="text" style="width:80% !important;">
                                   <button class="button-submit">
                                     <i class="mdi-action-search"></i>
                                   </button>
                                </form>
                            </td>
                            <td class="browser-default">
                                <form action="{!! route('admin.company.search.mail') !!}" method="get"> 
                                    <input name="mail"  placeholder="Company Email"  style="width:80% !important;" type="text">
                                    <button class="button-submit">
                                      <i class="mdi-action-search"></i>
                                   </button>
                                </form>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>

                        @foreach($users as $user)
                          <tr>
                              @if( $user->company->company_logo == null)
                                  <td><img src="http://www.gravatar.com/avatar/568c98a61e020dccaaba916ea15c561f.jpg?s=50&amp;d=mm&amp;r=g" class="user-image" style="width:50px;height:50px"  alt="User Image"></td>
                              @else
                                  <td><img  style="width:50px;height:50px"  alt="Profile Image" src={{ env('companyLogo','/company/logo/').$user->company->company_logo  }} /></td>
                              @endif

                             <td>{{ $user->name }}</td>
                             <td>{{ $user->email }}</td>  
                             <td>{{ $user->name }}</td>  
                             <td>                                     
                                <a class="btn dropdown-button waves-effect waves-light purple lighten-2" href="#!" data-activates="dropdown{{ $user->id }}">More<i class="mdi-navigation-arrow-drop-down right"></i></a>
                                <ul id="dropdown{{ $user->id }}" class="dropdown-content" style="width: 182px; position: absolute; top: 357.516px; left: 975.953px; opacity: 1; display: none;">
                                  <li><a href="{{ route('admin.company.jobs',$user->id) }}" class="-text" style="color:purple lighten-2">Jobs</a></li>
                                  <li><a href="{{ route('admin.edit.company',$user->id) }}" class="-text" style="color:purple lighten-2">Edit</a></li>
                               </ul>
                             </td>
                          </tr>
                        @endforeach                   
                      </tbody>
                    </table>
              </div>
         </div>

       
         <div style="text-align:center">
              {{ $users->render() }}
        </div>
    </div>
</div>

@endsection