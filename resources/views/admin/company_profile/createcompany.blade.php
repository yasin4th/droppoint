@extends('layouts.materialize')
@section('content')

{{-- <link rel="stylesheet" href="/style.css"/> --}}
<link rel="stylesheet" href="/css/materialize.min.css"/>
   
   
<div class="row">
	<div class="col-sm-12">
		<div class="card" style=" margin-top:20px">
			
			<div class="card-content">
				<div>
					<h5>Create Company</h5>
				</div>
			</div>

			<div class="card-content">			
							   
			   <form action="{{ route('admin.store.company') }}" method="POST" class="col s12" enctype="multipart/form-data" >
					 {!! csrf_field() !!}

					<div class="row">
						<div class="file-field input-field">
					      <div class="btn">
					        <span>File</span>
					        <input type="file">
					      </div>
					      <div class="file-path-wrapper">
					        <input class="file-path validate" type="text" placeholder="Upload your Company Logo">
					      </div>
					    </div>
				    </div>

					<div class="row">
				        <div class="input-field col s12">
				          <input name="name" type="text" class="validate">
				          <label>Company Name</label>
				        </div>
				    </div>

					<div class="row">
				        <div class="input-field col s12">
				          <input name="email" type="text" class="validate">
				          <label>Email</label>
				        </div>
				    </div>

					<div class="row">
				        <div class="input-field col s12">
				          <input name="password" id="password" type="password" class="validate" required>
				          <label>Password</label>
				        </div>
				    </div>

				    <div class="row">
				        <div class="input-field col s12">
				          <input name="company_address" type="text" class="validate">
				          <label>Address</label>
				        </div>
				    </div>

					<div class="row">
				        <div class="input-field col s12">
				          <input name="company_contactno" type="text" class="validate">
				          <label>Company Contact Number</label>
				        </div>
				    </div>

				    <div class="row">
				        <div class="input-field col s12">
				          <input name="person_incharge_name_1" type="text" class="validate">
				          <label>Person Incharge Name</label>
				        </div>
				    </div>

					<div class="row">
				        <div class="input-field col s12">
				          <input name="person_incharge_email_1" type="text" class="validate">
				          <label>Person Incharge Email</label>
				        </div>
				    </div>

					<div class="row">
				        <div class="input-field col s12">
				          <input name="person_incharge_contactno_1" type="text" class="validate">
				          <label>Person Incharge Contact No</label>
				        </div>
				    </div>

					<div class="card-action">
				
					<div class="row">
						<div class="col-sm-8 col-sm-offset-4">
						    {!! Form::submit(trans('Submit'), ['class' => 'btn-raised btn-primary btn']) !!}
							 <button class="btn-raised btn-warning btn" href="{{ route('admin.company') }}"> Cancel</button>
						</div>
					</div>
				
				</form>
			</div>
		</div>
	</div>
</div>


@endsection

@section('after-script')
@php ($error = '')
@if ($errors->any())
        @foreach ($errors->all() as $err)
            {{ $error = $err }}<br/>    
        @endforeach
        <script>toastr["error"]("{!! $error !!}", "Error")</script>
@elseif (Session::get('flash_success'))
    
        @if(is_array(json_decode(Session::get('flash_success'),true)))
            {{ $success = implode('', Session::get('flash_success')->all(':message<br/>')) }}
        @else
            <script>toastr["success"]("{!! Session::get('flash_success') !!}", "Success")</script>
        @endif
    
@endif

@endsection

