@extends('layouts.material')

@section('content')

<div class="">

	<div class="">
		<div class="">
			<div id="map1">
			</div>
			<input type="hidden" name="latitude" id="latitude"><input type="hidden" name="longitude" id="longitude">
		</div>
	</div>

</div>
@endsection

@section('scripts')

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFvcodwxV3SQptSx4LQ-H-gZVcAk-1lyI&libraries=visualization,places&callback=initMap"
		 async defer></script>
	<script>
		$(document).ready(function() {
			$('.container').removeClass('container');
		});

		var map, markers = [], places = [];

		function initMap() {
			map = new google.maps.Map(document.getElementById('map1'), {
				zoom: 5,
				center: {lat: 26.775, lng: 78}
			});

			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position) {
					var pos = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};

					map.setCenter(pos);

				}, function() {
					handleLocationError(true, infoWindow, map.getCenter());
				});
			} else {
				// Browser doesn't support Geolocation
				handleLocationError(false, infoWindow, map.getCenter());
			}


			@foreach($jobs as $job)
				@if ($job->latitude)
					places.push({
						latlng: new google.maps.LatLng({{$job->latitude}}, {{$job->longitude}}),
						id: '{{$job->id}}',
						title:'{{$job->title}}',
						salary:'{{$job->title}}\nRM {{$job->salary}} {{$job->salary_unit}}\n{{$job->start_date->format('D M j')}}'
					});
				@endif
			@endforeach

			google.maps.event.addDomListener(map,'zoom_changed', function() {
				var zoom =  map.getZoom();

				if (zoom == 7) {
					map.clearOverlays();
					map.addMarkers(0);
					console.log("Zoom 7")
				} else if (zoom == 8 || zoom == 10) {
					map.clearOverlays();
					map.addMarkers(1);
					console.log("Zoom 8 - 10")
				} else if (zoom == 11) {
					map.clearOverlays();
					map.addMarkers(2);
					console.log("Zoom 11")
				}
			});

			google.maps.Map.prototype.clearOverlays = function() {
				for (var i = 0; i < markers.length; i++ ) {
					markers[i].setMap(null);
				}
				markers.length = 0;
			};

			google.maps.Map.prototype.addMarkers = function(flag) {

				places.forEach(function(p) {
					if (flag == 0) {
						markers.push(new google.maps.Marker({
							map: map,
							position: p.latlng,
							// icon: getCircle('red'),
							strokeColor: 'white',
							strokeWeight: 4
						}));
					} else if (flag == 1){
						markers.push(new google.maps.Marker({
							map: map,
							position: p.latlng,
							icon: getCircle1('blue'),
							strokeColor: 'white',
							strokeWeight: 8
						}));
					} else if (flag == 2) {
						var m = new google.maps.Marker({
							map: map,
							position: p.latlng,
							title: p.salary
						});
						google.maps.event.addListener(m, 'click', function(){location.replace('/job/'+p.id+'/edit');});
						markers.push(m);
					}
				});
			};

			map.addMarkers(0);
		}

		function getCircle(color) {
			var size = Math.random();

			return {
				path: google.maps.SymbolPath.CIRCLE,
				fillColor: color,
				fillOpacity: .2,
				scale: size*50 ,
				strokeColor: 'white',
				strokeWeight: .5
			};
		}

		function getCircle1(color) {

			return {
				path: google.maps.SymbolPath.CIRCLE,
				fillColor: color,
				fillOpacity: .8,
				scale: 10 ,
				strokeColor: 'white',
				strokeWeight: .5
			};
		}
	</script>
@endsection