@extends('layouts.material')

@section('content')

<div class="row">

	<div class="col s12 m3">
			@include('includes.admin.sidemenu')
	</div>
	<div class="col m9">

		<div id="card-stats">
			<div class="row">
				<div class="col s12 m6 l3">
					<div class="card">
						<div class="card-content  green white-text">
							<p class="card-stats-title"><i class="mdi-social-group-add"></i> New Users</p>
							<h4 class="card-stats-number">{{$total= DB::table('users')->where('role', 2)->get()->count() }}</h4>
							@php
								$yesterday = DB::table('users')->where('role',2)->where('created_at','<',date('Y-m-d'))->get()->count();
								$balance = $total-$yesterday ;
							@endphp
							<p class="card-stats-compare"><i class="mdi-hardware-keyboard-arrow-up"></i>{{ $balance }}% <span class="green-text text-lighten-5">from yesterday</span>
							</p>
						</div>
						<div class="card-action  green darken-2">
							<div id="clients-bar" class="center-align"><canvas width="227" height="25" style="display: inline-block; width: 227px; height: 25px; vertical-align: top;"></canvas></div>
						</div>
					</div>
				</div>
				<div class="col s12 m6 l3">
					<div class="card">
						<div class="card-content pink lighten-1 white-text">
							<p class="card-stats-title"><i class="mdi-editor-insert-drive-file"></i> Active Jobs</p>
							<h4 class="card-stats-number">{{ $total= DB::table('jobs')->where('start_date', '>=', date('Y-m-d'))->get()->count() }}</h4>
							@php
								$previous= DB::table('jobs')->where('start_date', '>=', date("y-m-d", strtotime("-1 months")))->get()->count();
								$balance = $total-$previous ;
							@endphp
							<p class="card-stats-compare"><i class="mdi-hardware-keyboard-arrow-down"></i> {{ $balance }}% <span class="deep-purple-text text-lighten-5">from last month</span>
							</p>
						</div>
						<div class="card-action  pink darken-2">
							<div id="invoice-line" class="center-align"><canvas style="display: inline-block; width: 202px; height: 25px; vertical-align: top;" width="202" height="25"></canvas></div>
						</div>
					</div>
				</div>
				<div class="col s12 m6 l3 hide">
					<div class="card">
						<div class="card-content blue-grey white-text">
							<p class="card-stats-title"><i class="mdi-action-trending-up"></i> Today Profit</p>
							<h4 class="card-stats-number">$0.00</h4>
							<p class="card-stats-compare"><i class="mdi-hardware-keyboard-arrow-up"></i> 0% <span class="blue-grey-text text-lighten-5">from yesterday</span>
							</p>
						</div>
						<div class="card-action blue-grey darken-2">
							<div id="profit-tristate" class="center-align"><canvas width="227" height="25" style="display: inline-block; width: 227px; height: 25px; vertical-align: top;"></canvas></div>
						</div>
					</div>
				</div>
				<div class="col s12 m6 l3 hide">
					<div class="card">
						<div class="card-content purple white-text">
							<p class="card-stats-title"><i class="mdi-editor-attach-money"></i>Total Sales</p>
							<h4 class="card-stats-number">$0.00</h4>
							<p class="card-stats-compare"><i class="mdi-hardware-keyboard-arrow-up"></i> 0% <span class="purple-text text-lighten-5">last month</span>
							</p>
						</div>
						<div class="card-action purple darken-2">
							<div id="sales-compositebar" class="center-align"><canvas width="214" height="25" style="display: inline-block; width: 214px; height: 25px; vertical-align: top;"></canvas></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		@include('includes.alert')

		<div class="row">
			<div class="col s6">
				<div class="header-search-wrapper">
					<i class="mdi-action-search"></i>
					<form role="form" action="{{ route('job.all') }}" name="search" onsubmit="load_more();return false;">
						<input type="text" name="query" class="header-search-input z-depth-2 table-search" placeholder="Search by Job Title or User Name" value="">
					</form>
				</div>
			</div>
		</div>

		<div class="card">
			<div class="card-content hide" id="content1">
				<div class="row">
					<div class="col s6">
						<h4>Recent Jobs</h4>
					</div>
				</div>
				<table class="bordered responsive-table admin-dashboard-table" id="jobs">
					<thead>
						<tr>
							<th>Job Title</th>
							<th>Salary(RM)</th>
							<th>Vacancies</th>
							<!-- <th>User</th>
							<th>Location</th>
							<th>No.of working Days</th>
							<th>Date</th> -->
							<!-- <th>Applicants</th> -->
							<!-- <th>Action</th> -->
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
				<br>
				<br>
				<br>
				<br>
			</div>
		</div>
		<div class="col s12 hide" id="content2">
			<div>Showing 1 to <span id="to"></span> of <span id="total"></span> entries</div>
			<div class="row center loadmorediv">
				<button class="btn cyan waves-effect waves-light" onclick="load_more()">View more</button>
			</div>
		</div>
	</div>

</div>

	<div id="preview" class="modal modal-fixed-footer preview-modal">
		<div class="modal-content">
			<!-- <h5 class="center">Preview Job</h5> -->
			<div class="row">

				<div class="col m12 s12">
					<div class="card-content">
						<div class="row">
							<div class="col s12">
								<a href="#!" class="modal-action modal-close close-preview"><i class="mdi-navigation-cancel"></i></a>
							</div>
						</div>
						<div class="row">
							<div class="col s12">

								<img src="#" class="responsive-img" id="Avatar">
							</div>
						</div>
						<div class="row  retail-sales">
							<div class="col s12">
								<h5 id="Title"></h5>
								<P class="" id="Location"></P>
							</div>
						</div>
						<hr>
						<div class="row icon-div">
						<div class="col s2 m2">
						<img src="/images/dollar.png" class="responsive-img">
						</div>
						<div class="col s5 m5">
						<h6 >Salary</h6>
						</div>
						<div class="col s5 m5">
						<h6 id="Salary"></h6>
						</div>
					</div>
					<div class="row icon-div">
						<div class="col s2 m2">
						<img src="/images/calendar.png" class="responsive-img">
						</div>
						<div class="col s5 m5">
						<h6> Working start Date</h6>
						</div>
						<div class="col s5 m5">
						<h6 id="StartDate" </h6>
						</div>
					</div>
					<div class="row icon-div">
						<div class="col s2 m2">
						<img src="/images/watch.png" class="responsive-img">
						</div>
						<div class="col s5 m5">
						<h6>Working Hours</h6>
						</div>
						<div class="col s5 m5">
						<h6 id="StartTime"></h6>
						</div>
					</div>
					<div class="row icon-div">
						<div class="col s2 m2">
						<img src="/images/job_icon.png" class="responsive-img">
						</div>
						<div class="col s5 m5">
						<h6>Type of job</h6>
						</div>
						<div class="col s5 m5">
						<h6 id="Type"></h6>
						</div>
					</div>
						<!--<div class="row icon-div">
							<div class="col s3 m3">
								<img src="/images/dollar.png" class="responsive-img">
								<h6 </h6>
							</div>
							<div class="col s3 m3">
								<img src="/images/calendar.png" class="responsive-img">
								<h6 id=""></h6>
							</div>
							<div class="col s3 m3">
								<img src="/images/watch.png" class="responsive-img">
								<h6 id="">10am-6pm</h6>
							</div>
							<div class="col s3 m3">
								<img src="/images/car_1.png" class="responsive-img">
								<h6>0.9km</h6>
							</div>
						</div>!-->
						<!-- <hr>
						<div class="row about-job">
							<div class="col s12">
								<h5>About this job</h5>
								<p id="About"></p>
							</div>
						</div> -->
						<hr>
						<div class="row role-responsibility">
							<div class="col s12">
								<h5>Role and Responsibilities</h5>
								<textarea class="materialize-textarea border_hide" rows="6" cols="8"></textarea>
							</div>
						</div>
<!-- 						<hr>
						<div class="row about-job">
							<div class="col s12">
								<h5>Tags</h5>
								<ul>
									<li></li>
								</ul>
							</div>
						</div> -->
						<hr>
						<div class="row dress-code">
							<div class="col s12">
								<h5>Dress Code</h5>
								<p id="Uniform"></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




@endsection

@section('scripts')
<script src="/js/materialize/plugins/fullcalendar/lib/moment.min.js" type="text/javascript" charset="utf-8"></script>

<script>
	var page = 1, $jobs = [];

	$(document).ready (function(){
		load_more();

		$('[name=query]').change(function (e) {
			page = 1; //refresh
		});
	});

	load_more = function()
	{
		if(page != 0)
		{
			query = $('[name=query]').val();
			$.ajax({
				type : 'get',
				url  : '{{ route('admin.jobs.recent') }}?page='+page+'&query='+query,
			}).done(function(res) {
				fillJobs(res);
			});
		}
	}

	fillJobs = function(result)
	{
		html = '';

		if(!result.next_page_url) {
			$('.loadmorediv').addClass('hide');
		} else {
			$('.loadmorediv').removeClass('hide');
		}

		if(result.data.length == 0){
			// html += '<div class="card job-count0">';
			// html += '<div class="card-content">';
			html += '<h4 class="center"><a href="{{ route('job.create') }}">Post a job to get started.</a></h4>';
			// html += '</div>';
			// html += '</div>';
			$('#content1').html(html);
			$('.loadmorediv').addClass('hide');
			$('#content1').removeClass('hide');
			$('#content1').parent().addClass('job-count0');
		}
		else
		{
			if (page == 1)
			{
				$('#jobs tbody').html('');
			}

			query = $('[name=query]').val();
			if(query != '')
			{
				$('#content1 h4').html("Search results for '"+query+"'");
			}else{
				$('#content1 h4').html("Recent Jobs");
			}


			$('#content1').removeClass('hide');
			$('#content2').removeClass('hide');

			html = '';
			$jobs = result.data;
			result.data.forEach(function(job, index)
			{
				html += '<tr style="display:none;">';
				html += '<td><a href="/job/'+job.id+'/edit"><b>' + job.title + '</b></a></br>'+
						moment(job.start_date).format('DD MMMM, YYYY')+'-'+
						job.state +', '+job.country+'</br>'+
						'<a href="/admin/users/'+job.user_id+'/edit">'+ job.name +'</a></td>';
				/*html += '<td>'+ job.working_hours +'</td>';*/
				html += '<td>'+job.salary+' '+job.salary_unit+'</td>';
				html += '<td><a href="/job/'+job.id+'/applicants">'+job.hired_count+' hired / ' + job.vacancy + '</a></td>';
				/*html += '<td><a href="/job/'+job.id+'/applicants">'+ job.applicants_count +'</a></td>';*/
				html += '<td><ul id="dropdown' + job.id + '" class="dropdown-content">';
				html += '<li><a class="preview" href="#preview" onclick="preview('+index+')">Preview Job</a></li>';
				html += '<li><a href="/job/'+job.id+'/edit">Edit</a></li>';
				html += '<li><a href="/admin/jobs/'+job.id+'/delete" class="DeleteJob">Delete</a></li>';
				html += '</ul><a href="#" class="dropdown-button font-color" data-activates="dropdown'+job.id+'" data-beloworigin="true">More<i class="mdi-navigation-arrow-drop-down"></i></a></td>';
				html += '</tr>';
			});

			$('#jobs tbody').append(html);
			$('tr').show(500);+
			$('#to').html(result.to);
			$('#total').html(result.total);
			$('.dropdown-button').dropdown();
			$('.preview').click(function(){
				$('#preview').openModal();
			});
			page = (result.current_page == result.last_page) ? 0 : page + 1;
		}

	}

	preview = function(index)
	{ 
		console.log($jobs[index]);
		$('#Avatar').attr('src',$jobs[index].cover_picture);;
		$('#Location').html($jobs[index].location);
		$('#Salary').html("RM"+ " "+ $jobs[index].salary +" "+ $jobs[index].salary_unit);
		$('#StartTime').html($jobs[index].start_time +" "+"to"+" "+ $jobs[index].end_time);
		$('#StartDate').html($jobs[index].start_date);
		$('#About').html($jobs[index].about);
		$('#Uniform').html($jobs[index].uniform);
		$('#Title').html($jobs[index].title);
		$('#Type').html($jobs[index].type);
		$('.materialize-textarea').html($jobs[index].requirements);

		/*
		html += '<li>'+ $jobs[index].requirements +'</li>';*/
		/*console.log($jobs[index].tags.name);
		html = '';
		$jobs[index].tags.forEach(function(tag) {
			if (tag.id > 6 && tag.id < 12) {
				html +='<li>'+ tag.name + '</li>';
			}
		});
*/
		$('#preview ul').html(html);
	}

	$(document).ready(function(){
		$('.DeleteJob').click(function(e){
			alert ('abcd');
			/*e.preventDefault();
			var answer = confirm('Do you want to delete this job?');
			if(answer == true){
				location.replace($(this).attr('href'));
			}
			else{
				return false;
			}*/
		});
	});

</script>
@endsection
