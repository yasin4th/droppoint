@extends('layouts.material')

@section('content')
{{-- {!! Breadcrumbs::renderIfExists('job') !!} --}}
<div class="row">

	<div class="col s12 m3">
		<div class="card profile-sidebar">
			@include('includes.admin.sidemenu')
		</div>
	</div>
	<div class="col m9">
		<div id="rootwizard1" class="form-wizard form-wizard-horizontal">
			<form class="form floating-label" method="POST" action="{{ route('admin.jobs.store') }}" onsubmit="myButton.disabled = true; return true;" enctype="multipart/form-data">
				{{ csrf_field() }}

				<div class="form-wizard-nav">
					<div class="progress" style="width: 66.667%;"><div class="progress-bar progress-bar-primary"></div></div>
					<ul class="nav nav-justified nav-pills tabs">
						<li class="tab active"><a href="#tab1" class="active"><span class="step">1</span> <span class="title">Details</span></a></li>
						<li class="tab"><a href="#tab2"><span class="step">2</span> <span class="title">Location</span></a></li>
						<li class="tab"><a href="#tab3"><span class="step">3</span> <span class="title">Tags</span></a></li>
					</ul>
				</div>
				<div class="card">
					<div class="card-content">
						<div id="tab1" class="tabContent">
							<div class="col s12">
								<h5>Create your job</h5>
							</div>
							<br><br>
							<div class="input-field col s12 m6">
								<select name="user_id" id="user">
									<option value=""> Please Select --</option>
									@foreach($users as $user)
										<option value="{{ $user->id }}"> {{ $user->name }}</option>
									@endforeach
								</select>
								<label for="user">Please select a company</label>
							</div>
							<div class="col s12 m6"></div>
							<div class="col s12">
								<p>What kind of jobs do you offer?</p>
							</div>
							<div class="col s6 m3">
								<select class="browser-default" name="type">
									<option value="part-time">Part time</option>
									<option value="full-time">Full time</option>
								</select>
							</div>
							<div class="col s6 m3">
								<select name="vacancy" class="browser-default">
									<option value="1">No of vacancy 1</option>
									<option value="2">No of vacancy 2</option>
									<option value="3">No of vacancy 3</option>
									<option value="4">No of vacancy 4</option>
									<option value="5">No of vacancy 5</option>
									<option value="6">No of vacancy 6</option>
									<option value="7">No of vacancy 7</option>
									<option value="8">No of vacancy 8</option>
									<option value="9">No of vacancy 9</option>
									<option value="10">No of vacancy 10</option>
								</select>
							</div>
							<div class="col s7"></div>

							<div class="{{ $errors->has('title') ? ' has-error' : '' }}">
								<div class="input-field col s12 m6 mrg-t30">
									<input type="text" name="title" class="validate" placeholder="Be simple & specific! e.g. Pizza Hut Waiter/Waitress" required>
									<label for="title">Job Title</label>
									@if ($errors->has('title'))
									<span class="help-block">
										<strong>{{ $errors->first('title') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="col s7"></div>
							<div class="input-field col s12 m6">
								<textarea name="about" class="materialize-textarea validate" cols="30" rows="10" length="500" maxlength="500"></textarea>
								<label for="about">Briefly describe the job</label>
								{{-- <span class="character-counter" style="float: right; font-size: 12px; height: 1px;"> --}}
								</span>
							</div>
							<div class="input-field col s12">
								<p class="mrg-bot10"><b>Job Descriptions</b></p>
								<h6>Craft concise yet interesting job descriptions to attract your ideal applicants (Roles and responsibilities, dress code, etc)</h6>
								<textarea class="textarea-hauto" id="requirements" name="requirements" cols="30" rows="6">{{ isset($job) ? $job->requirements : ''}}</textarea>

								<!-- <textarea name="requirements" class="materialize-textarea validate" cols="30" rows="10" length="500" maxlength="500"></textarea>
								<label for="about">Job Requirements</label> -->
								{{-- <span class="character-counter" style="float: right; font-size: 12px; height: 1px;">
								</span> --}}
							</div>
							<div class="col s12">
								<p><b>Highest Education</b></p>
							</div>
							<div class="input-field col s12 m6">
								<select name="highest_education" id="highest_education">
									<option value="pmr">PMR</option>
									<option value="spm">SPM</option>
									<option value="stpm">STPM</option>
									<option value="a-level">A-Level</option>
									<option value="foundation-or-marticulation">Foundation or Marticulation</option>
									<option value="diploma">Diploma</option>
									<option value="advanced-diploma">Advanced Diploma</option>
									<option value="bachelor-with-honours">Bachelor with Honours</option>
								</select>
								<label for="highest_education">Standard of knowledge required for the job</label>
							</div>
							<div class="col s12 mrg-bot30">
								<p>When is the first day at work?</p>
								<div class="row">
									<div class="input-field col s12 m6">
										<input name="start_date" type="date" class="datepicker">
										<label for="start_date">Working Start Date</label>
									</div>
								</div>
							</div>
							<div class="col s12">
								<p>Tell us your working hours</p>
								<div class="row">
									<div class="input-field col s6 m3">
										<input name="start_time" id="start_time" class="timepicker" type="time" value="09:00">
										<label for="start_time">Start Time</label>
									</div>
									<div class="input-field col s6 m3">
										<input name="end_time" id="end_time" class="timepicker" type="time" value="17:00">
										<label for="end_time">End Time</label>
									</div>
								</div>
							</div>
							<div class="">
								<div class="input-field col s12 inline-radio-input mrg-bot15">
									<p>How many days for the job?<br><small><strong>Working Days</strong></small></p>
									<span>
										<input name="working" type="radio" id="working1" checked="" value="1">
										<label for="working1">One Day</label>
									</span>
									<span>
										<input name="working" type="radio" id="working2" value="5">
										<label for="working2">Monday - Friday</label>
									</span>
									<span>
										<input name="working" type="radio" id="working3" value="2">
										<label for="working3">Weekend (Sat-Sun)</label>
									</span>
									<span>
										<input name="working" type="radio" id="working4" value="30">
										<label for="working4">One Month</label>
									</span>
									<span>
										<input name="working" type="radio" id="working5" value="other">
										<label for="working5">Other</label>
									</span>
									<input type="number" name="working_days" class="validate inline-input hide" min="0" value="0">
								</div>
							</div>

								<div class="input-field col s12 inline-radio-input">
									<p>How much are you paying a day?<br><small><strong>Salary Per Day</strong></small></p>
									<span>
										<input name="salary" type="radio" id="salary1" value="50" checked="">
										<label for="salary1">RM 50</label>
									</span>
									<span>
										<input name="salary" type="radio" id="salary2" value="80">
										<label for="salary2">RM 80</label>
									</span>
									<span>
										<input name="salary" type="radio" id="salary3" value="120">
										<label for="salary3">RM 120</label>
									</span>
									<span>
										<input name="salary" type="radio" id="salary4" value="other">
										<label for="salary4">Pick Amount</label>
									</span>
									<input type="number" name="amount" class="validate inline-input hide" min="0" value="0" step="0.01">
								</div>

							{{-- <div class="input-field col s12">
									<div class="row {{ $errors->has('vacancy') ? ' has-error' : '' }}">
										<div class="input-field col s4">
											<input type="number" name="vacancy" class="validate" min="1" max="10" value="1">
											<label for="vacancy">Number of vacancies</label>

										</div>
									</div>
								</div> --}}
							<div class="col s12">
								<br><br>
								<h6>How can we contact you?</h6>
								<div class="row">
									<div class="col s12 m6">
										<div class="row">
											<div class="input-field col s12 m12">
												<input type="text" name="contact_email" class="validate" value="{{ Auth::user()->email }}" list="data-email" id="contact_email" autocomplete="off">

												<datalist id="data-email">

												</datalist>

												<label for="contact_email">Contact Email</label>
											</div>
										</div>
										<div class="row">
											<div class="input-field col s12 m12">

												<input type="text" name="contact_phone" class="validate" value="">
												<label for="phone">Contact telephone</label>

											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col s12">
								<h6>Do you have an external link to be shared? (optional)</h6>
								<div class="row">
									<div class="input-field col s12 m6">
										<input type="text" name="contact_link" class="validate">
										<label for="link">Link</label>
									</div>
								</div>
								<br>
							</div>
							<div class="col s12 m6">
								<h6>Select a picture</h6>
								<p class="grey-text">To beautify your job post</p>
								<form action="#">
									<div class="file-field input-field">
										<div class="btn light-blue darken-2">
											<span>File</span>
											<input type="file" name="avatar">
										</div>
										<div class="file-path-wrapper">
											<input class="file-path validate" type="text" placeholder="Upload any image">
										</div>
									</div>
								</form>
							</div>
						</div><!--end #tab1 -->
						<div id="tab2" class="tabContent">
							<div class="col s12">
							<h5>Where is your job located?</h5>
							</div>
							<br><br>
							<div class="col s12">
								<div class="row">
									<div class="col s12">
									  <ul class="tabs">
										<li class="tab col s3 active"><a class="active" href="#test1">Autosearch</a></li>
										<li class="tab col s3"><a href="#test2">Manual</a></li>
									  </ul>
									</div>
									<div id="test1" class="col s12">
										<input id="pac-input" type="text" placeholder="Search Box">
										<div id="map">
										</div>
									</div>
									<div id="test2" class="col s12">
										<div class="row">
											<div class=" col s12 m7">
												<div class="col s12">
													<label for="loc_country">Country</label>
													<select name="loc_country" id="countries" class="browser-default">
														<option value="" disabled selected>-Choose-</option>
													</select>
												</div>

												<div class="col s6">
													<label for="loc_state">State</label>
													<select name="loc_state" class="browser-default">
														<option value="" disabled selected>-Choose-</option>
													</select>
												</div>

												<div class="input-field col s6">
														<input type="text" name="loc_city" id="" class="validate">
														<label for="loc_city">City</label>
												</div>

												<div class="input-field col s12">
														<input type="text" name="loc_address" class="validate">
														<label for="loc_address">Address</label>
												</div>

												<div class="input-field col s12">
														<input type="text" name="zipcode" class="validate">
														<label for="zipcode">Postal/Zip code</label>
												</div>
											</div>
											<div class="col s1"></div>
											<div class="col s12 m4 right">
												<div class="card-panel hoverable">
													 <i class="material-icons">info_outline</i>
													 <p>Providing the  essentials helps guests feel at home in your place.</p>
													 <br>
													 <p>Some hosts provide breakfast, or just coffee and tea. None of these things are required, but sometimes they add a nice touch to help guests feel welcome.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div><!--end #tab2 -->
						<div id="tab3" class="tabContent mrg-l15">
							<div class="col s12">
								<h5>Job requirements?</h5>
							</div>
							<br><br>
							<div class="">
								<div class="col s12">
									<p>
										<input type="checkbox" id="skill1" name="skill1" />
										<label for="skill1">An access to reliable transportation?</label>
									</p>
									<div class="row skillContent">
										<div class="input-field col s12">
											<select name="vehicle" id="Vehicle">
												<option value=""> Please Select --</option>
												<option value="1">Car</option>
												<option value="2"> Motorbike</option>
												<option value="3">Not required</option>
											</select>
											<label for="Address">Please select one of the following</label>
										</div>
									</div>
									<p>
										<input type="checkbox" id="skill2" name="language" />
										<label for="skill2">Language Requirement</label>
									</p>
									<div class="row skillContent">
										<div class="input-field col s12">
											<select name="language" id="Language">
												<option value=""> Please Select --</option>
												<option value="4">English</option>
												<option value="5">Chinese</option>
												<option value="6">Bahasa Melayu</option>
												<option value="others">Others</option>
											</select>
											<label for="Language">Maximum 3 languages selection</label>
										</div>
									</div>
									<p>
										<input type="checkbox" id="skill3" name="skill3" />
										<label for="skill3">Certification and Licences</label>
									</p>
									<div class="col s12 skillContent">
										<p>
											<input type="checkbox" name="cert[]" id="cert1" value="7" />
											<label for="cert1">Food Safety Certification(Food Handling)</label>
										</p>
										<p>
											<input type="checkbox" name="cert[]" id="cert2" value="8" />
											<label for="cert2">Food Safety for Managers Certification</label>
										</p>
										<p>
											<input type="checkbox" name="cert[]" id="cert3" value="9" />
											<label for="cert3">Bartender Mixology Certification</label>
										</p>
										<p>
											<input type="checkbox" name="cert[]" id="cert4" value="10" />
											<label for="cert4">Barista Certification</label>
										</p>
										<p>
											<input type="checkbox" name="cert[]" id="cert5" value="11" />
											<label for="cert5">Security Guard Certification</label>
										</p>
										<p>
											<input type="checkbox" name="cert[]" id="cert6" value="" />
											<label for="cert6">Others</label>
										</p>
									</div>
								</div>
							</div>
						</div><!--end #tab3 -->
						<div class="col s12 mrg-tb40">
							<button id="back" type="buttton" class="waves-effect waves-light btn light-blue darken-2">BACK</button>
							<button id="submit" type="submit" class="waves-effect waves-light btn light-blue darken-2 right" name="myButton">SUBMIT</button>
							<button id="next" type="buttton" class="waves-effect waves-light btn light-blue darken-2 right">NEXT</button>
							<input type="hidden" id="latitude" name="latitude" value="">
							<input type="hidden" id="longitude" name="longitude" value="">
						</div>

				</div><!--end .card -->
			</form>
		</div><!--end #rootwizard -->
	</div>
</div>
@endsection

@section('scripts')

	<script type="text/javascript" src="{{ asset('js/custom/map.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>

	<script>
	var input = document.getElementById('contact_email');
	var datalist = document.getElementById('data-email');

	tinymce.init({
		selector: '#requirements',
		height: 200,
		menubar: false,
		themes: "modern",
		plugins: [
			' advlist lists'
			/*'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code'*/
		],
		toolbar: 'bullist numlist',
		/*toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',*/
		setup: function(editor) {
			editor.on('change', function(e) {
				document.getElementById('requirements').innerHTML = editor.getContent();
			});
		}
	});


	$(document).ready (function(){
		$('body').on('keydown', 'input, select, textarea', function(e) {
			var self = $(this)
			  , form = self.parents('form:eq(0)')
			  , focusable
			  , next
			  ;
			if (e.keyCode == 13) {
				focusable = form.find('input,a,select,button,textarea').filter(':visible');
				next = focusable.eq(focusable.index(this)+1);
				if (next.length) {
					next.focus();
				} else {
					form.submit();
				}
				return false;
			}
		});

		$('select').material_select();
		$('#back').hide();
		$('#submit').hide();
		$('.skillContent').hide();

		// var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);
		$('.datepicker').pickadate({
			selectMonths: true, // Creates a dropdown to control month
			selectYears: 15, // Creates a dropdown of 15 years to control year
			autoclose: true,
		});

		picker = $('.datepicker').pickadate('picker');
		picker.set('min',true);

		$('.timepicker[name=start_time]').pickatime({
			formatSubmit: 'HH:i',
			twelvehour: false,
			default: '09:00:00',
			autoclose: true,
		});

		$('.timepicker[name=end_time]').pickatime({
			formatSubmit: 'HH:i',
			twelvehour: false,
			default: '18:00:00',
			autoclose: true,
		});

		var navListItems = $('.nav>.tab>a'), content = $('.tabContent');

		navListItems.click(function(e){
			e.preventDefault();
			var $target = $($(this).attr('href')),
				$item = $(this);
			$target.find('input:eq(0)').focus();
			$item.parent().addClass('active');
			index = parseInt($(this).attr('href').substr(-1));

			$('ul.tabs').tabs('select_tab', 'tab'+$index);

			for (var i = 1; i <= 3; i++) {
				if(i < index)
				{
					$('[href=#tab'+i+']').parent().addClass('done');
				}
				else if (i == index)
				{
					$('[href=#tab'+i+']').parent().removeClass('done');
					// $('[href=#tab'+i+']').parent().removeClass('active');
				} else
				{
					$('[href=#tab'+i+']').parent().removeClass('done');
					$('[href=#tab'+i+']').parent().removeClass('active');
				}
			}
			if (index == 1) {
				$('#back').hide();
				$('#next').show();
				$('#submit').hide();
			} else if(index == 3) {
				$('#next').hide();
				$('#back').show();
				$('#submit').show();
			} else {
				$('#back').show();
				$('#next').show();
				$('#submit').hide();
			}

			setTimeout(function() {
				google.maps.event.trigger(map, 'resize');
			}, 1000);

			$('.progress-bar-primary').css('width',((index-1)*50)+'%');
		});

		$('#back').click(function(e) {
			e.preventDefault();
			$c = $('.nav>.tab>a.active');
		});

		$('#next').click(function(e) {
			e.preventDefault();
			$next = parseInt($('.nav>.tab>a.active').attr('href').substr(-1)) + 1;
			$('[href=#tab'+$next+']').click();
		});

		$('#back').click(function(e) {
			e.preventDefault();
			$back = parseInt($('.nav>.tab>a.active').attr('href').substr(-1)) - 1;
			$('[href=#tab'+$back+']').click();
		});

		$('[id^=skill]').on('change', function(e) {
			ob = $(this).parent().next().toggleClass('abcd');
			if(ob.hasClass('abcd')){
				ob.show(300);
			}
			else{
				ob.hide(300);
			}
		});
		$('a[href=#tab2]').click(function(e) {
			$('#tab2 ul.tabs li:first-child a').click();
		});
		$('a[href=#test2]').click(function(e) {
			updatePlace();
			$('input[name=loc_city]').val(_place.city).focus();
			$('input[name=zipcode]').val(_place.postal_code).focus();
			$('select[name=loc_country]').val(_place.country).change().focus();
			$('input[name=loc_address]').val(_place.address).focus();
		});

		$('select[name=loc_country]').change(function(e) {

			val = $(this).val();
			id = $('option[value="'+val+'"]').attr('data-id');
			$.ajax({
				type : 'get',
				url  : '{{ route('country.index') }}/'+id,
			}).done(function(res) {
				$('select[name=loc_state]').html(res);
				if(_place.state){
					$('select[name=loc_state]').val(_place.state);
				}

				$('select').material_select();
			});
		});

		$('[name=working],[name=salary]').click(function(e) {
			if(this.value == 'other')
				$(this).parent().parent().children().eq(-1).removeClass('hide');
			else
				$(this).parent().parent().children().eq(-1).addClass('hide');
		})

	});
	</script>

	<script>
		var countries = {};
		$(document).ready(function(){
			getCountries();
		});

		function getCountries()
		{
			$.ajax({
				type : 'get',
				url  : '{{ route('country.index') }}',
			}).done(function(res) {
				countries = res;
				fillCountries(res);
			});
		}

		function fillCountries(res)
		{
			html = '';
			res.forEach(function(c) {
				html += '<option value="'+c.name+'" data-id="'+c.id+'">'+c.name+'</option>';
			});
			$('#countries').html(html);
			$('select').material_select();
		}

	</script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFvcodwxV3SQptSx4LQ-H-gZVcAk-1lyI&libraries=places&callback=initAutocomplete"
		 async defer></script>
@endsection