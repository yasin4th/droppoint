@extends('layouts.material')

@section('content')

<div class="row">

    <div class="col s12 m3">
        <div class="card profile-sidebar">
            @include('includes.admin.sidemenu')
        </div>
    </div>
    <div class="col m9">

        <div class="row">
            <div class="col s10 m4">
                <div class="header-search-wrapper hide-on-med-only">
                    <i class="mdi-action-search"></i>
                    <form role="form" action="{{ route('admin.jobs') }}" name="search" method="GET">
                        <input type="text" name="query" class="header-search-input z-depth-2 table-search" placeholder="Search by Job Title" value="{{ $query }}">
                    </form>
                </div>
            </div>
            <div class="col s12 m8">
                <a href="{{ route('admin.jobs.export') }}?query={{ $query }}" class="btn-flat right teal-text btn-get-csv"><img src="/images/csv-icon.png" class="responsive-img"> Get CSV</a>

                <a href="{{ route('admin.jobs.create') }}" class="waves-effect waves-light btn teal darken-1 right mrg-tb15 admin-postjob-btn">Post Job</a>

                <a href="{{ route('admin.map') }}" class="waves-effect waves-light btn right mrg-tb15 map-btn">Map</a>
            </div>
        </div>

        <div class="card">
                <div class="card-content" id="content1">
                    <h5>All Jobs</h5>
                    <div class="table-scroll">
                        <table class="bordered all-table" id="jobs">
                            <thead>
                                <tr>
                                    <th>Job Title</th>
                                    <th>User</th>
                                    <th>Location</th>
                                    <th>Date</th>
                                    <th>Salary</th>
                                    <th>Vacancies</th>
                                    <th>Applicants</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($jobs as $job)
                                <tr>
                                    <td><a href="{{ route('admin.jobs.show', $job->id) }}"><strong>{{$job->title}}</strong></a><br>{{ str_limit($job->about, 20) }}</td>
                                    <td>{{ $job->username }}</td>
                                    <td>{{ $job->location }}</td>
                                    <td>{{ $job->start_date_read }}</td>
                                    <td>{{ $job->salary }}</td>
                                    <td><a href="{{ route('admin.jobs.show', $job->id) }}">{{ $job->hired_count }} hired/ {{$job->vacancy}}</a></td>
                                    <td><a href="{{ route('admin.jobs.show', $job->id) }}">{{ $job->applicants_count }}</a></td>
                                    <td>
                                        Inactive
                                        {{-- <ul id="dropdown{{ $job->iterator }}" class="dropdown-content">
                                            <li><a href="{{ route('job.show', $job->id) }}">Edit</a></li>
                                            <li>
                                            <form action="{{ route('job.destroy', $job->id) }}" method="POST">
                                                <a href="" onclick='$(this).parent().submit();' class="DeleteJob">Delete</a>
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                            </form>
                                            </li>
                                        </ul>
                                        <a href="#" class="btn cyan dropdown-button" data-activates="dropdown{{ $job->iterator }}" data-beloworigin="true">Choose<i class="mdi-navigation-arrow-drop-down right"></i></a> --}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <br>
                    <div class="col s12 m6 mrg-tb15">
                        <p>Showing {{ ($jobs->firstItem()) ?:0 }} to {{ $jobs->lastItem()?:0 }} of {{ $jobs->total() }} entries</p>
                    </div>
                    <div class="col s12 m6 mrg-tb15">
                        <div class="right">
                            {{ $jobs->links() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 hide" id="content2">
                <div>Showing 1 to <span id="to"></span> of <span id="total"></span> entries</div>
                <div class="row center loadmorediv">
                    <button class="btn cyan waves-effect waves-light" onclick="load_more()">View more</button>
                </div>
            </div>
    </div>

</div>
@endsection

@section('scripts')
<script src="/js/materialize/plugins/fullcalendar/lib/moment.min.js" type="text/javascript" charset="utf-8"></script>

<script>
    $('.DeleteJob').click(function(e){
        e.preventDefault();
        var answer = confirm('Do you want to delete this Job?');
        if(answer == true){
            location.replace($(this).attr('href'));
        }
        else{
            return false;
        }
    });
</script>
@endsection
