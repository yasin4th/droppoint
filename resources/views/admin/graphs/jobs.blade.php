@extends('layouts.material')

@section('content')
<div class="row">
	<div class="col s12 m3">
		<div class="profile-sidebar">
			@include('includes.graphsidebar')
		</div>
	</div>
	<div class="col s12 m9">
		<div class="card graph-header">
			<div class="col s7">
				<ul>
					<li>
						<a href="#" class="today-btn">Toaday</a>
					</li>
					<li>
						<div class="pagination">
						  <a href="#">
						  	<i class="fa fa-chevron-left"></i>
						  </a>
						  <a href="#">
						  	<i class="fa fa-chevron-right"></i>
						  </a>
						</div>
					</li>
				</ul>
				<p class="date">March 5-11-2017</p>
			</div>
			<div class="col s5">
				<ul class="right time-status">
					<li>
						<a href="#" class="time-btn active">Day</a>
					</li>
					<li>
						<a href="#" class="time-btn">Week</a>
					</li>
					<li>
						<a href="#" class="time-btn">Month</a>
					</li>
					<li>
						<a href="#" class="time-btn">Year</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
@endsection
