@extends('layouts.material')

@section('content')
{{-- {!! Breadcrumbs::renderIfExists('job') !!} --}}
<div class="row">

	<div class="col s12 m3">
		<div class="profile-sidebar">
			@include('includes.admin.sidemenu')
		</div>
	</div>
	<div class="col s12 m9" ng-app="myapp" ng-controller="tableCtrl">
		<div id="rootwizard1" class="form-wizard form-wizard-horizontal">
			@include('includes.alert')
			<form class="form floating-label" method="POST" action="{{ route('notification.store') }}" onsubmit="myButton.disabled = true; return true;" ng-controller="tableCtrl" >
					{{ csrf_field() }}
				<div class="card">
					<div class="card-content">
						<p class="center"><strong>Create Notification</strong></p>
						<br>
						<div class="row">
							<div class="col s2">
								<img src="/uploads/logo/avatar.jpg" class="responsive-img create-notification-img" alt="">
							</div>
							<div class="input-field col s10">
								<textarea id="message" class="materialize-textarea" name="message" required></textarea>
								<label for="message">Message</label>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col s12">
								<p>Who should see this?</p>
							</div>
							<br>
							<br>
							<div class="col s12">
								<div class="switch">
									<label>
										Private
										<input type="checkbox" name="type" checked ng-model="type" ng-change="change();">
										<span class="lever"></span>
										Public
									</label>
								</div>
							</div>
							<br>
							<br>
							<div class="col s12" ng-if="!type">
								<p>Only seen by:</p>
							</div>
						</div>
						<div class="row" ng-if="!type">
							<div class="col m12 s12">
								<select name="users" class="browser-default" required>
									@foreach($users as $user)
									<option value="{{$user->id}}">{{$user->name}}</option>
									@endforeach
								</select>
							</div>
							<br>
							<br>
						</div>
						<div class="row" ng-if="type">
							<div class="col m12 s12">
								<select name="rooms" class="browser-default" required>
									<option value="1">Announcements</option>
									<option value="3">General</option>
								</select>
							</div>
							<br>
							<br>
						</div>
						<br>
						<div class="row">
							<div class="col s12">
								<button id="submit" type="submit" class="waves-effect waves-light btn light-blue darken-2 right" name="myButton">SUBMIT</button>
							</div>
						</div>
					</div>
				</div><!--end .card -->
			</form>
		</div><!--end #rootwizard -->
	</div>

</div>
@endsection

@section('scripts')
	<script type="text/javascript" src="{{ asset('js/angular/angular.min.js') }}"></script>
	<link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
	<script src="{{asset('js/select2.min.js')}}"></script>
	<script>
		var countries = {};
		$(document).ready(function(){
			// getCountries();
			// setTimeout(function(){$('select').select2();},500);

		});

		var app = angular.module('myapp', []);
		app.controller('tableCtrl', function($scope, $http, $window) {
			$scope.type = true;
			$scope.change = function () {
				// $('select').select2();
				// setTimeout(function(){$('select').select2();},100);
			};
		});

	</script>

@endsection