@extends('layouts.material')

@section('content')
{{-- {!! Breadcrumbs::renderIfExists('job') !!} --}}
<div class="row">

	<div class="col s12 m3">
		<div class="profile-sidebar">
			@include('includes.admin.sidemenu')
		</div>
	</div>
	<div class="col m9 s12">

		<div class="card">
			<div class="card-content">
				<p class="center"><strong>Notification and Message ({{count($notifications)}})</strong></p>
				<br>
				<div class="divider"></div>
				<br>

				@foreach($notifications as $n)
					<div class="row">
						<div class="col s2 m2 offset-s2 offset-m2">
							<img src="/uploads/logo/avatar.jpg" class="responsive-img notification-img" alt="">
						</div>
						<div class="col s7 m7 ">
							<div class="massage">
								<p>{{$n->message}} </p>
							</div>
							<p class="admin-time">{{\App\User::find($n->posted_by)->first()->name}} <span>
							@php
								$dt = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $n->created_at);
								echo $dt->diffForHumans(\Carbon\Carbon::now());
							@endphp
							</span></p>
						</div>
					</div>
				@endforeach

			</div>
		</div><!--end .card -->
	</div>

</div>
@endsection

@section('scripts')

	<script>
		var countries = {};
		$(document).ready(function(){
			// getCountries();
		});

	</script>

@endsection