@extends('layouts.material')

@section('content')

<div class="row">

	<div class="col s12 m3">
		@include('includes.admin.reportsidemenu')
	</div>
	<div class="col m9">

		<div class="row">
			<div class="col s12">
				<h5>Jobs Published Vs Expired</h5>
				<div id="canvas-holder" style="width:100%">
					<canvas id="canvas1"></canvas>
				</div>
				<h5>Jobs by type</h5>
				<div id="canvas-holder" style="width:100%">
					<canvas id="canvas2"></canvas>
				</div>
				<!-- <h5>Jobs Published</h5> -->
			</div>
		</div>

	</div>

</div>

@endsection

@section('scripts')
	<script src="/js/Chart.bundle.min.js"></script>
	<script>

		window.chartColors = {
			blue: 'rgb(54, 162, 235)',
			red: 'rgb(255, 99, 132)',
			orange: 'rgb(255, 159, 64)',
			yellow: 'rgb(255, 205, 86)',
			green: 'rgb(75, 192, 192)',
			purple: 'rgb(153, 102, 255)',
			grey: 'rgb(231,233,237)'
		};

		window.randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		}
		var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		var config1 = {
			type: 'line',
			data: {
				labels: [
					@foreach($published as $m)
						'{{$m['month']}}',
					@endforeach
				],
				datasets: [{
					label: "Published",
					backgroundColor: window.chartColors.red,
					borderColor: window.chartColors.red,
					data: [
						@foreach($published as $pub)
							{{$pub['count']}},
						@endforeach
					],
					fill: false,
				}, {
					label: "Expired",
					fill: false,
					backgroundColor: window.chartColors.blue,
					borderColor: window.chartColors.blue,
					data: [
						@foreach($expired as $exp)
							{{$exp['count']}},
						@endforeach
					],
				}]
			},
			options: {
				responsive: true,
				title:{
					display:true,
					text:'Published vs Expired'
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Month'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Jobs'
						}
					}]
				}
			}
		};

		var config2 = {
			type: 'bar',
			data: {
				labels:[
				 @foreach($published as $m)
						'{{$m['month']}}',
					@endforeach
				],
				datasets: [{
					label: "Full-Time",
					backgroundColor: window.chartColors.red,
					borderColor: window.chartColors.red,
					data: [
						@foreach($fulltime as $full)
							{{$full['count']}},
						@endforeach
					],
					fill: false,
				}, {
					label: "Part-Time",
					fill: false,
					backgroundColor: window.chartColors.blue,
					borderColor: window.chartColors.blue,
					data: [
						@foreach($parttime as $part)
							{{$part['count']}},
						@endforeach
					],
				}]
			},
			options: {
				responsive: true,
				title:{
					display:true,
					text:'Full-Time vs Part-Time'
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Month'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Jobs'
						}
					}]
				}
			}
		};

		window.onload = function() {
			var ctx1 = document.getElementById("canvas1").getContext("2d");
			window.canvas1 = new Chart(ctx1, config1);
			var ctx2 = document.getElementById("canvas2").getContext("2d");
			// config.type = "horizontalBar";
			window.canvas2 = new Chart(ctx2, config2);
		};
	</script>

@endsection