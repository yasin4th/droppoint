@extends('layouts.material')

@section('content')
<style>
	#cloud {
		width: 800px;
		height: 300px;
	}
</style>

<div class="row mrgt-20">
	<div class="col s12 m3">
		@include('includes.admin.reportsidemenu')
	</div>
	<div class="col m9">
		<div class="row">
			<div class="col s12 mrg-bot45">
				<div class="row">
					<div class="col s6">
						<h5>Tags</h5>
					</div>
					<div class="col s6 right-align">
						<a class='dropdown-button btn grey darken-1' href='#' data-activates='dropdown1'>Filter By</a>
						<ul id='dropdown1' class='dropdown-content'>
							<li><a href="/admin/report/tags?days=30">Last 30 Days</a></li>
							<li class="divider"></li>
							<li><a href="/admin/report/tags?days=90">Last 90 Days</a></li>
							<li class="divider"></li>
							<li><a href="/admin/report/tags?days=365">Last 12 Months</a></li>
							<li class="divider"></li>
							<li><a href="/admin/report/tags">All</a></li>
						</ul>
					</div>
				</div>
				<div class="card">
					<div class="card-content">
						<div  id="cloud" class="jQCloud">
						</div>
					</div>
				</div>
			</div>
			<div class="col s12">
				<div class="row">
					<div class="card">
						<div class="card-content">
							<h5 id="tag">Top 10 Tags</h5>
							<table class="bordered striped">
								<thead>
									<tr>
										<th></th>
										<th>Name</th>
										<th>Category</th>
										<th>Count</th>
									</tr>
								</thead>
								<tbody>
									@foreach($tags as $t)
										<tr>
											<td>{{ $loop->iteration }}</td>
											<td>{{ $t['name'] }}</td>
											<td>{{ $t['category'] }}</td>
											<td>{{ $t['countTags'] }}</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqcloud/1.0.4/jqcloud-1.0.4.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqcloud/1.0.4/jqcloud.css" />

<script type="text/javascript">
	var words = [
		@foreach($tags as $t)
		{text: "{{$t['name']}}", weight: {{rand(1,100)}}},
		@endforeach
	];
	$(document).ready(function(){ 
		$('#cloud').jQCloud(words,{
			shape:'elliptical',
			colors: [ 
				@foreach($tags as $t)
				`rgb(${Math.round(Math.random() * 255)},${Math.round(Math.random() * 255)},${Math.round(Math.random() * 255)})`,
				@endforeach
			]
		});
	});
</script>
@endsection