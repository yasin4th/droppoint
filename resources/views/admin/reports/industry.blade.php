@extends('layouts.material')

@section('content')

<div class="row">
	<div class="col s12 m3">
		@include('includes.admin.reportsidemenu')
	</div>
	<div class="col m6">
		<div class="row">
			<div class="col s12">
				<h5>Jobs by Industry</h5>
				<div id="canvas-holder" style = "width:80%;">
					<canvas id="canvas1"></canvas>
				</div>
			</div>
		</div>
	</div>
	<div class="col m3" class="hide">
		<ul id="company-list" > 
		<!-- @foreach ($industry as $ind)
			<li>{{$ind['company_name']}} </li>
		@endforeach -->
		</ul>
	</div>
</div>

@endsection

@section('scripts')
	<script src="/js/Chart.bundle.min.js"></script>
	<script>

		var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		var config1 = {
			type: 'pie',
			data: {
				datasets: [{
					data: [
						@foreach($industry as $i)
							{{$i['jobs_posted']}},
						@endforeach
					],backgroundColor: [
						@foreach($industry as $i)
							`rgb(${Math.round(Math.random() * 255)},${Math.round(Math.random() * 255)},${Math.round(Math.random() * 255)})`,
						@endforeach
					],
					label: 'Dataset 1'
				}],

				labels: [
				@foreach($industry as $i)
					'{!!$i['industry_name']!!}',
				@endforeach
				]
			},
			options: {
				responsive: true,
				legend: {
					position: 'bottom',
					horizontalAlign: 'right', // left, center ,right 
					/*
					verticalAlign: "center",*/ 
				},
				animation: {duration: 0, easing: 'linear'},
				hover: {
					animationDuration: 0, // duration of animations when hovering an item
				},
				responsiveAnimationDuration: 0,
			}
		};

		window.onload = function() {
			var ctx1 = document.getElementById("canvas1").getContext("2d");
			window.canvas1 = new Chart(ctx1, config1);
		};
	</script>
	<script>
		$(document).ready(function(){
			$('#canvas1').click(function(){
				$("#company-list").addClass('hide');
			});
		});
	</script>


@endsection