@extends('layouts.material')

@section('content')

<div class="row">

    <div class="col s12 m3">
        <div class="card profile-sidebar">
            @include('includes.admin.sidemenu')
        </div>
    </div>
    <div class="col m9">
        <div class="card">
            <div class="card-content">
                <div class="row">
                    <div class="col s12">
                        <h5 class="header">Add Badge</h5>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input type="text" name="title" class="validate">
                        <label for="title" class="">Title</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <textarea name="description" rows="5" class="materialize-textarea"></textarea>
                        <label for="description" class="">Description</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <textarea name="notification" rows="5" class="materialize-textarea"></textarea>
                        <label for="notification" class="">Notification</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <select name="badge">
                        </select>
                        <label for="Address">Select Category</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input type="text" name="credits-title" class="validate">
                        <label for="credits-title" class="">Credits Title</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <textarea name="amount-to-credit" rows="5" class="materialize-textarea"></textarea>
                        <label for="amount-to-credit" class="">Amount to Credit</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <button type="submit" class="waves-effect waves-light btn light-blue darken-2 right">Submit <i class="mdi-content-send right"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('scripts')
<script src="/js/materialize/plugins/fullcalendar/lib/moment.min.js" type="text/javascript" charset="utf-8"></script>

<script>
</script>
@endsection

