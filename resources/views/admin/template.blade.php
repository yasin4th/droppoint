@extends('layouts.material')

@section('content')

<div class="row">

	<div class="col s12 m3">
		<div class="profile-sidebar">
			@include('includes.admin.sidemenu')
		</div>
	</div>
	<div class="col s12 m9">

		<div class="row">
			<div class="col s10 m6">
				<div class="header-search-wrapper">
					<i class="mdi-action-search"></i>
					<form role="form" action="{{ route('admin.template') }}" name="search" method="GET">
						<input type="text" name="query" class="header-search-input z-depth-2 table-search" placeholder="Search by Template Title" value="{{ $query }}">
					</form>
				</div>
			</div>
			<div class="col s12 m6">
				<div class="right mrg-tb15">
					<a href="{{ route('admin.template.create') }}" class="waves-effect waves-light btn teal darken-1 right">Add Template</a>
				</div>
			</div>
		</div>
		<div class="card">
			<h4 style="margin-left:10px;">
				@if($query)
					Search results '{{$query}}'
				@else
					All JobTemplates
				@endif
			</h4>
			<div class="card-content" id="content1">
				<div class="table-scroll">
					<table class="bordered all-table" id="users">
						<thead>
							<tr>
								<th>Title (Template)</th>
								<th>Requirements</th>
								<th>Tags</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($tmpJobs as $job)
							<tr>
								<td>{{$job->title}}</td>
								<td><span id="two_line" class="truncate_admin">{!!$job->requirements!!}</span></td>
								<td>
									@php
										$tags = explode(',',$job->tags);
										$tagNames = \App\Tag::whereIn('id', $tags)->get();
									@endphp
									@if(count($tagNames))
									{{implode(',',$tagNames->pluck('name')->all())}}
									@endif
								</td>
								@if($job->status == '1')
								<td><span class="task-cat green"><a href="/admin/template/{{$job->id}}/status" >Active</a></span></td>
								@else
								<td><span class="task-cat orange"><a href="/admin/template/{{$job->id}}/status" >Inactive</a></span></td>
								@endif
								<td>
									<ul id="dropdown{{$loop->iteration}}" class="dropdown-content">
										<li><a href="/admin/template/{{$job->id}}/edit">Edit</a></li>
										<li><a href="/admin/template/{{$job->id}}/delete" class="DeleteTemplate">Delete</a></li>
									</ul>
										<a href="#" class="dropdown-button font-color" data-activates="dropdown{{$loop->iteration}}">More<i class="mdi-navigation-arrow-drop-down"></i></a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="col s12 m6 mrg-tb15">
					<p>Showing {{ ($tmpJobs->firstItem()) ?:0 }} to {{ $tmpJobs->lastItem()?:0 }} of {{ $tmpJobs->total() }} entries</p>
				</div>
				<div class="col s12 m6 mrg-tb15">
					<div class="right">
						{{ $tmpJobs->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection

@section('scripts')
	<script src="/js/materialize/plugins/fullcalendar/lib/moment.min.js" type="text/javascript" charset="utf-8"></script>
<script>
$('.pagination li [rel="next"]').html('more »');
$('.DeleteTemplate').click(function(e){
	e.preventDefault();
	var answer = confirm('Do you want to delete this Template?');
	if(answer == true){
		location.replace($(this).attr('href'));
	}
	else{
		return false;
	}
});

$('').click(function(){
	$('this').text('Inactive');

});

</script>



@endsection

