@extends('layouts.material')

@section('content')

<div class="row">

	<div class="col s12 m3">
		<div class="profile-sidebar">
			@include('includes.admin.sidemenu')
		</div>
	</div>
	<div class="col s12 m9">

		<div class="row">
			<div class="col s10 m6">
				<div class="header-search-wrapper">
					<i class="mdi-action-search"></i>
					<form role="form" action="{{ route('admin.users') }}" name="search" method="GET">
						<input type="text" name="query" class="header-search-input z-depth-2 table-search" placeholder="Search by User Name or Email" value="{{ ($query) ? $query : '' }}">
					</form>
				</div>
			</div>
			<div class="col s12 m6">
				{{-- <a href="{{ route('admin.jobs.export') }}?query={{ $query }}" class="btn-flat right teal-text btn-get-csv"><img src="/images/csv-icon.png" class="responsive-img"> Get CSV</a> --}}
				<div class="right mrg-tb15">
					<a href="{{ route('admin.users.create') }}" class="waves-effect waves-light btn teal darken-1 right">Add User</a>
				</div>
			</div>
		</div>

		<div class="card">
			<div class="card-content" id="content1">
				<h4>
					@if($query)
						Search results '{{$query}}'
					@else
						All Users
					@endif
				</h4>
				<div class="table-scroll">
					<table class="bordered all-table" id="users">
						<thead>
							<tr>
								<th>Name</th>
								<th>Email</th>
								<th>Phone</th>
								<th>Location</th>
								<th>Industry</th>
								<th>Status</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							{{-- {{dd(count($users))}} --}}
							@foreach ($users as $user)
								{{-- {{dd($user->company->address)}} --}}
								@if($user->company)
								<tr>
									<td><a href="/admin/users/{{$user->id}}/edit">{{$user->name}}</a></td>
									<td>{{$user->email}}</td>
									<td>{{$user->company->phone}}</td>
									<td>{{($user->company)?$user->company->address:''}}</td>
									<td>
										{{$user->company->indus['name']}}
									</td>
									<td>{!! getStatusBadge($user->company->status) !!}</td>
									<td>
										<ul id="dropdown{{$loop->iteration}}" class="dropdown-content">
											@if($user->company->status == '1')
											<li><a href="/admin/users/{{$user->id}}/status/4">Inactive</a></li>
											@else
											<li><a href="/admin/users/{{$user->id}}/status/1">Active</a></li>
											@endif
											<li class="divider"></li>
											<li><a href="/admin/users/{{$user->id}}/edit">Edit</a></li>
											<li><a href="/admin/users/{{$user->id}}/delete" class="DeleteJob">Delete</a></li>
										</ul>
										<a href="#" class="dropdown-button font-color" data-activates="dropdown{{$loop->iteration}}">More<i class="mdi-navigation-arrow-drop-down"></i></a>
									</td>
								</tr>
								@endif
							@endforeach
						</tbody>
					</table>
				</div>
				<br>
				<br>
				<div class="col s12 m6 mrg-tb15">
					<p>Showing {{ ($users->firstItem()) ?:0 }} to {{ $users->lastItem()?:0 }} of {{ $users->total() }} entries</p>
				</div>
				<div class="col s12 m6 mrg-tb15">
					<div class="right">
						{{ $users->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection

@section('scripts')
	<script src="/js/materialize/plugins/fullcalendar/lib/moment.min.js" type="text/javascript" charset="utf-8"></script>
	<script>
		$(document).ready(function(){
			$('.pagination li [rel="next"]').html('more »');
		});

		$('.DeleteJob').click(function(e){
			e.preventDefault();
			var answer = confirm('Do you want to delete this user?');
			if(answer == true){
				location.replace($(this).attr('href'));
			}
			else{
				return false;
			}
		});
	</script>
@endsection