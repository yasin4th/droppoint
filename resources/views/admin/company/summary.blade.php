@extends('layouts.material')

@section('content')

<div class="row">

    <div class="col s12 m3">
        <div class="card profile-sidebar">
            @include('includes.admin.usersidebar')
        </div>
    </div>
    <div class="col m9">
        <div class="card">
            <div class="card-content">
                <div class="row">
                    <div class="col s12">
                        <div class="col s12 m12">
                            <h5 class="header">Summary Information</h5>
                            <ul id="projects-collection" class="collection">
                                <li class="collection-item avatar">
                                    <i class="mdi-file-folder circle light-blue"></i>
                                    <span class="collection-header">Credits</span>
                                    <p>{{$user->levels->credits}} Credits Left</p>
                                </li>

                                @foreach($user->achievements->take(5) as $a)
                                <li class="collection-item">
                                    <div class="row">
                                        <div class="col s6">
                                            <p class="collections-title">{{$a->description}}</p>
                                        </div>
                                        <div class="col s3">
                                            <span class="task-cat {{ str_contains($a->description, 'debited') ? 'yellow darken-4' : 'teal'}}">{{ str_contains($a->description, 'debited') ? 'Used' : 'Credits'}}</span>
                                        </div>
                                        <div class="col s3">
                                            <p>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $a->added_date)->format('d F Y') }}</p>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                                {{-- <li class="collection-item">
                                    <div class="row">
                                        <div class="col s6">
                                            <p class="collections-title">Job posted 1 credit used</p>
                                        </div>
                                        <div class="col s3">
                                            <span class="task-cat yellow darken-4">Used</span>
                                        </div>
                                        <div class="col s3">
                                            <p>07 Dec 2016</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    <div class="row">
                                        <div class="col s6">
                                            <p class="collections-title">The Great Reward earns 12 credits</p>
                                        </div>
                                        <div class="col s3">
                                            <span class="task-cat teal">Credits</span>
                                        </div>
                                        <div class="col s3">
                                            <p>07 Dec 2016</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    <div class="row">
                                        <div class="col s6">
                                            <p class="collections-title">The Great Reward earns 12 credits</p>
                                        </div>
                                        <div class="col s3">
                                            <span class="task-cat teal">Credits</span>
                                        </div>
                                        <div class="col s3">
                                            <p>07 Dec 2016</p>
                                        </div>
                                    </div>
                                </li> --}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('scripts')

@endsection