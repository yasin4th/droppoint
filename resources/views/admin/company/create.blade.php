@extends('layouts.material')

@section('content')
<div class="row">
	<div class="col s12 m3">
		@include('includes.admin.sidemenu')
	</div>
	<div class="col s12 m9">
		<div class="card  z-depth-2">
			<div class="card-content">
				<!-- <div class="col s3 m3"></div> -->
				<div class="col s12 m6 card-content">
					@include('includes.alert')

					<form class="login-form" role="form" method="POST" action="{{ route('admin.users.register') }}">
						{{ csrf_field() }}

						<div class="row">
							<div class="input-field col s12 center">
								<h5>Add Company</h5>
							</div>
						</div>
						<div class="row margin">
							<div class="input-field col s12{{ $errors->has('name') ? ' has-error' : '' }}">
								<i class="mdi-social-person-outline prefix"></i>
								<input id="name" name="name" type="text" value="{{ old('name') }}" required autofocus>
								<label for="name" class="center-align">Name</label>
								@if ($errors->has('name'))
									<span class="help-block">
										<span>{{ $errors->first('name') }}</span>
									</span>
								@endif
							</div>
						</div>
						<div class="row margin">
							<div class="input-field col s12{{ $errors->has('email') ? ' has-error' : '' }}">
								<i class="mdi-communication-email prefix"></i>
								<input id="email" type="email" name="email">
								<label for="email" class="center-align">Email</label>
								@if ($errors->has('email'))
									<span class="help-block">
										<span>{{ $errors->first('email') }}</span>
									</span>
								@endif
							</div>
						</div>
						<div class="row margin">
							<div class="input-field col s12{{ $errors->has('password') ? ' has-error' : '' }}">
								<i class="mdi-action-lock-outline prefix"></i>
								<input id="password" type="password" name="password">
								<label for="password">Password</label>
								@if ($errors->has('password'))
									<span class="help-block">
										<span>{{ $errors->first('password') }}</span>
									</span>
								@endif
							</div>
						</div>
						<div class="row margin">
							<div class="input-field col s12{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
								<i class="mdi-action-lock-outline prefix"></i>
								<input id="password-again" type="password" name="confirm" required>
								<label for="password-again">Password again</label>
								@if ($errors->has('confirm'))
									<span class="help-block">
										<span>{{ $errors->first('confirm') }}</span>
									</span>
								@endif
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<button class="btn cyan waves-effect waves-light col"> Create </button>
							</div>
						</div>
					</form>
				</div><!--end #rootwizard -->
			</div>
		</div>
	</div>

</div>
@endsection

