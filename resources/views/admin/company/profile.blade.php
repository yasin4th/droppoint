@extends('layouts.material')

@section('content')

<div class="row">
	<div class="col s12">
		<div class="row">
			<div class="col s12 m3">
				<div class="card profile-sidebar">
					@include('includes.admin.usersidebar')
				</div>
			</div>
			<div class="col s9">
				@include('includes.alert')

				<h5>Edit Profile "{{$user->name}}"</h5>
				<p>This information will be seen by applicants when viewing associated jobs.</p>

				<form class="" role="form" method="POST" action="{{ route('admin.users.update', $user->id) }}">
					{{ csrf_field() }}

					<div class="row">
						<div class="input-field col s12{{ $errors->has('company_type') ? ' has-error' : '' }}">
							<select name="type">
								<option value="" disabled>Choose Company Type</option>
								<option value="individual" {{ ($user->company->type == 'individual') ? 'selected' : '' }}>Individual (unregistered business)</option>
								<option value="soleProprietorship" {{ ($user->company->type == 'soleProprietorship') ? 'selected' : '' }}>Sole Proprietorship (personal business)</option>
								<option value="partnership" {{ ($user->company->type == 'partnership') ? 'selected' : '' }}>Partnership</option>
								<option value="privateCompany" {{ ($user->company->type == 'privateCompany') ? 'selected' : '' }}>Private Company</option>
								<option value="publicCompany" {{ ($user->company->type == 'publicCompany') ? 'selected' : '' }}>Public Company</option>
								<option value="nonProfitOrganization" {{ ($user->company->type == 'nonProfitOrganization') ? 'selected' : '' }}>Non Profit Organization</option>
								<option value="educationalInstitution" {{ ($user->company->type == 'educationalInstitution') ? 'selected' : '' }}>Educational Institution</option>
								<option value="governmentEntity" {{ ($user->company->type == 'governmentEntity') ? 'selected' : '' }}>Government Entity</option><option value="corporation" {{ ($user->company->type == 'corporation') ? 'selected' : '' }}>Corporation</option>
							</select>
							<label>Company Type</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12{{ $errors->has('name') ? ' has-error' : '' }}">
							<input id="name" name="name" type="text" class="validate" value="{{ Request::old('name') ?: $user->name }}">
							<label for="name">Company Name</label>
							@if ($errors->has('name'))
							<span class="help-block">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12{{ $errors->has('website') ? ' has-error' : '' }}">
						  <input id="website" name="website" type="text" class="validate" value="{{ Request::old('website') ?: $user->company->website }}">
						  <label for="website">Website</label>
							@if ($errors->has('website'))
							<span class="help-block">
								<strong>{{ $errors->first('website') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="input-field col s6 m6{{ $errors->has('industry') ? ' has-error' : 'industry' }}">
							<select name="industry" class="browser-default">
								@php
									$industries = DB::table('ref_industry_list')->get();
								@endphp
								<option value="" disabled>-Please Select-</option>

								@foreach($industries as $industry)
									<option value="{{$industry->id}}" {{ ($user->company->industry == $industry->id) ? 'selected' : '' }}>{{$industry->name}}</option>
								@endforeach
							</select>
							<label class="active">Industry</label>
						</div>

						<div class="input-field col s6 m6">
							<select class="browser-default" name="size">
								<option value="" disabled>-Please Select-</option>
								<option value="1-9" {{ ($user->company->size == '1-9') ? 'selected' : '' }}>1-9 employees</option>
								<option value="10-49" {{ ($user->company->size == '10-49') ? 'selected' : '' }}>10-49 employees</option>
								<option value="50-249" {{ ($user->company->size == '50-249') ? 'selected' : '' }}>50-249 employees</option>
								<option value="250+" {{ ($user->company->size == '250+') ? 'selected' : '' }}>250+ employees</option>
							</select>
							<label class="active">Company Size</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12{{ $errors->has('tagline') ? ' has-error' : '' }}">
							<textarea name="tagline" class="materialize-textarea validate" cols="30" rows="10" length="140" maxlength="140">{{ Request::old('tagline') ?: $user->company->tagline }}</textarea>
							<label for="tagline">Elevator Pitch</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12{{ $errors->has('address') ? ' has-error' : '' }}">
						  <input id="address" name="address" type="text" class="validate" value="{{ Request::old('address') ?: $user->company->address }}">
						  <label class="active">Address</label>
							@if ($errors->has('address'))
							<span class="help-block">
								<strong>{{ $errors->first('address') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="input-field col s6">
							<select name="country" class="browser-default">
								<option value="" selected>-Please Select-</option>
								<option value="MY" data-name="Malaysia" {{($user->company->country == 'MY')?'selected':''}}>Malaysia</option>
								<option value="ID" data-name="Indonesia" {{($user->company->country == 'ID')?'selected':''}}>Indonesia</option>
								<option value="SG" data-name="Singapore" {{($user->company->country == 'SG')?'selected':''}}>Singapore</option>
								@php
									$countries = DB::table('ref_country_list')->whereNotIn('name', ['Malaysia', 'Indonesia', 'Singapore'])->orderBy('name')->get();
								@endphp
								@foreach($countries as $country)
									<option value="{{$country->code}}" data-name="{{$country->name}}" {{($user->company->country == $country->code)?'selected':''}}>{{$country->name}}</option>
								@endforeach
							</select>
							<label class="active">Country</label>
						</div>
						<div class="input-field col s6">
							<input id="city" name="city" type="text" class="validate" value="{{ Request::old('city') ?: $user->company->city }}">
							<label>City</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s6{{ $errors->has('zipcode') ? ' has-error' : '' }}">
							<input id="zipcode" name="zipcode" type="text" class="validate" value="{{ Request::old('zipcode') ?: $user->company->zipcode }}">
							<label>Postal / ZipCode</label>
							@if ($errors->has('zipcode'))
							<span class="help-block">
								<strong>{{ $errors->first('zipcode') }}</strong>
							</span>
							@endif
						</div>
						<div class="input-field col s6{{ $errors->has('phone') ? ' has-error' : '' }}">
							<input id="phone" name="phone" type="text" class="validate" value="{{ Request::old('phone') ?: $user->company->phone }}">
							<label>Phone</label>
							@if ($errors->has('phone'))
							<span class="help-block">
								<strong>{{ $errors->first('phone') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12{{ $errors->has('about') ? ' has-error' : '' }}">
							<textarea id="about" name="about" class="materialize-textarea">{!! Request::old('about') ?: $user->company->about !!}</textarea>
							<label>About the company</label>
							@if ($errors->has('about'))
							<span class="help-block">
								<strong>{{ $errors->first('about') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="row">
						<div class="col s12">
							<button type="submit" class="waves-effect waves-light btn light-blue darken-2">SAVE</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
{{-- <script>
	$(document).ready(function() {
		$('select[name=country]').change(function(e) {
			val = $(this).val();
			id = $('option[value="'+val+'"]').attr('data-id');
			$.ajax({
				type : 'get',
				url  : '{{ route('country.index') }}/'+id,
			}).done(function(res) {
				$('select[name=loc_state]').html(res);
				if(_place.state){
					$('select[name=loc_state]').val(_place.state);
				}
				$('select').material_select();
			});
		});
	});
</script> --}}

<script>
	$(document).ready(function(){

		$("#address").on("focus", geolocate)
	});

	var autocomplete;

	function initAutocomplete() {
		autocomplete = new google.maps.places.Autocomplete((document.getElementById('address')), {types: ['geocode']});
		//autocomplete.addListener('place_changed', fillInAddress);
	}

	/*function fillInAddress() {
		var place = autocomplete.getPlace();
		$("#latitude").val(place.geometry.address.lat());
		$("#longitude").val(place.geometry.address.lng());
	}*/

	function geolocate() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var geolocation = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
				var circle = new google.maps.Circle({
					center: geolocation,
					radius: position.coords.accuracy
				});
				autocomplete.setBounds(circle.getBounds());
			});
		}
	}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBA49o4nTKm6sEzirzmdhv_aQcGWQ7k0-I&libraries=places&callback=initAutocomplete"></script>
@endsection





