@extends('layouts.angular-material')
@php
	//$boards = \App\ChatRoom::where('type','board')->get();
	//$queries = \App\ChatRoom::where('type', 'query')->where('status', 'active')->get();
	//$users = (Auth::user()->isAdmin()) ? \App\User::where('role',2)->get() : [Auth::user()];
	$board_icons = [
						['icon'=>'mdi-social-group', 'color'=>'blue'],
						['icon'=>'mdi-maps-local-offer', 'color'=>'green'],
						['icon'=>'mdi-alert-error', 'color'=>'yellow']
					];
@endphp

@section('content')

	<div class="container" ng-controller="chatCtrl" ng-cloak>
		<div id="mail-app" class="section">
			<div class="row">
				<div class="col s12">
					<div id="email-list" class="col s12 m4 l4 card-panel z-depth-1">
						<board class="add_new modal-trigger" href="#addquery" ng-if="company == 1" style="cursor: pointer;">
							<img src="/uploads/logo/avatar.jpg" alt="" class="circle responsive-img">
							<a href="" class="right"><i class="material-icons left">add</i>New</a>
						</board>
						<ul class="collection">
							<li class="collection-item avatar email-unread" ng-repeat="board in boards">
								<board ng-if="board.type=='board'&& board.status=='active'  " ng-click="set(board.id)" ng-class="{selected : room === board.id}">
									<i class="mdi-social-group blue-text"></i>
									<span class="email-title">@{{board.name}}</span>
									<p class="truncate grey-text ultra-small">@{{board.description}}</p>
									<!-- <a href="#!" class="secondary-content"><span class="new badge blue">4</span></a> -->
								</board>
								<board ng-if="board.type=='query'" ng-click="set(board.id)"  ng-class="{selected : room === board.id}">
									@if(Auth::user()->isAdmin())
									<a href="/admin/users/@{{board.user_id}}/edit">
									@endif
										<span class="circle red lighten-1">@{{board.name[0] | titleCase}}</span>
									@if(Auth::user()->isAdmin())
									</a>
									@endif
									<span class="email-title">@{{board.name | titleCase}}</span>
									<p class="truncate grey-text ultra-small">@{{board.description | titleCase}}</p>
									<a href="#!" class="secondary-content email-time"><span class="blue-text ultra-small">@{{board.updated_at | time}}</span></a>
								</board>
							</li>
						</ul>
					</div>
					<div id="email-details" class="col s12 m8 l8 card-panel">
						<div class="chat-header">
							<div class="chat-action">
								<div class="user-img left">
								@if(Auth::user()->isAdmin())
									<a href="/admin/users/@{{boards[__get(room)].user.id}}/edit">
								@endif
										<img ng-src="@{{boards[__get(room)].user.avatar}}">
										<h5> @{{boards[__get(room)].user.name}}</h5>
								@if(Auth::user()->isAdmin())
									</a>
								@endif
								</div>

								<a href="#!" class="dropdown-button right"  data-activates='chat-user'><span><i class="mdi-navigation-more-vert" ng-if="company != 1"></i></span></a>
								<ul id='chat-user' class='dropdown-content'>
									<li><a href="#!" ng-click="resolved()">Resolved</a></li>
								</ul>
							</div>
							<!-- <div class="col s12 m5"> -->
									<!-- <div class="chat-search">
										<button type="submit" class="btn left"><i class="material-icons">search</i></button>
										<input type="text" class="search">
									</div> -->
									<!-- </div> -->
									<!-- <div class="col s12 m12">
										<div class="chat-action">
											<div class="user-img left">
												<img src="uploads/logo/@{{boards[__get(room)].user.avatar}}">
												<h5> @{{boards[__get(room)].user.name}}</h5>
											</div>
											<a href="#!" class="dropdown-button right"  ng-if="company != 1" data-activates='chat-user'><span><i class="mdi-navigation-more-vert"></i></span></a>
											<ul id="chat-user" class="dropdown-content" ng-if="company != 1">
												<li><a href="#!" ng-click="resolved()">Resolved</a></li>
											</ul>
										</div>
									</div> -->
								<!-- </div> -->

						</div>
						<div class="email-content-wrap">
							<div class="row">
								<div class="col s12 m12 l12">
									<ul class="collection" class="autoscroll" id="chat-wrapper">
										<li class="collection-item avatar" ng-repeat="msg in boards[__get(room)].messages">
										@if(Auth::user()->isAdmin())
											<a href="/admin/users/@{{msg.sender.id}}/edit">
										@endif
											<div class="user-pic">
												<div>
													<img ng-src="@{{msg.sender.avatar}}" alt="" class="responsive-img">
												</div>
											</div>
												<span class="email-title blue-text">@{{msg.sender_name}}</span>
										@if(Auth::user()->isAdmin())
											</a>
										@endif
											<!-- <p class="truncate grey-text ultra-small">To me, John Doe</p> -->
											<!-- <p class="grey-text ultra-small">@{{msg.human}}</p> -->
											<div class="email-content">
												<p ng-bind-html="msg.message"></p>
											</div>
										</li>
									</ul>
								</div>
								<div class="col s12 m12 l12">
								</div>
							</div>
							<div class="row chat-input-box" ng-if="company==0 || !(room == 3 || room == 1)">
								<div class="col s12 m12 l12">
									<hr>
									<div class="input-box">
										<article contenteditable="true" id="message" ng-model="message" stripBr="true">
										</article>
										<!-- <aside ng-bind-html="attach"></aside> -->
										<aside id="attachments">
											<div class="attachmentsDiv">
												<div class="attach" ng-repeat="attach in attachs">
													<a href="@{{attach.src}}" target="_blank"><i class="fa @{{getFileType(attach.type)}}"></i></a>
													<a href="#" class="msg-trash">
														<i class="fa fa-trash" aria-hidden="true"></i>
													</a>
													<a href="#" class="msg-arrow">
														<i class="fa fa-arrow-down" aria-hidden="true"></i>
													</a>
													<div class="progress" ng-if="!attach.status">
														<div class="indeterminate"></div>
													</div>
												</div>
											</div>
										</aside>
										<ul id="user-suggestion" class='dropdown-content'>
											<li ng-repeat="user in admins"><a href="#" data-user="user.id"  ng-click="setAdmin($index, $event)">@{{user.name}}</a></li>
										</ul>
										<div class="chat_icons">
											<i class="fa fa-paperclip" onclick="document.getElementById('fileInput').click();" style="cursor: pointer;"></i>
											<i class="fa fa-paper-plane" ng-click="sendMessage()" style="cursor: pointer;"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<input type="file" id="fileInput" style="display: none" ng-model="image" onchange="angular.element(this).scope().getAttachment()">
		<div id="addquery" class="modal">
			<div class="modal-content">
				<div class="container">
					<div id="mail-app" class="section">
						<div class="row">
							<div id="email-details" class="col s12 m12 l12">
								<div class="chat-header">
									<div class="chat-action">
										<div class="user-img left">
											<img src="/uploads/logo/avatar.jpg" alt="" class="circle responsive-img">
											<h5>{{Auth::user()->name}}</h5>
										</div>
										<a href="#!" class="modal-action modal-close waves-effect waves-green black-text right
										">Cancel</a>
									</div>
								</div>
								<div class="email-content-wrap">
									<div class="row chat-input-box">
										<div class="col s12 m12 l12">
											<hr>
											<div class="input-box">
												<form ng-submit="addBoard()">
													<input type="text" name="" id="query_name">
													<div class="chat_icons">
														<a ng-click="addBoard()" style="cursor:pointer;">
															<i class="fa fa-paper-plane"></i>
														</a>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- <div class="modal-footer">
				<a href="#!" class="modal-action modal-close waves-effect waves-green btn blue btn-flat left white-text">Close</a>
				<a href="#!" class="modal-action modal-close waves-effect waves-green btn blue white-text btn-flat" ng-click="addBoard()">Create</a>
			</div> -->
		</div>
	</div>


	</div>
@endsection

@section('scripts')
	<script src="/js/socket.1.4.5.js"></script>
	<script src="/js/materialize/plugins/fullcalendar/lib/moment.min.js" type="text/javascript" charset="utf-8"></script>

	<script type="text/javascript">
		var socket = io.connect('{{env('CHAT_SERVER')}}', {secure:true});
	</script>

	<script type="text/javascript">
		var app = angular.module('myapp', ['ngAnimate','ngMaterial','ngSanitize']);

		app.service('RoomService', function() {
			this.getRooms = function(scope, http, timeout) {
				http.get('/boards/'+((scope.company == 1)?scope.sender:0)).then(function(response) {
					scope.boards = response.data;
					console.log(response.data);
					scope.room = (scope.boards.length)?scope.boards[0].id:0;
					angular.forEach(scope.boards, function(room, i){
						http.get(`/messages/${room.id}/0`).then(function(response){
							room.messages = response.data;
							timeout(autoscroll, 0, false);
						});
					});
				});
			}

			this.getAdmins = function(scope, http) {
				var admins = [];
				http.get('/admins').then(function(response) {
					response.data.forEach(function(user) {
						admins.push({
							id: user.id,
							name: user.name,
							email: user.email
						})
					})
					scope.admins = admins;
				})
			}

		});

		// controllers

		app.controller('chatCtrl', function($scope, $http, $window, $timeout, RoomService, $sce) {
			$scope.sender = {{Auth::user()->id}};
			$scope.company = {{Auth::user()->isCompany()? 1 : 0}};
			$scope.room = 0;
			$scope.messages = [];
			$scope.boards = [];
			$scope.attachs = [];
			$scope.admins = [];
			RoomService.getAdmins($scope, $http);
			RoomService.getRooms($scope, $http, $timeout)

			/*$http.get('/boards/'+(($scope.company == 1)?$scope.sender:0)).then(function(response) {
				$scope.boards = response.data;
				$scope.room = ($scope.boards.length)?$scope.boards[0].id:0;
				angular.forEach($scope.boards, function(room, i){
					$http.get(`/messages/${room.id}/0`).then(function(response){
						room.messages = response.data;
						$timeout(autoscroll, 0, false);
					});
				});
			});*/

			socket.on('received', function (result) {
				room_id = result.data.room_id;
				var index = $scope.__get(room_id);
				if($scope.boards[index].messages.length) {
					id = $scope.boards[index].messages[$scope.boards[index].messages.length-1].id;
				}
				else {
					id = 0;
				}

				$http.get('/messages/'+room_id+'/'+id).then(function(response) {
					$scope.boards[index].messages.pop();
					$scope.boards[index].messages = $scope.boards[index].messages.concat(response.data);
					// $scope.attach = '';
					$timeout(autoscroll, 0, false);
				});
			});

			socket.on('sendBoard', function (result) {
				result.data.id = result.result.insertId;
				result.data.messages = [];
				$scope.boards.push(result.data);
				$scope.$apply();
			});

			$scope.addBoard = function() {
				query_name = document.getElementById('query_name').value;
				if(query_name != '') {

				$('#addquery').closeModal();
				query_description = ''; //document.getElementById('query_description').value;
				socket.emit('addBoard', { user_id: $scope.sender, name: query_name, description: query_description });
				document.getElementById('query_name').value = '';
				} else {
					Materialize.toast('Please enter your query.',1500);
				}
			}

			$scope.upload = function(files) {
				console.log(files);
				if (files && files.length) {
					var formData = new FormData();

					for (var i = 0; i < files.length; i++) {
						formData.append('file[]',files[i]);
						$scope.attachs.push({name: files[i].name,type: files[i].type, status: false})
					}
					$scope.$apply();

					$http({
						method: "post",
						url: "upload",
						data: formData,
						headers: { 'Content-Type': undefined},
					}).then(function(response) {

						$scope.attachs.forEach(function(attach) {
							attach.status = true;
						});

						console.log($scope.attachs);

					}, function(response) {

					});
				}
			}

			$scope.getAttachment = function() {
				$scope.upload(document.getElementById('fileInput').files)
			}

			$scope.set = function(room) {
				$scope.room = room;
				$scope.attachs.length = 0;
				$timeout(autoscroll, 0, false);
			}

			$scope.__get = function(room_id){
				var index = 0
				$scope.boards.forEach(function(el,i){if(el.id == room_id){index = i;}})
				return index;
			}

			$scope.setAdmin = function(index, e) {
				e.preventDefault();

				var msgs = document.getElementById('message').innerHTML.replace(/&nbsp;/g, ' ').replace(/[\s\n\r]+/g, ' ').trim().split(' ');



				if(msgs.length > 0) {
					roomuser = $scope.boards[$scope.__get($scope.room)].user_id;
					msgs[msgs.length - 1] = '<a href="#" class="tagged-admin admin'+$scope.admins[index].id+' room'+$scope.room+' sender'+$scope.sender+'">@' + $scope.admins[index].name + '</a> ';
					document.getElementById('user-suggestion').style.display = "none";
					document.getElementById('user-suggestion').style.opacity = 0;
					document.getElementById("message").focus();
					document.getElementById('message').innerHTML = msgs.join(' ') + '&nbsp;';
					// $scope.message = $sce.trustAsHtml(msgs.join(' ')+ ' ');
					$timeout(function(){placeCaretAtEnd(document.getElementById('message'))}, 100);
				}
			}

			$scope.resolved = function() {
				socket.emit('send', { message: '<span class="resolved">Resolved</span>', sender_id: $scope.sender, room_id: $scope.room });
			}

			$scope.sendMessage = function() {
				console.log('message');
			}

			$scope.getFileType = function(type) {
				var cls = 'fa-file';
				switch(type) {
					case 'image/jpeg':
						cls = 'fa-file-image-o';
						break;
					case 'application/pdf':
						cls = 'fa-file-pdf-o';
						break;
					case 'image/png':
						cls = 'fa-file-image-o';
						break;
					case 'docx':
						cls = 'fa-file-word-o';
						break;
					default:
						cls = 'fa-file';
						break;
				}
				return cls;
			}
		});

		//directives
		app.directive('contenteditable', ['$sce', '$rootScope', function($sce, $rootScope) {
			return {
				restrict: 'A',
				require: 'ngModel',
				link: function(scope, element, attrs, ngModel) {
					if (!ngModel)
					{
						console.log('no ng model');
						return;
					}
					// Specify how UI should be updated
					ngModel.$render = function() {
						element.html($sce.getTrustedHtml(ngModel.$viewValue || ''));
					};

					// Listen for change events to enable binding
					element.on('blur keyup change', function(event) {

						if(!event.shiftKey && event.key == 'Enter') {
							html = element.html();
							html= html.replace(/<div><br><\/div>$/,'') + document.getElementById('attachments').innerHTML;
							socket.emit('send', { message: html, sender_id: scope.sender, room_id: scope.room });
							element.html('');
							scope.attachs.length = 0;
						} else if(event.shift && event.key == 'Enter') {
							// console.log('shift+enter')
						}

						parse();
						scope.$evalAsync(read);
						// event.preventDefault();
					});

					// Listen for Dragover
					element.on('dragover', function(event) {
						element.addClass('dragover');
						event.preventDefault();
					});

					element.on('dragleave', function(event) {
						element.removeClass('dragover');
						event.preventDefault();
					});

					element.on('drop', function(event) {
						element.removeClass('dragover');
						scope.upload(event.dataTransfer.files);
						event.preventDefault();
					});
					read(); // initialize

					// Write data to the model
					function read() {
						var html = element.html();
						// When we clear the content editable the browser leaves a <br> behind
						// If strip-br attribute is provided then we strip this out
						if ( attrs.stripBr && html == '<br>' ) {
							html = '';
						}
						ngModel.$setViewValue(html);
					}

					function parse() {
						var strs = element.html().replace(/&nbsp;/g, ' ').split(' ');
						if (strs.length > 0) {
							var last = strs[strs.length - 1];
							// console.log(last)
							if(last.indexOf('@') == 0) {
								document.getElementById('user-suggestion').style.display = "block";
								document.getElementById('user-suggestion').style.opacity = 1;
							} else {
								document.getElementById('user-suggestion').style.display = "none";
								document.getElementById('user-suggestion').style.opacity = 0;
							}
						}
					}
				}
			};
		}]);

		app.filter('titleCase', function() {
			return function(input) {
				input = input || '';
				return input.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1);});
			};
		});

		app.filter('time', function() {
			return function(input) {
				input = input || '';
				return moment(input,'YYYY-MM-DD HH:mm:ss').format('hh:mm A');
			};
		});

		autoscroll = function() {
			var scroller = document.querySelector('.email-content-wrap ul');
			scroller.scrollTop = scroller.scrollHeight;
		}

		function placeCaretAtEnd(el) {
			el.focus();
			if (typeof window.getSelection != "undefined"
			&& typeof document.createRange != "undefined") {
				var range = document.createRange();
				range.selectNodeContents(el);
				range.collapse(false);
				var sel = window.getSelection();
				sel.removeAllRanges();
				sel.addRange(range);
			} else if (typeof document.body.createTextRange != "undefined") {
				var textRange = document.body.createTextRange();
				textRange.moveToElementText(el);
				textRange.collapse(false);
				textRange.select();
			}
		}
	</script>
@endsection
