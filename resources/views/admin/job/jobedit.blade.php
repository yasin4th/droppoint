@extends('layouts.app')

@section('content')

<div class='container'>
     {!! Form::model($job,['method'=>'PUT','route'=>['admin.update.job','id'=> $job->id],'files' => true])!!}

        <div class="row">
    <div class="col s12">

        <div class="row">
            
            <div class="col s12" style="margin:10px;">
              <ul class="tabs">
                <li class="tab col s3"><a href="#f1" style="color: purple;">Job Details</a></li>
                <li class="tab col s3"><a href="#f2" style="color: purple;">Time</a></li>
                <li class="tab col s3"><a href="#f3" style="color: purple;">Location</a></li>
              </ul>
            </div>

            <div id="f1" class="col s12">


                @if (Auth::user()->role == '1')
                  <div class="row" style="margin-left: 12px; margin-top: 23px;">
                        <div class="input-field col s12">
                        <i class="material-icons prefix">business</i>
                           {!! Form::select('user_id', $company,null,['class' => 'validate', 'required'=>'required', 'placeholder'=>'Select a company ']) !!}
                            <label for="category" data-error="wrong" data-success="">Company</label>
                        </div>
                    </div> 
                @endif

                <div class="row" style="margin-left: 12px; margin-top: 23px;">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">perm_identity</i>

                        {!! Form::text('job_title', null, ['class' => 'validate', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                        <label for="job_title" data-error="wrong" data-success="">Job Title</label>
                    </div>
                </div>
            
                <div class="row" style="margin-left: 12px; margin-top: 23px;">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">message</i>

                        {!! Form::textarea('responsibilities', null, ['class' => 'materialize-textarea', 'length="120"', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                        <label for="responsibilities" data-error="wrong" data-success="">Responsibilities</label>
                    </div>
                </div>

                <div class="row" style="margin-left: 12px; margin-top: 23px;">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">shopping_basket</i>

                        {!! Form::text('wages', null, ['class' => 'validate', 'type'=>'text', isset($disabled) ? 'disabled' : '',  'required' => '']) !!}
                        <label for="wages" data-error="wrong" data-success="">Wages</label>
                    </div>

                    <div class="input-field col s6">
                        <i class="material-icons prefix">android</i>

                        {!! Form::text('dress_code', null, ['class' => 'validate', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                        <label for="dress_code" data-error="wrong" data-success="">Dress Code</label>
                    </div>
                </div>

                <div class="row" style="margin-left: 12px; margin-top: 23px;">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">view_module</i>

                        {!! Form::text('category', null, ['class' => 'validate', 'type'=>'text',isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                        <label for="category" data-error="wrong" data-success="">Category</label>
                    </div>
                </div>

                

        {{--         <div class="row" style="margin-left: 12px; margin-top: 23px;">
                    <div class="input-field col s3">
                        {!! Form::select('category', $category, null, ['class' => 'validate ', 'type'=>'text',isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                        <label for="category" data-error="wrong" data-success="">Category</label>
                    </div>
                </div> --}} 

            </div>
            
            <div id="f2" class="col s12">
                
                <div class="row" style="margin-left: 12px; margin-top: 23px;">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">today</i>

                        {!! Form::date('start_date', null, ['class' => 'datepicker', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                        <label for="start_date" data-error="wrong" data-success="">Start Date</label>
                    </div>
                    
                    <div class="input-field col s6">
                        <i class="material-icons prefix">today</i>

                        {!! Form::date('end_date', null, ['class' => 'datepicker', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                        <label for="end_date" data-error="wrong" data-success="">End Date</label>
                    </div>
                </div>


                <div class="row" style="margin-left: 12px; margin-top: 23px;">
                    
                    

                    <div class="input-field col s6">
                        
                        <i class="material-icons prefix">av_timer</i>

                        {!! Form::text('start_time', null, ['class' => '', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                        
                        <label for="start_time" data-error="wrong" data-success="">
                               Start Time
                        </label>
                    </div>
                    
                    <div class="input-field col s6">

                        <i class="material-icons prefix">av_timer</i>

                        {!! Form::text('end_time', null, ['class' => 'validate', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                        <label for="end_time" data-error="wrong" data-success="">End Time</label>
                    </div>
                </div>

            </div>


            <div id="f3" class="col s12">
                
                <div class="row" style="margin-left: 12px; margin-top: 23px;">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">store</i>

                        {!! Form::textarea('job_location', null, ['class' => 'materialize-textarea','length="120"', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                        <label for="job_location" data-error="wrong" data-success="">Job Location</label>
                    </div>
                </div>

                <div class="row" style="margin-left: 12px; margin-top: 23px;">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">my_location</i>

                        {!! Form::text('latitude', null, ['class' => 'validate', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                        <label for="latitude" data-error="wrong" data-success="">Latitude</label>
                    </div>
                    
                    <div class="input-field col s6">
                        <i class="material-icons prefix">my_location</i>
                        {!! Form::text('longitude', null, ['class' => 'validate', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                        <label for="longitude" data-error="wrong" data-success="">Longitude</label>
                    </div>

                    <div class="input-field col s12">
                       <button style="left:500px;" class="btn waves-effect waves-light deep-purple lighten-2 pull-right" type="submit" name="action">Submit
                         <i class="material-icons right">send</i>
                       </button>
                    </div>
                </div>

            </div>

        </div>
          
    </div>
</div>


    {!! Form::close() !!}
</div>


@endsection


@section('after-script')
@php ($error = '')

@if ($errors->any())
            <ul>
                @foreach ($errors->all() as $err)
                    <li>{{ $error = $err }}</li> 
                @endforeach
            </ul>



        <script>toastr["error"]("{!! $error !!}", "Error")</script>
@elseif (Session::get('flash_success'))
    
        @if(is_array(json_decode(Session::get('flash_success'),true)))
            {{ $success = implode('', Session::get('flash_success')->all(':message<br/>')) }}
        @else
            <script>toastr["success"]("{!! Session::get('flash_success') !!}", "Success")</script>
        @endif
    
@endif
@endsection