@extends('layouts.app')

@section('content')

{!! Form::open(['method' => 'DELETE', 'route'=>['admin.destroy.job', $job->id ]]) !!}

  <li><button type="submit" class="waves-effect waves-light btn red darken-1">Delete</button></li>

{!! Form::close() !!}


@endsection


@section('after-script')
@php ($error = '')

@if ($errors->any())
            <ul>
                @foreach ($errors->all() as $err)
                    <li>{{ $error = $err }}</li> 
                @endforeach
            </ul>



        <script>toastr["error"]("{!! $error !!}", "Error")</script>
@elseif (Session::get('flash_success'))
    
        @if(is_array(json_decode(Session::get('flash_success'),true)))
            {{ $success = implode('', Session::get('flash_success')->all(':message<br/>')) }}
        @else
            <script>toastr["success"]("{!! Session::get('flash_success') !!}", "Success")</script>
        @endif
    
@endif
@endsection