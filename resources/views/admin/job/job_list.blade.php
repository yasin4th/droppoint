@extends('layouts.materialize')
@section('content')

<style type="text/css">

.button-submit{
  background-color:transparent;
  border:none;
}

</style>

<link rel="stylesheet" href="/css/materialize.min.css"/>


<div class="row" style="margin-top:15px; margin-bottom: -10px">

  <div class="col s8" style="margin-left: 20px;">
    <nav class="white">
      <div class="nav-wrapper">
        <form >
          <div class="input-field">
            <input id="search" type="search"  required>
            <label for="search"><i class="material-icons" style="color:black;">search</i></label>
            <i class="material-icons">close</i>
          </div>
        </form>
      </div>
    </nav>
  </div>

  <div class="col s2 right-align" style="margin-left: 82px;">
    <p><a href="{{route('admin.create.jobs')}}" class="btn waves-effect waves-light" style="background-color:#401055">Create Job</a></p>
  </div>
            
</div>

 <div class="row" style="padding-left: 20px; margin-top:20px;">        
      <div class="col s12">
          <div class="row section">
            <div class="col s12 m12 l12">
              <nav style="background-color:#401055">
                 <div class="nav-wrapper">
                     <div class="col s12">
                        <a href="{{route('dashboard')}}" class="breadcrumb">Dashboard</a>
                        <a href="#" class="breadcrumb">All Jobs </a>
                      </div>
                      <span class="new badge">{{$countjobs}}</span>
                 </div>
              </nav>
           </div>
         </div>

         

       
            <div class="card-panel">
                <div class="row">
                    <table class="responsive-table bordered">
                      
                      <thead>
                        <tr>
                            <th data-field="id">Company</th>
                            
                            <th data-field="id">Job Title
                               <a href="/admin/jobs/all{{ !isset($type) ? '?by=job_title&type=asc' : '' }} "><i class="mdi-editor-format-line-spacing" style="color:black;"></i></a>
                            </th>
                                            
                            <th data-field="Location">Job Location
                              <a href="/admin/jobs/all{{ !isset($type) ? '?by=job_location&type=asc' : '' }} "><i class="mdi-editor-format-line-spacing" style="color:black;"></i></a>
                            </th>

                            <th data-field="start">Start Date
                                <a href="/admin/jobs/all{{ !isset($type) ? '?by=start_date&type=asc' : '' }} "><i class="mdi-editor-format-line-spacing" style="color:black;"></i></a>
                            </th>
                             
                            {{-- <th data-field="Action">Action</th> --}}
                        </tr>
                      </thead>

                    <tbody>
                        
                        {{-- <tr>
                            <td></td>
                            <td class="browser-default">
                                <form action="{!! route('admin.company.search.name') !!}" method="get"> 
                                   <input name="name"  placeholder="Company Name"  type="text" style="width:80% !important;">
                                   <button class="button-submit">
                                     <i class="mdi-action-search"></i>
                                   </button>
                                </form>
                            </td>
                            <td class="browser-default">
                                <form action="{!! route('admin.company.search.mail') !!}" method="get"> 
                                    <input name="mail"  placeholder="Company Email"  style="width:80% !important;" type="text">
                                    <button class="button-submit">
                                      <i class="mdi-action-search"></i>
                                   </button>
                                </form>
                            </td>
                            <td></td>
                            <td></td>
                        </tr> --}}

                        @foreach($jobs as $job)
                          <tr>
                             <td>{{ $job->user->name }}</td>
                             <td>{{ $job->job_title }}</td>
                             <td>{{ $job->job_location }}</td>
                             <td>{{ $job->start_date }}</td> 

                             <td>                                     
                                <a class=" dropdown-button waves-effect waves-light" style="color:#401055" href="#!" data-activates="dropdown{{ $job->id }}">More{{-- <i class="mdi-navigation-arrow-drop-down right"></i> --}}</a>

                                <ul id="dropdown{{ $job->id }}" class="dropdown-content" style="width: 182px; position: absolute; top: 357.516px; left: 975.953px; opacity: 1; display: none;">
                                  <li><a href="{{ route('admin.view.job',$job->id) }}" class="-text" style="color:purple lighten-2">View</a></li>
                                  <li><a href="{{ route('admin.edit.job',$job->id) }}" class="-text" style="color:purple lighten-2">Edit</a></li>
                                  {{-- <li><a href="{{ route('admin.destroy.job',$job->id) }}" class="-text" style="color:purple lighten-2">Delete</a></li> --}}

                                  {!! Form::open(['method' => 'DELETE', 'route'=>['admin.destroy.job', $job->id ]]) !!}

                                      <li><button type="submit">Delete</button></li>

                                  {!! Form::close() !!}


                                </ul>
                             </td>
                          </tr>

                        @endforeach                   
                      </tbody>
                    </table>
              </div>
         </div>

       
         <div style="text-align:center">
              {{ $jobs->render() }}
        </div>
    </div>
</div>

@endsection

@section('after-script')
@php ($error = '')
@if ($errors->any())
        @foreach ($errors->all() as $err)
            {{ $error = $err }}<br/>    
        @endforeach
        <script>toastr["error"]("{!! $error !!}", "Error")</script>
@elseif (Session::get('flash_success'))
    
        @if(is_array(json_decode(Session::get('flash_success'),true)))
            {{ $success = implode('', Session::get('flash_success')->all(':message<br/>')) }}
        @else
            <script>toastr["success"]("{!! Session::get('flash_success') !!}", "Success")</script>
        @endif
    
@endif

@endsection