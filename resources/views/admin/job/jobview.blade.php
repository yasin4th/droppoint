@extends('layouts.materialize')

@section('content')


<link rel="stylesheet" href="/css/materialize.min.css"/>
<link href="/css/app.css" rel="stylesheet">


{!! Form::open(['route' => 'admin.save.jobs','class' => 'form-horizontal', 'method' => 'post']) !!}

<div class="row" style="margin-top:20px;">
    
    <div class="col s10" style="padding: 0px; margin-left: 50px;">
            
        <div class="collection with-header" style="border-radius:7px;">   
            <div class="collection-header" style="background-color:white;">
                <h4 style="font-size:24px;">Add Job</h4>
            </div>
            
            <div class="collection-item" style="padding:0;">
                <ul class="tabs action">
                    <li class="tab"><a style="color: black;" href="#f1">Job Details</a></li>
                    <li class="tab"><a style="color: black;" href="#f2">Additional Information</a></li>
                </ul>
            </div>
        </div>

    </div>

</div>

<div class="row">

    <div id="f1" class="col s10 card" style="border-radius:7px; margin-left:50px;">

        @if (Auth::user()->role == '1')
          <div class="row card-content" style="margin-left: 55px; margin-top: 55px;">
                <div class="input-field col s10" style="margin-bottom:-45px;">
                   {!! Form::select('user_id', $company,null,['class' => 'validate', 'required'=>'required', 'placeholder'=>'Select a company ']) !!}
                    <label style="margin-top:-45px;" for="category" data-error="wrong" data-success="">Company</label>
                </div>
            </div> 
        @endif

        <div class="row card-content" style="margin-left: 55px; margin-top: 23px;">
            <div class="input-field col s10" style="margin-bottom:-15px;">

                {!! Form::text('job_title', null, ['class' => 'validate', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                <label for="job_title" data-error="wrong" data-success="">Job Title</label>
            </div>
        </div>
    

        <div class="row card-content" style="margin-left: 55px; margin-top: 23px;">
            
            

            <div class="input-field col s10" style="margin-bottom:-30px;">
                
                
                {!! Form::textarea('responsibilities', null, ['class' => 'editme', 'type'=>'text', isset($disabled) ? 'disabled' : '']) !!}
                <label for="responsibilities" data-error="wrong" data-success="" style="margin-top:-45px; font-size:15px">Responsibilities</label>

            </div>
        </div>

        <div class="row card-content" style="margin-left: 55px; margin-top: 40px; margin-bottom:-30px;">
            <div class="input-field col s2">

                {!! Form::text('wages', null, ['class' => 'validate', 'type'=>'text', 'placeholder' => 'RM', isset($disabled) ? 'disabled' : '',  'required' => '']) !!}
                <label for="wages" data-error="wrong" data-success="">Wages</label>
            </div>
            
            <div class="input-field col s3">
                {!! Form::select('duration', ['H' => 'Per Hour', 'W' => 'Per Week', 'M' => 'Per Month']);
                !!}
            </div>

        </div>


        <div class="row card-content" style="margin-left: 55px; margin-top: 23px; margin-bottom:-30px;">
            <div class="input-field col s4">

                {!! Form::date('start_date', null, ['class' => 'datepicker', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                <label for="start_date" data-error="wrong" data-success="">Start Date</label>
            </div>
            
            <div class="input-field col s4">

                {!! Form::date('end_date', null, ['class' => 'datepicker', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                <label for="end_date" data-error="wrong" data-success="">End Date</label>
            </div>
        </div>


        <div class="row card-content" style="margin-left: 55px; margin-top: 23px; margin-bottom:-30px;">

            <div class="input-field col s3">

                {!! Form::text('start_time', null, ['class' => 'timepicker', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                
                <label for="start_time" data-error="wrong" data-success="">
                       Start Time
                </label>
            </div>
            
            <div class="input-field col s3">


                {!! Form::text('end_time', null, ['class' => 'timepicker', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                <label for="end_time" data-error="wrong" data-success="">End Time</label>
            </div>

        </div>

        <div class="row card-content" style="margin-left: 55px; margin-top: 23px; margin-bottom:-30px;">
            <div class="input-field col s10">
                <div class="validate">
                    <label for="">Map</label>
                    <input type="text" placeholder="" id="searchmap">
                    <div id="map-canvas" style="margin-left:-10px; margin-top: 10px;"></div>
                </div>
            </div>
        </div>

        <div class="row card-content" style="margin-left: 55px; margin-top: 40px; margin-bottom:-50px;">
            <div class="input-field col s5">

                {!! Form::text('latitude', null, ['class' => 'validate', 'id' => 'lat', 'placeholder' => '', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                <label for="latitude" data-error="wrong" data-success="">Latitude</label>
            </div>
            
            <div class="input-field col s5">
                {!! Form::text('longitude', null, ['class' => 'validate', 'id' => 'lng', 'placeholder' => '', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                <label for="longitude" data-error="wrong" data-success="">Longitude</label>
            </div>
        </div>

        <div class="row card-content" style="margin-left: 55px; margin-top: 40px;">
                <div class="input-field col s10">
                   <button class="btn waves-effect waves-light" type="submit" name="action" style="background-color:#1D1C2C;">Submit
                     <i class="material-icons right">send</i>
                   </button>
                </div>
             </div>
        </div>

    </div>
        
    <div class="row">

        <div id="f2" class="col s10 card" style="border-radius:7px; margin-left:50px;">
            
             <div class="row card-content" style="margin-left: 55px; margin-top: 23px;">
                <div class="input-field col s10" style="margin-bottom:-40px;">

                    {!! Form::textarea('job_location', null, ['class' => 'materialize-textarea','length="120"', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                    <label for="job_location" data-error="wrong" data-success="">Job Location</label>
                </div>
             </div>

             <div class="row card-content" style="margin-left: 55px; margin-top: 40px; margin-bottom:-30px;">
                
                

                <div class="input-field col s10">

                    {!! Form::textarea('other_details', null, ['class' => 'editme', 'type'=>'text', isset($disabled) ? 'disabled' : '']) !!}
                    <label for="other_details" data-error="wrong" data-success="" style="margin-top:-45px; font-size:15px">Other Details</label>
                    
                </div>
             </div>

             <div class="row card-content" style="margin-left: 55px; margin-top: 40px; margin-bottom:-50px;">
                <div class="input-field col s5">

                    {!! Form::text('dress_code', null, ['class' => 'validate', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                    <label for="dress_code" data-error="wrong" data-success="">Dress Code</label>
                </div>

                <div class="input-field col s5" style="margin-top:30px; margin-left:10px;">

                    {!! Form::select('category', $category, null, ['class' => 'validate ', 'type'=>'text', isset($disabled) ? 'disabled' : '', 'required' => '']) !!}
                    <label style="margin-top:-45px;" for="category" data-error="wrong" data-success="">Category</label>
                </div>
             </div>

             <div class="row card-content" style="margin-left: 55px; margin-top: 60px; margin-bottom: 30px;">
                <div class="input-field col s10">
                   <button class="btn waves-effect waves-light" type="submit" name="action" style="background-color:#1D1C2C;">Submit
                     <i class="material-icons right">send</i>
                   </button>
                </div>
             </div>
        </div>

    </div>

      
</div>

{!! Form::close() !!}



@endsection


@section('after-script')
@php ($error = '')

@if ($errors->any())
            <ul>
                @foreach ($errors->all() as $err)
                    <li>{{ $error = $err }}</li> 
                @endforeach
            </ul>



        <script>toastr["error"]("{!! $error !!}", "Error")</script>
@elseif (Session::get('flash_success'))
    
        @if(is_array(json_decode(Session::get('flash_success'),true)))
            {{ $success = implode('', Session::get('flash_success')->all(':message<br/>')) }}
        @else
            <script>toastr["success"]("{!! Session::get('flash_success') !!}", "Success")</script>
        @endif
    
@endif
@endsection
