@extends('layouts.materialize')

@section('content')
<style type="text/css">
	.pagination li span {
    color: #fff;
    display: inline-block;
    font-size: 1.2rem;
    padding: 0 10px;
    line-height: 30px;}

    input:not([type]), input[type=text], input[type=password], input[type=email], input[type=url], input[type=time], input[type=date], input[type=datetime], input[type=datetime-local], input[type=tel], input[type=number], input[type=search], textarea.materialize-textarea {
    background-color:#f2f2f2;
/*    border: none;
    border-bottom: 1px solid #9e9e9e;
    border-radius: 0;*/
    outline: none;
    height: 1rem;
    width: 60%;
    font-size: 1.2rem;
    margin: 0 0 0px 0;
    padding: 0;
    padding-top: 10px;
    box-shadow:none;
}

.button-submit{
  background-color:transparent;
  border:none;
}


	
</style>

<div class="row">

{{-- 		<div class="col s12">
			<a style="float:right; margin-top: 30px;" class="btn-floating waves-effect waves-light deep-purple lighten-2" href="{{route('admin.create.jobs')}}"><i class="material-icons">add</i></a>
		</div> --}}
		
		<div class="col s12">

		<div class="collection card-panel" style="padding:0px !important;margin-top:20px">
      		<div class="collection-item black-text">
        	<h4 style="font-size:24px">{{$user->name}}

        	<span class="new badge deep-purple lighten-2" data-badge-caption="jobs" style="font-size:16px">{{$countcompanyjobs}}</span></h4>
      		</div>
   	</div>

            <table class="striped">
              <thead>
                <tr>
                   	<th data-field="id">Job Title</th>                  
                    <th data-field="Location">Job Location</th>
                     <th data-field="start">Start Date</th>
                     <th data-field="End">Wages (RM)</th>
                    <th data-field="Action">Action</th>
                </tr>
              </thead>
        			<tbody>
            <!--    <tr>             
                     <td class="browser-default" ><form  action="{!! route('admin.job.search.jobTitle') !!}"  method="get"> <input id="job"  placeholder="Job Title"  type="text">   <button class="button-submit"><i class="material-icons" style="color:#b9b9b9">search</i></button></form></td>
                     <td class="browser-default"><form  action="{!! route('admin.job.search.jobLocation') !!}"  method="get"><input name="location"  placeholder="Job Location"  type="text">    <button class="button-submit"><i class="material-icons" style="color:#b9b9b9">search</i></button></form></td>
                     <td class="browser-default"><form  action="{!! route('admin.job.search.jobDate') !!}"  method="get"> <input name="date"  type="date"><button class="button-submit">  <input type="hidden" name="hidden" value={{$user->id}} /> <i class="material-icons" style="color:#b9b9b9">search</i></button></form></td>
                     <td></td>
                     <td></td>
                 </tr> -->
        		     @foreach($jobs as $job)
        		     <tr>
        		     
        		          <td>{{ $job->job_title }}</td>
        		          <td>{{ $job->wages }}</td>
        		          <td>{{ $job->job_location }}</td>
                      <td>{{ $job->start_date }}</td>
        		          <td>	
        		          <a href='#'><i class="material-icons">edit</i></a>	          
        		           <a href='#'><i class="material-icons">delete</i></a>
        		         </td>
        		     </tr>
        		     @endforeach
        			 </tbody>
            </table>

              <div style="text-align:center">
	          {{ $jobs->render() }}
	         </div>
          </div>   


		</div>

@endsection

@section('after-script')
@php ($error = '')
@if ($errors->any())
        @foreach ($errors->all() as $err)
            {{ $error = $err }}<br/>    
        @endforeach
        <script>toastr["error"]("{!! $error !!}", "Error")</script>
@elseif (Session::get('flash_success'))
    
        @if(is_array(json_decode(Session::get('flash_success'),true)))
            {{ $success = implode('', Session::get('flash_success')->all(':message<br/>')) }}
        @else
            <script>toastr["success"]("{!! Session::get('flash_success') !!}", "Success")</script>
        @endif
    
@endif

@endsection