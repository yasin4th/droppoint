@extends('layouts.material')
@section('content')
{{-- {!! Breadcrumbs::renderIfExists('job') !!} --}}
<div class="row">
	<div class="col s12 m3">
		<div class="profile-sidebar">
			@include('includes.admin.sidemenu')
		</div>
	</div>
	<div class="col s12 m9">
		<div id="rootwizard1" class="form-wizard form-wizard-horizontal">
			<form class="form floating-label" method="POST" action="/admin/template/{{$temp->id}}/updateAll"  onsubmit="document.forms.edit.newtag.value = $('#select_key').val(); if(!validate(4)) {return false};  myButton.disabled = true; return true;" enctype="multipart/form-data" name="edit">
				<input type="hidden" name="newtag">
				<div class="form-wizard-nav">

					<div class="progress" style="width: 50%;"><div class="progress-bar progress-bar-primary"></div></div>

					<ul class="nav nav-justified nav-pills tabs" id="toptabs">
						<li class="tab active"><a href="#tab1" class="active"><span class="step">1</span> <span class="title">Details</span></a></li>
						<li class="tab"><a href="#tab2"><span class="step">2</span> <span class="title">Tags</span></a></li>
					</ul>
				</div>
				<h5>Post a Job-Template</h5>
				@include('includes.alert')
				{{ csrf_field() }}

				<div class="card">
					<div class="card-content create_card">

						<div id="tab1" class="tabContent">

							<div class="col s12 mrg-bot15">
								<h5 class="subtitle">Describe the Job-Template</h5>
							</div>
						<!-- 	<div class="col s7 mrg-bot60"></div> -->
							<div class="col s12">
								<b>Job Details</b>
							</div>
							<div class="{{ $errors->has('title') ? ' has-error' : '' }}">
								<div class="input-field col s12 m6 mrg-t30">
									<input type="text" id="title" name="title" class="validate" value="{{$temp->title}}" placeholder="Be simple & specific! Example: Fashion Retail Assistant" id="title" required >
									<label for="title">Job Title</label>
									@if ($errors->has('title'))
									<span class="help-block">
										<strong>{{ $errors->first('title') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="col s7"></div>
							<!-- <div class="input-field col s12 m6">
								<textarea name="about" id="aboutWork" class="materialize-textarea validate" placeholder="Example: I need someone who has great customer service skills to interact with our customers with a friendly, helpful attitude. You will also need to help with merchandising, housekeeping and other general duties." cols="30" rows="10" length="500" maxlength="500" required >{{ $temp->about }}</textarea>
								<label for="about">Briefly the work to be done</label>
								<span class="character-counter" style="float: right; font-size: 12px; height: 1px;">
								</span>
							</div> -->
							<div class="input-field col s12">
								<p class="mrg-bot10"><b>Roles and Responsibilities</b></p>
								<p class="text-muted">Provide the tasks, roles and responsibilities to help the applicant understand what tasks he/she will and will not perform. List the most important duties first and be sure to include specific information regarding frequency of tasks. Example: You will spend 20 percent of the day updating Daily Sales Reports.</p>
								<textarea class="textarea-hauto validate" id="requirements" name="requirements"  placeholder="Type Here" cols="30" rows="6" >{!! str_replace('<br/>','\n',$temp->requirements) !!}</textarea>
							</div>

							<div class="col s7 mrg-bot30"></div>
							<div class="col s12 m6 mrg-bot30">
								<b>Upload a picture to attract more candidates</b>
								<form action="#">
									<div class="file-field input-field">
										<!-- <div class="btn light-blue darken-2">
											<span>Upload</span>
											<input type="file" name="avatar" >
										</div>
										<div class="file-path-wrapper">
											<input class="file-path validate" type="text" value="" placeholder="Upload any image"  id="cover_picture">
										</div> -->
										<input type="file" id="dropify" name="avatar" data-max-file-size="3M" data-allowed-file-extensions="png jpg jpeg gif" value="{{ $temp->cover_picture }}"/>
										<div class="file-path-wrapper">
											<input class="file-path validate" type="text" value="{{ $temp->cover_picture }}" placeholder="Upload any image">
										</div>
									</div>
								</form>
							</div>

							<div class="col s12 mrg-bot30">
								<b>Filter to only Malaysian applicants?</b>
								<div class="row">
									<div class="col s12 m6">
										<p class="text-muted"></p>
										@php
										$nationality = DB::table('ref_nationality')->get();
										@endphp
										<select name="nationality">
											@foreach($nationality as $na)
											<option value="{{$na->value}}" {{($na->value == 'Malaysian' ? 'selected' : '')}}  >{{$na->value}}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>

							<div class="col s7 mrg-bot30"></div>
							<div class="col s12 mrg-bot30">
								<b>This job-Template should be </b>
								<p>
									<input name="status" type="radio" id="test1" value="1" {{($temp->status =='1')?'checked':''}}  />
									<label for="test1">Active</label>
								</p>
								<p>
									<input name="status" type="radio" id="test2" value="0" {{($temp->status =='0')?'checked':''}} />
									<label for="test2">Inactive</label>
								</p>
							</div>

						</div><!--end #tab1 -->
						<div id="tab2" class="tabContent mrg-l15">
							<div class="col s12 mrg-bot30">
								<h5 class="subtitle">Job Requirements</h5>
								<p class="text-muted">Specify the requirements you're looking for in a successful application. Applicants may still apply if they do not meet your preferences, but they will be clearly notified that they are at a disadvantage.</p>
							</div>
							<div class="col s12">
								<p>
									<input type="checkbox" id="skill4" name="skill4" />
									<label for="skill4">Minimum Education</label>
								</p>
								<div class="col s12 skillContent">
								@php
									$tags = DB::table('tags')->get();
								@endphp
								@foreach($tags as $tag)
									@if($tag->category == 'education')
										<p>
											<input type="checkbox" name="edu[]" id="edu{{$tag->id}}" {{(in_array($tag->id, $temp->tags))? 'checked': ''}} value="{{$tag->id}}" />
											<label for="edu{{$tag->id}}">{{$tag->name}}</label>
										</p>
									@endif
								@endforeach
									<!-- <p>
										<input type="checkbox" name="edu[]" id="edu2" value="15" />
										<label for="edu2">SPM</label>
									</p> -->
								</div>
							</div>
							<div class="col s12">
								<p>
									<input type="checkbox" id="skill5" name="skill5"/>
									<label for="skill5">Experience level</label>
								</p>
								<div class="col s12 skillContent">
								@foreach($tags as $tag)
								@if($tag->category == 'experience')
										<p>
											<input type="checkbox" name="exp[]" id="exp{{$tag->id}}" {{(in_array($tag->id, $temp->tags))? 'checked': ''}} value="{{$tag->id}}" />
											<label for="exp{{$tag->id}}" >{{$tag->name}}</label>
										</p>
								@endif
								@endforeach
								</div>
							</div>

							<div class="col s12">
								<p>
									<input type="checkbox" id="skill1" name="skill1" />
									<label for="skill1">Access to Transportation</label>
								</p>
								<div class="col s12 skillContent">
								@foreach($tags as $tag)
								@if($tag->category == 'transport')
									<p>
										<input type="checkbox" name="trans[]" id="trans{{$tag->id}}" {{(in_array($tag->id, $temp->tags))? 'checked': ''}} value="{{$tag->id}}" />
										<label for="trans{{$tag->id}}">{{$tag->name}}</label>
									</p>
								@endif
								@endforeach
								</div>
							</div>

							<div class="col s12">
								<p>
									<input type="checkbox" id="skill2" name="skill2"/>
									<label for="skill2">Language</label>
								</p>
								<div class="col s12 skillContent">
								@foreach($tags as $tag)
								@if($tag->category == 'language')
									<p>
										<input type="checkbox" name="lang[]" id="lang{{$tag->id}}" {{(in_array($tag->id, $temp->tags))? 'checked': ''}} value="{{$tag->id}}" />
										<label for="lang{{$tag->id}}">{{$tag->name}}</label>
									</p>
								@endif
								@endforeach
								</div>
							</div>

							<div class="col s12">
								<p>
									<input type="checkbox" id="skill3" name="skill3" />
									<label for="skill3">Certification and Licences</label>
								</p>
								<div class="col s12 skillContent">
								@foreach($tags as $tag)
								@if($tag->category == 'certification')
									<p>
										<input type="checkbox" name="cert[]" id="cert{{$tag->id}}" {{(in_array($tag->id, $temp->tags))? 'checked': ''}} value="{{$tag->id}}" />
										<label for="cert{{$tag->id}}">{{$tag->name}}</label>
									</p>
								@endif
								@endforeach
								</div>
							</div>

							<div class="col s12">
								<h5 class="subtitle">Tag your job</h5>
								<p class="text-muted">Tags are a great way for people to search for jobs that have a common topic</p>
							</div>
							<div class="col s12 mrg-bot30 mrgt-15" id="tagsDisplay">
								@foreach($tags as $tag)
									@if($tag->category == 'tag' && in_array($tag->id, $temp->tags))
										<div class='chip purple white-text'>
											{{$tag->name}}
											<i class="close material-icons">close</i>
										</div> 
									@endif
								@endforeach
								</div>

							<div class="col s12 mrg-bot30 mrgt-15">
								<a id="addtags" class="modal-trigger waves-effect waves-light btn grey lighten-1" href="#tag-modal">Add tags</a>
							</div>
						</div> <!--end #tab3 -->
						<div class="col s12 mrg-tb40">
							<button id="back" type="buttton" class="waves-effect waves-light btn light-blue darken-2">BACK</button>
							<!-- <button id="draft" type="buttton" class="waves-effect waves-light btn grey lighten-1">SAVE DRAFT</button> -->
							<button id="submit"  class="waves-effect waves-light btn light-blue darken-2 right" name="myButton">POST TEMPLATE</button>
							<button id="update" class="btn waves-effect waves-light btn light-blue darken-2 right " name="updateButton" type="submit">UPDATE TEMPLATE</a>
							<button id="next" type="buttton" class="waves-effect waves-light btn light-blue darken-2 right">NEXT</button>
						</div>
					</div>
				</div><!--end .card -->
			</form>
		</div><!--end #rootwizard -->
	</div>
</div>
<!-- Modal Structure -->
<div id="tag-modal" class="modal">
	<div class="modal-content">
		<h5>Add Tags</h5>
		<p>Tags may contain letters, numbers, colons, dashes and underscores.</p>
		<div class="modal-content">
			<select name="tags" multiple class="browser-default" id="select_key">
				@foreach(\App\Tag::where('category','tag')->get() as $tag)
				<option value="{{$tag->id}}">{{$tag->name}}</option>
				@endforeach
			</select>
		</div>
		<button id="add" type="buttton" class="modal-action modal-close waves-effect waves-light btn light-blue darken-2 tag-btn" onclick="document.getElementById('addtags').innerHTML='Edit Tags'">Add tags</button>
		<a href="#" class="modal-close"><i class="fa fa-times"></i></a>
	</div>

	<!-- <div class="modal-footer">
	<a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
</div> -->
</div>
@endsection

@section('scripts')

	<link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
	<script src="{{asset('js/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>

	<script>
		validate = function(index) {

		if(index > 1) {
			if($('[name=title]').val() == '') {
				Materialize.toast('Please Enter Title', 1000);
				setTimeout(function() {
					$('ul.tabs').tabs('select_tab', 'tab1');
				}, 10);
				return false;
			}
			// if($('[name=about]').val() == '') {
			// 	Materialize.toast('Please Enter About', 1000);
			// 	setTimeout(function() {
			// 		$('ul.tabs').tabs('select_tab', 'tab1');
			// 	}, 10);
			// 	return false;
			// }
			if($('[name=requirements]').val() == '') {
				Materialize.toast('Please Enter Roles and Responsibilities', 5000);
				setTimeout(function() {
					$('ul.tabs').tabs('select_tab', 'tab1');
				}, 10);
				return false;
			}

			if($('[name=start_date]').val() == '') {
				Materialize.toast('Please Enter Start Date', 1000);
				setTimeout(function() {
					$('ul.tabs').tabs('select_tab', 'tab1');
				}, 10);
				return false;
			}
			if($('[name=start_time]').val() == '') {
				Materialize.toast('Please Enter start_time', 1000);
				setTimeout(function() {
					$('ul.tabs').tabs('select_tab', 'tab1');
				}, 10);
				return false;
			}
			if($('[name=end_time]').val() == '') {
				Materialize.toast('Please Enter end_time', 1000);
				setTimeout(function() {
					$('ul.tabs').tabs('select_tab', 'tab1');
				}, 10);
				return false;
			}
		}

		if(index > 3) {
			if($('[name=street]').val() == '') {
				Materialize.toast('Please Enter Unit, Floor, Building', 1000);

				setTimeout(function() {
					// $('ul.tabs').tabs('select_tab', 'tab3');
					$('#maptabs').tabs('select_tab','test2');
				}, 100);
				return false;
			}
			if($('[name=loc_address]').val() == '') {
				Materialize.toast('Please Confirm Address', 1000);

				setTimeout(function() {
					// $('ul.tabs').tabs('select_tab', 'tab3');
					$('#maptabs').tabs('select_tab','test2');
				}, 100);
				return false;
			}
			if($('[name=loc_state]').val() == '') {
				Materialize.toast('Please Confirm State', 1000);

				setTimeout(function() {
					// $('ul.tabs').tabs('select_tab', 'tab2');
					$('#maptabs').tabs('select_tab','test2');
				}, 100);
				return false;
			}
			if($('[name=zipcode]').val() == '') {
				Materialize.toast('Please Confirm Postal/Zipcode', 1000);

				setTimeout(function() {
					// $('ul.tabs').tabs('select_tab', 'tab2');
					$('#maptabs').tabs('select_tab','test2');
				}, 100);
				return false;
			}
		}
		return true;
	}
	</script>

	<script>
	var input = document.getElementById('contact_email');
	var datalist = document.getElementById('data-email');

	tinymce.init({
		selector: '#',
		height: 200,
		menubar: false,
		elementpath: false,
		themes: "modern",
		plugins: [
			' advlist lists'
			/*'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code'*/
		],
		toolbar: 'bullist numlist',
		/*toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',*/
		setup: function(editor) {
			editor.on('change', function(e) {
				document.getElementById('requirements').innerHTML = editor.getContent();
			});
		}
	});

	$(document).ready (function(){
		$('.skillContent').hide();

		$('select').material_select();

		$('#back').hide();
		$('#submit').hide();
		$('#update').hide();
		$('.skillContent').hide();

		var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);

		$("select[name=tags]").on('change', function(e) {
			$('#add').text('SAVE TAGS');
		});

		var navListItems = $('.nav>.tab>a'), content = $('.tabContent');

		navListItems.click(function(e){

			e.preventDefault();

			var $target = $($(this).attr('href')),
				$item = $(this);
			// $target.find('input:eq(0)').focus();
			$item.parent().addClass('active');
			index = parseInt($(this).attr('href').substr(-1));

			// $('ul.tabs').tabs('select_tab', `tab${index}`);

			for (var i = 1; i <= 2; i++) {
				if(i < index)
				{
					$('[href=#tab'+i+']').parent().addClass('done');
				}
				else if (i == index)
				{
					$('[href=#tab'+i+']').parent().removeClass('done');
					// $('[href=#tab'+i+']').parent().removeClass('active');
				} else
				{
					$('[href=#tab'+i+']').parent().removeClass('done');
					$('[href=#tab'+i+']').parent().removeClass('active');
				}
			}
			if (index == 1) {
				$('#back').hide();
				$('#next').show();
				$('#submit').hide();
				$('#update').hide();
			} else {
				$('#next').hide();
				$('#back').show();
				$('#submit').hide();
				$('#update').show();
			}

			$('.progress-bar-primary').css('width',((index-1)*100)+'%');
		});

		// $('#back').click(function(e) {
		// 	e.preventDefault();
		// 	$c = $('.nav>.tab>a.active');
		// });

		$('#next').click(function(e) {
			// console.log('clicked next');
			e.preventDefault();
			$next = parseInt($('.nav>.tab>a.active').attr('href').substr(-1)) + 1;
			$(navListItems[1]).click();
			// $('[href=#tab1]').click();
			return;

			$('#submit').show();
			if($("#tab").attr('href') == '#tab2') {
				return;
			}
			// $('[href=#tab'+$next+']').click();
		});

		$('#back').click(function(e) {
			e.preventDefault();
			$('ul.tabs').tabs('select_tab','tab1');
			return;

		});

		$('[id^=skill]').on('change', function(e) {
			ob = $(this).parent().next().toggleClass('abcd');
			if(ob.hasClass('abcd')){
				ob.show(300);
			}
			else{
				ob.hide(300);
			}
		});

		/*
		$('a[href=#tab2]').click(function(e) {
			$('#tab1 ul.tabs li:first-child a').click();
		});
		$('a[href=#tab1]').click(function(e) {
			$('#submit').show();
			$('#next').hide();
		});*/

		$('select').material_select();
		$('select[name=tags]').select2({tags: true});
		$('select[name=tags]').on('change', function(e) {
			chips = $(this).val();
			html = '';
			if(chips.length > 0) {
				chips.forEach(function(chip){
					if(!isNaN(chip)) {
						c = $(`select[name=tags] option[value=${chip}]`).html();
						html += `<div class='chip purple white-text'>${c}<i class="close material-icons">close</i></div> `;
					} else {
						html += `<div class='chip purple white-text'>${chip}<i class="close material-icons">close</i></div> `;
					}

				})
			}
			$('#tagsDisplay').html(html);
		});
	});


	/*$(document).ready(function(){
		$('#update').click(function(){
			tag_ids = [];
			tags = $('[id^=edu]:checked');
			$.each(tags, function(i, tag) {
				if($(tag).val() != 'on')
				tag_ids.push($(tag).val())
			});

			tags = $('[id^=exp]:checked');
			$.each(tags, function(i, tag) {
				if($(tag).val() != 'on')
				tag_ids.push($(tag).val())
			});

			tags = $('[id^=cert]:checked');
			$.each(tags, function(i, tag) {
				if($(tag).val() != 'on')
				tag_ids.push($(tag).val())
			});

			tags = $('[id^=lang]:checked');
			$.each(tags, function(i, tag) {
				if($(tag).val() != 'on')
				tag_ids.push($(tag).val())
			});

			tags = $('[id^=trans]:checked');
			$.each(tags, function(i, tag) {
				if($(tag).val() != 'on')
				tag_ids.push($(tag).val())
			});

			tag_ids.push($("#character").val());

				var req = {};
				req._token  = '{{ csrf_token() }}';
				req.title = $('#title').val();
				req.aboutWork = $('#aboutWork').val();
				req.requirements = tinyMCE.get('requirements').getContent();
				req.tags = tag_ids.join(',');*/
				/*req.working_hours = $('.working_hours').val();
				req.salary = $('.salary').val();
				req.job_type = $('#job_type').val();
				req.status = $('#status').val();
				req.location = $('.location').val();
				req.description = $('textarea#description').val();
				console.log(req.character);
				req.contact_email= $('.email').val();
				req.contact_phone = $('.phone').val();
				req.contact_link = $('.link').val();*/
				/*$.ajax({
					type : 'post',
					url  : '/admin/template/{{$temp->id}}/updateAll',
					data : req,
				}).done(function(res){
					Materialize.toast(res.message,4000);
				});
		});

	});*/

	tinymce.init({
		selector: '#requirements',
		height: 300,
		menubar: false,
		// statusbar: false,
		elementpath: false,
		themes: "modern",
		plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code'
		],
		toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
		setup: function(editor) {
			editor.on('change', function(e) {
				var request = {};
				request._token  = '{{ csrf_token() }}';
				request.value = editor.getContent();

				$.ajax({
					'type': 'POST',
					'url':  "#",
					'data': request
				}).done(function(response) {

				});
			});
		}
	});
	</script>

<script>
	$( document ).ready(function() {
		$('#dropify').dropify({
			messages: {
				'default': '<p class="center">Drag and drop a file here or click</p>',
				'replace': '<p class="center">Drag and drop or click to replace</p>',
				'remove':  '<p class="center">Remove</p>'
			}
		});

		$('#skill5').prop('checked', true).change();
		$('#skill4').prop('checked', true).change();
		$('#skill1').prop('checked', true).change();
		$('#skill2').prop('checked', true).change();
		$('#skill3').prop('checked', true).change();
	});

</script>
@endsection
