@extends('layouts.angular-material')

@section('stylesheet')
	<link href="{{asset('js/materialize/plugins/jsgrid/css/jsgrid1.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
	<link href="{{asset('js/materialize/plugins/jsgrid/css/jsgrid-theme.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
@endsection

@section('content')

	<div class="row">

		<div class="col s12 m3">
			<div class="profile-sidebar">
				@include('includes.sidemenu')
			</div>
		</div>
		<div class="col s12 m9">
			<div class="card">
				<div class="card-content">
					<div class="row">
						<div class="col s12">
							<h5 class="header">Payments History</h5>
							<div class="payment_history">
								<table>
									<thead>
										<tr>
											<th data-field="id">ORDER ID</th>
											<th data-field="name">PAYMENT DATE</th>
											<th data-field="price">PLAN</th>
											<th data-field="price">PAYMENT VIA</th>
											<th data-field="price">AMOUNT</th>
											<th data-field="price"></th>
										</tr>
									</thead>

									<tbody>
										@foreach(Auth::user()->company->orders as $order)
										<tr>
											<td>{{$order->id}}</td>
											<td>{{ $order->payment_date->format('d M, Y') }}</td>
											<td>{{ (floatval($order->amount) > 0) ? 'Paid' : 'Free'}} ({{$order->plan}})</td>
											<td>{{$order->payment_via}}</td>
											<td>$ {{$order->amount}}</td>
											<td>
												<i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
								<!-- <div class="col s12 m6 mrg-tb15">
									<p>Showing entries</p>
								</div> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

</div>


	<!-- Modal Structure -->
	<div id="manage" class="modal" ng-controller="tableCtrl">
		<div class="modal-content">
			<h5>Manage Credits</h5>
			<form action="">
				<div class="row margin">
					<div class="input-field col s12">
						<select name="badge" id="badge" class="browser-default" ng-model="to">
							<option value="" disabled selected>-Choose Badge-</option>

						</select>
					</div>
				</div>
				<div class="row margin">
					<div class="input-field col s12">

							Amounts to Credit :

					</div>
				</div>

			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="waves-effect waves-light btn light-blue darken-2 right"  ng-click="submit()">Submit <i class="mdi-content-send right"></i></button>
		</div>
	</div>
@endsection
