@extends('layouts.material')

@section('content')

<div class="row">

	<div class="col s12 m3">
		<div class="profile-sidebar">
			@include('includes.sidemenu')
		</div>
	</div>
	<div class="col s12 m9">
		<div class="row">
			<div class="col s12">
				<div class="col s12 m12">
					<div class="card">
						<div class="card-content">
							<h5 class="header">Summary Information</h5>
							<ul id="projects-collection" class="collection purchase-div">
								<li class="collection-item avatar left_credit">
									<img src="/images/credit-card.svg" width="50px" class="responsive-img">
									<!-- <i class="mdi-file-folder circle light-blue"></i> -->
									<span class="collection-header">Credits</span>
									<p class="left-credits">{{Auth::user()->levels->credits}} Credits Left</p>
								</li>
								{{--@if(Auth::user()->levels->credits < 3)--}}
								<li class="collection-item avatar alert_box">
									<div class="alert">
										<p>Your prepaid balance is running low. Please purchase more before they are all used</p>
									</div>
									<a href="/cmp/package" class="waves-effect waves-light btn bg-green">Purchase more</a>
								</li>
								{{--@endif--}}
							</ul>
							@if(count(Auth::user()->received) > 0)
							@php
								$last = Auth::user()->received->last();
								$lastcredits = preg_replace('/\D+/', '', $last->description);
							@endphp
							<table class="summary-table">
								<thead>
									<tr>
										<th data-field="id">Issued</th>
										<th data-field="name">Purchased Credits</th>
										<th data-field="price">Credits Balance</th>
										<th data-field="price">Expires</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $last->added_date)->format('d M Y') }}</td>
										<td>{{$lastcredits}}</td>
										<td>{{Auth::user()->levels->credits}}</td>
										<td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $last->added_date)->addMonths(12)->format('d M Y') }}</td>
									</tr>
								</tbody>
							</table>
							@endif
						</div>
					</div>
					<div class="apply-coupon">
						<div class="card">
							<div class="card-content">
								<h6 class="header">Apply Coupon</h6>
								<div class="row">
								<div class="col s11 offset-s2">
								<!-- <div class="coupon-alert">
									<p>nvbjgnbjg</p>
									<span>dhgcvdjghcv</span>
								</div> -->
								</div>
									<div class="col s11 offset-s1">
										<form action="" method="POST">
											{{ csrf_field() }}
											<table>
												<tr>
													<td colspan="2">
													@if(isset($status) && $status=='FAILED')
														<div id="card-alert" class="red lighten-5">
															<div class="card-content red-text">
																<p>{!!$message!!}</p>
															</div>
														</div>
													@elseif(isset($status) && $status=='SUCCESS')
														<div id="card-alert" class="green lighten-5">
															<div class="card-content green-text">
																<p>{!!$message!!}</p>
															</div>
														</div>
													@endif

														<div id="msg" class="red lighten-5" style="display: none;">
															<div class="card-content" green-text">
																<p>Whoops!There were some problems with your input.
																 The code field is required.
																 </p>
															</div>
														</div>

													</td>
												</tr>
												<tr>
													<td style="width:25%">
														<p>Coupon Code</p>
													</td>
													<td style="">
														<input type="text" name="coupon" id="coupon_text">
													</td>
												</tr>
													<tr>
														<td></td>
														<td>
															<button type="submit" class="waves-effect waves-light btn coupon-btn bg-green" onclick="return valid();">Apply Coupon</button>
														</td>
													</tr>
												</tr>
											</table>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<h6 class="recnt-trns">Recent Transactions</h6>
					<div class="card">
						<div class="card-content">
							<ul id="projects-collection" class="collection">
								@foreach(Auth::user()->achievements->take(25) as $a)
								<li class="collection-item">
									<div class="row">
										<div class="col s6">
											<p class="collections-title">{{$a->description}}</p>
										</div>
										<div class="col s3">
											<span class="task-cat {{ str_contains($a->description, 'debited') ? 'yellow darken-4' : 'teal'}}">{{ str_contains($a->description, 'deducted') ? 'Used' : 'Credits'}}</span>
										</div>
										<div class="col s3">
											<p>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $a->added_date)->format('d M Y') }}</p>
										</div>
									</div>
								</li>
								@endforeach

							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection
@section('scripts')
<script type="text/javascript">
	function valid()
	{
		var v=$("#coupon_text").val();
		if(v=="")
		{	$("#msg").show();
			return false;
		}
		else
		{
			return true;
		}
	}
</script>
@endsection