@extends('layouts.material')

@section('content')

<div class="row">
	<div class="col s12">
		<div class="row">
			<div class="col s12 m3">
				@include('includes.sidemenu')
			</div>
			<div class="col s12 m9">
				@include('includes.alert')

				<h5>Upload logo</h5>
				<div class="row">
					<div class="col s12">
						<form action="{{ route('cmp.profile.logo') }}" method="POST" enctype="multipart/form-data">
							{{ csrf_field() }}
							<input type="file" id="dropify" name="logo" data-max-file-size="3M" data-allowed-file-extensions="png jpg jpeg gif" required>
							<button type="submit" class="waves-effect waves-light btn light-blue darken-2">SAVE</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('scripts')

<script>
$( document ).ready(function() {
	$('#dropify').dropify({
		messages: {
			'default': '<p class="center">Drag and drop a file here or click</p>',
			'replace': '<p class="center">Drag and drop or click to replace</p>',
			'remove':  '<p class="center">Remove</p>'
		}
	});
});
</script>

@endsection
