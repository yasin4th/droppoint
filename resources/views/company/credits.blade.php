@extends('layouts.angular-material')

@section('stylesheet')
	<link href="{{asset('js/materialize/plugins/jsgrid/css/jsgrid1.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
	<link href="{{asset('js/materialize/plugins/jsgrid/css/jsgrid-theme.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
@endsection

@section('content')

	<div class="row">

		<div class="col s12 m3">
			<div class="profile-sidebar">
				@include('includes.sidemenu')
			</div>
		</div>
		<div class="col m9">
			<div class="card">
				<div class="card-content">
					<div class="row">
						<div class="col s12">
							<h5 class="header">Transaction History</h5>
							{{-- <a href="#manage" class="modal-trigger waves-effect waves-light btn light-blue darken-2 right mrg-top-42"><i class="mdi-action-dashboard"></i> MANAGE</a> --}}
							{{-- <ul id="projects-collection" class="collection">
								<li class="collection-item avatar">
									<i class="mdi-file-folder circle light-blue"></i>
									<span class="collection-header">Credits</span>
									<p>{{($user->levels) ? $user->levels->credits : '0' }} Credits Left</p>
								</li>
							</ul> --}}
							<div class="jsgrid-grid-body">
								<table class="jsgrid-table">
									<thead>
										<tr class="jsgrid-header-row">
											<th>Type</th>
											<th>Description</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										@foreach($achievements as $a)
										<tr class="jsgrid-row">
											<td>{{$a->transaction}}</td>
											<td>{{$a->description}}</td>
											<td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $a->added_date)->format('d M Y') }}</td>
										</tr>
										@endforeach
										{{-- <tr class="jsgrid-row">
											<td>Credits</td>
											<td>The Great Reward earns 50 rewards</td>
											<td>07 Dec 2016</td>
										</tr>
										<tr class="jsgrid-row">
											<td>Credits</td>
											<td>The Great Reward earns 50 rewards</td>
											<td>07 Dec 2016</td>
										</tr>
										<tr class="jsgrid-row">
											<td>Credits</td>
											<td>The Great Reward earns 50 rewards</td>
											<td>07 Dec 2016</td>
										</tr> --}}
									</tbody>
								</table>
								<div class="col s12 m6 mrg-tb15">
									<p>Showing {{ ($achievements->firstItem()) ?:0 }} to {{ $achievements->lastItem()?:0 }} of {{ $achievements->total() }} entries</p>
								</div>
								<div class="col s12 m6 mrg-tb15">
									<div class="right">
										{{ $achievements->links() }}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>


	<!-- Modal Structure -->
	<div id="manage" class="modal" ng-controller="tableCtrl">
		<div class="modal-content">
			<h5>Manage Credits</h5>
			<form action="">
				<div class="row margin">
					<div class="input-field col s12">
						<select name="badge" id="badge" class="browser-default" ng-model="to">
							<option value="" disabled selected>-Choose Badge-</option>
							@foreach($badges as $b)
							<option value="{{$b->credits}}" data-id="{{$b->id}}"
							@if ($loop->first)
								selected
							@endif
							>{{$b->title}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="row margin">
					<div class="input-field col s12">
						@verbatim
							Amounts to Credit : {{to}}
						@endverbatim
					</div>
				</div>
				{{-- <div class="row margin">
					<div class="input-field col s12">
						<input type="text" name="company" class="validate">
						<label for="company" class="">Company</label>
					</div>
				</div> --}}
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="waves-effect waves-light btn light-blue darken-2 right"  ng-click="submit()">Submit <i class="mdi-content-send right"></i></button>
		</div>
	</div>
@endsection

@section('scripts')
	<script>
		var app = angular.module('myapp', []);
		app.controller('tableCtrl', function($scope, $http) {
			$scope.submit = function()
			{
				id = $('option[value='+$scope.to+']').attr('data-id');
				if(id != '') {

				$http.get('{{ route('admin.users.credits', $user->id) }}/'+id)
					.then(function(response){
						location.reload();
						Materialize.toast(response.data.message, 400);
					});
				}
			}
		});
	</script>
@endsection