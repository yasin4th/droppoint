@extends('layouts.material')

@section('content')

<div class="row">
    <div class="col s6 offset-s3">
        <div class="success-div">
            <h5 class="center">Payment has been successful</h5>
            <p class="center">Redirect to dashboard</p>
            <a href="/dashboard" class="waves-effect waves-light btn bg-green right">Go back</a>
        </div>
    </div>
</div>

@endsection