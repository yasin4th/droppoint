@extends('layouts.material')

@section('content')

<div id="profile-page" class="section">
	<!-- profile-page-header -->
	<div id="profile-page-header" class="card">
		<div class="card-image waves-effect waves-block waves-light">
			<img class="activator" src="/images/profile-banner.png" alt="user background">
		</div>
		<figure class="card-profile-image">
			<img src="/images/avatar.jpg" alt="profile image" class="circle z-depth-2 responsive-img activator">
		</figure>
		<div class="card-content">
			<div class="row">
				<div class="col s3 offset-s2">
					<h4 class="card-title grey-text text-darken-4">{{ Auth::user()->name }}</h4>
					<p class="medium-small grey-text">{{ Auth::user()->role_name }}</p>
				</div>
				<div class="col s2 center-align">
					<h4 class="card-title grey-text text-darken-4">10+</h4>
					<p class="medium-small grey-text">Jobs</p>
				</div>
				<div class="col s2 center-align">
					<h4 class="card-title grey-text text-darken-4">6</h4>
					<p class="medium-small grey-text">Completed Projects</p>
				</div>
				<div class="col s2 center-align">
					<h4 class="card-title grey-text text-darken-4">$ 1,253,000</h4>
					<p class="medium-small grey-text">Busness Profit</p>
				</div>
				<div class="col s1 right-align">
					<a class="btn-floating activator waves-effect waves-light darken-2 right">
						<i class="mdi-action-perm-identity"></i>
					</a>
				</div>
			</div>
		</div>
		<div class="card-reveal" style="display: none; transform: translateY(0px);">
			<p>
				<span class="card-title grey-text text-darken-4">{{ Auth::user()->name }} <i class="mdi-navigation-close right"></i></span>
				<span><i class="mdi-action-perm-identity cyan-text text-darken-2"></i> {{ Auth::user()->role_name }}</span>
			</p>

			<p>{{ Auth::user()->company->about }}</p>

			<p><i class="mdi-action-perm-phone-msg cyan-text text-darken-2"></i> +1 (612) 222 8989</p>
			<p><i class="mdi-communication-email cyan-text text-darken-2"></i> {{ Auth::user()->email }}</p>
			<p><i class="mdi-social-cake cyan-text text-darken-2"></i> 18th June 1990</p>
			<p><i class="mdi-device-airplanemode-on cyan-text text-darken-2"></i> BAR - AUS</p>
		</div>
	</div>
	<!--/ profile-page-header -->

	<!-- profile-page-content -->
	<div id="profile-page-content" class="row">
		<!-- profile-page-sidebar-->
		<div id="profile-page-sidebar" class="col s12 m4">
			<!-- Profile About  -->
			<div class="card light-blue">
				<div class="card-content white-text">
					<span class="card-title">About Me!</span>
					<p>{{ Auth::user()->company->about}}</p>
				</div>
			</div>
			<!-- Profile About  -->

			<!-- Profile About Details  -->
			<ul id="profile-page-about-details" class="collection z-depth-1">
				<li class="collection-item">
					<div class="row">
						<div class="col s5 grey-text darken-1"><i class="mdi-action-wallet-travel"></i> Project</div>
						<div class="col s7 grey-text text-darken-4 right-align">ABC Name</div>
					</div>
				</li>
				<li class="collection-item">
					<div class="row">
						<div class="col s5 grey-text darken-1"><i class="mdi-social-poll"></i> Skills</div>
						<div class="col s7 grey-text text-darken-4 right-align">HTML, CSS</div>
					</div>
				</li>
				<li class="collection-item">
					<div class="row">
						<div class="col s5 grey-text darken-1"><i class="mdi-social-domain"></i> Lives in</div>
						<div class="col s7 grey-text text-darken-4 right-align">NY, USA</div>
					</div>
				</li>
				<li class="collection-item">
					<div class="row">
						<div class="col s5 grey-text darken-1"><i class="mdi-social-cake"></i> Birth date</div>
						<div class="col s7 grey-text text-darken-4 right-align">18th June, 1991</div>
					</div>
				</li>
			</ul>
			<!--/ Profile About Details  -->

		</div>
		<!-- profile-page-sidebar-->

		<!-- profile-page-wall -->
		<div id="profile-page-wall" class="col s12 m8">
			<!-- profile-page-wall-share -->
			<div id="profile-page-wall-share" class="row">
				<div class="col s12">
					<ul class="tabs tab-profile z-depth-1 light-blue" style="width: 100%;">
						<li class="tab col s3"><a class="white-text waves-effect waves-light active" href="#UpdateStatus"><i class="mdi-editor-border-color"></i> Update Status</a>
						</li>
						<li class="tab col s3"><a class="white-text waves-effect waves-light" href="#AddPhotos"><i class="mdi-image-camera-alt"></i> Add Photos</a>
						</li>
						<li class="tab col s3"><a class="white-text waves-effect waves-light" href="#CreateAlbum"><i class="mdi-image-photo-album"></i> Create Album</a>
						</li>
						<div class="indicator" style="right: 434px; left: 0px;"></div></ul>
						<!-- UpdateStatus-->
						<div id="UpdateStatus" class="tab-content col s12  grey lighten-4">
							<div class="row">
								<div class="col s2">
									<img src="/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image-post">
								</div>
								<div class="input-field col s10">
									<textarea id="textarea" row="2" class="materialize-textarea"></textarea>
									<label for="textarea" class="">What's on your mind?</label>
								</div>
							</div>
							<div class="row">
								<div class="col s12 m6 share-icons">
									<a href="#"><i class="mdi-image-camera-alt"></i></a>
									<a href="#"><i class="mdi-action-account-circle"></i></a>
									<a href="#"><i class="mdi-hardware-keyboard-alt"></i></a>
									<a href="#"><i class="mdi-communication-location-on"></i></a>
								</div>
								<div class="col s12 m6 right-align">
									<!-- Dropdown Trigger -->
									<a class="dropdown-button btn" href="#" data-activates="profliePost"><i class="mdi-action-language"></i> Public</a><ul id="profliePost" class="dropdown-content">
									<li><a href="#!"><i class="mdi-action-language"></i> Public</a></li>
									<li><a href="#!"><i class="mdi-action-face-unlock"></i> Friends</a></li>
									<li><a href="#!"><i class="mdi-action-lock-outline"></i> Only Me</a></li>
								</ul>

								<!-- Dropdown Structure -->


								<a class="waves-effect waves-light btn"><i class="mdi-maps-rate-review left"></i>Post</a>
							</div>
						</div>
					</div>
					<!-- AddPhotos -->
					<div id="AddPhotos" class="tab-content col s12  grey lighten-4" style="display: none;">
						<div class="row">
							<div class="col s2">
								<img src="images/avatar.jpg" alt="" class="circle responsive-img valign profile-image-post">
							</div>
							<div class="input-field col s10">
								<textarea id="textarea" row="2" class="materialize-textarea"></textarea>
								<label for="textarea" class="">Share your favorites photos!</label>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m6 share-icons">
								<a href="#"><i class="mdi-image-camera-alt"></i></a>
								<a href="#"><i class="mdi-action-account-circle"></i></a>
								<a href="#"><i class="mdi-hardware-keyboard-alt"></i></a>
								<a href="#"><i class="mdi-communication-location-on"></i></a>
							</div>
							<div class="col s12 m6 right-align">
								<!-- Dropdown Trigger -->
								<a class="dropdown-button btn" href="#" data-activates="profliePost2"><i class="mdi-action-language"></i> Public</a><ul id="profliePost2" class="dropdown-content">
								<li><a href="#!"><i class="mdi-action-language"></i> Public</a></li>
								<li><a href="#!"><i class="mdi-action-face-unlock"></i> Friends</a></li>
								<li><a href="#!"><i class="mdi-action-lock-outline"></i> Only Me</a></li>
							</ul>

							<!-- Dropdown Structure -->


							<a class="waves-effect waves-light btn"><i class="mdi-maps-rate-review left"></i>Post</a>
						</div>
					</div>
				</div>
				<!-- CreateAlbum -->
				<div id="CreateAlbum" class="tab-content col s12  grey lighten-4" style="display: none;">
					<div class="row">
						<div class="col s2">
							<img src="images/avatar.jpg" alt="" class="circle responsive-img valign profile-image-post">
						</div>
						<div class="input-field col s10">
							<textarea id="textarea" row="2" class="materialize-textarea"></textarea>
							<label for="textarea" class="">Create awesome album.</label>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m6 share-icons">
							<a href="#"><i class="mdi-image-camera-alt"></i></a>
							<a href="#"><i class="mdi-action-account-circle"></i></a>
							<a href="#"><i class="mdi-hardware-keyboard-alt"></i></a>
							<a href="#"><i class="mdi-communication-location-on"></i></a>
						</div>
						<div class="col s12 m6 right-align">
							<!-- Dropdown Trigger -->
							<a class="dropdown-button btn" href="#" data-activates="profliePost3"><i class="mdi-action-language"></i> Public</a><ul id="profliePost3" class="dropdown-content">
							<li><a href="#!"><i class="mdi-action-language"></i> Public</a></li>
							<li><a href="#!"><i class="mdi-action-face-unlock"></i> Friends</a></li>
							<li><a href="#!"><i class="mdi-action-lock-outline"></i> Only Me</a></li>
						</ul>

						<!-- Dropdown Structure -->


						<a class="waves-effect waves-light btn"><i class="mdi-maps-rate-review left"></i>Post</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/ profile-page-wall-share -->

</div>
<!--/ profile-page-wall -->

</div>
</div>
@endsection
