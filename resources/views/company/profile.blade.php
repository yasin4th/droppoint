@extends('layouts.material')

@section('content')

<div class="row">
	<div class="col s12">
		<div class="row">
			<div class="col s12 m3">
				@include('includes.sidemenu')
			</div>
			<div class="col s12 m9">
				@include('includes.alert')

				<h5>About your company</h5>
				<p>This information will be seen by applicants when viewing associated jobs.</p>
				<form class="" role="form" method="POST" action="{{ route('cmp.profile') }}">
					{{ csrf_field() }}
					{{-- <div class="row">
							<div class="input-field col s12">
								<textarea name="description" class="materialize-textarea validate" cols="30" rows="10">{{ Request::old('description') ?: Auth::user()->company->description }}</textarea> !
								<label for="description">Sub-description</label>
							</div>
					</div> --}}
					<div class="row">
						<div class="input-field col s6">
							<select name="type">
								<option value="" disabled>Choose Company Type</option>
								<option value="Individual" {{ (Auth::user()->company->type == 'Individual') ? 'selected' : '' }}>Individual (unregistered business)</option>
								<option value="SoleProprietorship" {{ (Auth::user()->company->type == 'soleProprietorship') ? 'selected' : '' }}>Sole Proprietorship (personal business)</option>
								<option value="Partnership" {{ (Auth::user()->company->type == 'Partnership') ? 'selected' : '' }}>Partnership</option>
								<option value="Private Company" {{ (Auth::user()->company->type == 'Private Company') ? 'selected' : '' }}>Private Company</option>
								<option value="Public Company" {{ (Auth::user()->company->type == 'Public Company') ? 'selected' : '' }}>Public Company</option>
								<option value="NonProfit Organization" {{ (Auth::user()->company->type == 'NonProfit Organization') ? 'selected' : '' }}>NonProfit Organization</option>
								<option value="Educational Institution" {{ (Auth::user()->company->type == 'Educational Institution') ? 'selected' : '' }}>Educational Institution</option>
								<option value="Government Entity" {{ (Auth::user()->company->type == 'Government Entity') ? 'selected' : '' }}>Government Entity</option><option value="Corporation" {{ (Auth::user()->company->type == 'Corporation') ? 'selected' : '' }}>Corporation</option>
							</select>
							<label>Company Type</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12{{ $errors->has('name') ? ' has-error' : '' }}">
							<input id="name" name="name" type="text" class="validate" value="{{ Request::old('name') ?: Auth::user()->name }}">
							<label for="name">Company Name</label>
							@if ($errors->has('name'))
							<span class="help-block">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<!--<div class="row">
						<div class="input-field col s12{{ $errors->has('address') ? ' has-error' : '' }}">
						  <input id="address" name="address" type="text" class="validate" value="{{ Request::old('address') ?: Auth::user()->company->address }}">
						  <label for="address">Address</label>
							@if ($errors->has('address'))
							<span class="help-block">
								<strong>{{ $errors->first('address') }}</strong>
							</span>
							@endif
						</div>
					</div>!-->
					<div class="row">
						<div class="input-field col s12{{ $errors->has('website') ? ' has-error' : '' }}">
						  <input id="website" name="website" type="text" class="validate" value="{{ Request::old('website') ?: Auth::user()->company->website }}">
						  <label for="website">Website</label>
							@if ($errors->has('website'))
							<span class="help-block">
								<strong>{{ $errors->first('website') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="input-field col s6">
							<select name="industry" class="selectpicker">
								<option value="" disabled>-Please Select-</option>
								@php
									$industries = DB::table('ref_industry_list')->get();
								@endphp

								@foreach($industries as $industry)
									<option value="{{$industry->id}}" {{ (Auth::user()->company->industry == $industry->id) ? 'selected' : '' }}>{{$industry->name}}</option>
								@endforeach
							</select>
							<label>Industry</label>
						</div>
						<div class="input-field col s6">
							<select class="selectpicker" name="size">
								<option value="" disabled>-Please Select-</option>
								<option {{ (Auth::user()->company->size == '1-9') ? 'selected' : '' }} value="1-9">1-9 employees</option>
								<option {{ (Auth::user()->company->size == '10-49') ? 'selected' : '' }} value="10-49">10-49 employees</option>
								<option {{ (Auth::user()->company->size == '50-249') ? 'selected' : '' }} value="50-249">50-249 employees</option>
								<option {{ (Auth::user()->company->size == '250+') ? 'selected' : '' }} value="250+">250+ employees</option>
							</select>
							<label>Company Size</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12{{ $errors->has('tagline') ? ' has-error' : '' }}">
							<textarea name="tagline" class="materialize-textarea validate" cols="30" rows="10" length="140" maxlength="140">{{ Request::old('tagline') ?: Auth::user()->company->tagline }}</textarea>
							<label for="tagline">Elevator Pitch</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<input id="building" name="building" type="text" class="validate"  value="{{ Auth::user()->company->building }}">
							<label for="building">Unit, Floor, Building</label>
							@if ($errors->has('building'))
							<span class="help-block">
								<strong>{{ $errors->first('building') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12{{ $errors->has('address') ? ' has-error' : '' }}">
						  <input id="address" name="address" type="text" class="validate" value="{{ Request::old('address') ?: Auth::user()->company->address }}">
						  <label for="address">Address</label>
							@if ($errors->has('address'))
							<span class="help-block">
								<strong>{{ $errors->first('address') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="input-field col s6">
							<select name="country" class="">
								<option value="" disabled selected>-Please Select-</option>
                                <option value="MY" data-name="Malaysia" {{(Auth::user()->company->country == 'MY')?'selected':''}}>Malaysia</option>
                                <option value="ID" data-name="Indonesia" {{(Auth::user()->company->country == 'ID')?'selected':''}}>Indonesia</option>
                                <option value="SG" data-name="Singapore" {{(Auth::user()->company->country == 'SG')?'selected':''}}>Singapore</option>
								@php
									$countries = DB::table('ref_country_list')->whereNotIn('name', ['Malaysia', 'Indonesia', 'Singapore'])->orderBy('name')->get();
								@endphp
								@foreach($countries as $country)
									<option value="{{$country->code}}" data-name="{{$country->name}}" {{(Auth::user()->company->country == $country->code)?'selected':''}}>{{$country->name}}</option>
								@endforeach
							</select>
							<label>Country</label>
						</div>
						<div class="input-field col s6{{ $errors->has('city') ? ' has-error' : '' 	}} ">
								<input id="city" name="city" type="text" class="validate" value="{{ Request::old('city') ?: Auth::user()->company->city }}">
								<label>City</label>
								@if ($errors->has('city'))
								<span class="help-block">
									<strong>{{ $errors->first('city') }}</strong>
								</span>
								@endif
						</div>
					</div>
					<div class="row">
						<div class="input-field col s6{{ $errors->has('zipcode') ? ' has-error' : '' }}">
							<input id="zipcode" name="zipcode" type="text" class="validate" value="{{ Request::old('zipcode') ?: Auth::user()->company->zipcode }}">
							<label>Postal / ZipCode</label>
							@if ($errors->has('zipcode'))
							<span class="help-block">
								<strong>{{ $errors->first('zipcode') }}</strong>
							</span>
							@endif
						</div>

						<div class="input-field col s6">
							@php
									$states = DB::table('ref_state_list')->where('country_code',Auth::user()->company->country)->orderBy('name')->get();
							@endphp
							<select name="state" class="selectpicker">
							<option value="" disabled="">-Choose-</option>
							@foreach($states as $state )
							<option value="{{$state->name}}" date-name="{{$state->name}}" {{(Auth::user()->company->state==$state->name)?'selected':''}}>{{$state->name}}</option>
							@endforeach
							</select>
							<label>State</label>
							<span class="help-block"></span>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s6{{ $errors->has('phone') ? ' has-error' : '' }}">
							<input id="phone" name="phone" type="text" class="validate" value="{{ Request::old('phone') ?: Auth::user()->company->phone }}">
							<label>Phone</label>
							@if ($errors->has('phone'))
							<span class="help-block">
								<strong>{{ $errors->first('phone') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<button type="submit" class="waves-effect waves-light btn light-blue darken-2">SAVE</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
	$(document).ready(function(){

		$("#address").on("focus", geolocate)
	});

	var autocomplete;

	function initAutocomplete() {
		autocomplete = new google.maps.places.Autocomplete((document.getElementById('address')), {types: ['geocode']});
		//autocomplete.addListener('place_changed', fillInAddress);
	}

	/*function fillInAddress() {
		var place = autocomplete.getPlace();
		$("#latitude").val(place.geometry.address.lat());
		$("#longitude").val(place.geometry.address.lng());
	}*/

	function geolocate() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var geolocation = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
				var circle = new google.maps.Circle({
					center: geolocation,
					radius: position.coords.accuracy
				});
				autocomplete.setBounds(circle.getBounds());
			});
		}
	}

		$(document).ready(function(){
			$('select[name=country]').change(function(e) {
				var req={};
				req.code = $(this).val();
				req._token='{{csrf_token()}}';
				$.ajax({
				type : 'post',
				url  : '/cmp/profile/state',
				data : req,
				}).done(function(res) {
					$('select[name=state]').html(res);
					$('select').material_select();
				});
			});
		});


</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBA49o4nTKm6sEzirzmdhv_aQcGWQ7k0-I&libraries=places&callback=initAutocomplete"></script>
@endsection