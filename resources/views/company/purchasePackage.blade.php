@extends('layouts.material')

@section('content')

	<div class="row mrgt-15">

		<!-- <div class="col s12 m3">
			<div class="profile-sidebar">
				@include('includes.sidemenu')
			</div>
		</div> -->
		<div class="col s12 m8 offset-m2">
			<div class="row">
				<form action="{{route('cmp.package')}}" name="packageForm" method="POST">
				{{csrf_field()}}
					<div class="col s12 m12">
						<div class="card select-package">
							<div class="card-content">
								<h6>Select Package</h6>
								<div class="row">
									<div class="col s9 offset-s3">
											<p>
												<input name="credits" type="radio" id="test1" value="10" checked="checked"/>
												<label for="test1">5 Credits ($10)</label>
											</p>
											<p>
												<input name="credits" type="radio" id="test2" value="20"/>
												<label for="test2">10 Credits ($20)</label>
											</p>
											<p>
												<input name="credits" type="radio" id="test3" value="30"/>
												<label for="test3">25 Credits ($30 – Save $20!)</label>
											</p>
											<p>
												<input name="credits" type="radio" id="test4" value="40"/>
												<label for="test4">50 Credits ($40 – Save $60!)</label>
											</p>
										<br>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>

				<div class="col s12 m12">
					<div class="card extra-billing">
						<div class="card-content">
							<h6>Extra Billing Information</h6>
							<div class="row">
								<div class="col s9 offset-s2">
									<form action="/cmp/gst" method="POST">
										{{csrf_field()}}
										<table style="margin-left:-5%;">
											<tr>
												<td style="">
												</td>
												<td style="" class="">
													<div class="billing-alert">
														<p>If you need to add specific contact or tax information to your receipts, like your full business name, GST number, or address of record, add it here. We'll make sure it shows up on every receipt.</p>
													</div>
												</td>
											</tr>
											<tr>
												<td style="width:17%">
													<p>Text</p>
												</td>
												<td style="">
													<textarea name="gst_no">{{Auth::user()->company->gst_info}}</textarea>
												</td>
											</tr>
											<tr>
												<td style="">
													<p>GST No.</p>
												</td>
												<td style="">
													<input type="text" name="gst_info" value="{{Auth::user()->company->gst_info}}">
												</td>
											</tr>
											<tr>
												<td></td>
												<td>
													<button type="submit" class="waves-effect waves-light btn billing-btn green darken-1">Save billing information</button>
												</td>
											</tr>
										</table>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col s12 m12">
					<div class="card payment-method">
						<!-- <div class="card-content"> -->
						<div class="col s12 m12">
							<h6 class="hed-method">Select Payment Method</h6>
						</div>
							<!-- <div class="row"> -->
								<div class="">
									<ul class="tabs">
										<li class="tab tab1">
											<a href="#tab1">
												<img src="/uploads/payments/visa.png" alt="" class="responsive-img">
												<img src="/uploads/payments/master.png" alt="" class="responsive-img">
											</a>
										</li>
										<li class="tab tab2">
											<a href="#tab2">
												<img src="/uploads/payments/paypal-1.png" alt="" class="responsive-img paypal-img" style="">
											</a>
										</li>
										<li class="tab tab3">
											<a href="#tab3" style="padding-top: 15px;">Bank Deposit</a>
										</li>
									</ul>
								</div>
							<!-- </div> -->
							<div class="row">
								<div id="tab1" class="col s12">
									<form action="" method="POST">
										<ul>
											<li>
												<label>Name on Card</label>
												<p>
													<input type="text" name="firstname" value="" placeholder="Name on Card">
													<i class="fa fa-user"></i>
												</p>
											</li>
											<li>
												<label>Card Number</label>
												<p>
													<input type="text" name="lastname" value="" placeholder="#### - #### - #### - ####">
													<i class="fa fa-credit-card"></i>
												</p>
											</li>
											<li>
												<label>Expires On</label>
												<p>
													<input type="text" name="lastname" value="" placeholder="MM / YY">
													<i class="fa fa-calendar-o"></i>
												</p>
											</li>
											<li>
												<label>Security Code</label>
												<p>
													<input type="text" name="lastname" value="" placeholder="CVC">
													<i class="fa fa-lock"></i>
													<i class="fa fa-question-circle"></i>
												</p>
											</li>
											<li>
												<label>Country</label>
												<p>
													<input type="text" name="lastname" value="" placeholder="Country">
													<i class="fa fa-globe"></i>
												</p>
											</li>
											<li>
												<label>Portal Code</label>
												<p>
													<input type="text" name="lastname" value="" placeholder="Postal Code">
													<i class="fa fa-map-marker"></i>
												</p>
											</li>
										</ul>
									</form>
									<div class="payment-footer">
										<a class="waves-effect waves-light btn pay-btn billing-btn green darken-1 right">Proceed with payments</a>
									</div>
								</div>
								<div id="tab2" class="col s12">
									<h6>Make a deposit into your account via PayPal's secure checkout.</h6>
									<div class="accepts">
										<p>PayPal accepts</p>
										<img src="/uploads/payments/visa.png" alt="" class="responsive-img">
										<img src="/uploads/payments/master.png" alt="" class="responsive-img">
										<img src="/uploads/payments/american.png" alt="" class="responsive-img">
										<img src="/uploads/payments/discover.png" alt="" class="responsive-img">
									</div>
									<div class="payment-footer">
										<a class="waves-effect waves-light btn pay-btn billing-btn right green darken-1 mrgt-15" onclick="document.forms.packageForm.submit();">Proceed with Paypal</a>
									</div>
								</div>

								<div id="tab3" class="col s12">
									<h6>An invoice will be issued to {company name} and email to {email}.</h6>
									<p>Please follow the payment instructions in the email. Terms and conditions apply.</p>
									<div class="payment-footer">
										<a class="waves-effect waves-light btn pay-btn billing-btn right green darken-1 mrgt-15">Proceed with Email</a>
									</div>
								</div>
							</div>

						<!-- </div> -->
					</div>
				</div>
			</div>
		</div>
	</div>


@endsection
@section('scripts')

<script type="text/javascript">
	$(document).ready(function(){
		$('ul.tabs').tabs('select_tab', 'test1');
	});
</script>
@endsection
