<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" id="paypal" style="display: none;">

	<!-- Identify your business so that you can collect the payments. -->
	<input type="hidden" name="business" value="{{env('PAYPAL_BUSINESS', 'business@arya.com')}}">

	<!-- Specify a Buy Now button. -->
	<input type="hidden" name="cmd" value="_xclick">
	<input type="hidden" name="tx" value="TransactionId">
	<input type="hidden" name="at" value="_notify-synch">

	<!-- Specify details about the item that buyers will purchase. -->
	<input type="hidden" name="item_name" value="credits">
	<input type="hidden" name="item_number" value="{{$amount}}">
	<input type="hidden" name="custom" value="{{Auth::user()->id}}">
	<input type="hidden" name="currency_code" value="USD">
	<input type="hidden" name="amount" value="{{$credits}}">
	<input type="hidden" name="return" value="{{url('/')}}/payment/success">
	<input type="hidden" name="cancel_return" value="{{url('/')}}/payment/failed">

	<!-- Display the payment button. -->
	<input type="image" name="submit" border="0" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online" value="PDT">
	<img alt="" border="0" width="1" height="1"
	src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">
</form>

<script>
	window.onload = function(e) {
		document.forms[0].submit();
	}
</script>
