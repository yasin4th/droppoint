@extends('layouts.material')

@section('content')

<div class="row">
	{{-- <div class="panel-heading">Register</div> --}}
	{{-- {!! Breadcrumbs::render('profile') !!} --}}
	<div class="col s12">
		<div class="row">
			<div class="col s12 m3">
				<h5>My Profile</h5>
				@include('includes.sidemenu')
			</div>
			<div class="col s12 m9">
				@include('includes.alert')

				<h5>Profile</h5>
				<img src="{{ Auth::user()->company->image }}" alt="" class="responsive-img valign profile-image">
				<br>
				Company Name  : {{ Auth::user()->name }}
				<br>
				Email : {{ Auth::user()->email }}
				<br>
				@if(Auth::user()->company->about)
				<div class="card light-blue">
				  <div class="card-content white-text">
					<span class="card-title">About Me!</span>
					<p>{{ Auth::user()->company->about }}</p>
				  </div>
				</div>
				@endif

			</div>
		</div>
	</div>
</div>
@endsection
