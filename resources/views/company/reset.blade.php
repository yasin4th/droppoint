@extends('layouts.material')

@section('content')

<div class="row">
	<div class="col s12">
		<div class="row">
			<div class="col s12 m3">
				@include('includes.sidemenu')
			</div>
			<div class="col s12 m9">
				<h5>Reset Password</h5>
				<form class="" role="form" method="POST" action="{{ route('cmp.profile.reset') }}">
					{{ csrf_field() }}

					<div class="row">
						<div class="input-field col s12{{ $errors->has('password') ? ' has-error' : '' }}">
							<input id="password" name="password" type="password" class="validate" required>
							<label for="password">Password</label>
							@if ($errors->has('password'))
								<span class="help-block">
								<strong>{{ $errors->first('password') }}</strong>
								</span>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12{{ $errors->has('new_password') ? ' has-error' : '' }}">
							<input id="new_password" name="new_password" type="password" class="validate" required>
							<label for="new_password">New Password</label>
							@if ($errors->has('new_password'))
								<span class="help-block">
								<strong>{{ $errors->first('new_password') }}</strong>
								</span>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12{{ $errors->has('confirm_password') ? ' has-error' : '' }}">
							<input id="confirm_password" name="confirm_password" type="password" class="validate" required>
							<label for="confirm_password">Confirm New Password</label>
							@if ($errors->has('confirm_password'))
							<span class="help-block">
							<strong>{{ $errors->first('confirm_password') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<button type="submit" class="waves-effect waves-light btn light-blue darken-2">SAVE</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
