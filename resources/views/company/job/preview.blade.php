@extends('layouts.material')

@section('content')

<div class="row preview-div">
	<div class="col m4 s12">
		<div class="livelihood ">
			<h6>Well done! You have successfully create livelihood opportunity to someone.</h6>
			<p>Here is how your job post will be like LIVE! </p>
		</div>
	</div>
	<div class="col m4 s12">
		<div class="card">
			<div class="card-content">
				<div class="row">
					<div class="col s12">
						<img src="/uploads/jobs/{{$job->avatar}}" class="responsive-img">
					</div>
				</div>
				<div class="row  retail-sales">
					<div class="col s12">
						<h5>{{$job->title}}</h5>
						<P class="">Empire Shopping Gallery, 47500 subang jaya</P>
					</div>
				</div>
				<hr>
				<div class="row icon-div">
					<div class="col s3 m3">
					<img src="/images/dollar.png" class="responsive-img">
					<h6>RM{{$job->salary}} {{$job->salary_unit}}</h6>
					</div>
					<div class="col s3 m3">
					<img src="/images/calendar.png" class="responsive-img">
					<h6>Aug 26-27</h6>
					</div>
					<div class="col s3 m3">
					<img src="/images/watch.png" class="responsive-img">
					<h6>10am-6pm</h6>
					</div>
					<div class="col s3 m3">
					<img src="/images/car_1.png" class="responsive-img">
					<h6>0.9km</h6>
					</div>
				</div>
				<hr>
				<div class="row about-job">
					<div class="col s12">
						<h5>About this job</h5>
						<p>{{$job->about}}</p>
					</div>
				</div>
				<hr>
				<div class="row role-responsibility">
					<div class="col s12">
						<h5>Role and Responsibilities</h5>
						<ol>
							@foreach($job->tags as $tag)
							@if($tag->id > 6 && $tag->id < 12)
							<li>{{$tag->name}}</li>
							@endif
							<!-- <li>Excelent communication skill</li> -->
							<!-- <li>Ability to work exceptionaly well with other in a strong  team envirement</li>
							<li>Flexible schedule must be able to work add hours including  nights.Weekends and public holidays.</li>
							<li>Must be able to work outdoors</li>
							<li>Must be able to stand  amd  remain mobile samping missions </li> -->
							@endforeach
						</ol>
					</div>
				</div>
				<hr>
				<div class="row dress-code">
					<div class="col s12">
						<h5>Dress Code</h5>
						<p>Uniform (only S and M avaliable) will be provided</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row mrg-t7">
	<div class="col m2 s6 offset-m8">
		<button id="next" type="buttton" class="waves-effect waves-light btn right review-btn" onclick="location.replace('/cmp/job');">Back</button>
	</div>
	<div class="col s6 m2">
		@if($job->status == 0)
		<button id="publish" type="buttton" class="waves-effect waves-light btn review-btn" onclick="publish(1);">Publish</button>
		@else
		<button id="publish" type="buttton" class="waves-effect waves-light btn review-btn" onclick="publish(0);">Un Publish</button>
		@endif
	</div>
</div>

@endsection

@section('scripts')
	<script>
		publish = function(value) {

			var request = {};
			request._token  = '{{ csrf_token() }}';
			request.value = value;

			$.ajax({
				'type': 'POST',
				'url':  "{{ route('job.show', $job->id) }}/update/status",
				'data': request
			}).done(function(response) {
				if(response.trim() == 'Successfully updated' && value == 1) {
					Materialize.toast('Your job has been published.', 1000);
					$('#publish').attr('onclick', 'publish(0);');
					$('#publish').html('Un Publish');
				}
				if(response.trim() == 'Successfully updated' && value == 0) {
					Materialize.toast('Your job has been unpublished.', 1000);
					$('#publish').attr('onclick', 'publish(1);');
					$('#publish').html('Publish');
				}

			});
		}
	</script>
@endsection