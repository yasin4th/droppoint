@extends('layouts.material')

@section('content')

<div class="row">

    <div class="col m3">
        @include('includes.sidemenu')
    </div>
    <div class="col m9">
        <div class="card">
            <br>
            <div class="col s12">
                <div class="row">
                    <div class="col s12">
                      <ul class="tabs">
                        <li class="tab col s3 active"><a class="active" href="#applicants">Applican (<span id="applicants-count"></span>)</a></li>
                        <li class="tab col s3"><a href="#hired">Hired (<span id="hired-count"></span>)</a></li>
                      </ul>
                    </div>
                    <div class="clearfix"></div>
                    <div class="card-content">
                        {{-- Applicants --}}
                        <div id="applicants" class="col s12">
                            <div class="row">
                                <div class="col s6">
                                    <label class="left label-left">Sort:</label>
                                    <select class="left col s10" id="sort">
                                        <option value="all" selected >Recommended for this job</option>
                                        <option value="newest">Newest Applicants</option>
                                        <option value="oldest">Oldest applicants</option>
                                        <option value="hours">Most hours worked</option>
                                        <option value="rejected">Rejected</option>
                                    </select>
                                </div>
                                <div class="col s6">
                                    <p class="line-height50">
                                        <input type="checkbox" id="favourites">
                                        <label for="favourites">Only show shortlisted applicants</label>
                                    </p>
                                </div>
                            </div>
                            <ul class="collection">

                            </ul>
                            <div class="row">
                                <div class="col s12 m6 mrg-tb15">
                                    <p>Showing <span id="from">0</span> to <span id="to">0</span> of <span id="total">0</span> entries</p>
                                </div>
                                <div class="col s12 center loadmorediv">
                                    <a class="btn" onclick="load_applicants();">View more</a>
                                </div>
                            </div>
                        </div>
                        {{-- Hired --}}
                        <div id="hired" class="col s12">
                            <div class="row">
                                <div class="col s8">
                                    <p class="hired-applicants-counts"><i class="mdi-hardware-keyboard-arrow-up"></i> <strong><span id="hire-count">0</span> Hired Applicants</strong> out of <strong><span id="vacancy-count">{{ DB::table('cmp_jobs')->find($job_id)->vacancy }}</span> Vacancies</strong></p>
                                </div>
                                <div class="col s4">
                                    <a href="{{ route('applicants.exportHired', $job_id) }}" class="btn-flat right teal-text btn-get-csv"><img src="/images/csv-icon.png" class="responsive-img"> Get CSV</a>
                                </div>
                            </div>
                            <ul class="collection">
                                <li class="collection-item avatar">
                                    <img src="/images/avatar.jpg" alt="" class="circle">
                                    <span class="title">Title</span>
                                    <p>First Line
                                        <a href="#" class="waves-effect waves-light btn deep-purple darken-2 right btn-small">REMOVE</a>
                                    </p>
                                </li>
                                <li class="collection-item avatar">
                                    <i class="mdi-file-folder circle"></i>
                                    <span class="title">Title</span>
                                    <p>First Line
                                        <a href="#" class="waves-effect waves-light btn deep-purple darken-2 right btn-small">REMOVE</a>
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('scripts')
<script>
    var applicant_page = 1;

    $(document).ready (function(){
        load_applicants();
        load_hired();
        setEvent();
    });

    setEvent = function()
    {
        $('#sort').change(function(e) {
            applicant_page = 1;
            $('#applicants ul.collection').empty();
            load_applicants();
        });

        $('#favourites').click(function(e) {
            applicant_page = 1;
            $('#applicants ul.collection').empty();
            load_applicants();
        });
    }

    setEvents = function()
    {
        $('.hire-applicant').off('click');
        $('.hire-applicant').click(function(e){
            e.preventDefault();
            id = $(this).attr('data-id');
            $.ajax({
                type : 'get',
                url  : '{{ route('applicants.index') }}/' + id + '/status/2',
            }).done(function(res) {
                Materialize.toast(res.message, 1000);
                if(res.status == 1){
                    $('#sort').change();
                    load_hired();
                }
            });
        });

        $('.star-applicant').off('click');
        $('.star-applicant').click(function(e){
            e.preventDefault();
            ob = this;
            id = $(this).attr('data-id');
            star = ($(ob).attr('data-starred') == 1) ? 0 : 1;
            $.ajax({
                type : 'get',
                url  : '{{ route('applicants.index') }}/' + id + '/starred/'+star,
            }).done(function(res) {
                Materialize.toast(res.message, 1000);
                if(res.status == 1){
                    $(ob).attr('data-starred', star);
                    $(ob).children().first().toggleClass('grey-text').toggleClass('pink-text');
                }
            });
        });

        $('.delete-applicant').off('click');
        $('.delete-applicant').click(function(e){
            e.preventDefault();
            ob = this;
            id = $(this).attr('data-id');

            $.ajax({
                type : 'GET',
                // url  : '{{ route('applicants.index') }}/' + id +'/destroy',
                url  : '{{ route('applicants.index') }}/' + id +'/status/4',
            }).done(function(res) {
                Materialize.toast(res.message, 1000);
                $('#sort').change();
                load_hired();
            });
        });

        $('.remove-applicant').off('click');
        $('.remove-applicant').click(function(e){
            e.preventDefault();
            id = $(this).attr('data-id');
            $.ajax({
                type : 'get',
                url  : '{{ route('applicants.index') }}/' + id + '/status/1',
            }).done(function(res) {
                Materialize.toast(res.message, 1000);
                $('#sort').change();
                load_hired();
            });
        });
    }

    load_applicants = function()
    {
        if(applicant_page != 0)
        {
            var req = {};
            req.sort = $('#sort').val();
            fav = document.getElementById('favourites');
            req.favourites = (fav.checked) ? 1 : 0;
            req._token  = '{{ csrf_token() }}';

            $.ajax({
                type : 'post',
                url  : '{{ route('job.applicants', $job_id) }}?page='+applicant_page,
                data : req,
            }).done(function(res) {
                fillApplicants(res, req.sort);
            });
        }
    }

    load_hired = function()
    {
        $.ajax({
            type : 'get',
            url  : '{{ route('job.hired', $job_id) }}',
        }).done(function(res) {
            fillhired(res);
        });
    }

    fillApplicants = function(result, sort)
    {
        html = '';

        if(!result.next_page_url){
            $('.loadmorediv').addClass('hide');
        }
        if(result.data.length == 0)
        {
            html += '<li class="collection-item avatar">';
            html += '<h5 class="center"><span>No Applicants found for this job.</span></h5>';
            html += '</li>';
            $('#applicants ul.collection').html(html);
            $('.loadmorediv').addClass('hide');
        }
        else
        {
            result.data.forEach(function(applicant)
            {
                html += '<li class="collection-item avatar">';
                html += '<img src="/images/avatar.jpg" alt="" class="circle">';
                html += '<span class="title"><strong>' + applicant.name + '</strong></span>';
                html += '<p><strong>' + applicant.working_hours + '</strong> hrs';

                if(sort != 'rejected')
                {
                    html += '<a class="btn-floating waves-effect waves-light right btn-small delete-applicant" data-id="'+applicant.id+'"><i class="mdi-content-clear"></i></a>';
                    html += '<a data-id="'+applicant.id+'" class="waves-effect waves-light btn teal darken-1 right btn-small hire-applicant">HIRE</a>';
                    html += '</p>';
                    html += '<a data-id="'+applicant.id+'" data-starred="'+applicant.starred+'" class="secondary-content star-applicant" href="#!"><i class="mdi-action-grade ';
                    html += (!applicant.starred) ? 'grey-text': 'pink-text';
                    html += '"></i></a>';
                }

                html += '</li>';
            });

            $('#applicants ul.collection').append(html);
            setEvents();
        }

        to = (result.to) ? result.to : 0;
        $('#to').html(to);
        $('#from').html((result.total) ? 1 : 0);
        $('#total').html(result.total);
        $('#applicants-count').html(result.total);
        applicant_page = (result.current_page == result.last_page) ? 0 : applicant_page + 1;
    }

    fillhired = function(result)
    {
        html = '';
        if(result.length == 0)
        {
            html += '<li class="collection-item avatar">';
            html += '<h5 class="center"><span>No Hired Applicants found for this job.</span></h5>';
            html += '</li>';
        }
        else
        {
            result.forEach(function(applicant)
            {
                html += '<li class="collection-item avatar">';
                html += '<i class="mdi-file-folder circle"></i>';
                html += '<span class="title">' + applicant.name + '</span>';
                html += '<p><strong>' + applicant.working_hours + '</strong> hrs';
                html += '<a href="#" data-id="'+applicant.id+'" class="waves-effect waves-light btn deep-purple darken-2 right btn-small remove-applicant">REMOVE</a>';
                html += '</p>';
                html += '</li>';
            });
        }

        $('#hired ul').html(html);
        $('#hire-count').html(result.length);
        $('#hired-count').html(result.length);

        setEvents();
    }
</script>
@endsection
