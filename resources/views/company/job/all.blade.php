@extends('layouts.material')

@section('content')

<div class="row">

	<div class="col s12 m3">
		@include('includes.sidemenu')
	</div>
	<div class="col s12 m9">
		<div class="card">
			<div class="card-content">
				<div class="row">
					<div class="col s12 m12">
						<a href="{{ route('job.create') }}" class="waves-effect waves-light btn teal darken-1 right">Post Job</a>
					</div>

					<!-- <div class="input-field col s6">
						<input name="Search" type="text" class="validate" required>
						<label for="Search">Search</label>
					</div> -->
					<div class="col s6">
						<div class="header-search-wrapper">
							<i class="mdi-action-search"></i>
							<form role="form" action="{{ route('job.all') }}" name="search" method="GET">
								<input type="text" name="query" class="header-search-input z-depth-2 table-search" placeholder="Search" value="{{ $query }}">
							</form>
						</div>
					</div>
					<div class="col s6">
						<a href="{{ route('job.export') }}" class="btn-flat right teal-text btn-get-csv"><img src="/images/csv-icon.png" class="responsive-img"> Get CSV</a>
					</div>
					<div class="col s12">
						<h4>
							@if($query)
								Search results '{{$query}}'
							@else
								All Jobs
							@endif
						</h4>
						<div class="table-scroll">
							<table class="bordered all-table" id="jobs">
								<thead>
									<tr>
										<th>Job Title</th>
										<th>Published Date</th>
										<th>Salary (RM)</th>
										<th>Vacancies</th>
										<th>Applicants</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									@foreach($jobs as $job)
									<tr>
										<td>
											<a href="{{ route('job.show', $job->id) }}"><strong>{{ $job->title }}</strong></a>
											<br>
											{{ str_limit($job->about,40) }}
										</td>
										<td>{{ $job->start_date_read }}</td>
										<td>{{ $job->salary }}</td>
										<td><a href="{{ route('job.show', $job->id) }}">0 hired / {{ $job->vacancy }}</a></td>
										<td>{{ count($job->applicants) }}</td>
										<td>Open</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="col s12 m6 mrg-tb15">
						<p>Showing {{ ($jobs->firstItem()) ?:0 }} to {{ $jobs->lastItem()?:0 }} of {{ $jobs->total() }} entries</p>
					</div>
					<div class="col s12 m6 mrg-tb15">
						<div class="right">
							{{ $jobs->links() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection

@section('scripts')

@endsection