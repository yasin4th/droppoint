@extends('layouts.material')

@section('content')

<div class="row">

	<!-- <div class="col s12 m3">
		@if(Auth::user()->isAdmin())
			@include('includes.admin.sidemenu')
		@else
			@include('includes.sidemenu')
		@endif
	</div> -->
	<div class="col s12 m12">
		<div class="card">
			<div class="card-content">
				<div class="row">
					<div class="col s10 m4">
						<div class="header-search-wrapper">
							<i class="mdi-action-search"></i>
							<form role="form" action="" name="search" method="GET">
								<input type="text" name="query" class="header-search-input z-depth-2 table-search" placeholder="Search" value="">
							</form>
						</div>
					</div>
					<div class="col s12 m8">
						<a href="{{ route('job.export') }}" class="btn-flat right teal-text btn-get-csv"><img src="/images/csv-icon.png" class="responsive-img"> Get CSV</a>
						<a href="{{ route('job.create') }}" class="waves-effect waves-light btn teal darken-1 right mrg-tb15">Post Job</a>
						@if(Auth::user()->isAdmin())
							<a href="{{ route('admin.map') }}" class="waves-effect waves-light btn right mrg-tb15 map-btn">Map</a>
						@endif
					</div>

					<!-- <div class="input-field col s6">
						<input name="Search" type="text" class="validate" required>
						<label for="Search">Search</label>
					</div> -->
					<div class="col s6">
					</div>
					<div class="col s12">
						<h4>
							{{-- @if($query)
								Search results '#'
							@else
								All Active Jobs
							@endif --}}
						</h4>
						<div class="table-scroll">
							<table class="bordered all-table" id="jobs">
								<thead>
									<tr>
										<th>Job Title</th>
									@if(Auth::user()->isAdmin())
										<th>User</th>
									@endif
										<th>Salary (RM)</th>
										<th>Vacancies</th>
										<th>Applicants</th>
										<!-- <th>User</th> -->
										<th>Created</th>
										<th>Expiry</th>
										<th>Status </th>
										<!-- <th>Action</th> -->
									</tr>
								</thead>
								<tbody>
								@foreach($jobs as $job)
									<tr>
										<td>
											<a href="{{ route('job.edit', $job->id) }}"><strong>{{ $job->title }}</strong></a>
											<br>
											{{ str_limit($job->about,40) }}
										</td>
									@if(Auth::user()->isAdmin())
										<td><a href="{{ route('job.edit', $job->id) }}">{{ $job->user->name }}</a></td>
									@endif
										<td>{{ $job->salary }} {{ $job->salary_unit }}</td>
										<td><a href="/job/{{$job->id}}/applicants">{{count($job->hired)}} hired / {{ $job->vacancy }}</a></td>
										<td><a href="/job/{{$job->id}}/applicants">{{ count($job->applicants) }}</a></td>
										<!-- <td>{{ $job->action }}</td> -->
										<td>{{ $job->start_date->format('D M j, Y') }}</td>
										<td>{{ $job->start_date->addMonths(12)->format('D M j, Y') }}</td>
										<!-- <td>{{ $job->working_days}}</td> -->
										<td>{{ ($job->status == 0) ? 'Deactivate' : 'Active'}}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="col s12 m6 mrg-tb15">
						<p>Showing {{ ($jobs->firstItem()) ?:0 }} to {{ $jobs->lastItem()?:0 }} of {{ $jobs->total() }} entries</p>
					</div>
					<div class="col s12 m6 mrg-tb15">
						<div class="right">
							{{ $jobs->links() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection

@section('scripts')

@endsection