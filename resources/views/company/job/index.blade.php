@php
	$i = \Carbon\Carbon::now();
	$day = $tmp = $i->dayOfWeek;

	$html = "<div class='date'><b><span class='month-alignment' >{$i->format('M')}</span></b>";
	$htmlMonths = "<div class='months'>";
	$months = [];

	while($tmp != 0) {
		$html .= "<span></span>";
	//	dd($html);
		$tmp--;
	}

	$tmp = 1;
	while($tmp < 93) {

		$holiday = \App\Holidays::where('holidaydate',$i->format('Y-m-d'))->where('holidaytype','PH')->first();
		$class = ( \App\CmpJob::where('user_id', Auth::user()->id)->where('start_date',$i->format('Y-m-d'))->count() > 0 ) ? 'purple' : '';
		if($holiday) {
			$html .= "<span ng-click=\"append('{$i->format('Y-m-d')}');\" class='holiday-default holiday tooltipped {$class}' data-position='right' data-delay='50' data-tooltip='{$holiday->description}' val='{$i->format('Y-m-d')}'>{$i->day}</span>";
		} else {
			$open = ($class != 'purple') ? 'open()' : '';
			$html .= "<span ng-click=\"append('{$i->format('Y-m-d')}'); {$open}\" class='{$class}' val='{$i->format('Y-m-d')}'>{$i->day}</span>";
		}

		if($i->dayOfWeek == 6 && $tmp == 92) {
			$html .= "</div>";
		}
		if($i->dayOfWeek == 6 && $tmp < 92) {
			$html .= "</div><div class='date'>";
			$j = \Carbon\Carbon::createFromFormat('d-m-Y', $i->format('d-m-Y'))->addDay();
			if($i->format('m') == $j->format('m')) {
				$html .= "<b><span class='month-alignment' ></span></b>";
			}
		}
		if($i->dayOfWeek < 6 && $tmp == 92) {
			$html .= "</div>";
		}
		// echo $tmp. " " . $i->dayOfWeek ." \n";
		$i->addDay();
		$tmp++;



		if(!in_array($i->format('M'), $months)) {
			//array_push($months, $i->format('M'));
		}

		if($i->day == 1) {
			$num = $i->dayOfWeek;
			//dd($num);
			if($num != 0) {
				for($z = 1; $z <= 7; $z++) {
					$html .= "<span></span>";
					if(($z+$num) == 7 ) {
						$html .= "</div><div class='date'><b><span class='month-alignment' >{$i->format('M')}</span></b>";
						//dd($html);
					}
				}
			}
			else{
			
				//$html = substr($html, 0, strlen($html) - 57);
				$html .= "<b><span class='month-alignment' >{$i->format('M')}</span></b>";
			}
		}
	}

	foreach ($months as $key => $month) {
		// $htmlMonths .= "<span class='month{$key}'>{$month}</span>";
	}
	$htmlMonths .= "</div>";
@endphp

@extends('layouts.angular-material')

@section('content')
<style>
	.date span {
		cursor: pointer;
	}
	.date span.holiday {
		background-color: orange;
	}
	.date span.purple {
		background-color: #7030a0;
		color: #fff;
	}
</style>

<div class="row" ng-controller="tableCtrl" ng-cloak>
	<div class="col m12">
		<div class="card">
			<div class="profile-cover-img"></div>
			<div class="row">
				<div class="col s12 m2">
					<a href="#modal1" class="collection-item{{ setActive('cmp.profile') }}" id="profile-pic">
						<div class="profile-pic">
							<div>
								<a href="/cmp/profile"><img src="{{ Auth::user()->company->logo }}" class="responsive-img" alt="Profile Image"></a>
							</div>
						</div>
					</a>
				</div>
				<div class="col s12 m10">
					<div class="row center profile-details">
						<div class="col s12 m4">
							<a href="">
								<h5><a href="/cmp/profile">{{ title_case(Auth::user()->name) }}</a></h5>
								<p>{{preg_replace('/(?<!\ )[A-Z]/', ' $0', Auth::user()->company->type)}}</p>
							</a>
						</div>
						<div class="col s12 m4">
							<a href="#">
								<h5><a href="/cmp/profile">{{title_case(Auth::user()->company->state) }}</a></h5>
								<p>State</p>
							</a>
						</div>
						<div class="col s12 m4">
							<h5 class="truncate" title="{{ Auth::user()->company->website }}"><a href="{{ http_url(Auth::user()->company->website) }}">{{ Auth::user()->company->website }}</a></h5>
							<p>Website</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col m12 jobs-status-div">
		<div class="row">
			<div class="col s12 m10">
				<div class="row">
					<!-- <div class="col s12 m3 s-mrg-bot5">
						<a href="#">
							<div class="light-blue darken-3 jobs-status-box">
								<p><i class="mdi-content-add-circle"></i> Pending Jobs</p>
								<p><strong>{{ count($jobs) }}</strong></p>
							</div>
						</a>
					</div> -->
					<div class="col s12 m4 s-mrg-bot5">
						<a href="/job/active">
							<div class="blue-bg-box jobs-status-box">
								<p><i class="mdi-notification-event-available"></i> Active Jobs</p>
								<p><strong>{{ Auth::user()->company->recent->count() }}</strong></p>
							</div>
						</a>
					</div>
					<div class="col s12 m4 s-mrg-bot5">
						<a href="{{ route('job.index') }}">
							<div class="blue-bg-box jobs-status-box">
								<p><i class="mdi-notification-event-busy"></i> Expired Jobs</p>
								<p><strong>{{ $previous }}</strong></p>
							</div>
						</a>
					</div>
					<div class="col s12 m4 s-mrg-bot5">
						<a href="{{ route('cmp.credits.summary') }}">
							<div class="blue-bg-box jobs-status-box">
								<p><i class="mdi-action-stars"></i> Credit Balance</p>
								<p><strong>{{ $credits }}</strong></p>
								@if($credits <= 2)
								@endif
							</div>

						</a>
					</div>
						<div class="col s12 m3">
					</div>
				</div>
			</div>
			@if($credits <= 0)
				<div class="col s12 m2">
					<a href="#credit-post-job-modal" class="waves-effect waves-light btn-large purple darken-1 right credit-post-job-btn">Post Job</a>
				</div>
			@else
				<div class="col s12 m2">
					<a href="{{ route('job.create') }}" class="waves-effect waves-light btn-large purple darken-1 right">Post Job</a>
				</div>
			@endif
		</div>
	</div>

	<!--Calendar-->
	<div class="col s12 mrg-bot30">
		<div class="card">
			<div class="row card-content">
				<div class="col s12 m12">
					<p>Recently Posted Jobs</p>
					<div class="custom-calender">
						{!!$htmlMonths!!}
					<div class="days">
							<span></span>
							<span>Sun</span>
							<span>Mon</span>
							<span>Tue</span>
							<span>Wed</span>
							<span>Thu</span>
							<span>Fri</span>
							<span>Sat</span>
						</div>
						{!!$html!!}
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Calendar -->

	<div class="col s12 m12">

		@include('includes.alert')
			<div class="card">
				<div class="card-content supervisor-div" id="content1" ng-show="total > 0" ng-cloak>
				<div class="row">
					<div class="col s12 m4">
						<ul class="tabs" style="margin-top: 0px;">
							<li class="tab"><a href="#active-jobs">Active Jobs</a></li>
							<li class="tab"><a href="#jobs-in-progress">Jobs-In-Progress</a></li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div id="active-jobs" class="col s12">
						<div class="row">
							<form ng-submit="submit()">
								<div class="input-field col s3 offset-s9">
									<!-- <i class="material-icons prefix right">search</i>!-->
									<input id="icon_telephone" type="text" class="validate" ng-model="query">
									<label for="icon_telephone">Search</label>
								</div>
							</form>
						</div>


						<table class="bordered" id="jobs">
							<thead>
								<tr>
									<th ng-click="sortBy('title')" style="width:20%;">
										Job Title
								<span class="sortorder" ng-show="propertyName === 'title'" ng-class="{reverse: reverse}"></span>
									</th>
									<th ng-click="sortBy('salary')" style="text-align: center;">
										Salary (RM)
										<span class="sortorder" ng-show="propertyName === 'salary'" ng-class="{reverse: reverse}"></span>
									</th>
									<th ng-click="sortBy('vacancy')" style="text-align: center;">
										Hires
										<span class="sortorder" ng-show="propertyName === 'vacancy'" ng-class="{reverse: reverse}"></span>
									</th>
									<th style="text-align: center;">Start Date</th>
									<th style="text-align: right;">
										Working Days
									</th>
									<th ng-click="sortBy('status')" style="text-align: right;">Status
										<span class="sortorder" ng-show="propertyName === 'status'" ng-class="{reverse: reverse}"></span>
									</th>
									<th style="text-align: right;"></th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="job in jobs | orderBy: propertyName:reverse" class="animate">
									<td><a href="/job/@{{job.id}}/edit" class="text-purple" style="font-weight:600">@{{job.title}}</a><p>@{{job.location}}</p></td>
									<td style="text-align: center;">@{{job.salary}} @{{job.salary_unit}}</td>
									<td style="text-align: center;"><a href="/job/@{{job.id}}/applicants" class="text-purple">@{{job.hired_count}} hired / @{{job.vacancy}}</a></td>
									<td style="text-align: right;">@{{job.start_date | time}}</td>
									<td style="text-align: right;">@{{job.working_days_count}}</td>
									<td style="text-align: center;">
										<span ng-if="job.status == 1">Active</span>
										<span ng-if="job.status == 2">Pending</span>
									</td>
									<td style="text-align: right;">
										<ul id="dropdown@{{job.id}}" class="dropdown-content">
											<li><a class="text-small" href="/job/@{{job.id}}/applicants">View applicants</a></li>
											<li><a class="preview text-small" href="#preview@{{job.id}}" ng-model="job">Preview</a></li>
											<li><a class="text-small" href="/cmp/job/@{{job.id}}/copy">Repost</a></li>
											<li><a class="text-small" onclick="return confirm('are you sure you want to delete the job?');"  href="/cmp/job/@{{job.id}}/delete" >Close Job</a></li>
										</ul>
										<a href="javascript:void(0);" class="btn-flat dropdown-button text-purple text-case-remove" data-beloworigin="true" data-activates="dropdown@{{job.id}}" data-hover="true">More<i class="mdi-navigation-arrow-drop-down right"></i></a>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="mrg-t30">@{{to}} of @{{total}} entries</div>
						<br>
						<br>
						<div class="col s12" id="content2" ng-show="total > 0">
							<div class="row center loadmorediv" ng-show="to !== total">
								<button class="btn-flat waves-effect waves-light text-purple text-case-remove" ng-click="load()">View more</button>
							</div>
						</div>
						<br>
						<br>
						<br>
					</div>
					<div id="jobs-in-progress" class="col s12">
						<div class="row">
							<form ng-submit="submit()">
								<div class="input-field col s3 offset-s9">
									<!-- <i class="material-icons prefix right">search</i>!-->
									<input id="icon_telephone" type="text" class="validate" ng-model="query">
									<label for="icon_telephone">Search</label>
								</div>
							</form>
						</div>

						<table class="bordered  jobs-in-progress-table more_padding">
							<tbody>
								<tr ng-repeat="app in jobsProgress | orderBy: propertyName:reverse" class="animate">
									<td>
										<div class="img-box">
											<img src="/uploads/logo/avatar.jpg" alt="" class="circle responsive-img">
										</div>
									</td>
									<td style="font-weight:600;">
										<h6><a href="/job/@{{app.job_id}}/edit">@{{app.job.title}}</a></h6>
										<h6><a href="#applicant-modal" ng-click="applicant(app);">@{{app.name}}</a></h6>
									</td>
									<td>
										<p style="text-align:right;">Completed @{{app.created_at | time1}}</p>
										<a href="#applicant-feedback-modal" ng-click="feedback(app);" data-id="1" data-name="@{{app.name}}" class="feedback waves-effect waves-light btn right btn-small modal-trigger hired-trigger teal lighten-2">GIVE FEEDBACK</a>
									</td>
								</tr>
							</tbody>
						</table>
						<br>
						<br>
						<div class="col s12" id="content2" ng-show="total > 0">
							<div class="row center loadmorediv" ng-show="to !== total">
								<button class="btn-flat waves-effect waves-light text-purple text-case-remove" ng-click="load()">View more</button>
							</div>
						</div>
						<br>
						<br>
						<br>
					</div>
				</div>
			</div>
		</div>
		<div class="col s12" id="content2" ng-show="total > 0">
			<div class="row center loadmorediv" ng-show="to !== total">
				<!-- <button class="btn cyan waves-effect waves-light" ng-click="load()">View more</button> -->
			</div>
		</div>

		<div class="card job-count0" ng-show="total === 0">
			<div class="card-content">
				<h4 class="center"><a href="{{ route('job.create') }}">Post a job to get started.</a></h4>
				<p class="center"><br> It’s an easy application that can be done in 5 minutes! </p>
				<br>
				<div class="col s12 center">
					<a href="{{ route('job.create') }}" class="waves-effect waves-light btn teal darken-1">Post Job</a>
					<br>
					<br>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal Structure -->
	<div id="preview" class="modal modal-fixed-footer preview-modal">
		<div class="modal-content">
			<div class="row preview-div">

				<div class="col m12 s12">
					<div class="card-content">
						<div class="row">
							<div class="col s12">
								<a href="#!" class="modal-action modal-close close-preview"><i class="mdi-navigation-cancel"></i></a>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<img src="#" class="responsive-img" id="previewAvatar">
							</div>
						</div>
						<div class="row  retail-sales">
							<div class="col s12">
								<h5 id="previewTitle"></h5>
							</div>
						</div>
						<hr>
						<div class="row icon-div">
							<div class="col s2 m2">
							<img src="/images/dollar.png" class="responsive-img">
							</div>
							<div class="col s5 m5">
							<h6 >Salary</h6>
							</div>
							<div class="col s5 m5">
							<h6 id="previewSalary"></h6>
							</div>
						</div>
						<div class="row icon-div">
							<div class="col s2 m2">
							<img src="/images/calendar.png" class="responsive-img">
							</div>
							<div class="col s5 m5">
							<h6> Working Date</h6>
							</div>
							<div class="col s5 m5">
							<h6 id="previewStartDate"> </h6>
							</div>
						</div>
						<div class="row icon-div">
							<div class="col s2 m2">
							<img src="/images/watch.png" class="responsive-img">
							</div>
							<div class="col s5 m5">
							<h6>Working Hours</h6>
							</div>
							<div class="col s5 m5">
							<h6 id="previewTime"></h6>
							</div>
						</div>
						<div class="row icon-div">
							<div class="col s2 m2">
							<img src="/images/job_icon.png" class="responsive-img">
							</div>
							<div class="col s5 m5">
							<h6>Type of job</h6>
							</div>
							<div class="col s5 m5">
							<h6 id="previewType"></h6>
							</div>
						</div>
						<hr>
						<div class="row role-responsibility">
							<div class="col s12">
								<h5>Role and Responsibilities</h5>
								<!-- <section id="roles"></section> -->
								<textarea class="materialize-textarea border_hide" rows="6" cols="8" id="roles"></textarea>
							</div>
						</div>
						<hr>
						<div class="row dress-code">
							<div class="col s12">
								<h5>Dress Code</h5>
								<p id="previewUniform"></p>
							</div>
						</div>
						<hr>
						<div class="row dress-code">
							<div class="col s12">
								<h5>Location</h5>
								<p id="previewLocation"></p>
							</div>
						</div>
						<hr>
						<div class="contact-instructions">
							<div class="row">
								<div class="col s12">
									<h5>Contact Instructions</h5>
								</div>
							</div>
							<div class="row">
								<div class="col s5 m5">
								<h6 >Contact Email</h6>
								</div>
								<div class="col s7 m7">
								<p id="previewEmail"></p>
								</div>
							</div>
							<div class="row">
								<div class="col s5 m5">
								<h6 >Contact Phone</h6>
								</div>
								<div class="col s7 m7">
								<p id="previewPhone"></p>
								</div>
							</div>
							<div class="row">
								<div class="col s5 m5">
								<h6 >Contact Link</h6>
								</div>
								<div class="col s7 m7">
								<p id="previewLink"></p>
								</div>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col s12 tags">
								<h5>Tags</h5>
								<ul class="ul-bullets" id="abc">
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Start Feedback modal -->
<div id="applicant-feedback-modal" class="modal">
	<div class="modal-content">
		<h5 class="center">FEEDBACK</h5>
		<div class="display-flex">
			<div class="applicant-pic">
				<img src="/images/avatar.jpg" class="responsive-img" alt="Profile Image">
			</div>
			<div class="applicant-feedback-content">
				<h6><strong class="feedbackName" id="feedbackName"></strong></h6>
				<label for="experience">Share your experience of James Nelson to the community:</label>
				<div class="input-field col s12">
					<textarea name="experience" class="materialize-textarea validate" rows="10"></textarea>

				</div>
				<label for="user">Quick suggested feedbacks:</label>
				<div class="input-field col s12">
					<div class="select-wrapper initialized"><span class="caret">▼</span><input type="text" class="select-dropdown" readonly="true" data-activates="select-options-dc7e663a-5b17-4d73-6754-d63610943062" value=" Please Select --"><ul id="select-options-dc7e663a-5b17-4d73-6754-d63610943062" class="dropdown-content select-dropdown "><li class=""><span> Please Select --</span></li><li class=""><span> Great Work</span></li><li class=""><span> It was a real pleasure working with him.</span></li><li class=""><span> I'm looking forward to working with him again. Great worker.</span></li></ul><select name="feedback" id="user" class="initialized">
						<option value="0"> Please Select --</option>
						<option value="1"> Great Work</option>
						<option value="2"> It was a real pleasure working with him.</option>
						<option value="3"> I'm looking forward to working with him again. Great worker.</option>
					</select></div>

				</div>
				<p>Would you recommend this worker to a friend or a colleague?</p>

				<div class="star-rating-div">
					<i class="mdi-action-star-rate active" data-id="1"></i>
					<i class="mdi-action-star-rate" data-id="2"></i>
					<i class="mdi-action-star-rate" data-id="3"></i>
					<i class="mdi-action-star-rate" data-id="4"></i>
					<i class="mdi-action-star-rate" data-id="5"></i>
				</div>
			</div>
		</div>
		<div class="applicant-border center">
			<button type="button" class="waves-effect waves-red btn-flat modal-action modal-close btn-small">CANCEL</button>
			<a href="#applicant-feeback1-modal" class="waves-effect waves-light btn teal darken-1 btn-small" id="feedbackNext">NEXT</a>
		</div>
	</div>
</div>

<div id="applicant-modal" class="modal applicant-modal">
	<div class="modal-content center">
		<div class="applicant-pic">
			<img src="/images/avatar.jpg" class="responsive-img" alt="Profile Image" id="applicantImage">
		</div>
		<h6><strong class="applicantName" id="applicantName"></strong></h6>
		<p><small><i class="mdi-communication-phone"></i><span id="applicantPhone"></span></small><br><small><i class="mdi-communication-email"></i><span id="applicantEmail"></span></small></p>
		<div class="applicant-border">
			<p class="applicant-time">
				<span id="applicantHours">00</span><br>
				<span>HOUR</span>
			</p>
		</div>
		<div class="applicant-border" id="hireDiv">
			<button type="button" class="waves-effect waves-light btn teal darken-1 btn-small hire-applicant modal-action modal-close" id="applicantHired">Hire</button>
		</div>
		<div class="modal-close-div">
			<a href="#!" class="modal-action modal-close"><i class="mdi-navigation-close"></i></a>
		</div>
	</div>
</div>
<!-- End Feedback Modal -->

<!-- Modal Structure -->
<!-- <div id="modal2" class="modal">
	<div class="modal-content">
	  <h6 class="center">Credits Running Low </h6>
		<div class="credit-info">
			<a class="btn-floating waves-effect waves-light blue darken-2 white-text center circle_2">{{ $credits }}</a>
			@if($credits == 0)
				<img src="/images/batery_2.png" class="img-responsive batery">
			@else
				<img src="/images/batery_1.png" class="img-responsive batery">
			@endif
			<img src="/images/batery_1.png" class="img-responsive batery">
			<a class="waves-effect waves-light btn purchase-btn">Purchase more credits</a>
			<a class="waves-effect waves-light btn purchase-btn">Refer & Earn more credits</a>
			<p class="grey-text">Refer to a company or affiliate and earn 5 credits free<br/> when they post 2 jobs </p>
		</div>
	</div>
	<div class="modal-footer">
	  <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
	</div>
</div> -->


<!--start of purchase credits-modal !-->
	<div id="credit-post-job-modal" class="modal preview-modal">
		<div class="modal-content">
			<!-- <h5 class="center">Preview Job</h5> -->
			<div class="row">

				<div class="col m12 s12">
					<div class="card-content">
						<div class="row">
							<div class="col s12">
								<a href="#!" class="modal-action modal-close close-preview"><i class="mdi-navigation-cancel"></i></a>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<h4 class="center">You have insufficient credits </h4>
							</div>
							<div class="col s12">
								<h6 class="center">Do you want to purchase credits ?</h6>
							</div>
							<div class="col s6 left-align">
								<button class="btn waves-effect modal-close waves-light btn-sm grey darken-1">CANCEL</button>
							</div>
							<div class="col s6 right-align">
								<a href="/cmp/credits/summary" class="btn waves-effect waves-light btn-sm purple darken-1">PURCHASE</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!--end of purchase credits-modal !-->

<!--start of job-create modal !-->
	<div id="job-create-modal" class="modal preview-modal">
		<div class="modal-content">
			<div class="row">

				<div class="col m12 s12">
					<div class="card-content">
						<div class="row">
							<div class="col s12">
								<a href="#!" class="modal-action modal-close close-preview"><i class="mdi-navigation-cancel"></i></a>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<h4 class="center">Create a Job?</h4>
							</div>
							<div class="col s6 left-align">
								<button class="btn waves-effect modal-close waves-light btn-sm grey darken-1">CANCEL</button>
							</div>
							<div class="col s6 right-align">
								<a href="/job/create" class="btn waves-effect waves-light btn-sm purple darken-1">POST JOB</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!--end of job-create modal !-->

@endsection

@section('scripts')

<script src="/js/materialize/plugins/fullcalendar/lib/moment.min.js" type="text/javascript" charset="utf-8"></script>

<script>
var app = angular.module('myapp', ['ngAnimate','ngMaterial']);
app.controller('tableCtrl', function($scope, $http, $window, $timeout) {
	$scope.to = 0;
	$scope.total = 0;
	$scope.query = '';
	$scope.page = 1;
	$scope.propertyName = 'start_date';
	$scope.reverse = false;
	$scope.jobs = [];
	$scope.jobsProgress = [];
	$scope.date = '';

	$window.onload = function() {
		$http.post('{{ route('job.recent') }}?page='+$scope.page+'&sort='+$scope.propertyName+'&reverse='+$scope.reverse+'&query='+$scope.query,{date: $scope.date})
			.then(function(response){
				$scope.jobs = response.data.data;
				$scope.preview = ($scope.jobs.length > 0) ? $scope.jobs[0] : null;
				$scope.to = response.data.to;
				$scope.total = response.data.total;
				// console.log($scope.preview);
				// $timeout(function(){ $('.dropdown-button').dropdown(); $(".modal-trigger").leanModal();}, 100);
			});
		$http.get('{{ route('job.progress') }}?page='+$scope.page+'&sort='+$scope.propertyName+'&reverse='+$scope.reverse+'&query='+$scope.query+'&date='+$scope.date)
			.then(function(response){
				$scope.jobsProgress = response.data;
			});
	};

	$scope.load = function() {
		$scope.page++;
		$http.post('{{ route('job.recent') }}?page='+$scope.page+'&sort='+$scope.propertyName+'&reverse='+$scope.reverse+'&query='+$scope.query, {date:$scope.date})
			.then(function(response){
				if (response.data.data.length > 0) {
					$scope.jobs = $scope.jobs.concat(response.data.data);
					$scope.to = response.data.to;
					$scope.total = response.data.total;
					// element.querySelector('.modal-trigger').leanModal();
					// element.querySelector('.dropdown-button').dropdown();
					// $timeout(function(){ $('.dropdown-button').dropdown(); $(".modal-trigger").leanModal();}, 100);
				}
			});
	};

	$scope.sortBy = function(param) {

		$scope.jobs = [];

		$scope.reverse = ($scope.propertyName === param) ? !$scope.reverse : false;
		$scope.page = 1;
		$scope.propertyName = param;

		$http.post('{{ route('job.recent') }}?page='+$scope.page+'&sort='+$scope.propertyName+'&reverse='+$scope.reverse+'&query='+$scope.query,{date:$scope.date})
			.then(function(response){
				if (response.data.data.length > 0) {
					$scope.jobs = response.data.data;
					$scope.to = response.data.to;
					$scope.total = response.data.total;
					// $timeout(function(){ $('.dropdown-button').dropdown(); }, 500);
				}
			});
	}

	$scope.submit = function() {
		$http.post('{{ route('job.recent') }}?page='+$scope.page+'&sort='+$scope.propertyName+'&reverse='+$scope.reverse+'&query='+$scope.query,{date: $scope.date})
			.then(function(response){
				$scope.jobs = response.data.data;
				$scope.to = response.data.to;
				$scope.total = response.data.total;
				// $timeout(function(){ $('.dropdown-button').dropdown(); }, 500);
			});
	};

	$scope.append = function(d) {
		$scope.page = 1;
		el = document.querySelector("[val='"+d+"']");
		angular.element(document.getElementsByClassName('clicked')).removeClass('clicked');
		angular.element(el).toggleClass('clicked');

		if(angular.element(el).hasClass('holiday-default')) {
			// angular.element(el).toggleClass('holiday');
		}
		value = [];
		purple = document.querySelectorAll('span.clicked');

		if (purple.length > 0) {
			purple.forEach(function(p) {
				value.push(angular.element(p).attr('val'));
			})
		}

		$scope.date = value.join(',');
		$scope.submit();
	}

	$scope.applicant =function(app) {
		document.getElementById('applicantName').innerHTML = app.name;
		document.getElementById('applicantEmail').innerHTML = app.email;
		document.getElementById('applicantHours').innerHTML = app.working_hours;
		document.getElementById('applicantPhone').innerHTML = app.phone;
		$('#applicant-modal').openModal();
	}
	$scope.feedback = function(app) {
		document.getElementById('feedbackName').innerHTML = app.name;
		$('#applicant-feedback-modal').openModal();
	}

	$scope.open = function(app) {
		$('#job-create-modal').openModal();
	}

});

app.directive('dropdownButton', function($timeout)
{
	return {
		restrict: "C",
		link: function(scope, element, attrs)
		{
			$timeout(function(){
				$(element).dropdown();
			},200);
		}
	};
});

app.filter('time', function() {
	return function(input) {
		input = input || '';
		return moment(input,'YYYY-MM-DD').format('ddd DD MMM');
	};
});

app.filter('time1', function() {
	return function(input) {
		input = input || '';
		return moment(input,'YYYY-MM-DD HH:ii:ss').format('ddd DD MMM');
	};
});

app.directive('preview', function($timeout, $http)
{
	return {
		restrict: "C",
		require: 'ngModel',
		link: function(scope, element, attrs, ngModel)
		{
			element.on('click', function(event) {
				html1='';
				html2='';
				scope.job.tags.forEach(function(tag) {
					if ((tag.category = 'transport') || (tag.category = 'language') || (tag.category = 'certification') || (tag.category = 'education') || (tag.category = 'tag'))
					{
						html1 +='<li>'+ tag.name + '</li>';
					}
				});
				html2 += scope.job.requirements;

				angular.element(document.querySelector('#roles')).html(html2);
				angular.element(document.getElementById('abc')).html(html1);
				angular.element(document.getElementById('previewTitle')).html(scope.job.title);
				angular.element(document.getElementById('previewAbout')).html(scope.job.about);
				angular.element(document.getElementById('previewSalary')).html(" RM "+scope.job.salary+" "+scope.job.salary_unit);
				angular.element(document.getElementById('previewStartDate')).html(scope.job.start_date.replace(' 00:00:00',''));
				angular.element(document.getElementById('previewUniform')).html(scope.job.uniform);
				angular.element(document.getElementById('previewLocation')).html(scope.job.location+",  "+scope.job.street+",  "+scope.job.state+",  "+scope.job.country+",  "+scope.job.zipcode);
				angular.element(document.getElementById('previewType')).html(scope.job.type);
				angular.element(document.getElementById('previewTime')).html(scope.job.start_time +" "+" to "+" "+scope.job.end_time);
				angular.element(document.getElementById('previewAvatar')).attr('src', scope.job.cover_picture);
				angular.element(document.getElementById('previewEmail')).html(scope.job.contact_email);
				angular.element(document.getElementById('previewPhone')).html(scope.job.contact_phone);
				angular.element(document.getElementById('previewLink')).html(scope.job.contact_link);
				$('#preview').openModal();
				console.log(scope.job.cover_picture);
			});
		}
	};
});

app.filter('dateExpired', function() {
	return function(input) {
		input = input || '';
		// return moment(input, 'YYYY-MM-DD').add(12, 'months').format('DD MMM YYYY');
		return moment(input, 'YYYY-MM-DD').format('DD MMM');
	};
});
// function selectDay(el) {
// 	value = [];
// 	if($(el).hasClass('holiday-default')) {
// 		$(el).toggleClass('holiday');
// 	}
// 	$(el).toggleClass('purple');
// 	$('span.purple').each(function(i, item) {
// 		value.push($(item).attr('val'));
// 	});
// 	// console.log(value.join(','));
// 	// $scope.date = value.join(',');
// 	$('input[name=start_date]').val(value.join(','));
// }

function feedback(){
	$('#applicant-feedback-modal').openModal();
}

function applicant(){
	$('#applicant-modal').openModal();
}
$(document).ready(function(){
	$('.credit-post-job-btn').click(function(){
		$('#credit-post-job-modal').openModal();
	});
});

localStorage.setItem('user', {{Auth::user()->id}})
</script>
<script>
$(document).ready(function(){
	var a = $('.custom-calender span[val*="-01"]').eq(0).position();
	console.log(a);
})
</script>

@endsection
