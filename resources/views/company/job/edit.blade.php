@extends('layouts.material')

@section('content')

<div class="row">

	<div class="col m3">
		<div class="card"></div>
		@include('includes.sidemenu')
	</div>
	<div class="col m9">
		<div id="rootwizard1" class="form-wizard form-wizard-horizontal">
			<form class="form floating-label" method="POST" action="{{ route('job.update', $job->id) }}">
				{{ csrf_field() }}
				{{ method_field('PUT') }}

				<div class="form-wizard-nav">
					<div class="progress" style="width: 66.667%;"><div class="progress-bar progress-bar-primary"></div></div>
					<ul class="nav nav-justified nav-pills tabs">
						<li class="tab active"><a href="#tab1" class="active"><span class="step">1</span> <span class="title">Details</span></a></li>
						<li class="tab"><a href="#tab2"><span class="step">2</span> <span class="title">Location</span></a></li>
						<li class="tab"><a href="#tab3"><span class="step">3</span> <span class="title">Tags</span></a></li>
					</ul>
				</div>
				<div class="card">
					<div class="col s12">
						<h5>Edit job</h5>
					</div>
					<div id="tab1" class="tabContent">
						<br><br>
						<div class="col s12">
							<div class="col s6{{ $errors->has('title') ? ' has-error' : '' }}">
								<div class="input-field">
									<input type="text" name="title" class="validate" required value="{{ Request::old('title') ?: $job->title }}">
									<label for="title">Job Title</label>
									@if ($errors->has('title'))
									<span class="help-block">
										<strong>{{ $errors->first('title') }}</strong>
									</span>
									@endif
								</div>
							</div>
						</div>
						<div class="col s12">
							<div class="input-field">
								<textarea name="about" class="materialize-textarea cols="30" rows="10">{{ Request::old('about') ?: $job->about }}</textarea>
								<label for="about">Tell us about your job</label>
								@if ($errors->has('about'))
									<span class="help-block">
										<strong>{{ $errors->first('about') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="col s4">
							<div class="input-field">
								<input name="start_date" type="date" class="datepicker" required data-default="{{ Request::old('start_date') ?: $job->start_date }}" value="{{ Request::old('start_date') ?: $job->start_date }}">
								<label for="start_date">Working Start Date</label>
							</div>
						</div>
						<div class="col s2">
							<div class="input-field">
								<label for="start_time">Start Time</label>
								<input name="start_time" id="start_time" class="timepicker" type="time" data-default="{{Request::old('start_time') ?: $job->start_time}}" value="{{Request::old('start_time') ?: $job->start_time}}">
							</div>
						</div>
						<div class="col s2">
							<div class="input-field">
								<label for="end_time">End Time</label>
								<input name="end_time" id="end_time" class="timepicker" type="time" data-default="{{ Request::old('end_time') ?: $job->end_time }}" value="{{ Request::old('end_time') ?: $job->end_time }}">
							</div>
						</div>
						<div class="col s12{{ $errors->has('working_days') ? ' has-error' : '' }}">
							<div class="input-field">
								<input type="number" name="working_days" class="validate" min="0" required value="{{ Request::old('working_days') ?: $job->working_days }}">
								<label for="working_days">Total Working Days</label>
								@if ($errors->has('working_days'))
									<span class="help-block">
										<strong>{{ $errors->first('working_days') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="col s12{{ $errors->has('salary') ? ' has-error' : '' }}">
							<p>Salary</p>
							<p class="range-field">
								<input name="salary" type="range" id="test5" min="0" max="100000" value="0" class="active" required value="{{ Request::old('salary') ?: $job->salary }}"><span class="thumb" style="height: 0px; width: 0px; top: 10px; margin-left: -6px;"><span class="value">0</span></span>
							</p>
						</div>
						<div class="col s12">
							<div class="col s4{{ $errors->has('vacancy') ? ' has-error' : '' }}">
								<div class="input-field">
									<input type="number" name="vacancy" class="validate" min="0" value="0" required value="{{ Request::old('vacancy') ?: $job->vacancy }}">
									<label for="vacancy">Number of vacancies</label>
									@if ($errors->has('vacancy'))
									<span class="help-block">
										<strong>{{ $errors->first('vacancy') }}</strong>
									</span>
									@endif
								</div>
							</div>
						</div>
						<div class="col s12">
							<br><br>
							<h6>How would you like to connect?</h6>
							<div class="col s4{{ $errors->has('contact_email') ? ' has-error' : '' }}">
								<div class="input-field">
									<input type="text" name="contact_email" class="validate" required value="{{ Request::old('contact_email') ?: $job->contact_email }}">
									<label for="contact_email">Contact Email</label>
									@if ($errors->has('contact_email'))
									<span class="help-block">
										<strong>{{ $errors->first('contact_email') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="col s4{{ $errors->has('contact_phone') ? ' has-error' : '' }}">
								<div class="input-field">
									<input type="text" name="contact_phone" class="validate" required value="{{ Request::old('contact_phone') ?: $job->contact_phone }}">
									<label for="phone">Contact telephone</label>
									@if ($errors->has('contact_phone'))
									<span class="help-block">
										<strong>{{ $errors->first('contact_phone') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="col s4{{ $errors->has('contact_link') ? ' has-error' : '' }}">
								<div class="input-field">
									<input type="text" name="contact_link" class="validate" required value="{{ Request::old('contact_link') ?: $job->contact_link }}">
									<label for="link">Link</label>
									@if ($errors->has('contact_link'))
									<span class="help-block">
										<strong>{{ $errors->first('contact_link') }}</strong>
									</span>
									@endif
								</div>
							</div>
						</div>
					</div><!--end #tab1 -->
					<div id="tab2" class="tabContent">
						<br><br>
						<div class="col s12">
							<div class="row">
								<div class="col s12">
								<ul class="tabs">
									<li class="tab col s3 active"><a class="active" href="#test1">Autosearch</a></li>
									<li class="tab col s3"><a href="#test2">Manual</a></li>
								</ul>
								</div>
								<div id="test1" class="col s12">
									<div class="col s2">
										<p>Location:</p>
									</div>
									<div class="col s6">
										<div class="input-field">
											<input type="text" name="location" id="location" class="validate" placeholder="search">
										</div>
									</div>
								</div>
								<div id="test2" class="col s12">
									<div class="row">
										<div class="col s12">
											<div class="input-field">
												<input type="text" name="loc_address" class="validate">
												<label for="loc_address">Address</label>
											</div>
										</div>
										<div class="col s6">
											<div class="input-field">
												<input type="text" name="loc_country" class="validate">
												<label for="loc_country">Country</label>
											</div>
										</div>
										<div class="col s6">
											<div class="input-field">
												<input type="text" name="loc_city" class="validate">
												<label for="loc_city">City</label>
											</div>
										</div>
										<div class="col s6">
											<div class="input-field">
												<input type="text" name="zipcode" class="validate">
												<label for="zipcode">Postal/Zip code</label>
											</div>
										</div>
										<div class="col s6">
											<div class="input-field">
												<input type="text" name="phone" class="validate">
												<label for="phone">Phone</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div><!--end #tab2 -->
					<div id="tab3" class="tabContent">
						<br><br>
						<div class="">
							<div class="col s12">
								<p>
									<input type="checkbox" id="skill1" name="skill1" {{ ($job->vehicle) ? 'checked' : ''}}
									/>
									<label for="skill1">Access to vehicle</label>
								</p>
								<div class="col s12 {{ !($job->vehicle) ? 'hide' : ''}}">
									<div class="input-field">
										<select name="vehicle" id="Vehicle">
											<option value=""> Please Select --</option>
											<option value="car" {{ str_contains($job->vehicle, 'car') ? 'selected' : '' }}> Car</option>
											<option value="bike" {{ str_contains($job->vehicle, 'bike') ? 'selected' : '' }}> Bike</option>
										</select>
										<label for="Address">Please select one of the following</label>
									</div>
								</div>
								<p>
									<input type="checkbox" id="skill2" name="language" {{ ($job->language) ? 'checked' : ''}}/>
									<label for="skill2">Language Requirement</label>
								</p>
								<div class="col s12 {{ !($job->language) ? 'hide' : ''}}">
									<div class="input-field">
										<select name="language" id="Language">
											<option value=""> Please Select --</option>
											<option value="english" {{ str_contains($job->language, 'english') ? 'selected' : '' }}> English</option>
											<option value="hindi" {{ str_contains($job->language, 'hindi') ? 'selected' : '' }}> Hindi</option>
										</select>
										<label for="Language">Please select one of the following</label>
									</div>
								</div>
								<p>
									<input type="checkbox" id="skill3" name="skill3" {{ ($job->certificates) ? 'checked' : ''}} />
									<label for="skill3">Specialist certificates and Licences</label>
								</p>
								<div class="col s12 {{ !($job->certificates) ? 'hide' : ''}}">
									<p>
										<input type="checkbox" name="cert[]" id="cert1" value="first-aid" {{ str_contains($job->certificates, 'first-aid') ? 'checked' : '' }}/>
										<label for="cert1">First Aid Certificate</label>
									</p>
									<p>
										<input type="checkbox" name="cert[]" id="cert2" value="license-controller" {{ str_contains($job->certificates, 'license-controller') ? 'checked' : '' }}/>
										<label for="cert2">License Controller Qualification</label>
									</p>
									<p>
										<input type="checkbox" name="cert[]" id="cert3" value="general-managers" {{ str_contains($job->certificates, 'general-managers') ? 'checked' : '' }}/>
										<label for="cert3">General Managers Certificate</label>
									</p>
									<p>
										<input type="checkbox" name="cert[]" id="cert4" value="security-guard" {{ str_contains($job->certificates, 'security-guard') ? 'checked' : '' }}/>
										<label for="cert4">Security Guard Certificate of Approval</label>
									</p>
									<p>
										<input type="checkbox" name="cert[]" id="cert5" value="food-hygiene" {{ str_contains($job->certificates, 'food-hygiene') ? 'checked' : '' }}/>
										<label for="cert5">Food Hygiene Certificate</label>
									</p>
								</div>
							</div>
						</div>
					</div><!--end #tab3 -->
					<div class="col s12 mrg-tb40">
						<button id="back" type="buttton" class="waves-effect waves-light btn deep-purple lighten-2">BACK</button>
						<button id="submit" type="submit" class="waves-effect waves-light btn deep-purple lighten-2 right">SUBMIT</button>
						<button id="next" type="buttton" class="waves-effect waves-light btn deep-purple lighten-2 right">NEXT</button>
					</div>
				</div><!--end .card -->
			</form>
		</div><!--end #rootwizard -->
		{{-- <em class="text-caption">Form wizard</em> --}}
	</div>

</div>
@endsection

@section('scripts')
<script>
$(document).ready (function(){
	$('#back').hide();
	$('#submit').hide();

	$('.datepicker').pickadate({
		selectMonths: true, // Creates a dropdown to control month
		selectYears: 15 // Creates a dropdown of 15 years to control year
	});
	$('.timepicker').pickatime({
		formatSubmit: 'HH:i',
		twelvehour: false,
	});

	var navListItems = $('.nav>.tab>a'), content = $('.tabContent');

	navListItems.click(function(e){
		e.preventDefault();
		var $target = $($(this).attr('href')),
			$item = $(this);
		$target.find('input:eq(0)').focus();
		$item.parent().addClass('active');
		index = parseInt($(this).attr('href').substr(-1));
		for (var i = 1; i <= 3; i++) {
			if(i < index)
			{
				$('[href=#tab'+i+']').parent().addClass('active');
			}
			else
			{
				$('[href=#tab'+i+']').parent().removeClass('active');
			}
		}
		if(index == 1){
			$('#back').hide();
			$('#next').show();
			$('#submit').hide();
		}else if(index == 3){
			$('#next').hide();
			$('#back').show();
			$('#submit').show();
		}else{
			$('#back').show();
			$('#next').show();
			$('#submit').hide();
		}
		$('.progress-bar-primary').css('width',((index-1)*50)+'%');
	});

	$('#back').click(function(e){
		e.preventDefault();
		$c = $('.nav>.tab>a.active');
		console.log($c.attr('href'));

	});

	$('#next').click(function(e){
		e.preventDefault();
		$next = parseInt($('.nav>.tab>a.active').attr('href').substr(-1)) + 1;
		$('[href=#tab'+$next+']').click();
	});

	$('#back').click(function(e){
		e.preventDefault();
		$back = parseInt($('.nav>.tab>a.active').attr('href').substr(-1)) - 1;
		$('[href=#tab'+$back+']').click();
	});

	$('[id^=skill]').on('change', function(e) {
		$(this).parent().next().toggleClass('hide');
	});

});

</script>
@endsection