<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Eyewil</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link rel="stylesheet" href="/css/materialize.min.css"/>
    <!-- <link rel="stylesheet" href="/css/ionicons/css/ionicons.min.css"/> -->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"/>
    <style>
        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
            margin-bottom: 175px;
        }

        .title {
            font-size: 84px;
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
        }

        .links > a {
            color: #636b6f;
            padding: 0 50px;
            font-size: 20px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-top:160px;
        }

        .env_logo {
            height: 130px;
            width: 300px;
        }
        @media only screen  and (min-width: 1923px){
            .container {
                width: 100%;
            }
        }
        @media only screen  and (min-width: 993px){
            .container {
                width: 100% !important;
            }
        }
    </style>
    @include('includes.theme')
</head>

<body style="background-size: cover;">
    <nav style="background-color:#1D1C2C;">
        <div class="container">
            <!-- Dropdown Structure -->
            <ul id="dropdown1" class="dropdown-content">
                <a href="{{ url('/logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();" style="color: black;">Logout</a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </ul>
            {{--  <ul class="left">
            <li><h1 class="logo-wrapper"><a href="{{route('dashboard')}}" class="brand-logo darken-1" style="margin-left:70px;"> {{ config('app.name', 'DropPoint') }}</a></li>
        </ul> --}}
        <!-- Right Side Of Navbar -->
        <ul class="right" style="margin-right: 0px; float:right;">
            <!-- Authentication Links -->
            @if (Auth::guest())
            <li><a href="{{ url('/login') }}">Login</a></li>
            <li><a href="{{ url('/register') }}">Register</a></li>
            @else
            <li><a class="dropdown-button" href="#!" data-beloworigin="true" data-activates="dropdown1">{{ Auth::user()->name }}<i class="material-icons right">arrow_drop_down</i></a></li>
            @endif
        </ul>
    </div>
</nav>
<!--        <div class="row"> -->
<!--  <div class="content">
<div class="title m-b-md" style="margin-top:20px">
<img src="images/login-logo.png" alt="" class="circle responsive-img valign profile-image-login" width="150" height="150">
<br> Welcome
<p style="font-size:30px">You will be redirected in <span id="counter">5</span> second(s).</p>
</div>
</div>  -->
<div class="content">
    <div class="title m-b-md">
        <img src="{{ env('APP_LOGO') }}" alt="" class="responsive-img env_logo valign">
    </div>

    <div class="links">
        <a href="{{ url('/login') }}">Login</a>
        <a href="{{ url('/register') }}">Register</a>
    </div>
</div>




<!--   </div> -->
<!-- Scripts-->
<script type="text/javascript">
// function countdown() {
//     var i = document.getElementById('counter');
//     if (parseInt(i.innerHTML)<=0) {
//         location.href = 'login';
//     }
//     i.innerHTML = parseInt(i.innerHTML)-1;
// }
// setInterval(function(){ countdown(); },1000);
</script>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

<script type="text/javascript" src="/js/materialize.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

{{-- <script src="/js/app.js"></script> --}}

</body>
</html>
