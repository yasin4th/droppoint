<div class="card profile-sidebar">
	@if(str_contains(url()->current(), 'profile') || str_contains(url()->current(), 'credits') || str_contains(url()->current(), 'payments'))
		<div class="card-content">
			<div class="profile-userpic"> <img src="{{ Auth::user()->company->logo }}" class="responsive-img" alt=""> </div>
			<div class="profile-user-detials">
				<div class="profile-user-name"> {{ title_case(Auth::user()->name) }} </div>
				<div class="profile-user-designation"> {{ Auth::user()->role_name }} </div>
			</div>
		</div>
		<ul class="collection">
			<a href="{{ route('cmp.profile') }}" class="collection-item{{ setActive('cmp.profile') }}"><i class="mdi-image-timer-auto"></i> About You</a>
			<a href="{{ route('cmp.profile.logo') }}" class="collection-item{{ setActive('cmp.profile.logo') }}"><i class="mdi-image-photo"></i> Upload Logo</a>
			<a href="{{ route('cmp.profile.reset') }}" class="collection-item{{ setActive('cmp.profile.reset') }}"><i class="mdi-communication-vpn-key"></i> Reset Password</a>
			<a href="{{route('cmp.credits.summary')}}" class="collection-item{{ setActive('cmp.credits.summary') }}"><i class="mdi-action-view-list"></i> Credit Summary</a>
			<a href="{{route('cmp.credits')}}" class="collection-item{{ setActive('cmp.credits') }}"><i class="mdi-action-account-balance-wallet"></i> Transaction History</a>
			<a href="{{route('cmp.payments')}}" class="collection-item{{ setActive('cmp.payments') }}"><i class="mdi-action-payment"></i>Payment History</a>
			<a href="{{route('chat')}}" class="collection-item{{ setActive('chat') }}"><i class="mdi-action-payment"></i>Chat</a>
		</ul>
	@else
		<h6><strong>Jobs</strong></h6>
		<ul class="collection">
			<a href="{{ route('job.index') }}" class="collection-item{{ setActive('job.index') }}"><i class="mdi-action-assignment"></i> All Jobs</a>
			<a href="{{ route('job.create') }}" class="collection-item{{ setActive('job.create') }}"><i class="mdi-av-playlist-add"></i> Add Job</a>
			<a href="{{ route('chat') }}" class="collection-item{{ setActive('chat') }}"><i class="mdi-av-playlist-add"></i> Chat</a>
		</ul>
	@endif
</div>