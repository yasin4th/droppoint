<div class="card-content">
    <div class="profile-userpic"> <img src="/uploads/logo/avatar.jpg" class="responsive-img" alt=""> </div>
    <div class="profile-user-detials">
        <div class="profile-user-name"> {{ title_case(Auth::user()->name) }} </div>
        {{-- <div class="profile-user-designation"> {{ Auth::user()->role_name }} </div> --}}
    </div>
</div>
<ul class="collection">
    <a href="#/" class="collection-item"><i class="mdi-action-dashboard"></i> Badges</a>
    <a href="#/levels" class="collection-item"><i class="mdi-action-assignment"></i> Levels</a>
    <a href="#/points" class="collection-item"><i class="mdi-av-playlist-add"></i> Points</a>
    <a href="#/credits" class="collection-item"><i class="mdi-social-group"></i> Credits</a>
    <a href="#/packages" class="collection-item"><i class="mdi-social-person-add"></i> Packages</a>
    {{-- <a href="" class="collection-item"><i class="mdi-action-dashboard"></i> Manage Gamify</a> --}}
</ul>