<div class="card-content">
    <div class="profile-userpic"> <img src="{{ $user->company->logo }}" class="responsive-img" alt=""> </div>
    <div class="profile-user-detials">
        <div class="profile-user-name"> {{ $user->name }} </div>
        <div class="profile-user-designation"> {{ $user->role_name }} </div>
    </div>
</div>
<ul class="collection">
    <a href="{{ route('admin.users.edit', $user->id) }}" class="collection-item{{ setActive('admin.users.edit', $user->id) }}"><i class="mdi-image-timer-auto"></i> About You</a>
    <a href="{{ route('admin.users.logo', $user->id) }}" class="collection-item{{ setActive('admin.users.logo', $user->id) }}"><i class="mdi-image-photo"></i> Upload Logo</a>
    <a href="{{ route('admin.users.reset', $user->id) }}" class="collection-item{{ setActive('admin.users.reset', $user->id) }}"><i class="mdi-communication-vpn-key"></i> Reset Password</a>
    {{-- <a href="{{ route('admin.users.summary', $user->id) }}" class="collection-item{{ setActive('admin.users.summary', $user->id) }}"><i class="mdi-action-view-list"></i> Summary</a> --}}
    <a href="{{ route('admin.users.credits', $user->id) }}" class="collection-item{{ setActive('admin.users.credits', $user->id) }}"><i class="mdi-action-account-balance-wallet"></i> Credits</a>
</ul>