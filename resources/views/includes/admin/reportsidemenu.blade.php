<div class="card profile-sidebar {{strtolower(env('APP_COLOR'))}}">

	<ul class="collection">
		<a href="{{ route('report.jobs') }}" class="collection-item{{ setActive('report.jobs') }}"><i class="mdi-action-dashboard"></i> Jobs</a>
		<a href="{{ route('report.users') }}" class="collection-item{{ setActive('report.users') }}"><i class="mdi-action-dashboard"></i> Users</a>
		<a href="{{ route('report.location') }}" class="collection-item{{ setActive('report.location') }}"><i class="mdi-action-dashboard"></i> Location</a>
		<a href="{{ route('report.tags') }}" class="collection-item{{ setActive('report.tags') }}"><i class="mdi-action-dashboard"></i> Tags</a>
		<a href="{{ route('report.industry') }}" class="collection-item{{ setActive('report.industry') }}"><i class="mdi-action-dashboard"></i> Industry</a>
	</ul>
</div>