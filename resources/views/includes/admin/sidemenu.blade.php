<div class="card profile-sidebar {{strtolower(env('APP_COLOR'))}}">

	<div class="card-content">
		<div class="profile-userpic"> <a href="/admin/users"><img src="/uploads/logo/avatar.jpg" class="responsive-img" alt="" /></a></div>
		<div class="profile-user-detials">
			<div class="profile-user-name"><a href="{{ route('admin.users') }}"> {{ title_case(Auth::user()->name) }}</a> </div>
			{{-- <div class="profile-user-designation"> {{ Auth::user()->role_name }} </div> --}}
		</div>
	</div>
	<ul class="collection">
		<a href="{{ route('dashboard') }}" class="collection-item{{ setActive('dashboard') }}"><i class="mdi-action-dashboard"></i> Dashboard</a>
		<a href="{{ route('job.index') }}" class="collection-item{{ setActive('job.index') }}"><i class="mdi-action-assignment"></i> All Jobs</a>
		<a href="{{ route('job.create') }}" class="collection-item{{ setActive('job.create') }}"><i class="mdi-av-playlist-add"></i> Add Job</a>
		<a href="{{ route('admin.users') }}" class="collection-item{{ setActive('admin.users') }}"><i class="mdi-social-group"></i> All Users</a>
		<a href="{{ route('admin.users.create') }}" class="collection-item{{ setActive('admin.users.create') }}"><i class="mdi-social-person-add"></i> Add User</a>
		<a href="{{ route('admin.template') }}" class="collection-item{{ setActive('admin.template') }}"><i class="mdi-action-dashboard"></i> Manage Template</a>
		<a href="{{ route('admin.template.create') }}" class="collection-item{{ setActive('admin.template.create') }}"><i class="mdi-av-playlist-add"></i> Add Template</a>
		<a href="{{ route('admin.credits') }}" class="collection-item{{ setActive('admin.credits') }}"><i class="mdi-action-dashboard"></i> Manage Gamify</a>
		{{-- <a href="{{ route('admin.credits') }}" class="collection-item{{ setActive('admin.credits') }}"><i class="mdi-action-dashboard"></i> Manage Notification</a> --}}
		<a href="{{ route('notification.index') }}" class="collection-item{{ setActive('notification.index') }}"><i class="mdi-action-announcement"></i> Manage Notification</a>
		<a href="{{ route('notification.create') }}" class="collection-item{{ setActive('notification.create') }}"><i class="mdi-action-alarm-add"></i> Add Notification</a>
		<a href="{{ route('chat') }}" class="collection-item{{ setActive('chat') }}"><i class="mdi-av-playlist-add"></i> Chat</a>
	</ul>
</div>