@if(env('APP_COLOR') == 'RED')
	<link href="{{asset('css/themes/red.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
@elseif(env('APP_COLOR') == 'BLUE')
	<link href="{{asset('css/themes/blue.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
@elseif(env('APP_COLOR') == 'BLACK')
	<link href="{{asset('css/themes/black.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
@elseif(env('APP_COLOR') == 'CORAL')
	<link href="{{asset('css/themes/coral.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
@elseif(env('APP_COLOR') == 'PURPLE')
	<link href="{{asset('css/themes/purple.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
@endif