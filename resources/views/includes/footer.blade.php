<!-- START FOOTER -->
<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
        <span class="grey-text">Copyright © <?php echo date("Y"); ?> <a class="grey-text" href="" target="_blank">DropPoint</a> All rights reserved.</span>
            <span class="right"><a class="grey-text" href="#"></a></span>
        </div>
    </div>
</footer>
<!-- END FOOTER -->