	@php
		$notifications = \App\Notification::where('user_id', Auth::user()->id)->where('is_read', 0)->get();
	@endphp
	<!-- START HEADER -->
	<!-- <header id="header" class="page-topbar"> -->
		<!-- start header nav-->
		<div class="navbar-fixed admin-nav">
			<nav class="navbar-color">
				<div class="nav-wrapper">
				<div class="row">
					<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
					<!-- <div class="col s6 m2">
						<ul class="left graph">
							<li><h1 class="logo-wrapper"><a href="{{ route('dashboard') }}" class="brand-logo darken-1">Home</a> <span class="logo-text">Materialize</span></h1></li>
							@if(Auth::user()->isAdmin())
								<li class="graph-right"><h3 class=""><a href="{{ route('report.jobs') }}" class="">Reports</a></h3></li>
							@endif
						</ul>
					</div> -->
					<div class="col s12 m12">
						<a href="{{ route('dashboard') }}" class="main-logo"><img src="{{ env('APP_ICON') }}" alt="" class="responsive-img" /></a>
				      	<ul id="nav-mobile" class="hide-on-med-and-down left">
							<!-- <li><a href="{{ route('dashboard') }}" class="darken-1"><img src='{{{ asset('images/icon and typeface/256.png') }}}' alt="" class="responsive-img" /></a></li> -->
							@if(Auth::user()->isAdmin())
								<li><a href="{{ route('report.jobs') }}" class="">Reports</a></li>
							@endif

					        <li class="active"><a href="/dashboard">Jobs</a></li>
					        <li><a href="/chat">Support</a></li>
				      	</ul>

				      	<ul class="side-nav" id="mobile-demo">
							<li><a href="{{ route('dashboard') }}" class="darken-1">Home</a></li>
							@if(Auth::user()->isAdmin())
								<li><a href="{{ route('report.jobs') }}" class="">Reports</a></li>
							@endif
					        <li class="active"><a href="/dashboard">Jobs</a></li>
					        <li><a href="/chat">Support</a></li>
				      	</ul>
						<ul class="right">
							<li>
								<a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown" data-hover="false" onclick="markRead();"><i class="mdi-social-notifications" data-hover="false"><small class="notification-badge notification-count">{{ count($notifications) }}</small></i>
								</a>
								<ul id="notifications-dropdown" class="dropdown-content" style="white-space: nowrap; position: absolute; top: 64px; left: 969px; opacity: 1; display: none;">
									<li>
									<h5>NOTIFICATIONS <span class="new badge notification-count">{{ count($notifications) }}</span></h5>
									</li>
									<li class="divider"></li>
									@foreach(\App\Notification::where('user_id', Auth::user()->id)->get() as $notification)
										<li>
											<a href="{{(str_contains($notification->message, ['RoomID'])) ? '/chat': '#'}}"><i class="mdi-action-trending-up"></i> {!!$notification->message!!}</a>
											<time class="media-meta" datetime="{{$notification->ago}}">{{$notification->ago}}
											</time>
										</li>
									@endforeach


									{{-- <li>
									<a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
									<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
									</li>
									<li>
									<a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
									<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
									</li>
									<li>
									<a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
									<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
									</li>
									<li>
									<a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
									<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
									</li>
									<li>
									<a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
									<time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
									</li> --}}
								</ul>
							</li>
							<li>
								<ul id="profile-dropdown" class="dropdown-content">
									@if(Auth::user()->isCompany())
										<li><a href="{{ route('cmp.profile') }}"> <i class="mdi-action-account-circle"></i> Profile</a></li>
									@elseif(Auth::user()->isAdmin())
										{{-- <li><a href="{{ route('cmp.profile') }}"> <i class="mdi-action-account-circle"></i> Profile</a></li> --}}
									@endif
										<li><a href="{{ route('dashboard') }}"> <i class="mdi-action-assignment"></i> Jobs</a></li>
									<li class="divider"></li>
									<li><a href="{{ route('auth.logout') }}"> <i class="mdi-hardware-keyboard-tab"></i> Logout</a></li>
								</ul>
								<a href="#" class="dropdown-button waves-effect waves-light"  data-beloworigin="true" data-activates="profile-dropdown">
									<!-- <span class="header-user-profile-pic-box">
										<span class="header-user-profile-pic">
											<img src="{{ (Auth::user()->isCompany()) ? Auth::user()->company->image : '/uploads/logo/avatar.jpg' }}" alt="" class="circle responsive-img valign profile-image">
										</span>
									</span> -->
									<span class="header-user-name">{{ title_case(Auth::user()->name) }} <i class="mdi-navigation-arrow-drop-down right"></i></span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
	</div>
	<!-- end header nav-->
	<!-- </header> -->
	<!-- END HEADER -->

	<script type="text/javascript">
		markRead = function() {
			console.log('marking read')
			// return;
			$.ajax({
				type : 'get',
				url  : '/admin/notification/read',
			}).done(function(res) {
				setTimeout(function(){
					$('.notification-count').html(0);
				},1000);
			});
		}
	</script>
