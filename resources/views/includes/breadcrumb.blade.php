<div id="breadcrumbs-wrapper">    
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">  
            @if ($breadcrumbs)
                <ol class="breadcrumbs">
                    @foreach ($breadcrumbs as $breadcrumb)
                        @if (!$breadcrumb->last)
                            <li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
                        @else
                            <li class="active">{{ $breadcrumb->title }}</li>
                        @endif
                    @endforeach
                </ol>
            @endif
            </div>
        </div>
    </div>
</div>